/*! jQuery v3.3.1 | (c) JS Foundation and other contributors | jquery.org/license */
!function(e,t){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=e.document?t(e,!0):function(e){if(!e.document)throw new Error("jQuery requires a window with a document");return t(e)}:t(e)}("undefined"!=typeof window?window:this,function(e,t){"use strict";var n=[],r=e.document,i=Object.getPrototypeOf,o=n.slice,a=n.concat,s=n.push,u=n.indexOf,l={},c=l.toString,f=l.hasOwnProperty,p=f.toString,d=p.call(Object),h={},g=function e(t){return"function"==typeof t&&"number"!=typeof t.nodeType},y=function e(t){return null!=t&&t===t.window},v={type:!0,src:!0,noModule:!0};function m(e,t,n){var i,o=(t=t||r).createElement("script");if(o.text=e,n)for(i in v)n[i]&&(o[i]=n[i]);t.head.appendChild(o).parentNode.removeChild(o)}function x(e){return null==e?e+"":"object"==typeof e||"function"==typeof e?l[c.call(e)]||"object":typeof e}var b="3.3.1",w=function(e,t){return new w.fn.init(e,t)},T=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;w.fn=w.prototype={jquery:"3.3.1",constructor:w,length:0,toArray:function(){return o.call(this)},get:function(e){return null==e?o.call(this):e<0?this[e+this.length]:this[e]},pushStack:function(e){var t=w.merge(this.constructor(),e);return t.prevObject=this,t},each:function(e){return w.each(this,e)},map:function(e){return this.pushStack(w.map(this,function(t,n){return e.call(t,n,t)}))},slice:function(){return this.pushStack(o.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(e){var t=this.length,n=+e+(e<0?t:0);return this.pushStack(n>=0&&n<t?[this[n]]:[])},end:function(){return this.prevObject||this.constructor()},push:s,sort:n.sort,splice:n.splice},w.extend=w.fn.extend=function(){var e,t,n,r,i,o,a=arguments[0]||{},s=1,u=arguments.length,l=!1;for("boolean"==typeof a&&(l=a,a=arguments[s]||{},s++),"object"==typeof a||g(a)||(a={}),s===u&&(a=this,s--);s<u;s++)if(null!=(e=arguments[s]))for(t in e)n=a[t],a!==(r=e[t])&&(l&&r&&(w.isPlainObject(r)||(i=Array.isArray(r)))?(i?(i=!1,o=n&&Array.isArray(n)?n:[]):o=n&&w.isPlainObject(n)?n:{},a[t]=w.extend(l,o,r)):void 0!==r&&(a[t]=r));return a},w.extend({expando:"jQuery"+("3.3.1"+Math.random()).replace(/\D/g,""),isReady:!0,error:function(e){throw new Error(e)},noop:function(){},isPlainObject:function(e){var t,n;return!(!e||"[object Object]"!==c.call(e))&&(!(t=i(e))||"function"==typeof(n=f.call(t,"constructor")&&t.constructor)&&p.call(n)===d)},isEmptyObject:function(e){var t;for(t in e)return!1;return!0},globalEval:function(e){m(e)},each:function(e,t){var n,r=0;if(C(e)){for(n=e.length;r<n;r++)if(!1===t.call(e[r],r,e[r]))break}else for(r in e)if(!1===t.call(e[r],r,e[r]))break;return e},trim:function(e){return null==e?"":(e+"").replace(T,"")},makeArray:function(e,t){var n=t||[];return null!=e&&(C(Object(e))?w.merge(n,"string"==typeof e?[e]:e):s.call(n,e)),n},inArray:function(e,t,n){return null==t?-1:u.call(t,e,n)},merge:function(e,t){for(var n=+t.length,r=0,i=e.length;r<n;r++)e[i++]=t[r];return e.length=i,e},grep:function(e,t,n){for(var r,i=[],o=0,a=e.length,s=!n;o<a;o++)(r=!t(e[o],o))!==s&&i.push(e[o]);return i},map:function(e,t,n){var r,i,o=0,s=[];if(C(e))for(r=e.length;o<r;o++)null!=(i=t(e[o],o,n))&&s.push(i);else for(o in e)null!=(i=t(e[o],o,n))&&s.push(i);return a.apply([],s)},guid:1,support:h}),"function"==typeof Symbol&&(w.fn[Symbol.iterator]=n[Symbol.iterator]),w.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(e,t){l["[object "+t+"]"]=t.toLowerCase()});function C(e){var t=!!e&&"length"in e&&e.length,n=x(e);return!g(e)&&!y(e)&&("array"===n||0===t||"number"==typeof t&&t>0&&t-1 in e)}var E=function(e){var t,n,r,i,o,a,s,u,l,c,f,p,d,h,g,y,v,m,x,b="sizzle"+1*new Date,w=e.document,T=0,C=0,E=ae(),k=ae(),S=ae(),D=function(e,t){return e===t&&(f=!0),0},N={}.hasOwnProperty,A=[],j=A.pop,q=A.push,L=A.push,H=A.slice,O=function(e,t){for(var n=0,r=e.length;n<r;n++)if(e[n]===t)return n;return-1},P="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",M="[\\x20\\t\\r\\n\\f]",R="(?:\\\\.|[\\w-]|[^\0-\\xa0])+",I="\\["+M+"*("+R+")(?:"+M+"*([*^$|!~]?=)"+M+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+R+"))|)"+M+"*\\]",W=":("+R+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+I+")*)|.*)\\)|)",$=new RegExp(M+"+","g"),B=new RegExp("^"+M+"+|((?:^|[^\\\\])(?:\\\\.)*)"+M+"+$","g"),F=new RegExp("^"+M+"*,"+M+"*"),_=new RegExp("^"+M+"*([>+~]|"+M+")"+M+"*"),z=new RegExp("="+M+"*([^\\]'\"]*?)"+M+"*\\]","g"),X=new RegExp(W),U=new RegExp("^"+R+"$"),V={ID:new RegExp("^#("+R+")"),CLASS:new RegExp("^\\.("+R+")"),TAG:new RegExp("^("+R+"|[*])"),ATTR:new RegExp("^"+I),PSEUDO:new RegExp("^"+W),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+M+"*(even|odd|(([+-]|)(\\d*)n|)"+M+"*(?:([+-]|)"+M+"*(\\d+)|))"+M+"*\\)|)","i"),bool:new RegExp("^(?:"+P+")$","i"),needsContext:new RegExp("^"+M+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+M+"*((?:-\\d)?\\d*)"+M+"*\\)|)(?=[^-]|$)","i")},G=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Q=/^[^{]+\{\s*\[native \w/,J=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,K=/[+~]/,Z=new RegExp("\\\\([\\da-f]{1,6}"+M+"?|("+M+")|.)","ig"),ee=function(e,t,n){var r="0x"+t-65536;return r!==r||n?t:r<0?String.fromCharCode(r+65536):String.fromCharCode(r>>10|55296,1023&r|56320)},te=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,ne=function(e,t){return t?"\0"===e?"\ufffd":e.slice(0,-1)+"\\"+e.charCodeAt(e.length-1).toString(16)+" ":"\\"+e},re=function(){p()},ie=me(function(e){return!0===e.disabled&&("form"in e||"label"in e)},{dir:"parentNode",next:"legend"});try{L.apply(A=H.call(w.childNodes),w.childNodes),A[w.childNodes.length].nodeType}catch(e){L={apply:A.length?function(e,t){q.apply(e,H.call(t))}:function(e,t){var n=e.length,r=0;while(e[n++]=t[r++]);e.length=n-1}}}function oe(e,t,r,i){var o,s,l,c,f,h,v,m=t&&t.ownerDocument,T=t?t.nodeType:9;if(r=r||[],"string"!=typeof e||!e||1!==T&&9!==T&&11!==T)return r;if(!i&&((t?t.ownerDocument||t:w)!==d&&p(t),t=t||d,g)){if(11!==T&&(f=J.exec(e)))if(o=f[1]){if(9===T){if(!(l=t.getElementById(o)))return r;if(l.id===o)return r.push(l),r}else if(m&&(l=m.getElementById(o))&&x(t,l)&&l.id===o)return r.push(l),r}else{if(f[2])return L.apply(r,t.getElementsByTagName(e)),r;if((o=f[3])&&n.getElementsByClassName&&t.getElementsByClassName)return L.apply(r,t.getElementsByClassName(o)),r}if(n.qsa&&!S[e+" "]&&(!y||!y.test(e))){if(1!==T)m=t,v=e;else if("object"!==t.nodeName.toLowerCase()){(c=t.getAttribute("id"))?c=c.replace(te,ne):t.setAttribute("id",c=b),s=(h=a(e)).length;while(s--)h[s]="#"+c+" "+ve(h[s]);v=h.join(","),m=K.test(e)&&ge(t.parentNode)||t}if(v)try{return L.apply(r,m.querySelectorAll(v)),r}catch(e){}finally{c===b&&t.removeAttribute("id")}}}return u(e.replace(B,"$1"),t,r,i)}function ae(){var e=[];function t(n,i){return e.push(n+" ")>r.cacheLength&&delete t[e.shift()],t[n+" "]=i}return t}function se(e){return e[b]=!0,e}function ue(e){var t=d.createElement("fieldset");try{return!!e(t)}catch(e){return!1}finally{t.parentNode&&t.parentNode.removeChild(t),t=null}}function le(e,t){var n=e.split("|"),i=n.length;while(i--)r.attrHandle[n[i]]=t}function ce(e,t){var n=t&&e,r=n&&1===e.nodeType&&1===t.nodeType&&e.sourceIndex-t.sourceIndex;if(r)return r;if(n)while(n=n.nextSibling)if(n===t)return-1;return e?1:-1}function fe(e){return function(t){return"input"===t.nodeName.toLowerCase()&&t.type===e}}function pe(e){return function(t){var n=t.nodeName.toLowerCase();return("input"===n||"button"===n)&&t.type===e}}function de(e){return function(t){return"form"in t?t.parentNode&&!1===t.disabled?"label"in t?"label"in t.parentNode?t.parentNode.disabled===e:t.disabled===e:t.isDisabled===e||t.isDisabled!==!e&&ie(t)===e:t.disabled===e:"label"in t&&t.disabled===e}}function he(e){return se(function(t){return t=+t,se(function(n,r){var i,o=e([],n.length,t),a=o.length;while(a--)n[i=o[a]]&&(n[i]=!(r[i]=n[i]))})})}function ge(e){return e&&"undefined"!=typeof e.getElementsByTagName&&e}n=oe.support={},o=oe.isXML=function(e){var t=e&&(e.ownerDocument||e).documentElement;return!!t&&"HTML"!==t.nodeName},p=oe.setDocument=function(e){var t,i,a=e?e.ownerDocument||e:w;return a!==d&&9===a.nodeType&&a.documentElement?(d=a,h=d.documentElement,g=!o(d),w!==d&&(i=d.defaultView)&&i.top!==i&&(i.addEventListener?i.addEventListener("unload",re,!1):i.attachEvent&&i.attachEvent("onunload",re)),n.attributes=ue(function(e){return e.className="i",!e.getAttribute("className")}),n.getElementsByTagName=ue(function(e){return e.appendChild(d.createComment("")),!e.getElementsByTagName("*").length}),n.getElementsByClassName=Q.test(d.getElementsByClassName),n.getById=ue(function(e){return h.appendChild(e).id=b,!d.getElementsByName||!d.getElementsByName(b).length}),n.getById?(r.filter.ID=function(e){var t=e.replace(Z,ee);return function(e){return e.getAttribute("id")===t}},r.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&g){var n=t.getElementById(e);return n?[n]:[]}}):(r.filter.ID=function(e){var t=e.replace(Z,ee);return function(e){var n="undefined"!=typeof e.getAttributeNode&&e.getAttributeNode("id");return n&&n.value===t}},r.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&g){var n,r,i,o=t.getElementById(e);if(o){if((n=o.getAttributeNode("id"))&&n.value===e)return[o];i=t.getElementsByName(e),r=0;while(o=i[r++])if((n=o.getAttributeNode("id"))&&n.value===e)return[o]}return[]}}),r.find.TAG=n.getElementsByTagName?function(e,t){return"undefined"!=typeof t.getElementsByTagName?t.getElementsByTagName(e):n.qsa?t.querySelectorAll(e):void 0}:function(e,t){var n,r=[],i=0,o=t.getElementsByTagName(e);if("*"===e){while(n=o[i++])1===n.nodeType&&r.push(n);return r}return o},r.find.CLASS=n.getElementsByClassName&&function(e,t){if("undefined"!=typeof t.getElementsByClassName&&g)return t.getElementsByClassName(e)},v=[],y=[],(n.qsa=Q.test(d.querySelectorAll))&&(ue(function(e){h.appendChild(e).innerHTML="<a id='"+b+"'></a><select id='"+b+"-\r\\' msallowcapture=''><option selected=''></option></select>",e.querySelectorAll("[msallowcapture^='']").length&&y.push("[*^$]="+M+"*(?:''|\"\")"),e.querySelectorAll("[selected]").length||y.push("\\["+M+"*(?:value|"+P+")"),e.querySelectorAll("[id~="+b+"-]").length||y.push("~="),e.querySelectorAll(":checked").length||y.push(":checked"),e.querySelectorAll("a#"+b+"+*").length||y.push(".#.+[+~]")}),ue(function(e){e.innerHTML="<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";var t=d.createElement("input");t.setAttribute("type","hidden"),e.appendChild(t).setAttribute("name","D"),e.querySelectorAll("[name=d]").length&&y.push("name"+M+"*[*^$|!~]?="),2!==e.querySelectorAll(":enabled").length&&y.push(":enabled",":disabled"),h.appendChild(e).disabled=!0,2!==e.querySelectorAll(":disabled").length&&y.push(":enabled",":disabled"),e.querySelectorAll("*,:x"),y.push(",.*:")})),(n.matchesSelector=Q.test(m=h.matches||h.webkitMatchesSelector||h.mozMatchesSelector||h.oMatchesSelector||h.msMatchesSelector))&&ue(function(e){n.disconnectedMatch=m.call(e,"*"),m.call(e,"[s!='']:x"),v.push("!=",W)}),y=y.length&&new RegExp(y.join("|")),v=v.length&&new RegExp(v.join("|")),t=Q.test(h.compareDocumentPosition),x=t||Q.test(h.contains)?function(e,t){var n=9===e.nodeType?e.documentElement:e,r=t&&t.parentNode;return e===r||!(!r||1!==r.nodeType||!(n.contains?n.contains(r):e.compareDocumentPosition&&16&e.compareDocumentPosition(r)))}:function(e,t){if(t)while(t=t.parentNode)if(t===e)return!0;return!1},D=t?function(e,t){if(e===t)return f=!0,0;var r=!e.compareDocumentPosition-!t.compareDocumentPosition;return r||(1&(r=(e.ownerDocument||e)===(t.ownerDocument||t)?e.compareDocumentPosition(t):1)||!n.sortDetached&&t.compareDocumentPosition(e)===r?e===d||e.ownerDocument===w&&x(w,e)?-1:t===d||t.ownerDocument===w&&x(w,t)?1:c?O(c,e)-O(c,t):0:4&r?-1:1)}:function(e,t){if(e===t)return f=!0,0;var n,r=0,i=e.parentNode,o=t.parentNode,a=[e],s=[t];if(!i||!o)return e===d?-1:t===d?1:i?-1:o?1:c?O(c,e)-O(c,t):0;if(i===o)return ce(e,t);n=e;while(n=n.parentNode)a.unshift(n);n=t;while(n=n.parentNode)s.unshift(n);while(a[r]===s[r])r++;return r?ce(a[r],s[r]):a[r]===w?-1:s[r]===w?1:0},d):d},oe.matches=function(e,t){return oe(e,null,null,t)},oe.matchesSelector=function(e,t){if((e.ownerDocument||e)!==d&&p(e),t=t.replace(z,"='$1']"),n.matchesSelector&&g&&!S[t+" "]&&(!v||!v.test(t))&&(!y||!y.test(t)))try{var r=m.call(e,t);if(r||n.disconnectedMatch||e.document&&11!==e.document.nodeType)return r}catch(e){}return oe(t,d,null,[e]).length>0},oe.contains=function(e,t){return(e.ownerDocument||e)!==d&&p(e),x(e,t)},oe.attr=function(e,t){(e.ownerDocument||e)!==d&&p(e);var i=r.attrHandle[t.toLowerCase()],o=i&&N.call(r.attrHandle,t.toLowerCase())?i(e,t,!g):void 0;return void 0!==o?o:n.attributes||!g?e.getAttribute(t):(o=e.getAttributeNode(t))&&o.specified?o.value:null},oe.escape=function(e){return(e+"").replace(te,ne)},oe.error=function(e){throw new Error("Syntax error, unrecognized expression: "+e)},oe.uniqueSort=function(e){var t,r=[],i=0,o=0;if(f=!n.detectDuplicates,c=!n.sortStable&&e.slice(0),e.sort(D),f){while(t=e[o++])t===e[o]&&(i=r.push(o));while(i--)e.splice(r[i],1)}return c=null,e},i=oe.getText=function(e){var t,n="",r=0,o=e.nodeType;if(o){if(1===o||9===o||11===o){if("string"==typeof e.textContent)return e.textContent;for(e=e.firstChild;e;e=e.nextSibling)n+=i(e)}else if(3===o||4===o)return e.nodeValue}else while(t=e[r++])n+=i(t);return n},(r=oe.selectors={cacheLength:50,createPseudo:se,match:V,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(e){return e[1]=e[1].replace(Z,ee),e[3]=(e[3]||e[4]||e[5]||"").replace(Z,ee),"~="===e[2]&&(e[3]=" "+e[3]+" "),e.slice(0,4)},CHILD:function(e){return e[1]=e[1].toLowerCase(),"nth"===e[1].slice(0,3)?(e[3]||oe.error(e[0]),e[4]=+(e[4]?e[5]+(e[6]||1):2*("even"===e[3]||"odd"===e[3])),e[5]=+(e[7]+e[8]||"odd"===e[3])):e[3]&&oe.error(e[0]),e},PSEUDO:function(e){var t,n=!e[6]&&e[2];return V.CHILD.test(e[0])?null:(e[3]?e[2]=e[4]||e[5]||"":n&&X.test(n)&&(t=a(n,!0))&&(t=n.indexOf(")",n.length-t)-n.length)&&(e[0]=e[0].slice(0,t),e[2]=n.slice(0,t)),e.slice(0,3))}},filter:{TAG:function(e){var t=e.replace(Z,ee).toLowerCase();return"*"===e?function(){return!0}:function(e){return e.nodeName&&e.nodeName.toLowerCase()===t}},CLASS:function(e){var t=E[e+" "];return t||(t=new RegExp("(^|"+M+")"+e+"("+M+"|$)"))&&E(e,function(e){return t.test("string"==typeof e.className&&e.className||"undefined"!=typeof e.getAttribute&&e.getAttribute("class")||"")})},ATTR:function(e,t,n){return function(r){var i=oe.attr(r,e);return null==i?"!="===t:!t||(i+="","="===t?i===n:"!="===t?i!==n:"^="===t?n&&0===i.indexOf(n):"*="===t?n&&i.indexOf(n)>-1:"$="===t?n&&i.slice(-n.length)===n:"~="===t?(" "+i.replace($," ")+" ").indexOf(n)>-1:"|="===t&&(i===n||i.slice(0,n.length+1)===n+"-"))}},CHILD:function(e,t,n,r,i){var o="nth"!==e.slice(0,3),a="last"!==e.slice(-4),s="of-type"===t;return 1===r&&0===i?function(e){return!!e.parentNode}:function(t,n,u){var l,c,f,p,d,h,g=o!==a?"nextSibling":"previousSibling",y=t.parentNode,v=s&&t.nodeName.toLowerCase(),m=!u&&!s,x=!1;if(y){if(o){while(g){p=t;while(p=p[g])if(s?p.nodeName.toLowerCase()===v:1===p.nodeType)return!1;h=g="only"===e&&!h&&"nextSibling"}return!0}if(h=[a?y.firstChild:y.lastChild],a&&m){x=(d=(l=(c=(f=(p=y)[b]||(p[b]={}))[p.uniqueID]||(f[p.uniqueID]={}))[e]||[])[0]===T&&l[1])&&l[2],p=d&&y.childNodes[d];while(p=++d&&p&&p[g]||(x=d=0)||h.pop())if(1===p.nodeType&&++x&&p===t){c[e]=[T,d,x];break}}else if(m&&(x=d=(l=(c=(f=(p=t)[b]||(p[b]={}))[p.uniqueID]||(f[p.uniqueID]={}))[e]||[])[0]===T&&l[1]),!1===x)while(p=++d&&p&&p[g]||(x=d=0)||h.pop())if((s?p.nodeName.toLowerCase()===v:1===p.nodeType)&&++x&&(m&&((c=(f=p[b]||(p[b]={}))[p.uniqueID]||(f[p.uniqueID]={}))[e]=[T,x]),p===t))break;return(x-=i)===r||x%r==0&&x/r>=0}}},PSEUDO:function(e,t){var n,i=r.pseudos[e]||r.setFilters[e.toLowerCase()]||oe.error("unsupported pseudo: "+e);return i[b]?i(t):i.length>1?(n=[e,e,"",t],r.setFilters.hasOwnProperty(e.toLowerCase())?se(function(e,n){var r,o=i(e,t),a=o.length;while(a--)e[r=O(e,o[a])]=!(n[r]=o[a])}):function(e){return i(e,0,n)}):i}},pseudos:{not:se(function(e){var t=[],n=[],r=s(e.replace(B,"$1"));return r[b]?se(function(e,t,n,i){var o,a=r(e,null,i,[]),s=e.length;while(s--)(o=a[s])&&(e[s]=!(t[s]=o))}):function(e,i,o){return t[0]=e,r(t,null,o,n),t[0]=null,!n.pop()}}),has:se(function(e){return function(t){return oe(e,t).length>0}}),contains:se(function(e){return e=e.replace(Z,ee),function(t){return(t.textContent||t.innerText||i(t)).indexOf(e)>-1}}),lang:se(function(e){return U.test(e||"")||oe.error("unsupported lang: "+e),e=e.replace(Z,ee).toLowerCase(),function(t){var n;do{if(n=g?t.lang:t.getAttribute("xml:lang")||t.getAttribute("lang"))return(n=n.toLowerCase())===e||0===n.indexOf(e+"-")}while((t=t.parentNode)&&1===t.nodeType);return!1}}),target:function(t){var n=e.location&&e.location.hash;return n&&n.slice(1)===t.id},root:function(e){return e===h},focus:function(e){return e===d.activeElement&&(!d.hasFocus||d.hasFocus())&&!!(e.type||e.href||~e.tabIndex)},enabled:de(!1),disabled:de(!0),checked:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&!!e.checked||"option"===t&&!!e.selected},selected:function(e){return e.parentNode&&e.parentNode.selectedIndex,!0===e.selected},empty:function(e){for(e=e.firstChild;e;e=e.nextSibling)if(e.nodeType<6)return!1;return!0},parent:function(e){return!r.pseudos.empty(e)},header:function(e){return Y.test(e.nodeName)},input:function(e){return G.test(e.nodeName)},button:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&"button"===e.type||"button"===t},text:function(e){var t;return"input"===e.nodeName.toLowerCase()&&"text"===e.type&&(null==(t=e.getAttribute("type"))||"text"===t.toLowerCase())},first:he(function(){return[0]}),last:he(function(e,t){return[t-1]}),eq:he(function(e,t,n){return[n<0?n+t:n]}),even:he(function(e,t){for(var n=0;n<t;n+=2)e.push(n);return e}),odd:he(function(e,t){for(var n=1;n<t;n+=2)e.push(n);return e}),lt:he(function(e,t,n){for(var r=n<0?n+t:n;--r>=0;)e.push(r);return e}),gt:he(function(e,t,n){for(var r=n<0?n+t:n;++r<t;)e.push(r);return e})}}).pseudos.nth=r.pseudos.eq;for(t in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})r.pseudos[t]=fe(t);for(t in{submit:!0,reset:!0})r.pseudos[t]=pe(t);function ye(){}ye.prototype=r.filters=r.pseudos,r.setFilters=new ye,a=oe.tokenize=function(e,t){var n,i,o,a,s,u,l,c=k[e+" "];if(c)return t?0:c.slice(0);s=e,u=[],l=r.preFilter;while(s){n&&!(i=F.exec(s))||(i&&(s=s.slice(i[0].length)||s),u.push(o=[])),n=!1,(i=_.exec(s))&&(n=i.shift(),o.push({value:n,type:i[0].replace(B," ")}),s=s.slice(n.length));for(a in r.filter)!(i=V[a].exec(s))||l[a]&&!(i=l[a](i))||(n=i.shift(),o.push({value:n,type:a,matches:i}),s=s.slice(n.length));if(!n)break}return t?s.length:s?oe.error(e):k(e,u).slice(0)};function ve(e){for(var t=0,n=e.length,r="";t<n;t++)r+=e[t].value;return r}function me(e,t,n){var r=t.dir,i=t.next,o=i||r,a=n&&"parentNode"===o,s=C++;return t.first?function(t,n,i){while(t=t[r])if(1===t.nodeType||a)return e(t,n,i);return!1}:function(t,n,u){var l,c,f,p=[T,s];if(u){while(t=t[r])if((1===t.nodeType||a)&&e(t,n,u))return!0}else while(t=t[r])if(1===t.nodeType||a)if(f=t[b]||(t[b]={}),c=f[t.uniqueID]||(f[t.uniqueID]={}),i&&i===t.nodeName.toLowerCase())t=t[r]||t;else{if((l=c[o])&&l[0]===T&&l[1]===s)return p[2]=l[2];if(c[o]=p,p[2]=e(t,n,u))return!0}return!1}}function xe(e){return e.length>1?function(t,n,r){var i=e.length;while(i--)if(!e[i](t,n,r))return!1;return!0}:e[0]}function be(e,t,n){for(var r=0,i=t.length;r<i;r++)oe(e,t[r],n);return n}function we(e,t,n,r,i){for(var o,a=[],s=0,u=e.length,l=null!=t;s<u;s++)(o=e[s])&&(n&&!n(o,r,i)||(a.push(o),l&&t.push(s)));return a}function Te(e,t,n,r,i,o){return r&&!r[b]&&(r=Te(r)),i&&!i[b]&&(i=Te(i,o)),se(function(o,a,s,u){var l,c,f,p=[],d=[],h=a.length,g=o||be(t||"*",s.nodeType?[s]:s,[]),y=!e||!o&&t?g:we(g,p,e,s,u),v=n?i||(o?e:h||r)?[]:a:y;if(n&&n(y,v,s,u),r){l=we(v,d),r(l,[],s,u),c=l.length;while(c--)(f=l[c])&&(v[d[c]]=!(y[d[c]]=f))}if(o){if(i||e){if(i){l=[],c=v.length;while(c--)(f=v[c])&&l.push(y[c]=f);i(null,v=[],l,u)}c=v.length;while(c--)(f=v[c])&&(l=i?O(o,f):p[c])>-1&&(o[l]=!(a[l]=f))}}else v=we(v===a?v.splice(h,v.length):v),i?i(null,a,v,u):L.apply(a,v)})}function Ce(e){for(var t,n,i,o=e.length,a=r.relative[e[0].type],s=a||r.relative[" "],u=a?1:0,c=me(function(e){return e===t},s,!0),f=me(function(e){return O(t,e)>-1},s,!0),p=[function(e,n,r){var i=!a&&(r||n!==l)||((t=n).nodeType?c(e,n,r):f(e,n,r));return t=null,i}];u<o;u++)if(n=r.relative[e[u].type])p=[me(xe(p),n)];else{if((n=r.filter[e[u].type].apply(null,e[u].matches))[b]){for(i=++u;i<o;i++)if(r.relative[e[i].type])break;return Te(u>1&&xe(p),u>1&&ve(e.slice(0,u-1).concat({value:" "===e[u-2].type?"*":""})).replace(B,"$1"),n,u<i&&Ce(e.slice(u,i)),i<o&&Ce(e=e.slice(i)),i<o&&ve(e))}p.push(n)}return xe(p)}function Ee(e,t){var n=t.length>0,i=e.length>0,o=function(o,a,s,u,c){var f,h,y,v=0,m="0",x=o&&[],b=[],w=l,C=o||i&&r.find.TAG("*",c),E=T+=null==w?1:Math.random()||.1,k=C.length;for(c&&(l=a===d||a||c);m!==k&&null!=(f=C[m]);m++){if(i&&f){h=0,a||f.ownerDocument===d||(p(f),s=!g);while(y=e[h++])if(y(f,a||d,s)){u.push(f);break}c&&(T=E)}n&&((f=!y&&f)&&v--,o&&x.push(f))}if(v+=m,n&&m!==v){h=0;while(y=t[h++])y(x,b,a,s);if(o){if(v>0)while(m--)x[m]||b[m]||(b[m]=j.call(u));b=we(b)}L.apply(u,b),c&&!o&&b.length>0&&v+t.length>1&&oe.uniqueSort(u)}return c&&(T=E,l=w),x};return n?se(o):o}return s=oe.compile=function(e,t){var n,r=[],i=[],o=S[e+" "];if(!o){t||(t=a(e)),n=t.length;while(n--)(o=Ce(t[n]))[b]?r.push(o):i.push(o);(o=S(e,Ee(i,r))).selector=e}return o},u=oe.select=function(e,t,n,i){var o,u,l,c,f,p="function"==typeof e&&e,d=!i&&a(e=p.selector||e);if(n=n||[],1===d.length){if((u=d[0]=d[0].slice(0)).length>2&&"ID"===(l=u[0]).type&&9===t.nodeType&&g&&r.relative[u[1].type]){if(!(t=(r.find.ID(l.matches[0].replace(Z,ee),t)||[])[0]))return n;p&&(t=t.parentNode),e=e.slice(u.shift().value.length)}o=V.needsContext.test(e)?0:u.length;while(o--){if(l=u[o],r.relative[c=l.type])break;if((f=r.find[c])&&(i=f(l.matches[0].replace(Z,ee),K.test(u[0].type)&&ge(t.parentNode)||t))){if(u.splice(o,1),!(e=i.length&&ve(u)))return L.apply(n,i),n;break}}}return(p||s(e,d))(i,t,!g,n,!t||K.test(e)&&ge(t.parentNode)||t),n},n.sortStable=b.split("").sort(D).join("")===b,n.detectDuplicates=!!f,p(),n.sortDetached=ue(function(e){return 1&e.compareDocumentPosition(d.createElement("fieldset"))}),ue(function(e){return e.innerHTML="<a href='#'></a>","#"===e.firstChild.getAttribute("href")})||le("type|href|height|width",function(e,t,n){if(!n)return e.getAttribute(t,"type"===t.toLowerCase()?1:2)}),n.attributes&&ue(function(e){return e.innerHTML="<input/>",e.firstChild.setAttribute("value",""),""===e.firstChild.getAttribute("value")})||le("value",function(e,t,n){if(!n&&"input"===e.nodeName.toLowerCase())return e.defaultValue}),ue(function(e){return null==e.getAttribute("disabled")})||le(P,function(e,t,n){var r;if(!n)return!0===e[t]?t.toLowerCase():(r=e.getAttributeNode(t))&&r.specified?r.value:null}),oe}(e);w.find=E,w.expr=E.selectors,w.expr[":"]=w.expr.pseudos,w.uniqueSort=w.unique=E.uniqueSort,w.text=E.getText,w.isXMLDoc=E.isXML,w.contains=E.contains,w.escapeSelector=E.escape;var k=function(e,t,n){var r=[],i=void 0!==n;while((e=e[t])&&9!==e.nodeType)if(1===e.nodeType){if(i&&w(e).is(n))break;r.push(e)}return r},S=function(e,t){for(var n=[];e;e=e.nextSibling)1===e.nodeType&&e!==t&&n.push(e);return n},D=w.expr.match.needsContext;function N(e,t){return e.nodeName&&e.nodeName.toLowerCase()===t.toLowerCase()}var A=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;function j(e,t,n){return g(t)?w.grep(e,function(e,r){return!!t.call(e,r,e)!==n}):t.nodeType?w.grep(e,function(e){return e===t!==n}):"string"!=typeof t?w.grep(e,function(e){return u.call(t,e)>-1!==n}):w.filter(t,e,n)}w.filter=function(e,t,n){var r=t[0];return n&&(e=":not("+e+")"),1===t.length&&1===r.nodeType?w.find.matchesSelector(r,e)?[r]:[]:w.find.matches(e,w.grep(t,function(e){return 1===e.nodeType}))},w.fn.extend({find:function(e){var t,n,r=this.length,i=this;if("string"!=typeof e)return this.pushStack(w(e).filter(function(){for(t=0;t<r;t++)if(w.contains(i[t],this))return!0}));for(n=this.pushStack([]),t=0;t<r;t++)w.find(e,i[t],n);return r>1?w.uniqueSort(n):n},filter:function(e){return this.pushStack(j(this,e||[],!1))},not:function(e){return this.pushStack(j(this,e||[],!0))},is:function(e){return!!j(this,"string"==typeof e&&D.test(e)?w(e):e||[],!1).length}});var q,L=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;(w.fn.init=function(e,t,n){var i,o;if(!e)return this;if(n=n||q,"string"==typeof e){if(!(i="<"===e[0]&&">"===e[e.length-1]&&e.length>=3?[null,e,null]:L.exec(e))||!i[1]&&t)return!t||t.jquery?(t||n).find(e):this.constructor(t).find(e);if(i[1]){if(t=t instanceof w?t[0]:t,w.merge(this,w.parseHTML(i[1],t&&t.nodeType?t.ownerDocument||t:r,!0)),A.test(i[1])&&w.isPlainObject(t))for(i in t)g(this[i])?this[i](t[i]):this.attr(i,t[i]);return this}return(o=r.getElementById(i[2]))&&(this[0]=o,this.length=1),this}return e.nodeType?(this[0]=e,this.length=1,this):g(e)?void 0!==n.ready?n.ready(e):e(w):w.makeArray(e,this)}).prototype=w.fn,q=w(r);var H=/^(?:parents|prev(?:Until|All))/,O={children:!0,contents:!0,next:!0,prev:!0};w.fn.extend({has:function(e){var t=w(e,this),n=t.length;return this.filter(function(){for(var e=0;e<n;e++)if(w.contains(this,t[e]))return!0})},closest:function(e,t){var n,r=0,i=this.length,o=[],a="string"!=typeof e&&w(e);if(!D.test(e))for(;r<i;r++)for(n=this[r];n&&n!==t;n=n.parentNode)if(n.nodeType<11&&(a?a.index(n)>-1:1===n.nodeType&&w.find.matchesSelector(n,e))){o.push(n);break}return this.pushStack(o.length>1?w.uniqueSort(o):o)},index:function(e){return e?"string"==typeof e?u.call(w(e),this[0]):u.call(this,e.jquery?e[0]:e):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(e,t){return this.pushStack(w.uniqueSort(w.merge(this.get(),w(e,t))))},addBack:function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}});function P(e,t){while((e=e[t])&&1!==e.nodeType);return e}w.each({parent:function(e){var t=e.parentNode;return t&&11!==t.nodeType?t:null},parents:function(e){return k(e,"parentNode")},parentsUntil:function(e,t,n){return k(e,"parentNode",n)},next:function(e){return P(e,"nextSibling")},prev:function(e){return P(e,"previousSibling")},nextAll:function(e){return k(e,"nextSibling")},prevAll:function(e){return k(e,"previousSibling")},nextUntil:function(e,t,n){return k(e,"nextSibling",n)},prevUntil:function(e,t,n){return k(e,"previousSibling",n)},siblings:function(e){return S((e.parentNode||{}).firstChild,e)},children:function(e){return S(e.firstChild)},contents:function(e){return N(e,"iframe")?e.contentDocument:(N(e,"template")&&(e=e.content||e),w.merge([],e.childNodes))}},function(e,t){w.fn[e]=function(n,r){var i=w.map(this,t,n);return"Until"!==e.slice(-5)&&(r=n),r&&"string"==typeof r&&(i=w.filter(r,i)),this.length>1&&(O[e]||w.uniqueSort(i),H.test(e)&&i.reverse()),this.pushStack(i)}});var M=/[^\x20\t\r\n\f]+/g;function R(e){var t={};return w.each(e.match(M)||[],function(e,n){t[n]=!0}),t}w.Callbacks=function(e){e="string"==typeof e?R(e):w.extend({},e);var t,n,r,i,o=[],a=[],s=-1,u=function(){for(i=i||e.once,r=t=!0;a.length;s=-1){n=a.shift();while(++s<o.length)!1===o[s].apply(n[0],n[1])&&e.stopOnFalse&&(s=o.length,n=!1)}e.memory||(n=!1),t=!1,i&&(o=n?[]:"")},l={add:function(){return o&&(n&&!t&&(s=o.length-1,a.push(n)),function t(n){w.each(n,function(n,r){g(r)?e.unique&&l.has(r)||o.push(r):r&&r.length&&"string"!==x(r)&&t(r)})}(arguments),n&&!t&&u()),this},remove:function(){return w.each(arguments,function(e,t){var n;while((n=w.inArray(t,o,n))>-1)o.splice(n,1),n<=s&&s--}),this},has:function(e){return e?w.inArray(e,o)>-1:o.length>0},empty:function(){return o&&(o=[]),this},disable:function(){return i=a=[],o=n="",this},disabled:function(){return!o},lock:function(){return i=a=[],n||t||(o=n=""),this},locked:function(){return!!i},fireWith:function(e,n){return i||(n=[e,(n=n||[]).slice?n.slice():n],a.push(n),t||u()),this},fire:function(){return l.fireWith(this,arguments),this},fired:function(){return!!r}};return l};function I(e){return e}function W(e){throw e}function $(e,t,n,r){var i;try{e&&g(i=e.promise)?i.call(e).done(t).fail(n):e&&g(i=e.then)?i.call(e,t,n):t.apply(void 0,[e].slice(r))}catch(e){n.apply(void 0,[e])}}w.extend({Deferred:function(t){var n=[["notify","progress",w.Callbacks("memory"),w.Callbacks("memory"),2],["resolve","done",w.Callbacks("once memory"),w.Callbacks("once memory"),0,"resolved"],["reject","fail",w.Callbacks("once memory"),w.Callbacks("once memory"),1,"rejected"]],r="pending",i={state:function(){return r},always:function(){return o.done(arguments).fail(arguments),this},"catch":function(e){return i.then(null,e)},pipe:function(){var e=arguments;return w.Deferred(function(t){w.each(n,function(n,r){var i=g(e[r[4]])&&e[r[4]];o[r[1]](function(){var e=i&&i.apply(this,arguments);e&&g(e.promise)?e.promise().progress(t.notify).done(t.resolve).fail(t.reject):t[r[0]+"With"](this,i?[e]:arguments)})}),e=null}).promise()},then:function(t,r,i){var o=0;function a(t,n,r,i){return function(){var s=this,u=arguments,l=function(){var e,l;if(!(t<o)){if((e=r.apply(s,u))===n.promise())throw new TypeError("Thenable self-resolution");l=e&&("object"==typeof e||"function"==typeof e)&&e.then,g(l)?i?l.call(e,a(o,n,I,i),a(o,n,W,i)):(o++,l.call(e,a(o,n,I,i),a(o,n,W,i),a(o,n,I,n.notifyWith))):(r!==I&&(s=void 0,u=[e]),(i||n.resolveWith)(s,u))}},c=i?l:function(){try{l()}catch(e){w.Deferred.exceptionHook&&w.Deferred.exceptionHook(e,c.stackTrace),t+1>=o&&(r!==W&&(s=void 0,u=[e]),n.rejectWith(s,u))}};t?c():(w.Deferred.getStackHook&&(c.stackTrace=w.Deferred.getStackHook()),e.setTimeout(c))}}return w.Deferred(function(e){n[0][3].add(a(0,e,g(i)?i:I,e.notifyWith)),n[1][3].add(a(0,e,g(t)?t:I)),n[2][3].add(a(0,e,g(r)?r:W))}).promise()},promise:function(e){return null!=e?w.extend(e,i):i}},o={};return w.each(n,function(e,t){var a=t[2],s=t[5];i[t[1]]=a.add,s&&a.add(function(){r=s},n[3-e][2].disable,n[3-e][3].disable,n[0][2].lock,n[0][3].lock),a.add(t[3].fire),o[t[0]]=function(){return o[t[0]+"With"](this===o?void 0:this,arguments),this},o[t[0]+"With"]=a.fireWith}),i.promise(o),t&&t.call(o,o),o},when:function(e){var t=arguments.length,n=t,r=Array(n),i=o.call(arguments),a=w.Deferred(),s=function(e){return function(n){r[e]=this,i[e]=arguments.length>1?o.call(arguments):n,--t||a.resolveWith(r,i)}};if(t<=1&&($(e,a.done(s(n)).resolve,a.reject,!t),"pending"===a.state()||g(i[n]&&i[n].then)))return a.then();while(n--)$(i[n],s(n),a.reject);return a.promise()}});var B=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;w.Deferred.exceptionHook=function(t,n){e.console&&e.console.warn&&t&&B.test(t.name)&&e.console.warn("jQuery.Deferred exception: "+t.message,t.stack,n)},w.readyException=function(t){e.setTimeout(function(){throw t})};var F=w.Deferred();w.fn.ready=function(e){return F.then(e)["catch"](function(e){w.readyException(e)}),this},w.extend({isReady:!1,readyWait:1,ready:function(e){(!0===e?--w.readyWait:w.isReady)||(w.isReady=!0,!0!==e&&--w.readyWait>0||F.resolveWith(r,[w]))}}),w.ready.then=F.then;function _(){r.removeEventListener("DOMContentLoaded",_),e.removeEventListener("load",_),w.ready()}"complete"===r.readyState||"loading"!==r.readyState&&!r.documentElement.doScroll?e.setTimeout(w.ready):(r.addEventListener("DOMContentLoaded",_),e.addEventListener("load",_));var z=function(e,t,n,r,i,o,a){var s=0,u=e.length,l=null==n;if("object"===x(n)){i=!0;for(s in n)z(e,t,s,n[s],!0,o,a)}else if(void 0!==r&&(i=!0,g(r)||(a=!0),l&&(a?(t.call(e,r),t=null):(l=t,t=function(e,t,n){return l.call(w(e),n)})),t))for(;s<u;s++)t(e[s],n,a?r:r.call(e[s],s,t(e[s],n)));return i?e:l?t.call(e):u?t(e[0],n):o},X=/^-ms-/,U=/-([a-z])/g;function V(e,t){return t.toUpperCase()}function G(e){return e.replace(X,"ms-").replace(U,V)}var Y=function(e){return 1===e.nodeType||9===e.nodeType||!+e.nodeType};function Q(){this.expando=w.expando+Q.uid++}Q.uid=1,Q.prototype={cache:function(e){var t=e[this.expando];return t||(t={},Y(e)&&(e.nodeType?e[this.expando]=t:Object.defineProperty(e,this.expando,{value:t,configurable:!0}))),t},set:function(e,t,n){var r,i=this.cache(e);if("string"==typeof t)i[G(t)]=n;else for(r in t)i[G(r)]=t[r];return i},get:function(e,t){return void 0===t?this.cache(e):e[this.expando]&&e[this.expando][G(t)]},access:function(e,t,n){return void 0===t||t&&"string"==typeof t&&void 0===n?this.get(e,t):(this.set(e,t,n),void 0!==n?n:t)},remove:function(e,t){var n,r=e[this.expando];if(void 0!==r){if(void 0!==t){n=(t=Array.isArray(t)?t.map(G):(t=G(t))in r?[t]:t.match(M)||[]).length;while(n--)delete r[t[n]]}(void 0===t||w.isEmptyObject(r))&&(e.nodeType?e[this.expando]=void 0:delete e[this.expando])}},hasData:function(e){var t=e[this.expando];return void 0!==t&&!w.isEmptyObject(t)}};var J=new Q,K=new Q,Z=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,ee=/[A-Z]/g;function te(e){return"true"===e||"false"!==e&&("null"===e?null:e===+e+""?+e:Z.test(e)?JSON.parse(e):e)}function ne(e,t,n){var r;if(void 0===n&&1===e.nodeType)if(r="data-"+t.replace(ee,"-$&").toLowerCase(),"string"==typeof(n=e.getAttribute(r))){try{n=te(n)}catch(e){}K.set(e,t,n)}else n=void 0;return n}w.extend({hasData:function(e){return K.hasData(e)||J.hasData(e)},data:function(e,t,n){return K.access(e,t,n)},removeData:function(e,t){K.remove(e,t)},_data:function(e,t,n){return J.access(e,t,n)},_removeData:function(e,t){J.remove(e,t)}}),w.fn.extend({data:function(e,t){var n,r,i,o=this[0],a=o&&o.attributes;if(void 0===e){if(this.length&&(i=K.get(o),1===o.nodeType&&!J.get(o,"hasDataAttrs"))){n=a.length;while(n--)a[n]&&0===(r=a[n].name).indexOf("data-")&&(r=G(r.slice(5)),ne(o,r,i[r]));J.set(o,"hasDataAttrs",!0)}return i}return"object"==typeof e?this.each(function(){K.set(this,e)}):z(this,function(t){var n;if(o&&void 0===t){if(void 0!==(n=K.get(o,e)))return n;if(void 0!==(n=ne(o,e)))return n}else this.each(function(){K.set(this,e,t)})},null,t,arguments.length>1,null,!0)},removeData:function(e){return this.each(function(){K.remove(this,e)})}}),w.extend({queue:function(e,t,n){var r;if(e)return t=(t||"fx")+"queue",r=J.get(e,t),n&&(!r||Array.isArray(n)?r=J.access(e,t,w.makeArray(n)):r.push(n)),r||[]},dequeue:function(e,t){t=t||"fx";var n=w.queue(e,t),r=n.length,i=n.shift(),o=w._queueHooks(e,t),a=function(){w.dequeue(e,t)};"inprogress"===i&&(i=n.shift(),r--),i&&("fx"===t&&n.unshift("inprogress"),delete o.stop,i.call(e,a,o)),!r&&o&&o.empty.fire()},_queueHooks:function(e,t){var n=t+"queueHooks";return J.get(e,n)||J.access(e,n,{empty:w.Callbacks("once memory").add(function(){J.remove(e,[t+"queue",n])})})}}),w.fn.extend({queue:function(e,t){var n=2;return"string"!=typeof e&&(t=e,e="fx",n--),arguments.length<n?w.queue(this[0],e):void 0===t?this:this.each(function(){var n=w.queue(this,e,t);w._queueHooks(this,e),"fx"===e&&"inprogress"!==n[0]&&w.dequeue(this,e)})},dequeue:function(e){return this.each(function(){w.dequeue(this,e)})},clearQueue:function(e){return this.queue(e||"fx",[])},promise:function(e,t){var n,r=1,i=w.Deferred(),o=this,a=this.length,s=function(){--r||i.resolveWith(o,[o])};"string"!=typeof e&&(t=e,e=void 0),e=e||"fx";while(a--)(n=J.get(o[a],e+"queueHooks"))&&n.empty&&(r++,n.empty.add(s));return s(),i.promise(t)}});var re=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,ie=new RegExp("^(?:([+-])=|)("+re+")([a-z%]*)$","i"),oe=["Top","Right","Bottom","Left"],ae=function(e,t){return"none"===(e=t||e).style.display||""===e.style.display&&w.contains(e.ownerDocument,e)&&"none"===w.css(e,"display")},se=function(e,t,n,r){var i,o,a={};for(o in t)a[o]=e.style[o],e.style[o]=t[o];i=n.apply(e,r||[]);for(o in t)e.style[o]=a[o];return i};function ue(e,t,n,r){var i,o,a=20,s=r?function(){return r.cur()}:function(){return w.css(e,t,"")},u=s(),l=n&&n[3]||(w.cssNumber[t]?"":"px"),c=(w.cssNumber[t]||"px"!==l&&+u)&&ie.exec(w.css(e,t));if(c&&c[3]!==l){u/=2,l=l||c[3],c=+u||1;while(a--)w.style(e,t,c+l),(1-o)*(1-(o=s()/u||.5))<=0&&(a=0),c/=o;c*=2,w.style(e,t,c+l),n=n||[]}return n&&(c=+c||+u||0,i=n[1]?c+(n[1]+1)*n[2]:+n[2],r&&(r.unit=l,r.start=c,r.end=i)),i}var le={};function ce(e){var t,n=e.ownerDocument,r=e.nodeName,i=le[r];return i||(t=n.body.appendChild(n.createElement(r)),i=w.css(t,"display"),t.parentNode.removeChild(t),"none"===i&&(i="block"),le[r]=i,i)}function fe(e,t){for(var n,r,i=[],o=0,a=e.length;o<a;o++)(r=e[o]).style&&(n=r.style.display,t?("none"===n&&(i[o]=J.get(r,"display")||null,i[o]||(r.style.display="")),""===r.style.display&&ae(r)&&(i[o]=ce(r))):"none"!==n&&(i[o]="none",J.set(r,"display",n)));for(o=0;o<a;o++)null!=i[o]&&(e[o].style.display=i[o]);return e}w.fn.extend({show:function(){return fe(this,!0)},hide:function(){return fe(this)},toggle:function(e){return"boolean"==typeof e?e?this.show():this.hide():this.each(function(){ae(this)?w(this).show():w(this).hide()})}});var pe=/^(?:checkbox|radio)$/i,de=/<([a-z][^\/\0>\x20\t\r\n\f]+)/i,he=/^$|^module$|\/(?:java|ecma)script/i,ge={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};ge.optgroup=ge.option,ge.tbody=ge.tfoot=ge.colgroup=ge.caption=ge.thead,ge.th=ge.td;function ye(e,t){var n;return n="undefined"!=typeof e.getElementsByTagName?e.getElementsByTagName(t||"*"):"undefined"!=typeof e.querySelectorAll?e.querySelectorAll(t||"*"):[],void 0===t||t&&N(e,t)?w.merge([e],n):n}function ve(e,t){for(var n=0,r=e.length;n<r;n++)J.set(e[n],"globalEval",!t||J.get(t[n],"globalEval"))}var me=/<|&#?\w+;/;function xe(e,t,n,r,i){for(var o,a,s,u,l,c,f=t.createDocumentFragment(),p=[],d=0,h=e.length;d<h;d++)if((o=e[d])||0===o)if("object"===x(o))w.merge(p,o.nodeType?[o]:o);else if(me.test(o)){a=a||f.appendChild(t.createElement("div")),s=(de.exec(o)||["",""])[1].toLowerCase(),u=ge[s]||ge._default,a.innerHTML=u[1]+w.htmlPrefilter(o)+u[2],c=u[0];while(c--)a=a.lastChild;w.merge(p,a.childNodes),(a=f.firstChild).textContent=""}else p.push(t.createTextNode(o));f.textContent="",d=0;while(o=p[d++])if(r&&w.inArray(o,r)>-1)i&&i.push(o);else if(l=w.contains(o.ownerDocument,o),a=ye(f.appendChild(o),"script"),l&&ve(a),n){c=0;while(o=a[c++])he.test(o.type||"")&&n.push(o)}return f}!function(){var e=r.createDocumentFragment().appendChild(r.createElement("div")),t=r.createElement("input");t.setAttribute("type","radio"),t.setAttribute("checked","checked"),t.setAttribute("name","t"),e.appendChild(t),h.checkClone=e.cloneNode(!0).cloneNode(!0).lastChild.checked,e.innerHTML="<textarea>x</textarea>",h.noCloneChecked=!!e.cloneNode(!0).lastChild.defaultValue}();var be=r.documentElement,we=/^key/,Te=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,Ce=/^([^.]*)(?:\.(.+)|)/;function Ee(){return!0}function ke(){return!1}function Se(){try{return r.activeElement}catch(e){}}function De(e,t,n,r,i,o){var a,s;if("object"==typeof t){"string"!=typeof n&&(r=r||n,n=void 0);for(s in t)De(e,s,n,r,t[s],o);return e}if(null==r&&null==i?(i=n,r=n=void 0):null==i&&("string"==typeof n?(i=r,r=void 0):(i=r,r=n,n=void 0)),!1===i)i=ke;else if(!i)return e;return 1===o&&(a=i,(i=function(e){return w().off(e),a.apply(this,arguments)}).guid=a.guid||(a.guid=w.guid++)),e.each(function(){w.event.add(this,t,i,r,n)})}w.event={global:{},add:function(e,t,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,y=J.get(e);if(y){n.handler&&(n=(o=n).handler,i=o.selector),i&&w.find.matchesSelector(be,i),n.guid||(n.guid=w.guid++),(u=y.events)||(u=y.events={}),(a=y.handle)||(a=y.handle=function(t){return"undefined"!=typeof w&&w.event.triggered!==t.type?w.event.dispatch.apply(e,arguments):void 0}),l=(t=(t||"").match(M)||[""]).length;while(l--)d=g=(s=Ce.exec(t[l])||[])[1],h=(s[2]||"").split(".").sort(),d&&(f=w.event.special[d]||{},d=(i?f.delegateType:f.bindType)||d,f=w.event.special[d]||{},c=w.extend({type:d,origType:g,data:r,handler:n,guid:n.guid,selector:i,needsContext:i&&w.expr.match.needsContext.test(i),namespace:h.join(".")},o),(p=u[d])||((p=u[d]=[]).delegateCount=0,f.setup&&!1!==f.setup.call(e,r,h,a)||e.addEventListener&&e.addEventListener(d,a)),f.add&&(f.add.call(e,c),c.handler.guid||(c.handler.guid=n.guid)),i?p.splice(p.delegateCount++,0,c):p.push(c),w.event.global[d]=!0)}},remove:function(e,t,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,y=J.hasData(e)&&J.get(e);if(y&&(u=y.events)){l=(t=(t||"").match(M)||[""]).length;while(l--)if(s=Ce.exec(t[l])||[],d=g=s[1],h=(s[2]||"").split(".").sort(),d){f=w.event.special[d]||{},p=u[d=(r?f.delegateType:f.bindType)||d]||[],s=s[2]&&new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"),a=o=p.length;while(o--)c=p[o],!i&&g!==c.origType||n&&n.guid!==c.guid||s&&!s.test(c.namespace)||r&&r!==c.selector&&("**"!==r||!c.selector)||(p.splice(o,1),c.selector&&p.delegateCount--,f.remove&&f.remove.call(e,c));a&&!p.length&&(f.teardown&&!1!==f.teardown.call(e,h,y.handle)||w.removeEvent(e,d,y.handle),delete u[d])}else for(d in u)w.event.remove(e,d+t[l],n,r,!0);w.isEmptyObject(u)&&J.remove(e,"handle events")}},dispatch:function(e){var t=w.event.fix(e),n,r,i,o,a,s,u=new Array(arguments.length),l=(J.get(this,"events")||{})[t.type]||[],c=w.event.special[t.type]||{};for(u[0]=t,n=1;n<arguments.length;n++)u[n]=arguments[n];if(t.delegateTarget=this,!c.preDispatch||!1!==c.preDispatch.call(this,t)){s=w.event.handlers.call(this,t,l),n=0;while((o=s[n++])&&!t.isPropagationStopped()){t.currentTarget=o.elem,r=0;while((a=o.handlers[r++])&&!t.isImmediatePropagationStopped())t.rnamespace&&!t.rnamespace.test(a.namespace)||(t.handleObj=a,t.data=a.data,void 0!==(i=((w.event.special[a.origType]||{}).handle||a.handler).apply(o.elem,u))&&!1===(t.result=i)&&(t.preventDefault(),t.stopPropagation()))}return c.postDispatch&&c.postDispatch.call(this,t),t.result}},handlers:function(e,t){var n,r,i,o,a,s=[],u=t.delegateCount,l=e.target;if(u&&l.nodeType&&!("click"===e.type&&e.button>=1))for(;l!==this;l=l.parentNode||this)if(1===l.nodeType&&("click"!==e.type||!0!==l.disabled)){for(o=[],a={},n=0;n<u;n++)void 0===a[i=(r=t[n]).selector+" "]&&(a[i]=r.needsContext?w(i,this).index(l)>-1:w.find(i,this,null,[l]).length),a[i]&&o.push(r);o.length&&s.push({elem:l,handlers:o})}return l=this,u<t.length&&s.push({elem:l,handlers:t.slice(u)}),s},addProp:function(e,t){Object.defineProperty(w.Event.prototype,e,{enumerable:!0,configurable:!0,get:g(t)?function(){if(this.originalEvent)return t(this.originalEvent)}:function(){if(this.originalEvent)return this.originalEvent[e]},set:function(t){Object.defineProperty(this,e,{enumerable:!0,configurable:!0,writable:!0,value:t})}})},fix:function(e){return e[w.expando]?e:new w.Event(e)},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==Se()&&this.focus)return this.focus(),!1},delegateType:"focusin"},blur:{trigger:function(){if(this===Se()&&this.blur)return this.blur(),!1},delegateType:"focusout"},click:{trigger:function(){if("checkbox"===this.type&&this.click&&N(this,"input"))return this.click(),!1},_default:function(e){return N(e.target,"a")}},beforeunload:{postDispatch:function(e){void 0!==e.result&&e.originalEvent&&(e.originalEvent.returnValue=e.result)}}}},w.removeEvent=function(e,t,n){e.removeEventListener&&e.removeEventListener(t,n)},w.Event=function(e,t){if(!(this instanceof w.Event))return new w.Event(e,t);e&&e.type?(this.originalEvent=e,this.type=e.type,this.isDefaultPrevented=e.defaultPrevented||void 0===e.defaultPrevented&&!1===e.returnValue?Ee:ke,this.target=e.target&&3===e.target.nodeType?e.target.parentNode:e.target,this.currentTarget=e.currentTarget,this.relatedTarget=e.relatedTarget):this.type=e,t&&w.extend(this,t),this.timeStamp=e&&e.timeStamp||Date.now(),this[w.expando]=!0},w.Event.prototype={constructor:w.Event,isDefaultPrevented:ke,isPropagationStopped:ke,isImmediatePropagationStopped:ke,isSimulated:!1,preventDefault:function(){var e=this.originalEvent;this.isDefaultPrevented=Ee,e&&!this.isSimulated&&e.preventDefault()},stopPropagation:function(){var e=this.originalEvent;this.isPropagationStopped=Ee,e&&!this.isSimulated&&e.stopPropagation()},stopImmediatePropagation:function(){var e=this.originalEvent;this.isImmediatePropagationStopped=Ee,e&&!this.isSimulated&&e.stopImmediatePropagation(),this.stopPropagation()}},w.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,"char":!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:function(e){var t=e.button;return null==e.which&&we.test(e.type)?null!=e.charCode?e.charCode:e.keyCode:!e.which&&void 0!==t&&Te.test(e.type)?1&t?1:2&t?3:4&t?2:0:e.which}},w.event.addProp),w.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(e,t){w.event.special[e]={delegateType:t,bindType:t,handle:function(e){var n,r=this,i=e.relatedTarget,o=e.handleObj;return i&&(i===r||w.contains(r,i))||(e.type=o.origType,n=o.handler.apply(this,arguments),e.type=t),n}}}),w.fn.extend({on:function(e,t,n,r){return De(this,e,t,n,r)},one:function(e,t,n,r){return De(this,e,t,n,r,1)},off:function(e,t,n){var r,i;if(e&&e.preventDefault&&e.handleObj)return r=e.handleObj,w(e.delegateTarget).off(r.namespace?r.origType+"."+r.namespace:r.origType,r.selector,r.handler),this;if("object"==typeof e){for(i in e)this.off(i,t,e[i]);return this}return!1!==t&&"function"!=typeof t||(n=t,t=void 0),!1===n&&(n=ke),this.each(function(){w.event.remove(this,e,n,t)})}});var Ne=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,Ae=/<script|<style|<link/i,je=/checked\s*(?:[^=]|=\s*.checked.)/i,qe=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function Le(e,t){return N(e,"table")&&N(11!==t.nodeType?t:t.firstChild,"tr")?w(e).children("tbody")[0]||e:e}function He(e){return e.type=(null!==e.getAttribute("type"))+"/"+e.type,e}function Oe(e){return"true/"===(e.type||"").slice(0,5)?e.type=e.type.slice(5):e.removeAttribute("type"),e}function Pe(e,t){var n,r,i,o,a,s,u,l;if(1===t.nodeType){if(J.hasData(e)&&(o=J.access(e),a=J.set(t,o),l=o.events)){delete a.handle,a.events={};for(i in l)for(n=0,r=l[i].length;n<r;n++)w.event.add(t,i,l[i][n])}K.hasData(e)&&(s=K.access(e),u=w.extend({},s),K.set(t,u))}}function Me(e,t){var n=t.nodeName.toLowerCase();"input"===n&&pe.test(e.type)?t.checked=e.checked:"input"!==n&&"textarea"!==n||(t.defaultValue=e.defaultValue)}function Re(e,t,n,r){t=a.apply([],t);var i,o,s,u,l,c,f=0,p=e.length,d=p-1,y=t[0],v=g(y);if(v||p>1&&"string"==typeof y&&!h.checkClone&&je.test(y))return e.each(function(i){var o=e.eq(i);v&&(t[0]=y.call(this,i,o.html())),Re(o,t,n,r)});if(p&&(i=xe(t,e[0].ownerDocument,!1,e,r),o=i.firstChild,1===i.childNodes.length&&(i=o),o||r)){for(u=(s=w.map(ye(i,"script"),He)).length;f<p;f++)l=i,f!==d&&(l=w.clone(l,!0,!0),u&&w.merge(s,ye(l,"script"))),n.call(e[f],l,f);if(u)for(c=s[s.length-1].ownerDocument,w.map(s,Oe),f=0;f<u;f++)l=s[f],he.test(l.type||"")&&!J.access(l,"globalEval")&&w.contains(c,l)&&(l.src&&"module"!==(l.type||"").toLowerCase()?w._evalUrl&&w._evalUrl(l.src):m(l.textContent.replace(qe,""),c,l))}return e}function Ie(e,t,n){for(var r,i=t?w.filter(t,e):e,o=0;null!=(r=i[o]);o++)n||1!==r.nodeType||w.cleanData(ye(r)),r.parentNode&&(n&&w.contains(r.ownerDocument,r)&&ve(ye(r,"script")),r.parentNode.removeChild(r));return e}w.extend({htmlPrefilter:function(e){return e.replace(Ne,"<$1></$2>")},clone:function(e,t,n){var r,i,o,a,s=e.cloneNode(!0),u=w.contains(e.ownerDocument,e);if(!(h.noCloneChecked||1!==e.nodeType&&11!==e.nodeType||w.isXMLDoc(e)))for(a=ye(s),r=0,i=(o=ye(e)).length;r<i;r++)Me(o[r],a[r]);if(t)if(n)for(o=o||ye(e),a=a||ye(s),r=0,i=o.length;r<i;r++)Pe(o[r],a[r]);else Pe(e,s);return(a=ye(s,"script")).length>0&&ve(a,!u&&ye(e,"script")),s},cleanData:function(e){for(var t,n,r,i=w.event.special,o=0;void 0!==(n=e[o]);o++)if(Y(n)){if(t=n[J.expando]){if(t.events)for(r in t.events)i[r]?w.event.remove(n,r):w.removeEvent(n,r,t.handle);n[J.expando]=void 0}n[K.expando]&&(n[K.expando]=void 0)}}}),w.fn.extend({detach:function(e){return Ie(this,e,!0)},remove:function(e){return Ie(this,e)},text:function(e){return z(this,function(e){return void 0===e?w.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=e)})},null,e,arguments.length)},append:function(){return Re(this,arguments,function(e){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||Le(this,e).appendChild(e)})},prepend:function(){return Re(this,arguments,function(e){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=Le(this,e);t.insertBefore(e,t.firstChild)}})},before:function(){return Re(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this)})},after:function(){return Re(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this.nextSibling)})},empty:function(){for(var e,t=0;null!=(e=this[t]);t++)1===e.nodeType&&(w.cleanData(ye(e,!1)),e.textContent="");return this},clone:function(e,t){return e=null!=e&&e,t=null==t?e:t,this.map(function(){return w.clone(this,e,t)})},html:function(e){return z(this,function(e){var t=this[0]||{},n=0,r=this.length;if(void 0===e&&1===t.nodeType)return t.innerHTML;if("string"==typeof e&&!Ae.test(e)&&!ge[(de.exec(e)||["",""])[1].toLowerCase()]){e=w.htmlPrefilter(e);try{for(;n<r;n++)1===(t=this[n]||{}).nodeType&&(w.cleanData(ye(t,!1)),t.innerHTML=e);t=0}catch(e){}}t&&this.empty().append(e)},null,e,arguments.length)},replaceWith:function(){var e=[];return Re(this,arguments,function(t){var n=this.parentNode;w.inArray(this,e)<0&&(w.cleanData(ye(this)),n&&n.replaceChild(t,this))},e)}}),w.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(e,t){w.fn[e]=function(e){for(var n,r=[],i=w(e),o=i.length-1,a=0;a<=o;a++)n=a===o?this:this.clone(!0),w(i[a])[t](n),s.apply(r,n.get());return this.pushStack(r)}});var We=new RegExp("^("+re+")(?!px)[a-z%]+$","i"),$e=function(t){var n=t.ownerDocument.defaultView;return n&&n.opener||(n=e),n.getComputedStyle(t)},Be=new RegExp(oe.join("|"),"i");!function(){function t(){if(c){l.style.cssText="position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0",c.style.cssText="position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%",be.appendChild(l).appendChild(c);var t=e.getComputedStyle(c);i="1%"!==t.top,u=12===n(t.marginLeft),c.style.right="60%",s=36===n(t.right),o=36===n(t.width),c.style.position="absolute",a=36===c.offsetWidth||"absolute",be.removeChild(l),c=null}}function n(e){return Math.round(parseFloat(e))}var i,o,a,s,u,l=r.createElement("div"),c=r.createElement("div");c.style&&(c.style.backgroundClip="content-box",c.cloneNode(!0).style.backgroundClip="",h.clearCloneStyle="content-box"===c.style.backgroundClip,w.extend(h,{boxSizingReliable:function(){return t(),o},pixelBoxStyles:function(){return t(),s},pixelPosition:function(){return t(),i},reliableMarginLeft:function(){return t(),u},scrollboxSize:function(){return t(),a}}))}();function Fe(e,t,n){var r,i,o,a,s=e.style;return(n=n||$e(e))&&(""!==(a=n.getPropertyValue(t)||n[t])||w.contains(e.ownerDocument,e)||(a=w.style(e,t)),!h.pixelBoxStyles()&&We.test(a)&&Be.test(t)&&(r=s.width,i=s.minWidth,o=s.maxWidth,s.minWidth=s.maxWidth=s.width=a,a=n.width,s.width=r,s.minWidth=i,s.maxWidth=o)),void 0!==a?a+"":a}function _e(e,t){return{get:function(){if(!e())return(this.get=t).apply(this,arguments);delete this.get}}}var ze=/^(none|table(?!-c[ea]).+)/,Xe=/^--/,Ue={position:"absolute",visibility:"hidden",display:"block"},Ve={letterSpacing:"0",fontWeight:"400"},Ge=["Webkit","Moz","ms"],Ye=r.createElement("div").style;function Qe(e){if(e in Ye)return e;var t=e[0].toUpperCase()+e.slice(1),n=Ge.length;while(n--)if((e=Ge[n]+t)in Ye)return e}function Je(e){var t=w.cssProps[e];return t||(t=w.cssProps[e]=Qe(e)||e),t}function Ke(e,t,n){var r=ie.exec(t);return r?Math.max(0,r[2]-(n||0))+(r[3]||"px"):t}function Ze(e,t,n,r,i,o){var a="width"===t?1:0,s=0,u=0;if(n===(r?"border":"content"))return 0;for(;a<4;a+=2)"margin"===n&&(u+=w.css(e,n+oe[a],!0,i)),r?("content"===n&&(u-=w.css(e,"padding"+oe[a],!0,i)),"margin"!==n&&(u-=w.css(e,"border"+oe[a]+"Width",!0,i))):(u+=w.css(e,"padding"+oe[a],!0,i),"padding"!==n?u+=w.css(e,"border"+oe[a]+"Width",!0,i):s+=w.css(e,"border"+oe[a]+"Width",!0,i));return!r&&o>=0&&(u+=Math.max(0,Math.ceil(e["offset"+t[0].toUpperCase()+t.slice(1)]-o-u-s-.5))),u}function et(e,t,n){var r=$e(e),i=Fe(e,t,r),o="border-box"===w.css(e,"boxSizing",!1,r),a=o;if(We.test(i)){if(!n)return i;i="auto"}return a=a&&(h.boxSizingReliable()||i===e.style[t]),("auto"===i||!parseFloat(i)&&"inline"===w.css(e,"display",!1,r))&&(i=e["offset"+t[0].toUpperCase()+t.slice(1)],a=!0),(i=parseFloat(i)||0)+Ze(e,t,n||(o?"border":"content"),a,r,i)+"px"}w.extend({cssHooks:{opacity:{get:function(e,t){if(t){var n=Fe(e,"opacity");return""===n?"1":n}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{},style:function(e,t,n,r){if(e&&3!==e.nodeType&&8!==e.nodeType&&e.style){var i,o,a,s=G(t),u=Xe.test(t),l=e.style;if(u||(t=Je(s)),a=w.cssHooks[t]||w.cssHooks[s],void 0===n)return a&&"get"in a&&void 0!==(i=a.get(e,!1,r))?i:l[t];"string"==(o=typeof n)&&(i=ie.exec(n))&&i[1]&&(n=ue(e,t,i),o="number"),null!=n&&n===n&&("number"===o&&(n+=i&&i[3]||(w.cssNumber[s]?"":"px")),h.clearCloneStyle||""!==n||0!==t.indexOf("background")||(l[t]="inherit"),a&&"set"in a&&void 0===(n=a.set(e,n,r))||(u?l.setProperty(t,n):l[t]=n))}},css:function(e,t,n,r){var i,o,a,s=G(t);return Xe.test(t)||(t=Je(s)),(a=w.cssHooks[t]||w.cssHooks[s])&&"get"in a&&(i=a.get(e,!0,n)),void 0===i&&(i=Fe(e,t,r)),"normal"===i&&t in Ve&&(i=Ve[t]),""===n||n?(o=parseFloat(i),!0===n||isFinite(o)?o||0:i):i}}),w.each(["height","width"],function(e,t){w.cssHooks[t]={get:function(e,n,r){if(n)return!ze.test(w.css(e,"display"))||e.getClientRects().length&&e.getBoundingClientRect().width?et(e,t,r):se(e,Ue,function(){return et(e,t,r)})},set:function(e,n,r){var i,o=$e(e),a="border-box"===w.css(e,"boxSizing",!1,o),s=r&&Ze(e,t,r,a,o);return a&&h.scrollboxSize()===o.position&&(s-=Math.ceil(e["offset"+t[0].toUpperCase()+t.slice(1)]-parseFloat(o[t])-Ze(e,t,"border",!1,o)-.5)),s&&(i=ie.exec(n))&&"px"!==(i[3]||"px")&&(e.style[t]=n,n=w.css(e,t)),Ke(e,n,s)}}}),w.cssHooks.marginLeft=_e(h.reliableMarginLeft,function(e,t){if(t)return(parseFloat(Fe(e,"marginLeft"))||e.getBoundingClientRect().left-se(e,{marginLeft:0},function(){return e.getBoundingClientRect().left}))+"px"}),w.each({margin:"",padding:"",border:"Width"},function(e,t){w.cssHooks[e+t]={expand:function(n){for(var r=0,i={},o="string"==typeof n?n.split(" "):[n];r<4;r++)i[e+oe[r]+t]=o[r]||o[r-2]||o[0];return i}},"margin"!==e&&(w.cssHooks[e+t].set=Ke)}),w.fn.extend({css:function(e,t){return z(this,function(e,t,n){var r,i,o={},a=0;if(Array.isArray(t)){for(r=$e(e),i=t.length;a<i;a++)o[t[a]]=w.css(e,t[a],!1,r);return o}return void 0!==n?w.style(e,t,n):w.css(e,t)},e,t,arguments.length>1)}});function tt(e,t,n,r,i){return new tt.prototype.init(e,t,n,r,i)}w.Tween=tt,tt.prototype={constructor:tt,init:function(e,t,n,r,i,o){this.elem=e,this.prop=n,this.easing=i||w.easing._default,this.options=t,this.start=this.now=this.cur(),this.end=r,this.unit=o||(w.cssNumber[n]?"":"px")},cur:function(){var e=tt.propHooks[this.prop];return e&&e.get?e.get(this):tt.propHooks._default.get(this)},run:function(e){var t,n=tt.propHooks[this.prop];return this.options.duration?this.pos=t=w.easing[this.easing](e,this.options.duration*e,0,1,this.options.duration):this.pos=t=e,this.now=(this.end-this.start)*t+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),n&&n.set?n.set(this):tt.propHooks._default.set(this),this}},tt.prototype.init.prototype=tt.prototype,tt.propHooks={_default:{get:function(e){var t;return 1!==e.elem.nodeType||null!=e.elem[e.prop]&&null==e.elem.style[e.prop]?e.elem[e.prop]:(t=w.css(e.elem,e.prop,""))&&"auto"!==t?t:0},set:function(e){w.fx.step[e.prop]?w.fx.step[e.prop](e):1!==e.elem.nodeType||null==e.elem.style[w.cssProps[e.prop]]&&!w.cssHooks[e.prop]?e.elem[e.prop]=e.now:w.style(e.elem,e.prop,e.now+e.unit)}}},tt.propHooks.scrollTop=tt.propHooks.scrollLeft={set:function(e){e.elem.nodeType&&e.elem.parentNode&&(e.elem[e.prop]=e.now)}},w.easing={linear:function(e){return e},swing:function(e){return.5-Math.cos(e*Math.PI)/2},_default:"swing"},w.fx=tt.prototype.init,w.fx.step={};var nt,rt,it=/^(?:toggle|show|hide)$/,ot=/queueHooks$/;function at(){rt&&(!1===r.hidden&&e.requestAnimationFrame?e.requestAnimationFrame(at):e.setTimeout(at,w.fx.interval),w.fx.tick())}function st(){return e.setTimeout(function(){nt=void 0}),nt=Date.now()}function ut(e,t){var n,r=0,i={height:e};for(t=t?1:0;r<4;r+=2-t)i["margin"+(n=oe[r])]=i["padding"+n]=e;return t&&(i.opacity=i.width=e),i}function lt(e,t,n){for(var r,i=(pt.tweeners[t]||[]).concat(pt.tweeners["*"]),o=0,a=i.length;o<a;o++)if(r=i[o].call(n,t,e))return r}function ct(e,t,n){var r,i,o,a,s,u,l,c,f="width"in t||"height"in t,p=this,d={},h=e.style,g=e.nodeType&&ae(e),y=J.get(e,"fxshow");n.queue||(null==(a=w._queueHooks(e,"fx")).unqueued&&(a.unqueued=0,s=a.empty.fire,a.empty.fire=function(){a.unqueued||s()}),a.unqueued++,p.always(function(){p.always(function(){a.unqueued--,w.queue(e,"fx").length||a.empty.fire()})}));for(r in t)if(i=t[r],it.test(i)){if(delete t[r],o=o||"toggle"===i,i===(g?"hide":"show")){if("show"!==i||!y||void 0===y[r])continue;g=!0}d[r]=y&&y[r]||w.style(e,r)}if((u=!w.isEmptyObject(t))||!w.isEmptyObject(d)){f&&1===e.nodeType&&(n.overflow=[h.overflow,h.overflowX,h.overflowY],null==(l=y&&y.display)&&(l=J.get(e,"display")),"none"===(c=w.css(e,"display"))&&(l?c=l:(fe([e],!0),l=e.style.display||l,c=w.css(e,"display"),fe([e]))),("inline"===c||"inline-block"===c&&null!=l)&&"none"===w.css(e,"float")&&(u||(p.done(function(){h.display=l}),null==l&&(c=h.display,l="none"===c?"":c)),h.display="inline-block")),n.overflow&&(h.overflow="hidden",p.always(function(){h.overflow=n.overflow[0],h.overflowX=n.overflow[1],h.overflowY=n.overflow[2]})),u=!1;for(r in d)u||(y?"hidden"in y&&(g=y.hidden):y=J.access(e,"fxshow",{display:l}),o&&(y.hidden=!g),g&&fe([e],!0),p.done(function(){g||fe([e]),J.remove(e,"fxshow");for(r in d)w.style(e,r,d[r])})),u=lt(g?y[r]:0,r,p),r in y||(y[r]=u.start,g&&(u.end=u.start,u.start=0))}}function ft(e,t){var n,r,i,o,a;for(n in e)if(r=G(n),i=t[r],o=e[n],Array.isArray(o)&&(i=o[1],o=e[n]=o[0]),n!==r&&(e[r]=o,delete e[n]),(a=w.cssHooks[r])&&"expand"in a){o=a.expand(o),delete e[r];for(n in o)n in e||(e[n]=o[n],t[n]=i)}else t[r]=i}function pt(e,t,n){var r,i,o=0,a=pt.prefilters.length,s=w.Deferred().always(function(){delete u.elem}),u=function(){if(i)return!1;for(var t=nt||st(),n=Math.max(0,l.startTime+l.duration-t),r=1-(n/l.duration||0),o=0,a=l.tweens.length;o<a;o++)l.tweens[o].run(r);return s.notifyWith(e,[l,r,n]),r<1&&a?n:(a||s.notifyWith(e,[l,1,0]),s.resolveWith(e,[l]),!1)},l=s.promise({elem:e,props:w.extend({},t),opts:w.extend(!0,{specialEasing:{},easing:w.easing._default},n),originalProperties:t,originalOptions:n,startTime:nt||st(),duration:n.duration,tweens:[],createTween:function(t,n){var r=w.Tween(e,l.opts,t,n,l.opts.specialEasing[t]||l.opts.easing);return l.tweens.push(r),r},stop:function(t){var n=0,r=t?l.tweens.length:0;if(i)return this;for(i=!0;n<r;n++)l.tweens[n].run(1);return t?(s.notifyWith(e,[l,1,0]),s.resolveWith(e,[l,t])):s.rejectWith(e,[l,t]),this}}),c=l.props;for(ft(c,l.opts.specialEasing);o<a;o++)if(r=pt.prefilters[o].call(l,e,c,l.opts))return g(r.stop)&&(w._queueHooks(l.elem,l.opts.queue).stop=r.stop.bind(r)),r;return w.map(c,lt,l),g(l.opts.start)&&l.opts.start.call(e,l),l.progress(l.opts.progress).done(l.opts.done,l.opts.complete).fail(l.opts.fail).always(l.opts.always),w.fx.timer(w.extend(u,{elem:e,anim:l,queue:l.opts.queue})),l}w.Animation=w.extend(pt,{tweeners:{"*":[function(e,t){var n=this.createTween(e,t);return ue(n.elem,e,ie.exec(t),n),n}]},tweener:function(e,t){g(e)?(t=e,e=["*"]):e=e.match(M);for(var n,r=0,i=e.length;r<i;r++)n=e[r],pt.tweeners[n]=pt.tweeners[n]||[],pt.tweeners[n].unshift(t)},prefilters:[ct],prefilter:function(e,t){t?pt.prefilters.unshift(e):pt.prefilters.push(e)}}),w.speed=function(e,t,n){var r=e&&"object"==typeof e?w.extend({},e):{complete:n||!n&&t||g(e)&&e,duration:e,easing:n&&t||t&&!g(t)&&t};return w.fx.off?r.duration=0:"number"!=typeof r.duration&&(r.duration in w.fx.speeds?r.duration=w.fx.speeds[r.duration]:r.duration=w.fx.speeds._default),null!=r.queue&&!0!==r.queue||(r.queue="fx"),r.old=r.complete,r.complete=function(){g(r.old)&&r.old.call(this),r.queue&&w.dequeue(this,r.queue)},r},w.fn.extend({fadeTo:function(e,t,n,r){return this.filter(ae).css("opacity",0).show().end().animate({opacity:t},e,n,r)},animate:function(e,t,n,r){var i=w.isEmptyObject(e),o=w.speed(t,n,r),a=function(){var t=pt(this,w.extend({},e),o);(i||J.get(this,"finish"))&&t.stop(!0)};return a.finish=a,i||!1===o.queue?this.each(a):this.queue(o.queue,a)},stop:function(e,t,n){var r=function(e){var t=e.stop;delete e.stop,t(n)};return"string"!=typeof e&&(n=t,t=e,e=void 0),t&&!1!==e&&this.queue(e||"fx",[]),this.each(function(){var t=!0,i=null!=e&&e+"queueHooks",o=w.timers,a=J.get(this);if(i)a[i]&&a[i].stop&&r(a[i]);else for(i in a)a[i]&&a[i].stop&&ot.test(i)&&r(a[i]);for(i=o.length;i--;)o[i].elem!==this||null!=e&&o[i].queue!==e||(o[i].anim.stop(n),t=!1,o.splice(i,1));!t&&n||w.dequeue(this,e)})},finish:function(e){return!1!==e&&(e=e||"fx"),this.each(function(){var t,n=J.get(this),r=n[e+"queue"],i=n[e+"queueHooks"],o=w.timers,a=r?r.length:0;for(n.finish=!0,w.queue(this,e,[]),i&&i.stop&&i.stop.call(this,!0),t=o.length;t--;)o[t].elem===this&&o[t].queue===e&&(o[t].anim.stop(!0),o.splice(t,1));for(t=0;t<a;t++)r[t]&&r[t].finish&&r[t].finish.call(this);delete n.finish})}}),w.each(["toggle","show","hide"],function(e,t){var n=w.fn[t];w.fn[t]=function(e,r,i){return null==e||"boolean"==typeof e?n.apply(this,arguments):this.animate(ut(t,!0),e,r,i)}}),w.each({slideDown:ut("show"),slideUp:ut("hide"),slideToggle:ut("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(e,t){w.fn[e]=function(e,n,r){return this.animate(t,e,n,r)}}),w.timers=[],w.fx.tick=function(){var e,t=0,n=w.timers;for(nt=Date.now();t<n.length;t++)(e=n[t])()||n[t]!==e||n.splice(t--,1);n.length||w.fx.stop(),nt=void 0},w.fx.timer=function(e){w.timers.push(e),w.fx.start()},w.fx.interval=13,w.fx.start=function(){rt||(rt=!0,at())},w.fx.stop=function(){rt=null},w.fx.speeds={slow:600,fast:200,_default:400},w.fn.delay=function(t,n){return t=w.fx?w.fx.speeds[t]||t:t,n=n||"fx",this.queue(n,function(n,r){var i=e.setTimeout(n,t);r.stop=function(){e.clearTimeout(i)}})},function(){var e=r.createElement("input"),t=r.createElement("select").appendChild(r.createElement("option"));e.type="checkbox",h.checkOn=""!==e.value,h.optSelected=t.selected,(e=r.createElement("input")).value="t",e.type="radio",h.radioValue="t"===e.value}();var dt,ht=w.expr.attrHandle;w.fn.extend({attr:function(e,t){return z(this,w.attr,e,t,arguments.length>1)},removeAttr:function(e){return this.each(function(){w.removeAttr(this,e)})}}),w.extend({attr:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return"undefined"==typeof e.getAttribute?w.prop(e,t,n):(1===o&&w.isXMLDoc(e)||(i=w.attrHooks[t.toLowerCase()]||(w.expr.match.bool.test(t)?dt:void 0)),void 0!==n?null===n?void w.removeAttr(e,t):i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:(e.setAttribute(t,n+""),n):i&&"get"in i&&null!==(r=i.get(e,t))?r:null==(r=w.find.attr(e,t))?void 0:r)},attrHooks:{type:{set:function(e,t){if(!h.radioValue&&"radio"===t&&N(e,"input")){var n=e.value;return e.setAttribute("type",t),n&&(e.value=n),t}}}},removeAttr:function(e,t){var n,r=0,i=t&&t.match(M);if(i&&1===e.nodeType)while(n=i[r++])e.removeAttribute(n)}}),dt={set:function(e,t,n){return!1===t?w.removeAttr(e,n):e.setAttribute(n,n),n}},w.each(w.expr.match.bool.source.match(/\w+/g),function(e,t){var n=ht[t]||w.find.attr;ht[t]=function(e,t,r){var i,o,a=t.toLowerCase();return r||(o=ht[a],ht[a]=i,i=null!=n(e,t,r)?a:null,ht[a]=o),i}});var gt=/^(?:input|select|textarea|button)$/i,yt=/^(?:a|area)$/i;w.fn.extend({prop:function(e,t){return z(this,w.prop,e,t,arguments.length>1)},removeProp:function(e){return this.each(function(){delete this[w.propFix[e]||e]})}}),w.extend({prop:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return 1===o&&w.isXMLDoc(e)||(t=w.propFix[t]||t,i=w.propHooks[t]),void 0!==n?i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:e[t]=n:i&&"get"in i&&null!==(r=i.get(e,t))?r:e[t]},propHooks:{tabIndex:{get:function(e){var t=w.find.attr(e,"tabindex");return t?parseInt(t,10):gt.test(e.nodeName)||yt.test(e.nodeName)&&e.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),h.optSelected||(w.propHooks.selected={get:function(e){var t=e.parentNode;return t&&t.parentNode&&t.parentNode.selectedIndex,null},set:function(e){var t=e.parentNode;t&&(t.selectedIndex,t.parentNode&&t.parentNode.selectedIndex)}}),w.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){w.propFix[this.toLowerCase()]=this});function vt(e){return(e.match(M)||[]).join(" ")}function mt(e){return e.getAttribute&&e.getAttribute("class")||""}function xt(e){return Array.isArray(e)?e:"string"==typeof e?e.match(M)||[]:[]}w.fn.extend({addClass:function(e){var t,n,r,i,o,a,s,u=0;if(g(e))return this.each(function(t){w(this).addClass(e.call(this,t,mt(this)))});if((t=xt(e)).length)while(n=this[u++])if(i=mt(n),r=1===n.nodeType&&" "+vt(i)+" "){a=0;while(o=t[a++])r.indexOf(" "+o+" ")<0&&(r+=o+" ");i!==(s=vt(r))&&n.setAttribute("class",s)}return this},removeClass:function(e){var t,n,r,i,o,a,s,u=0;if(g(e))return this.each(function(t){w(this).removeClass(e.call(this,t,mt(this)))});if(!arguments.length)return this.attr("class","");if((t=xt(e)).length)while(n=this[u++])if(i=mt(n),r=1===n.nodeType&&" "+vt(i)+" "){a=0;while(o=t[a++])while(r.indexOf(" "+o+" ")>-1)r=r.replace(" "+o+" "," ");i!==(s=vt(r))&&n.setAttribute("class",s)}return this},toggleClass:function(e,t){var n=typeof e,r="string"===n||Array.isArray(e);return"boolean"==typeof t&&r?t?this.addClass(e):this.removeClass(e):g(e)?this.each(function(n){w(this).toggleClass(e.call(this,n,mt(this),t),t)}):this.each(function(){var t,i,o,a;if(r){i=0,o=w(this),a=xt(e);while(t=a[i++])o.hasClass(t)?o.removeClass(t):o.addClass(t)}else void 0!==e&&"boolean"!==n||((t=mt(this))&&J.set(this,"__className__",t),this.setAttribute&&this.setAttribute("class",t||!1===e?"":J.get(this,"__className__")||""))})},hasClass:function(e){var t,n,r=0;t=" "+e+" ";while(n=this[r++])if(1===n.nodeType&&(" "+vt(mt(n))+" ").indexOf(t)>-1)return!0;return!1}});var bt=/\r/g;w.fn.extend({val:function(e){var t,n,r,i=this[0];{if(arguments.length)return r=g(e),this.each(function(n){var i;1===this.nodeType&&(null==(i=r?e.call(this,n,w(this).val()):e)?i="":"number"==typeof i?i+="":Array.isArray(i)&&(i=w.map(i,function(e){return null==e?"":e+""})),(t=w.valHooks[this.type]||w.valHooks[this.nodeName.toLowerCase()])&&"set"in t&&void 0!==t.set(this,i,"value")||(this.value=i))});if(i)return(t=w.valHooks[i.type]||w.valHooks[i.nodeName.toLowerCase()])&&"get"in t&&void 0!==(n=t.get(i,"value"))?n:"string"==typeof(n=i.value)?n.replace(bt,""):null==n?"":n}}}),w.extend({valHooks:{option:{get:function(e){var t=w.find.attr(e,"value");return null!=t?t:vt(w.text(e))}},select:{get:function(e){var t,n,r,i=e.options,o=e.selectedIndex,a="select-one"===e.type,s=a?null:[],u=a?o+1:i.length;for(r=o<0?u:a?o:0;r<u;r++)if(((n=i[r]).selected||r===o)&&!n.disabled&&(!n.parentNode.disabled||!N(n.parentNode,"optgroup"))){if(t=w(n).val(),a)return t;s.push(t)}return s},set:function(e,t){var n,r,i=e.options,o=w.makeArray(t),a=i.length;while(a--)((r=i[a]).selected=w.inArray(w.valHooks.option.get(r),o)>-1)&&(n=!0);return n||(e.selectedIndex=-1),o}}}}),w.each(["radio","checkbox"],function(){w.valHooks[this]={set:function(e,t){if(Array.isArray(t))return e.checked=w.inArray(w(e).val(),t)>-1}},h.checkOn||(w.valHooks[this].get=function(e){return null===e.getAttribute("value")?"on":e.value})}),h.focusin="onfocusin"in e;var wt=/^(?:focusinfocus|focusoutblur)$/,Tt=function(e){e.stopPropagation()};w.extend(w.event,{trigger:function(t,n,i,o){var a,s,u,l,c,p,d,h,v=[i||r],m=f.call(t,"type")?t.type:t,x=f.call(t,"namespace")?t.namespace.split("."):[];if(s=h=u=i=i||r,3!==i.nodeType&&8!==i.nodeType&&!wt.test(m+w.event.triggered)&&(m.indexOf(".")>-1&&(m=(x=m.split(".")).shift(),x.sort()),c=m.indexOf(":")<0&&"on"+m,t=t[w.expando]?t:new w.Event(m,"object"==typeof t&&t),t.isTrigger=o?2:3,t.namespace=x.join("."),t.rnamespace=t.namespace?new RegExp("(^|\\.)"+x.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,t.result=void 0,t.target||(t.target=i),n=null==n?[t]:w.makeArray(n,[t]),d=w.event.special[m]||{},o||!d.trigger||!1!==d.trigger.apply(i,n))){if(!o&&!d.noBubble&&!y(i)){for(l=d.delegateType||m,wt.test(l+m)||(s=s.parentNode);s;s=s.parentNode)v.push(s),u=s;u===(i.ownerDocument||r)&&v.push(u.defaultView||u.parentWindow||e)}a=0;while((s=v[a++])&&!t.isPropagationStopped())h=s,t.type=a>1?l:d.bindType||m,(p=(J.get(s,"events")||{})[t.type]&&J.get(s,"handle"))&&p.apply(s,n),(p=c&&s[c])&&p.apply&&Y(s)&&(t.result=p.apply(s,n),!1===t.result&&t.preventDefault());return t.type=m,o||t.isDefaultPrevented()||d._default&&!1!==d._default.apply(v.pop(),n)||!Y(i)||c&&g(i[m])&&!y(i)&&((u=i[c])&&(i[c]=null),w.event.triggered=m,t.isPropagationStopped()&&h.addEventListener(m,Tt),i[m](),t.isPropagationStopped()&&h.removeEventListener(m,Tt),w.event.triggered=void 0,u&&(i[c]=u)),t.result}},simulate:function(e,t,n){var r=w.extend(new w.Event,n,{type:e,isSimulated:!0});w.event.trigger(r,null,t)}}),w.fn.extend({trigger:function(e,t){return this.each(function(){w.event.trigger(e,t,this)})},triggerHandler:function(e,t){var n=this[0];if(n)return w.event.trigger(e,t,n,!0)}}),h.focusin||w.each({focus:"focusin",blur:"focusout"},function(e,t){var n=function(e){w.event.simulate(t,e.target,w.event.fix(e))};w.event.special[t]={setup:function(){var r=this.ownerDocument||this,i=J.access(r,t);i||r.addEventListener(e,n,!0),J.access(r,t,(i||0)+1)},teardown:function(){var r=this.ownerDocument||this,i=J.access(r,t)-1;i?J.access(r,t,i):(r.removeEventListener(e,n,!0),J.remove(r,t))}}});var Ct=e.location,Et=Date.now(),kt=/\?/;w.parseXML=function(t){var n;if(!t||"string"!=typeof t)return null;try{n=(new e.DOMParser).parseFromString(t,"text/xml")}catch(e){n=void 0}return n&&!n.getElementsByTagName("parsererror").length||w.error("Invalid XML: "+t),n};var St=/\[\]$/,Dt=/\r?\n/g,Nt=/^(?:submit|button|image|reset|file)$/i,At=/^(?:input|select|textarea|keygen)/i;function jt(e,t,n,r){var i;if(Array.isArray(t))w.each(t,function(t,i){n||St.test(e)?r(e,i):jt(e+"["+("object"==typeof i&&null!=i?t:"")+"]",i,n,r)});else if(n||"object"!==x(t))r(e,t);else for(i in t)jt(e+"["+i+"]",t[i],n,r)}w.param=function(e,t){var n,r=[],i=function(e,t){var n=g(t)?t():t;r[r.length]=encodeURIComponent(e)+"="+encodeURIComponent(null==n?"":n)};if(Array.isArray(e)||e.jquery&&!w.isPlainObject(e))w.each(e,function(){i(this.name,this.value)});else for(n in e)jt(n,e[n],t,i);return r.join("&")},w.fn.extend({serialize:function(){return w.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var e=w.prop(this,"elements");return e?w.makeArray(e):this}).filter(function(){var e=this.type;return this.name&&!w(this).is(":disabled")&&At.test(this.nodeName)&&!Nt.test(e)&&(this.checked||!pe.test(e))}).map(function(e,t){var n=w(this).val();return null==n?null:Array.isArray(n)?w.map(n,function(e){return{name:t.name,value:e.replace(Dt,"\r\n")}}):{name:t.name,value:n.replace(Dt,"\r\n")}}).get()}});var qt=/%20/g,Lt=/#.*$/,Ht=/([?&])_=[^&]*/,Ot=/^(.*?):[ \t]*([^\r\n]*)$/gm,Pt=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Mt=/^(?:GET|HEAD)$/,Rt=/^\/\//,It={},Wt={},$t="*/".concat("*"),Bt=r.createElement("a");Bt.href=Ct.href;function Ft(e){return function(t,n){"string"!=typeof t&&(n=t,t="*");var r,i=0,o=t.toLowerCase().match(M)||[];if(g(n))while(r=o[i++])"+"===r[0]?(r=r.slice(1)||"*",(e[r]=e[r]||[]).unshift(n)):(e[r]=e[r]||[]).push(n)}}function _t(e,t,n,r){var i={},o=e===Wt;function a(s){var u;return i[s]=!0,w.each(e[s]||[],function(e,s){var l=s(t,n,r);return"string"!=typeof l||o||i[l]?o?!(u=l):void 0:(t.dataTypes.unshift(l),a(l),!1)}),u}return a(t.dataTypes[0])||!i["*"]&&a("*")}function zt(e,t){var n,r,i=w.ajaxSettings.flatOptions||{};for(n in t)void 0!==t[n]&&((i[n]?e:r||(r={}))[n]=t[n]);return r&&w.extend(!0,e,r),e}function Xt(e,t,n){var r,i,o,a,s=e.contents,u=e.dataTypes;while("*"===u[0])u.shift(),void 0===r&&(r=e.mimeType||t.getResponseHeader("Content-Type"));if(r)for(i in s)if(s[i]&&s[i].test(r)){u.unshift(i);break}if(u[0]in n)o=u[0];else{for(i in n){if(!u[0]||e.converters[i+" "+u[0]]){o=i;break}a||(a=i)}o=o||a}if(o)return o!==u[0]&&u.unshift(o),n[o]}function Ut(e,t,n,r){var i,o,a,s,u,l={},c=e.dataTypes.slice();if(c[1])for(a in e.converters)l[a.toLowerCase()]=e.converters[a];o=c.shift();while(o)if(e.responseFields[o]&&(n[e.responseFields[o]]=t),!u&&r&&e.dataFilter&&(t=e.dataFilter(t,e.dataType)),u=o,o=c.shift())if("*"===o)o=u;else if("*"!==u&&u!==o){if(!(a=l[u+" "+o]||l["* "+o]))for(i in l)if((s=i.split(" "))[1]===o&&(a=l[u+" "+s[0]]||l["* "+s[0]])){!0===a?a=l[i]:!0!==l[i]&&(o=s[0],c.unshift(s[1]));break}if(!0!==a)if(a&&e["throws"])t=a(t);else try{t=a(t)}catch(e){return{state:"parsererror",error:a?e:"No conversion from "+u+" to "+o}}}return{state:"success",data:t}}w.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Ct.href,type:"GET",isLocal:Pt.test(Ct.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":$t,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":JSON.parse,"text xml":w.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(e,t){return t?zt(zt(e,w.ajaxSettings),t):zt(w.ajaxSettings,e)},ajaxPrefilter:Ft(It),ajaxTransport:Ft(Wt),ajax:function(t,n){"object"==typeof t&&(n=t,t=void 0),n=n||{};var i,o,a,s,u,l,c,f,p,d,h=w.ajaxSetup({},n),g=h.context||h,y=h.context&&(g.nodeType||g.jquery)?w(g):w.event,v=w.Deferred(),m=w.Callbacks("once memory"),x=h.statusCode||{},b={},T={},C="canceled",E={readyState:0,getResponseHeader:function(e){var t;if(c){if(!s){s={};while(t=Ot.exec(a))s[t[1].toLowerCase()]=t[2]}t=s[e.toLowerCase()]}return null==t?null:t},getAllResponseHeaders:function(){return c?a:null},setRequestHeader:function(e,t){return null==c&&(e=T[e.toLowerCase()]=T[e.toLowerCase()]||e,b[e]=t),this},overrideMimeType:function(e){return null==c&&(h.mimeType=e),this},statusCode:function(e){var t;if(e)if(c)E.always(e[E.status]);else for(t in e)x[t]=[x[t],e[t]];return this},abort:function(e){var t=e||C;return i&&i.abort(t),k(0,t),this}};if(v.promise(E),h.url=((t||h.url||Ct.href)+"").replace(Rt,Ct.protocol+"//"),h.type=n.method||n.type||h.method||h.type,h.dataTypes=(h.dataType||"*").toLowerCase().match(M)||[""],null==h.crossDomain){l=r.createElement("a");try{l.href=h.url,l.href=l.href,h.crossDomain=Bt.protocol+"//"+Bt.host!=l.protocol+"//"+l.host}catch(e){h.crossDomain=!0}}if(h.data&&h.processData&&"string"!=typeof h.data&&(h.data=w.param(h.data,h.traditional)),_t(It,h,n,E),c)return E;(f=w.event&&h.global)&&0==w.active++&&w.event.trigger("ajaxStart"),h.type=h.type.toUpperCase(),h.hasContent=!Mt.test(h.type),o=h.url.replace(Lt,""),h.hasContent?h.data&&h.processData&&0===(h.contentType||"").indexOf("application/x-www-form-urlencoded")&&(h.data=h.data.replace(qt,"+")):(d=h.url.slice(o.length),h.data&&(h.processData||"string"==typeof h.data)&&(o+=(kt.test(o)?"&":"?")+h.data,delete h.data),!1===h.cache&&(o=o.replace(Ht,"$1"),d=(kt.test(o)?"&":"?")+"_="+Et+++d),h.url=o+d),h.ifModified&&(w.lastModified[o]&&E.setRequestHeader("If-Modified-Since",w.lastModified[o]),w.etag[o]&&E.setRequestHeader("If-None-Match",w.etag[o])),(h.data&&h.hasContent&&!1!==h.contentType||n.contentType)&&E.setRequestHeader("Content-Type",h.contentType),E.setRequestHeader("Accept",h.dataTypes[0]&&h.accepts[h.dataTypes[0]]?h.accepts[h.dataTypes[0]]+("*"!==h.dataTypes[0]?", "+$t+"; q=0.01":""):h.accepts["*"]);for(p in h.headers)E.setRequestHeader(p,h.headers[p]);if(h.beforeSend&&(!1===h.beforeSend.call(g,E,h)||c))return E.abort();if(C="abort",m.add(h.complete),E.done(h.success),E.fail(h.error),i=_t(Wt,h,n,E)){if(E.readyState=1,f&&y.trigger("ajaxSend",[E,h]),c)return E;h.async&&h.timeout>0&&(u=e.setTimeout(function(){E.abort("timeout")},h.timeout));try{c=!1,i.send(b,k)}catch(e){if(c)throw e;k(-1,e)}}else k(-1,"No Transport");function k(t,n,r,s){var l,p,d,b,T,C=n;c||(c=!0,u&&e.clearTimeout(u),i=void 0,a=s||"",E.readyState=t>0?4:0,l=t>=200&&t<300||304===t,r&&(b=Xt(h,E,r)),b=Ut(h,b,E,l),l?(h.ifModified&&((T=E.getResponseHeader("Last-Modified"))&&(w.lastModified[o]=T),(T=E.getResponseHeader("etag"))&&(w.etag[o]=T)),204===t||"HEAD"===h.type?C="nocontent":304===t?C="notmodified":(C=b.state,p=b.data,l=!(d=b.error))):(d=C,!t&&C||(C="error",t<0&&(t=0))),E.status=t,E.statusText=(n||C)+"",l?v.resolveWith(g,[p,C,E]):v.rejectWith(g,[E,C,d]),E.statusCode(x),x=void 0,f&&y.trigger(l?"ajaxSuccess":"ajaxError",[E,h,l?p:d]),m.fireWith(g,[E,C]),f&&(y.trigger("ajaxComplete",[E,h]),--w.active||w.event.trigger("ajaxStop")))}return E},getJSON:function(e,t,n){return w.get(e,t,n,"json")},getScript:function(e,t){return w.get(e,void 0,t,"script")}}),w.each(["get","post"],function(e,t){w[t]=function(e,n,r,i){return g(n)&&(i=i||r,r=n,n=void 0),w.ajax(w.extend({url:e,type:t,dataType:i,data:n,success:r},w.isPlainObject(e)&&e))}}),w._evalUrl=function(e){return w.ajax({url:e,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,"throws":!0})},w.fn.extend({wrapAll:function(e){var t;return this[0]&&(g(e)&&(e=e.call(this[0])),t=w(e,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&t.insertBefore(this[0]),t.map(function(){var e=this;while(e.firstElementChild)e=e.firstElementChild;return e}).append(this)),this},wrapInner:function(e){return g(e)?this.each(function(t){w(this).wrapInner(e.call(this,t))}):this.each(function(){var t=w(this),n=t.contents();n.length?n.wrapAll(e):t.append(e)})},wrap:function(e){var t=g(e);return this.each(function(n){w(this).wrapAll(t?e.call(this,n):e)})},unwrap:function(e){return this.parent(e).not("body").each(function(){w(this).replaceWith(this.childNodes)}),this}}),w.expr.pseudos.hidden=function(e){return!w.expr.pseudos.visible(e)},w.expr.pseudos.visible=function(e){return!!(e.offsetWidth||e.offsetHeight||e.getClientRects().length)},w.ajaxSettings.xhr=function(){try{return new e.XMLHttpRequest}catch(e){}};var Vt={0:200,1223:204},Gt=w.ajaxSettings.xhr();h.cors=!!Gt&&"withCredentials"in Gt,h.ajax=Gt=!!Gt,w.ajaxTransport(function(t){var n,r;if(h.cors||Gt&&!t.crossDomain)return{send:function(i,o){var a,s=t.xhr();if(s.open(t.type,t.url,t.async,t.username,t.password),t.xhrFields)for(a in t.xhrFields)s[a]=t.xhrFields[a];t.mimeType&&s.overrideMimeType&&s.overrideMimeType(t.mimeType),t.crossDomain||i["X-Requested-With"]||(i["X-Requested-With"]="XMLHttpRequest");for(a in i)s.setRequestHeader(a,i[a]);n=function(e){return function(){n&&(n=r=s.onload=s.onerror=s.onabort=s.ontimeout=s.onreadystatechange=null,"abort"===e?s.abort():"error"===e?"number"!=typeof s.status?o(0,"error"):o(s.status,s.statusText):o(Vt[s.status]||s.status,s.statusText,"text"!==(s.responseType||"text")||"string"!=typeof s.responseText?{binary:s.response}:{text:s.responseText},s.getAllResponseHeaders()))}},s.onload=n(),r=s.onerror=s.ontimeout=n("error"),void 0!==s.onabort?s.onabort=r:s.onreadystatechange=function(){4===s.readyState&&e.setTimeout(function(){n&&r()})},n=n("abort");try{s.send(t.hasContent&&t.data||null)}catch(e){if(n)throw e}},abort:function(){n&&n()}}}),w.ajaxPrefilter(function(e){e.crossDomain&&(e.contents.script=!1)}),w.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(e){return w.globalEval(e),e}}}),w.ajaxPrefilter("script",function(e){void 0===e.cache&&(e.cache=!1),e.crossDomain&&(e.type="GET")}),w.ajaxTransport("script",function(e){if(e.crossDomain){var t,n;return{send:function(i,o){t=w("<script>").prop({charset:e.scriptCharset,src:e.url}).on("load error",n=function(e){t.remove(),n=null,e&&o("error"===e.type?404:200,e.type)}),r.head.appendChild(t[0])},abort:function(){n&&n()}}}});var Yt=[],Qt=/(=)\?(?=&|$)|\?\?/;w.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var e=Yt.pop()||w.expando+"_"+Et++;return this[e]=!0,e}}),w.ajaxPrefilter("json jsonp",function(t,n,r){var i,o,a,s=!1!==t.jsonp&&(Qt.test(t.url)?"url":"string"==typeof t.data&&0===(t.contentType||"").indexOf("application/x-www-form-urlencoded")&&Qt.test(t.data)&&"data");if(s||"jsonp"===t.dataTypes[0])return i=t.jsonpCallback=g(t.jsonpCallback)?t.jsonpCallback():t.jsonpCallback,s?t[s]=t[s].replace(Qt,"$1"+i):!1!==t.jsonp&&(t.url+=(kt.test(t.url)?"&":"?")+t.jsonp+"="+i),t.converters["script json"]=function(){return a||w.error(i+" was not called"),a[0]},t.dataTypes[0]="json",o=e[i],e[i]=function(){a=arguments},r.always(function(){void 0===o?w(e).removeProp(i):e[i]=o,t[i]&&(t.jsonpCallback=n.jsonpCallback,Yt.push(i)),a&&g(o)&&o(a[0]),a=o=void 0}),"script"}),h.createHTMLDocument=function(){var e=r.implementation.createHTMLDocument("").body;return e.innerHTML="<form></form><form></form>",2===e.childNodes.length}(),w.parseHTML=function(e,t,n){if("string"!=typeof e)return[];"boolean"==typeof t&&(n=t,t=!1);var i,o,a;return t||(h.createHTMLDocument?((i=(t=r.implementation.createHTMLDocument("")).createElement("base")).href=r.location.href,t.head.appendChild(i)):t=r),o=A.exec(e),a=!n&&[],o?[t.createElement(o[1])]:(o=xe([e],t,a),a&&a.length&&w(a).remove(),w.merge([],o.childNodes))},w.fn.load=function(e,t,n){var r,i,o,a=this,s=e.indexOf(" ");return s>-1&&(r=vt(e.slice(s)),e=e.slice(0,s)),g(t)?(n=t,t=void 0):t&&"object"==typeof t&&(i="POST"),a.length>0&&w.ajax({url:e,type:i||"GET",dataType:"html",data:t}).done(function(e){o=arguments,a.html(r?w("<div>").append(w.parseHTML(e)).find(r):e)}).always(n&&function(e,t){a.each(function(){n.apply(this,o||[e.responseText,t,e])})}),this},w.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(e,t){w.fn[t]=function(e){return this.on(t,e)}}),w.expr.pseudos.animated=function(e){return w.grep(w.timers,function(t){return e===t.elem}).length},w.offset={setOffset:function(e,t,n){var r,i,o,a,s,u,l,c=w.css(e,"position"),f=w(e),p={};"static"===c&&(e.style.position="relative"),s=f.offset(),o=w.css(e,"top"),u=w.css(e,"left"),(l=("absolute"===c||"fixed"===c)&&(o+u).indexOf("auto")>-1)?(a=(r=f.position()).top,i=r.left):(a=parseFloat(o)||0,i=parseFloat(u)||0),g(t)&&(t=t.call(e,n,w.extend({},s))),null!=t.top&&(p.top=t.top-s.top+a),null!=t.left&&(p.left=t.left-s.left+i),"using"in t?t.using.call(e,p):f.css(p)}},w.fn.extend({offset:function(e){if(arguments.length)return void 0===e?this:this.each(function(t){w.offset.setOffset(this,e,t)});var t,n,r=this[0];if(r)return r.getClientRects().length?(t=r.getBoundingClientRect(),n=r.ownerDocument.defaultView,{top:t.top+n.pageYOffset,left:t.left+n.pageXOffset}):{top:0,left:0}},position:function(){if(this[0]){var e,t,n,r=this[0],i={top:0,left:0};if("fixed"===w.css(r,"position"))t=r.getBoundingClientRect();else{t=this.offset(),n=r.ownerDocument,e=r.offsetParent||n.documentElement;while(e&&(e===n.body||e===n.documentElement)&&"static"===w.css(e,"position"))e=e.parentNode;e&&e!==r&&1===e.nodeType&&((i=w(e).offset()).top+=w.css(e,"borderTopWidth",!0),i.left+=w.css(e,"borderLeftWidth",!0))}return{top:t.top-i.top-w.css(r,"marginTop",!0),left:t.left-i.left-w.css(r,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var e=this.offsetParent;while(e&&"static"===w.css(e,"position"))e=e.offsetParent;return e||be})}}),w.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(e,t){var n="pageYOffset"===t;w.fn[e]=function(r){return z(this,function(e,r,i){var o;if(y(e)?o=e:9===e.nodeType&&(o=e.defaultView),void 0===i)return o?o[t]:e[r];o?o.scrollTo(n?o.pageXOffset:i,n?i:o.pageYOffset):e[r]=i},e,r,arguments.length)}}),w.each(["top","left"],function(e,t){w.cssHooks[t]=_e(h.pixelPosition,function(e,n){if(n)return n=Fe(e,t),We.test(n)?w(e).position()[t]+"px":n})}),w.each({Height:"height",Width:"width"},function(e,t){w.each({padding:"inner"+e,content:t,"":"outer"+e},function(n,r){w.fn[r]=function(i,o){var a=arguments.length&&(n||"boolean"!=typeof i),s=n||(!0===i||!0===o?"margin":"border");return z(this,function(t,n,i){var o;return y(t)?0===r.indexOf("outer")?t["inner"+e]:t.document.documentElement["client"+e]:9===t.nodeType?(o=t.documentElement,Math.max(t.body["scroll"+e],o["scroll"+e],t.body["offset"+e],o["offset"+e],o["client"+e])):void 0===i?w.css(t,n,s):w.style(t,n,i,s)},t,a?i:void 0,a)}})}),w.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(e,t){w.fn[t]=function(e,n){return arguments.length>0?this.on(t,null,e,n):this.trigger(t)}}),w.fn.extend({hover:function(e,t){return this.mouseenter(e).mouseleave(t||e)}}),w.fn.extend({bind:function(e,t,n){return this.on(e,null,t,n)},unbind:function(e,t){return this.off(e,null,t)},delegate:function(e,t,n,r){return this.on(t,e,n,r)},undelegate:function(e,t,n){return 1===arguments.length?this.off(e,"**"):this.off(t,e||"**",n)}}),w.proxy=function(e,t){var n,r,i;if("string"==typeof t&&(n=e[t],t=e,e=n),g(e))return r=o.call(arguments,2),i=function(){return e.apply(t||this,r.concat(o.call(arguments)))},i.guid=e.guid=e.guid||w.guid++,i},w.holdReady=function(e){e?w.readyWait++:w.ready(!0)},w.isArray=Array.isArray,w.parseJSON=JSON.parse,w.nodeName=N,w.isFunction=g,w.isWindow=y,w.camelCase=G,w.type=x,w.now=Date.now,w.isNumeric=function(e){var t=w.type(e);return("number"===t||"string"===t)&&!isNaN(e-parseFloat(e))},"function"==typeof define&&define.amd&&define("jquery",[],function(){return w});var Jt=e.jQuery,Kt=e.$;return w.noConflict=function(t){return e.$===w&&(e.$=Kt),t&&e.jQuery===w&&(e.jQuery=Jt),w},t||(e.jQuery=e.$=w),w});

/*
== malihu jquery custom scrollbar plugin == 
Version: 3.1.5 
Plugin URI: http://manos.malihu.gr/jquery-custom-content-scroller 
Author: malihu
Author URI: http://manos.malihu.gr
License: MIT License (MIT)
*/

/*
Copyright Manos Malihutsakis (email: manos@malihu.gr)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
The code below is fairly long, fully commented and should be normally used in development. 
For production, use either the minified jquery.mCustomScrollbar.min.js script or 
the production-ready jquery.mCustomScrollbar.concat.min.js which contains the plugin 
and dependencies (minified). 
*/

(function(factory){
	if(typeof define==="function" && define.amd){
		define(["jquery"],factory);
	}else if(typeof module!=="undefined" && module.exports){
		module.exports=factory;
	}else{
		factory(jQuery,window,document);
	}
}(function($){
(function(init){
	var _rjs=typeof define==="function" && define.amd, /* RequireJS */
		_njs=typeof module !== "undefined" && module.exports, /* NodeJS */
		_dlp=("https:"==document.location.protocol) ? "https:" : "http:", /* location protocol */
		_url="cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js";
	if(!_rjs){
		if(_njs){
			require("jquery-mousewheel")($);
		}else{
			/* load jquery-mousewheel plugin (via CDN) if it's not present or not loaded via RequireJS 
			(works when mCustomScrollbar fn is called on window load) */
			$.event.special.mousewheel || $("head").append(decodeURI("%3Cscript src="+_dlp+"//"+_url+"%3E%3C/script%3E"));
		}
	}
	init();
}(function(){
	
	/* 
	----------------------------------------
	PLUGIN NAMESPACE, PREFIX, DEFAULT SELECTOR(S) 
	----------------------------------------
	*/
	
	var pluginNS="mCustomScrollbar",
		pluginPfx="mCS",
		defaultSelector=".mCustomScrollbar",
	
	
		
	
	
	/* 
	----------------------------------------
	DEFAULT OPTIONS 
	----------------------------------------
	*/
	
		defaults={
			/*
			set element/content width/height programmatically 
			values: boolean, pixels, percentage 
				option						default
				-------------------------------------
				setWidth					false
				setHeight					false
			*/
			/*
			set the initial css top property of content  
			values: string (e.g. "-100px", "10%" etc.)
			*/
			setTop:0,
			/*
			set the initial css left property of content  
			values: string (e.g. "-100px", "10%" etc.)
			*/
			setLeft:0,
			/* 
			scrollbar axis (vertical and/or horizontal scrollbars) 
			values (string): "y", "x", "yx"
			*/
			axis:"y",
			/*
			position of scrollbar relative to content  
			values (string): "inside", "outside" ("outside" requires elements with position:relative)
			*/
			scrollbarPosition:"inside",
			/*
			scrolling inertia
			values: integer (milliseconds)
			*/
			scrollInertia:950,
			/* 
			auto-adjust scrollbar dragger length
			values: boolean
			*/
			autoDraggerLength:true,
			/*
			auto-hide scrollbar when idle 
			values: boolean
				option						default
				-------------------------------------
				autoHideScrollbar			false
			*/
			/*
			auto-expands scrollbar on mouse-over and dragging
			values: boolean
				option						default
				-------------------------------------
				autoExpandScrollbar			false
			*/
			/*
			always show scrollbar, even when there's nothing to scroll 
			values: integer (0=disable, 1=always show dragger rail and buttons, 2=always show dragger rail, dragger and buttons), boolean
			*/
			alwaysShowScrollbar:0,
			/*
			scrolling always snaps to a multiple of this number in pixels
			values: integer, array ([y,x])
				option						default
				-------------------------------------
				snapAmount					null
			*/
			/*
			when snapping, snap with this number in pixels as an offset 
			values: integer
			*/
			snapOffset:0,
			/* 
			mouse-wheel scrolling
			*/
			mouseWheel:{
				/* 
				enable mouse-wheel scrolling
				values: boolean
				*/
				enable:true,
				/* 
				scrolling amount in pixels
				values: "auto", integer 
				*/
				scrollAmount:"auto",
				/* 
				mouse-wheel scrolling axis 
				the default scrolling direction when both vertical and horizontal scrollbars are present 
				values (string): "y", "x" 
				*/
				axis:"y",
				/* 
				prevent the default behaviour which automatically scrolls the parent element(s) when end of scrolling is reached 
				values: boolean
					option						default
					-------------------------------------
					preventDefault				null
				*/
				/*
				the reported mouse-wheel delta value. The number of lines (translated to pixels) one wheel notch scrolls.  
				values: "auto", integer 
				"auto" uses the default OS/browser value 
				*/
				deltaFactor:"auto",
				/*
				normalize mouse-wheel delta to -1 or 1 (disables mouse-wheel acceleration) 
				values: boolean
					option						default
					-------------------------------------
					normalizeDelta				null
				*/
				/*
				invert mouse-wheel scrolling direction 
				values: boolean
					option						default
					-------------------------------------
					invert						null
				*/
				/*
				the tags that disable mouse-wheel when cursor is over them
				*/
				disableOver:["select","option","keygen","datalist","textarea"]
			},
			/* 
			scrollbar buttons
			*/
			scrollButtons:{ 
				/*
				enable scrollbar buttons
				values: boolean
					option						default
					-------------------------------------
					enable						null
				*/
				/*
				scrollbar buttons scrolling type 
				values (string): "stepless", "stepped"
				*/
				scrollType:"stepless",
				/*
				scrolling amount in pixels
				values: "auto", integer 
				*/
				scrollAmount:"auto"
				/*
				tabindex of the scrollbar buttons
				values: false, integer
					option						default
					-------------------------------------
					tabindex					null
				*/
			},
			/* 
			keyboard scrolling
			*/
			keyboard:{ 
				/*
				enable scrolling via keyboard
				values: boolean
				*/
				enable:true,
				/*
				keyboard scrolling type 
				values (string): "stepless", "stepped"
				*/
				scrollType:"stepless",
				/*
				scrolling amount in pixels
				values: "auto", integer 
				*/
				scrollAmount:"auto"
			},
			/*
			enable content touch-swipe scrolling 
			values: boolean, integer, string (number)
			integer values define the axis-specific minimum amount required for scrolling momentum
			*/
			contentTouchScroll:25,
			/*
			enable/disable document (default) touch-swipe scrolling 
			*/
			documentTouchScroll:true,
			/*
			advanced option parameters
			*/
			advanced:{
				/*
				auto-expand content horizontally (for "x" or "yx" axis) 
				values: boolean, integer (the value 2 forces the non scrollHeight/scrollWidth method, the value 3 forces the scrollHeight/scrollWidth method)
					option						default
					-------------------------------------
					autoExpandHorizontalScroll	null
				*/
				/*
				auto-scroll to elements with focus
				*/
				autoScrollOnFocus:"input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']",
				/*
				auto-update scrollbars on content, element or viewport resize 
				should be true for fluid layouts/elements, adding/removing content dynamically, hiding/showing elements, content with images etc. 
				values: boolean
				*/
				updateOnContentResize:true,
				/*
				auto-update scrollbars each time each image inside the element is fully loaded 
				values: "auto", boolean
				*/
				updateOnImageLoad:"auto",
				/*
				auto-update scrollbars based on the amount and size changes of specific selectors 
				useful when you need to update the scrollbar(s) automatically, each time a type of element is added, removed or changes its size 
				values: boolean, string (e.g. "ul li" will auto-update scrollbars each time list-items inside the element are changed) 
				a value of true (boolean) will auto-update scrollbars each time any element is changed
					option						default
					-------------------------------------
					updateOnSelectorChange		null
				*/
				/*
				extra selectors that'll allow scrollbar dragging upon mousemove/up, pointermove/up, touchend etc. (e.g. "selector-1, selector-2")
					option						default
					-------------------------------------
					extraDraggableSelectors		null
				*/
				/*
				extra selectors that'll release scrollbar dragging upon mouseup, pointerup, touchend etc. (e.g. "selector-1, selector-2")
					option						default
					-------------------------------------
					releaseDraggableSelectors	null
				*/
				/*
				auto-update timeout 
				values: integer (milliseconds)
				*/
				autoUpdateTimeout:60
			},
			/* 
			scrollbar theme 
			values: string (see CSS/plugin URI for a list of ready-to-use themes)
			*/
			theme:"light",
			/*
			user defined callback functions
			*/
			callbacks:{
				/*
				Available callbacks: 
					callback					default
					-------------------------------------
					onCreate					null
					onInit						null
					onScrollStart				null
					onScroll					null
					onTotalScroll				null
					onTotalScrollBack			null
					whileScrolling				null
					onOverflowY					null
					onOverflowX					null
					onOverflowYNone				null
					onOverflowXNone				null
					onImageLoad					null
					onSelectorChange			null
					onBeforeUpdate				null
					onUpdate					null
				*/
				onTotalScrollOffset:0,
				onTotalScrollBackOffset:0,
				alwaysTriggerOffsets:true
			}
			/*
			add scrollbar(s) on all elements matching the current selector, now and in the future 
			values: boolean, string 
			string values: "on" (enable), "once" (disable after first invocation), "off" (disable)
			liveSelector values: string (selector)
				option						default
				-------------------------------------
				live						false
				liveSelector				null
			*/
		},
	
	
	
	
	
	/* 
	----------------------------------------
	VARS, CONSTANTS 
	----------------------------------------
	*/
	
		totalInstances=0, /* plugin instances amount */
		liveTimers={}, /* live option timers */
		oldIE=(window.attachEvent && !window.addEventListener) ? 1 : 0, /* detect IE < 9 */
		touchActive=false,touchable, /* global touch vars (for touch and pointer events) */
		/* general plugin classes */
		classes=[
			"mCSB_dragger_onDrag","mCSB_scrollTools_onDrag","mCS_img_loaded","mCS_disabled","mCS_destroyed","mCS_no_scrollbar",
			"mCS-autoHide","mCS-dir-rtl","mCS_no_scrollbar_y","mCS_no_scrollbar_x","mCS_y_hidden","mCS_x_hidden","mCSB_draggerContainer",
			"mCSB_buttonUp","mCSB_buttonDown","mCSB_buttonLeft","mCSB_buttonRight"
		],
		
	
	
	
	
	/* 
	----------------------------------------
	METHODS 
	----------------------------------------
	*/
	
		methods={
			
			/* 
			plugin initialization method 
			creates the scrollbar(s), plugin data object and options
			----------------------------------------
			*/
			
			init:function(options){
				
				var options=$.extend(true,{},defaults,options),
					selector=_selector.call(this); /* validate selector */
				
				/* 
				if live option is enabled, monitor for elements matching the current selector and 
				apply scrollbar(s) when found (now and in the future) 
				*/
				if(options.live){
					var liveSelector=options.liveSelector || this.selector || defaultSelector, /* live selector(s) */
						$liveSelector=$(liveSelector); /* live selector(s) as jquery object */
					if(options.live==="off"){
						/* 
						disable live if requested 
						usage: $(selector).mCustomScrollbar({live:"off"}); 
						*/
						removeLiveTimers(liveSelector);
						return;
					}
					liveTimers[liveSelector]=setTimeout(function(){
						/* call mCustomScrollbar fn on live selector(s) every half-second */
						$liveSelector.mCustomScrollbar(options);
						if(options.live==="once" && $liveSelector.length){
							/* disable live after first invocation */
							removeLiveTimers(liveSelector);
						}
					},500);
				}else{
					removeLiveTimers(liveSelector);
				}
				
				/* options backward compatibility (for versions < 3.0.0) and normalization */
				options.setWidth=(options.set_width) ? options.set_width : options.setWidth;
				options.setHeight=(options.set_height) ? options.set_height : options.setHeight;
				options.axis=(options.horizontalScroll) ? "x" : _findAxis(options.axis);
				options.scrollInertia=options.scrollInertia>0 && options.scrollInertia<17 ? 17 : options.scrollInertia;
				if(typeof options.mouseWheel!=="object" &&  options.mouseWheel==true){ /* old school mouseWheel option (non-object) */
					options.mouseWheel={enable:true,scrollAmount:"auto",axis:"y",preventDefault:false,deltaFactor:"auto",normalizeDelta:false,invert:false}
				}
				options.mouseWheel.scrollAmount=!options.mouseWheelPixels ? options.mouseWheel.scrollAmount : options.mouseWheelPixels;
				options.mouseWheel.normalizeDelta=!options.advanced.normalizeMouseWheelDelta ? options.mouseWheel.normalizeDelta : options.advanced.normalizeMouseWheelDelta;
				options.scrollButtons.scrollType=_findScrollButtonsType(options.scrollButtons.scrollType); 
				
				_theme(options); /* theme-specific options */
				
				/* plugin constructor */
				return $(selector).each(function(){
					
					var $this=$(this);
					
					if(!$this.data(pluginPfx)){ /* prevent multiple instantiations */
					
						/* store options and create objects in jquery data */
						$this.data(pluginPfx,{
							idx:++totalInstances, /* instance index */
							opt:options, /* options */
							scrollRatio:{y:null,x:null}, /* scrollbar to content ratio */
							overflowed:null, /* overflowed axis */
							contentReset:{y:null,x:null}, /* object to check when content resets */
							bindEvents:false, /* object to check if events are bound */
							tweenRunning:false, /* object to check if tween is running */
							sequential:{}, /* sequential scrolling object */
							langDir:$this.css("direction"), /* detect/store direction (ltr or rtl) */
							cbOffsets:null, /* object to check whether callback offsets always trigger */
							/* 
							object to check how scrolling events where last triggered 
							"internal" (default - triggered by this script), "external" (triggered by other scripts, e.g. via scrollTo method) 
							usage: object.data("mCS").trigger
							*/
							trigger:null,
							/* 
							object to check for changes in elements in order to call the update method automatically 
							*/
							poll:{size:{o:0,n:0},img:{o:0,n:0},change:{o:0,n:0}}
						});
						
						var d=$this.data(pluginPfx),o=d.opt,
							/* HTML data attributes */
							htmlDataAxis=$this.data("mcs-axis"),htmlDataSbPos=$this.data("mcs-scrollbar-position"),htmlDataTheme=$this.data("mcs-theme");
						 
						if(htmlDataAxis){o.axis=htmlDataAxis;} /* usage example: data-mcs-axis="y" */
						if(htmlDataSbPos){o.scrollbarPosition=htmlDataSbPos;} /* usage example: data-mcs-scrollbar-position="outside" */
						if(htmlDataTheme){ /* usage example: data-mcs-theme="minimal" */
							o.theme=htmlDataTheme;
							_theme(o); /* theme-specific options */
						}
						
						_pluginMarkup.call(this); /* add plugin markup */
						
						if(d && o.callbacks.onCreate && typeof o.callbacks.onCreate==="function"){o.callbacks.onCreate.call(this);} /* callbacks: onCreate */
						
						$("#mCSB_"+d.idx+"_container img:not(."+classes[2]+")").addClass(classes[2]); /* flag loaded images */
						
						methods.update.call(null,$this); /* call the update method */
					
					}
					
				});
				
			},
			/* ---------------------------------------- */
			
			
			
			/* 
			plugin update method 
			updates content and scrollbar(s) values, events and status 
			----------------------------------------
			usage: $(selector).mCustomScrollbar("update");
			*/
			
			update:function(el,cb){
				
				var selector=el || _selector.call(this); /* validate selector */
				
				return $(selector).each(function(){
					
					var $this=$(this);
					
					if($this.data(pluginPfx)){ /* check if plugin has initialized */
						
						var d=$this.data(pluginPfx),o=d.opt,
							mCSB_container=$("#mCSB_"+d.idx+"_container"),
							mCustomScrollBox=$("#mCSB_"+d.idx),
							mCSB_dragger=[$("#mCSB_"+d.idx+"_dragger_vertical"),$("#mCSB_"+d.idx+"_dragger_horizontal")];
						
						if(!mCSB_container.length){return;}
						
						if(d.tweenRunning){_stop($this);} /* stop any running tweens while updating */
						
						if(cb && d && o.callbacks.onBeforeUpdate && typeof o.callbacks.onBeforeUpdate==="function"){o.callbacks.onBeforeUpdate.call(this);} /* callbacks: onBeforeUpdate */
						
						/* if element was disabled or destroyed, remove class(es) */
						if($this.hasClass(classes[3])){$this.removeClass(classes[3]);}
						if($this.hasClass(classes[4])){$this.removeClass(classes[4]);}
						
						/* css flexbox fix, detect/set max-height */
						mCustomScrollBox.css("max-height","none");
						if(mCustomScrollBox.height()!==$this.height()){mCustomScrollBox.css("max-height",$this.height());}
						
						_expandContentHorizontally.call(this); /* expand content horizontally */
						
						if(o.axis!=="y" && !o.advanced.autoExpandHorizontalScroll){
							mCSB_container.css("width",_contentWidth(mCSB_container));
						}
						
						d.overflowed=_overflowed.call(this); /* determine if scrolling is required */
						
						_scrollbarVisibility.call(this); /* show/hide scrollbar(s) */
						
						/* auto-adjust scrollbar dragger length analogous to content */
						if(o.autoDraggerLength){_setDraggerLength.call(this);}
						
						_scrollRatio.call(this); /* calculate and store scrollbar to content ratio */
						
						_bindEvents.call(this); /* bind scrollbar events */
						
						/* reset scrolling position and/or events */
						var to=[Math.abs(mCSB_container[0].offsetTop),Math.abs(mCSB_container[0].offsetLeft)];
						if(o.axis!=="x"){ /* y/yx axis */
							if(!d.overflowed[0]){ /* y scrolling is not required */
								_resetContentPosition.call(this); /* reset content position */
								if(o.axis==="y"){
									_unbindEvents.call(this);
								}else if(o.axis==="yx" && d.overflowed[1]){
									_scrollTo($this,to[1].toString(),{dir:"x",dur:0,overwrite:"none"});
								}
							}else if(mCSB_dragger[0].height()>mCSB_dragger[0].parent().height()){
								_resetContentPosition.call(this); /* reset content position */
							}else{ /* y scrolling is required */
								_scrollTo($this,to[0].toString(),{dir:"y",dur:0,overwrite:"none"});
								d.contentReset.y=null;
							}
						}
						if(o.axis!=="y"){ /* x/yx axis */
							if(!d.overflowed[1]){ /* x scrolling is not required */
								_resetContentPosition.call(this); /* reset content position */
								if(o.axis==="x"){
									_unbindEvents.call(this);
								}else if(o.axis==="yx" && d.overflowed[0]){
									_scrollTo($this,to[0].toString(),{dir:"y",dur:0,overwrite:"none"});
								}
							}else if(mCSB_dragger[1].width()>mCSB_dragger[1].parent().width()){
								_resetContentPosition.call(this); /* reset content position */
							}else{ /* x scrolling is required */
								_scrollTo($this,to[1].toString(),{dir:"x",dur:0,overwrite:"none"});
								d.contentReset.x=null;
							}
						}
						
						/* callbacks: onImageLoad, onSelectorChange, onUpdate */
						if(cb && d){
							if(cb===2 && o.callbacks.onImageLoad && typeof o.callbacks.onImageLoad==="function"){
								o.callbacks.onImageLoad.call(this);
							}else if(cb===3 && o.callbacks.onSelectorChange && typeof o.callbacks.onSelectorChange==="function"){
								o.callbacks.onSelectorChange.call(this);
							}else if(o.callbacks.onUpdate && typeof o.callbacks.onUpdate==="function"){
								o.callbacks.onUpdate.call(this);
							}
						}
						
						_autoUpdate.call(this); /* initialize automatic updating (for dynamic content, fluid layouts etc.) */
						
					}
					
				});
				
			},
			/* ---------------------------------------- */
			
			
			
			/* 
			plugin scrollTo method 
			triggers a scrolling event to a specific value
			----------------------------------------
			usage: $(selector).mCustomScrollbar("scrollTo",value,options);
			*/
		
			scrollTo:function(val,options){
				
				/* prevent silly things like $(selector).mCustomScrollbar("scrollTo",undefined); */
				if(typeof val=="undefined" || val==null){return;}
				
				var selector=_selector.call(this); /* validate selector */
				
				return $(selector).each(function(){
					
					var $this=$(this);
					
					if($this.data(pluginPfx)){ /* check if plugin has initialized */
					
						var d=$this.data(pluginPfx),o=d.opt,
							/* method default options */
							methodDefaults={
								trigger:"external", /* method is by default triggered externally (e.g. from other scripts) */
								scrollInertia:o.scrollInertia, /* scrolling inertia (animation duration) */
								scrollEasing:"mcsEaseInOut", /* animation easing */
								moveDragger:false, /* move dragger instead of content */
								timeout:60, /* scroll-to delay */
								callbacks:true, /* enable/disable callbacks */
								onStart:true,
								onUpdate:true,
								onComplete:true
							},
							methodOptions=$.extend(true,{},methodDefaults,options),
							to=_arr.call(this,val),dur=methodOptions.scrollInertia>0 && methodOptions.scrollInertia<17 ? 17 : methodOptions.scrollInertia;
						
						/* translate yx values to actual scroll-to positions */
						to[0]=_to.call(this,to[0],"y");
						to[1]=_to.call(this,to[1],"x");
						
						/* 
						check if scroll-to value moves the dragger instead of content. 
						Only pixel values apply on dragger (e.g. 100, "100px", "-=100" etc.) 
						*/
						if(methodOptions.moveDragger){
							to[0]*=d.scrollRatio.y;
							to[1]*=d.scrollRatio.x;
						}
						
						methodOptions.dur=_isTabHidden() ? 0 : dur; //skip animations if browser tab is hidden
						
						setTimeout(function(){ 
							/* do the scrolling */
							if(to[0]!==null && typeof to[0]!=="undefined" && o.axis!=="x" && d.overflowed[0]){ /* scroll y */
								methodOptions.dir="y";
								methodOptions.overwrite="all";
								_scrollTo($this,to[0].toString(),methodOptions);
							}
							if(to[1]!==null && typeof to[1]!=="undefined" && o.axis!=="y" && d.overflowed[1]){ /* scroll x */
								methodOptions.dir="x";
								methodOptions.overwrite="none";
								_scrollTo($this,to[1].toString(),methodOptions);
							}
						},methodOptions.timeout);
						
					}
					
				});
				
			},
			/* ---------------------------------------- */
			
			
			
			/*
			plugin stop method 
			stops scrolling animation
			----------------------------------------
			usage: $(selector).mCustomScrollbar("stop");
			*/
			stop:function(){
				
				var selector=_selector.call(this); /* validate selector */
				
				return $(selector).each(function(){
					
					var $this=$(this);
					
					if($this.data(pluginPfx)){ /* check if plugin has initialized */
										
						_stop($this);
					
					}
					
				});
				
			},
			/* ---------------------------------------- */
			
			
			
			/*
			plugin disable method 
			temporarily disables the scrollbar(s) 
			----------------------------------------
			usage: $(selector).mCustomScrollbar("disable",reset); 
			reset (boolean): resets content position to 0 
			*/
			disable:function(r){
				
				var selector=_selector.call(this); /* validate selector */
				
				return $(selector).each(function(){
					
					var $this=$(this);
					
					if($this.data(pluginPfx)){ /* check if plugin has initialized */
						
						var d=$this.data(pluginPfx);
						
						_autoUpdate.call(this,"remove"); /* remove automatic updating */
						
						_unbindEvents.call(this); /* unbind events */
						
						if(r){_resetContentPosition.call(this);} /* reset content position */
						
						_scrollbarVisibility.call(this,true); /* show/hide scrollbar(s) */
						
						$this.addClass(classes[3]); /* add disable class */
					
					}
					
				});
				
			},
			/* ---------------------------------------- */
			
			
			
			/*
			plugin destroy method 
			completely removes the scrollbar(s) and returns the element to its original state
			----------------------------------------
			usage: $(selector).mCustomScrollbar("destroy"); 
			*/
			destroy:function(){
				
				var selector=_selector.call(this); /* validate selector */
				
				return $(selector).each(function(){
					
					var $this=$(this);
					
					if($this.data(pluginPfx)){ /* check if plugin has initialized */
					
						var d=$this.data(pluginPfx),o=d.opt,
							mCustomScrollBox=$("#mCSB_"+d.idx),
							mCSB_container=$("#mCSB_"+d.idx+"_container"),
							scrollbar=$(".mCSB_"+d.idx+"_scrollbar");
					
						if(o.live){removeLiveTimers(o.liveSelector || $(selector).selector);} /* remove live timers */
						
						_autoUpdate.call(this,"remove"); /* remove automatic updating */
						
						_unbindEvents.call(this); /* unbind events */
						
						_resetContentPosition.call(this); /* reset content position */
						
						$this.removeData(pluginPfx); /* remove plugin data object */
						
						_delete(this,"mcs"); /* delete callbacks object */
						
						/* remove plugin markup */
						scrollbar.remove(); /* remove scrollbar(s) first (those can be either inside or outside plugin's inner wrapper) */
						mCSB_container.find("img."+classes[2]).removeClass(classes[2]); /* remove loaded images flag */
						mCustomScrollBox.replaceWith(mCSB_container.contents()); /* replace plugin's inner wrapper with the original content */
						/* remove plugin classes from the element and add destroy class */
						$this.removeClass(pluginNS+" _"+pluginPfx+"_"+d.idx+" "+classes[6]+" "+classes[7]+" "+classes[5]+" "+classes[3]).addClass(classes[4]);
					
					}
					
				});
				
			}
			/* ---------------------------------------- */
			
		},
	
	
	
	
		
	/* 
	----------------------------------------
	FUNCTIONS
	----------------------------------------
	*/
	
		/* validates selector (if selector is invalid or undefined uses the default one) */
		_selector=function(){
			return (typeof $(this)!=="object" || $(this).length<1) ? defaultSelector : this;
		},
		/* -------------------- */
		
		
		/* changes options according to theme */
		_theme=function(obj){
			var fixedSizeScrollbarThemes=["rounded","rounded-dark","rounded-dots","rounded-dots-dark"],
				nonExpandedScrollbarThemes=["rounded-dots","rounded-dots-dark","3d","3d-dark","3d-thick","3d-thick-dark","inset","inset-dark","inset-2","inset-2-dark","inset-3","inset-3-dark"],
				disabledScrollButtonsThemes=["minimal","minimal-dark"],
				enabledAutoHideScrollbarThemes=["minimal","minimal-dark"],
				scrollbarPositionOutsideThemes=["minimal","minimal-dark"];
			obj.autoDraggerLength=$.inArray(obj.theme,fixedSizeScrollbarThemes) > -1 ? false : obj.autoDraggerLength;
			obj.autoExpandScrollbar=$.inArray(obj.theme,nonExpandedScrollbarThemes) > -1 ? false : obj.autoExpandScrollbar;
			obj.scrollButtons.enable=$.inArray(obj.theme,disabledScrollButtonsThemes) > -1 ? false : obj.scrollButtons.enable;
			obj.autoHideScrollbar=$.inArray(obj.theme,enabledAutoHideScrollbarThemes) > -1 ? true : obj.autoHideScrollbar;
			obj.scrollbarPosition=$.inArray(obj.theme,scrollbarPositionOutsideThemes) > -1 ? "outside" : obj.scrollbarPosition;
		},
		/* -------------------- */
		
		
		/* live option timers removal */
		removeLiveTimers=function(selector){
			if(liveTimers[selector]){
				clearTimeout(liveTimers[selector]);
				_delete(liveTimers,selector);
			}
		},
		/* -------------------- */
		
		
		/* normalizes axis option to valid values: "y", "x", "yx" */
		_findAxis=function(val){
			return (val==="yx" || val==="xy" || val==="auto") ? "yx" : (val==="x" || val==="horizontal") ? "x" : "y";
		},
		/* -------------------- */
		
		
		/* normalizes scrollButtons.scrollType option to valid values: "stepless", "stepped" */
		_findScrollButtonsType=function(val){
			return (val==="stepped" || val==="pixels" || val==="step" || val==="click") ? "stepped" : "stepless";
		},
		/* -------------------- */
		
		
		/* generates plugin markup */
		_pluginMarkup=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				expandClass=o.autoExpandScrollbar ? " "+classes[1]+"_expand" : "",
				scrollbar=["<div id='mCSB_"+d.idx+"_scrollbar_vertical' class='mCSB_scrollTools mCSB_"+d.idx+"_scrollbar mCS-"+o.theme+" mCSB_scrollTools_vertical"+expandClass+"'><div class='"+classes[12]+"'><div id='mCSB_"+d.idx+"_dragger_vertical' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>","<div id='mCSB_"+d.idx+"_scrollbar_horizontal' class='mCSB_scrollTools mCSB_"+d.idx+"_scrollbar mCS-"+o.theme+" mCSB_scrollTools_horizontal"+expandClass+"'><div class='"+classes[12]+"'><div id='mCSB_"+d.idx+"_dragger_horizontal' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>"],
				wrapperClass=o.axis==="yx" ? "mCSB_vertical_horizontal" : o.axis==="x" ? "mCSB_horizontal" : "mCSB_vertical",
				scrollbars=o.axis==="yx" ? scrollbar[0]+scrollbar[1] : o.axis==="x" ? scrollbar[1] : scrollbar[0],
				contentWrapper=o.axis==="yx" ? "<div id='mCSB_"+d.idx+"_container_wrapper' class='mCSB_container_wrapper' />" : "",
				autoHideClass=o.autoHideScrollbar ? " "+classes[6] : "",
				scrollbarDirClass=(o.axis!=="x" && d.langDir==="rtl") ? " "+classes[7] : "";
			if(o.setWidth){$this.css("width",o.setWidth);} /* set element width */
			if(o.setHeight){$this.css("height",o.setHeight);} /* set element height */
			o.setLeft=(o.axis!=="y" && d.langDir==="rtl") ? "989999px" : o.setLeft; /* adjust left position for rtl direction */
			$this.addClass(pluginNS+" _"+pluginPfx+"_"+d.idx+autoHideClass+scrollbarDirClass).wrapInner("<div id='mCSB_"+d.idx+"' class='mCustomScrollBox mCS-"+o.theme+" "+wrapperClass+"'><div id='mCSB_"+d.idx+"_container' class='mCSB_container' style='position:relative; top:"+o.setTop+"; left:"+o.setLeft+";' dir='"+d.langDir+"' /></div>");
			var mCustomScrollBox=$("#mCSB_"+d.idx),
				mCSB_container=$("#mCSB_"+d.idx+"_container");
			if(o.axis!=="y" && !o.advanced.autoExpandHorizontalScroll){
				mCSB_container.css("width",_contentWidth(mCSB_container));
			}
			if(o.scrollbarPosition==="outside"){
				if($this.css("position")==="static"){ /* requires elements with non-static position */
					$this.css("position","relative");
				}
				$this.css("overflow","visible");
				mCustomScrollBox.addClass("mCSB_outside").after(scrollbars);
			}else{
				mCustomScrollBox.addClass("mCSB_inside").append(scrollbars);
				mCSB_container.wrap(contentWrapper);
			}
			_scrollButtons.call(this); /* add scrollbar buttons */
			/* minimum dragger length */
			var mCSB_dragger=[$("#mCSB_"+d.idx+"_dragger_vertical"),$("#mCSB_"+d.idx+"_dragger_horizontal")];
			mCSB_dragger[0].css("min-height",mCSB_dragger[0].height());
			mCSB_dragger[1].css("min-width",mCSB_dragger[1].width());
		},
		/* -------------------- */
		
		
		/* calculates content width */
		_contentWidth=function(el){
			var val=[el[0].scrollWidth,Math.max.apply(Math,el.children().map(function(){return $(this).outerWidth(true);}).get())],w=el.parent().width();
			return val[0]>w ? val[0] : val[1]>w ? val[1] : "100%";
		},
		/* -------------------- */
		
		
		/* expands content horizontally */
		_expandContentHorizontally=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				mCSB_container=$("#mCSB_"+d.idx+"_container");
			if(o.advanced.autoExpandHorizontalScroll && o.axis!=="y"){
				/* calculate scrollWidth */
				mCSB_container.css({"width":"auto","min-width":0,"overflow-x":"scroll"});
				var w=Math.ceil(mCSB_container[0].scrollWidth);
				if(o.advanced.autoExpandHorizontalScroll===3 || (o.advanced.autoExpandHorizontalScroll!==2 && w>mCSB_container.parent().width())){
					mCSB_container.css({"width":w,"min-width":"100%","overflow-x":"inherit"});
				}else{
					/* 
					wrap content with an infinite width div and set its position to absolute and width to auto. 
					Setting width to auto before calculating the actual width is important! 
					We must let the browser set the width as browser zoom values are impossible to calculate.
					*/
					mCSB_container.css({"overflow-x":"inherit","position":"absolute"})
						.wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />")
						.css({ /* set actual width, original position and un-wrap */
							/* 
							get the exact width (with decimals) and then round-up. 
							Using jquery outerWidth() will round the width value which will mess up with inner elements that have non-integer width
							*/
							"width":(Math.ceil(mCSB_container[0].getBoundingClientRect().right+0.4)-Math.floor(mCSB_container[0].getBoundingClientRect().left)),
							"min-width":"100%",
							"position":"relative"
						}).unwrap();
				}
			}
		},
		/* -------------------- */
		
		
		/* adds scrollbar buttons */
		_scrollButtons=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				mCSB_scrollTools=$(".mCSB_"+d.idx+"_scrollbar:first"),
				tabindex=!_isNumeric(o.scrollButtons.tabindex) ? "" : "tabindex='"+o.scrollButtons.tabindex+"'",
				btnHTML=[
					"<a href='#' class='"+classes[13]+"' "+tabindex+" />",
					"<a href='#' class='"+classes[14]+"' "+tabindex+" />",
					"<a href='#' class='"+classes[15]+"' "+tabindex+" />",
					"<a href='#' class='"+classes[16]+"' "+tabindex+" />"
				],
				btn=[(o.axis==="x" ? btnHTML[2] : btnHTML[0]),(o.axis==="x" ? btnHTML[3] : btnHTML[1]),btnHTML[2],btnHTML[3]];
			if(o.scrollButtons.enable){
				mCSB_scrollTools.prepend(btn[0]).append(btn[1]).next(".mCSB_scrollTools").prepend(btn[2]).append(btn[3]);
			}
		},
		/* -------------------- */
		
		
		/* auto-adjusts scrollbar dragger length */
		_setDraggerLength=function(){
			var $this=$(this),d=$this.data(pluginPfx),
				mCustomScrollBox=$("#mCSB_"+d.idx),
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				mCSB_dragger=[$("#mCSB_"+d.idx+"_dragger_vertical"),$("#mCSB_"+d.idx+"_dragger_horizontal")],
				ratio=[mCustomScrollBox.height()/mCSB_container.outerHeight(false),mCustomScrollBox.width()/mCSB_container.outerWidth(false)],
				l=[
					parseInt(mCSB_dragger[0].css("min-height")),Math.round(ratio[0]*mCSB_dragger[0].parent().height()),
					parseInt(mCSB_dragger[1].css("min-width")),Math.round(ratio[1]*mCSB_dragger[1].parent().width())
				],
				h=oldIE && (l[1]<l[0]) ? l[0] : l[1],w=oldIE && (l[3]<l[2]) ? l[2] : l[3];
			mCSB_dragger[0].css({
				"height":h,"max-height":(mCSB_dragger[0].parent().height()-10)
			}).find(".mCSB_dragger_bar").css({"line-height":l[0]+"px"});
			mCSB_dragger[1].css({
				"width":w,"max-width":(mCSB_dragger[1].parent().width()-10)
			});
		},
		/* -------------------- */
		
		
		/* calculates scrollbar to content ratio */
		_scrollRatio=function(){
			var $this=$(this),d=$this.data(pluginPfx),
				mCustomScrollBox=$("#mCSB_"+d.idx),
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				mCSB_dragger=[$("#mCSB_"+d.idx+"_dragger_vertical"),$("#mCSB_"+d.idx+"_dragger_horizontal")],
				scrollAmount=[mCSB_container.outerHeight(false)-mCustomScrollBox.height(),mCSB_container.outerWidth(false)-mCustomScrollBox.width()],
				ratio=[
					scrollAmount[0]/(mCSB_dragger[0].parent().height()-mCSB_dragger[0].height()),
					scrollAmount[1]/(mCSB_dragger[1].parent().width()-mCSB_dragger[1].width())
				];
			d.scrollRatio={y:ratio[0],x:ratio[1]};
		},
		/* -------------------- */
		
		
		/* toggles scrolling classes */
		_onDragClasses=function(el,action,xpnd){
			var expandClass=xpnd ? classes[0]+"_expanded" : "",
				scrollbar=el.closest(".mCSB_scrollTools");
			if(action==="active"){
				el.toggleClass(classes[0]+" "+expandClass); scrollbar.toggleClass(classes[1]); 
				el[0]._draggable=el[0]._draggable ? 0 : 1;
			}else{
				if(!el[0]._draggable){
					if(action==="hide"){
						el.removeClass(classes[0]); scrollbar.removeClass(classes[1]);
					}else{
						el.addClass(classes[0]); scrollbar.addClass(classes[1]);
					}
				}
			}
		},
		/* -------------------- */
		
		
		/* checks if content overflows its container to determine if scrolling is required */
		_overflowed=function(){
			var $this=$(this),d=$this.data(pluginPfx),
				mCustomScrollBox=$("#mCSB_"+d.idx),
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				contentHeight=d.overflowed==null ? mCSB_container.height() : mCSB_container.outerHeight(false),
				contentWidth=d.overflowed==null ? mCSB_container.width() : mCSB_container.outerWidth(false),
				h=mCSB_container[0].scrollHeight,w=mCSB_container[0].scrollWidth;
			if(h>contentHeight){contentHeight=h;}
			if(w>contentWidth){contentWidth=w;}
			return [contentHeight>mCustomScrollBox.height(),contentWidth>mCustomScrollBox.width()];
		},
		/* -------------------- */
		
		
		/* resets content position to 0 */
		_resetContentPosition=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				mCustomScrollBox=$("#mCSB_"+d.idx),
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				mCSB_dragger=[$("#mCSB_"+d.idx+"_dragger_vertical"),$("#mCSB_"+d.idx+"_dragger_horizontal")];
			_stop($this); /* stop any current scrolling before resetting */
			if((o.axis!=="x" && !d.overflowed[0]) || (o.axis==="y" && d.overflowed[0])){ /* reset y */
				mCSB_dragger[0].add(mCSB_container).css("top",0);
				_scrollTo($this,"_resetY");
			}
			if((o.axis!=="y" && !d.overflowed[1]) || (o.axis==="x" && d.overflowed[1])){ /* reset x */
				var cx=dx=0;
				if(d.langDir==="rtl"){ /* adjust left position for rtl direction */
					cx=mCustomScrollBox.width()-mCSB_container.outerWidth(false);
					dx=Math.abs(cx/d.scrollRatio.x);
				}
				mCSB_container.css("left",cx);
				mCSB_dragger[1].css("left",dx);
				_scrollTo($this,"_resetX");
			}
		},
		/* -------------------- */
		
		
		/* binds scrollbar events */
		_bindEvents=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt;
			if(!d.bindEvents){ /* check if events are already bound */
				_draggable.call(this);
				if(o.contentTouchScroll){_contentDraggable.call(this);}
				_selectable.call(this);
				if(o.mouseWheel.enable){ /* bind mousewheel fn when plugin is available */
					function _mwt(){
						mousewheelTimeout=setTimeout(function(){
							if(!$.event.special.mousewheel){
								_mwt();
							}else{
								clearTimeout(mousewheelTimeout);
								_mousewheel.call($this[0]);
							}
						},100);
					}
					var mousewheelTimeout;
					_mwt();
				}
				_draggerRail.call(this);
				_wrapperScroll.call(this);
				if(o.advanced.autoScrollOnFocus){_focus.call(this);}
				if(o.scrollButtons.enable){_buttons.call(this);}
				if(o.keyboard.enable){_keyboard.call(this);}
				d.bindEvents=true;
			}
		},
		/* -------------------- */
		
		
		/* unbinds scrollbar events */
		_unbindEvents=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				namespace=pluginPfx+"_"+d.idx,
				sb=".mCSB_"+d.idx+"_scrollbar",
				sel=$("#mCSB_"+d.idx+",#mCSB_"+d.idx+"_container,#mCSB_"+d.idx+"_container_wrapper,"+sb+" ."+classes[12]+",#mCSB_"+d.idx+"_dragger_vertical,#mCSB_"+d.idx+"_dragger_horizontal,"+sb+">a"),
				mCSB_container=$("#mCSB_"+d.idx+"_container");
			if(o.advanced.releaseDraggableSelectors){sel.add($(o.advanced.releaseDraggableSelectors));}
			if(o.advanced.extraDraggableSelectors){sel.add($(o.advanced.extraDraggableSelectors));}
			if(d.bindEvents){ /* check if events are bound */
				/* unbind namespaced events from document/selectors */
				$(document).add($(!_canAccessIFrame() || top.document)).unbind("."+namespace);
				sel.each(function(){
					$(this).unbind("."+namespace);
				});
				/* clear and delete timeouts/objects */
				clearTimeout($this[0]._focusTimeout); _delete($this[0],"_focusTimeout");
				clearTimeout(d.sequential.step); _delete(d.sequential,"step");
				clearTimeout(mCSB_container[0].onCompleteTimeout); _delete(mCSB_container[0],"onCompleteTimeout");
				d.bindEvents=false;
			}
		},
		/* -------------------- */
		
		
		/* toggles scrollbar visibility */
		_scrollbarVisibility=function(disabled){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				contentWrapper=$("#mCSB_"+d.idx+"_container_wrapper"),
				content=contentWrapper.length ? contentWrapper : $("#mCSB_"+d.idx+"_container"),
				scrollbar=[$("#mCSB_"+d.idx+"_scrollbar_vertical"),$("#mCSB_"+d.idx+"_scrollbar_horizontal")],
				mCSB_dragger=[scrollbar[0].find(".mCSB_dragger"),scrollbar[1].find(".mCSB_dragger")];
			if(o.axis!=="x"){
				if(d.overflowed[0] && !disabled){
					scrollbar[0].add(mCSB_dragger[0]).add(scrollbar[0].children("a")).css("display","block");
					content.removeClass(classes[8]+" "+classes[10]);
				}else{
					if(o.alwaysShowScrollbar){
						if(o.alwaysShowScrollbar!==2){mCSB_dragger[0].css("display","none");}
						content.removeClass(classes[10]);
					}else{
						scrollbar[0].css("display","none");
						content.addClass(classes[10]);
					}
					content.addClass(classes[8]);
				}
			}
			if(o.axis!=="y"){
				if(d.overflowed[1] && !disabled){
					scrollbar[1].add(mCSB_dragger[1]).add(scrollbar[1].children("a")).css("display","block");
					content.removeClass(classes[9]+" "+classes[11]);
				}else{
					if(o.alwaysShowScrollbar){
						if(o.alwaysShowScrollbar!==2){mCSB_dragger[1].css("display","none");}
						content.removeClass(classes[11]);
					}else{
						scrollbar[1].css("display","none");
						content.addClass(classes[11]);
					}
					content.addClass(classes[9]);
				}
			}
			if(!d.overflowed[0] && !d.overflowed[1]){
				$this.addClass(classes[5]);
			}else{
				$this.removeClass(classes[5]);
			}
		},
		/* -------------------- */
		
		
		/* returns input coordinates of pointer, touch and mouse events (relative to document) */
		_coordinates=function(e){
			var t=e.type,o=e.target.ownerDocument!==document && frameElement!==null ? [$(frameElement).offset().top,$(frameElement).offset().left] : null,
				io=_canAccessIFrame() && e.target.ownerDocument!==top.document && frameElement!==null ? [$(e.view.frameElement).offset().top,$(e.view.frameElement).offset().left] : [0,0];
			switch(t){
				case "pointerdown": case "MSPointerDown": case "pointermove": case "MSPointerMove": case "pointerup": case "MSPointerUp":
					return o ? [e.originalEvent.pageY-o[0]+io[0],e.originalEvent.pageX-o[1]+io[1],false] : [e.originalEvent.pageY,e.originalEvent.pageX,false];
					break;
				case "touchstart": case "touchmove": case "touchend":
					var touch=e.originalEvent.touches[0] || e.originalEvent.changedTouches[0],
						touches=e.originalEvent.touches.length || e.originalEvent.changedTouches.length;
					return e.target.ownerDocument!==document ? [touch.screenY,touch.screenX,touches>1] : [touch.pageY,touch.pageX,touches>1];
					break;
				default:
					return o ? [e.pageY-o[0]+io[0],e.pageX-o[1]+io[1],false] : [e.pageY,e.pageX,false];
			}
		},
		/* -------------------- */
		
		
		/* 
		SCROLLBAR DRAG EVENTS
		scrolls content via scrollbar dragging 
		*/
		_draggable=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				namespace=pluginPfx+"_"+d.idx,
				draggerId=["mCSB_"+d.idx+"_dragger_vertical","mCSB_"+d.idx+"_dragger_horizontal"],
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				mCSB_dragger=$("#"+draggerId[0]+",#"+draggerId[1]),
				draggable,dragY,dragX,
				rds=o.advanced.releaseDraggableSelectors ? mCSB_dragger.add($(o.advanced.releaseDraggableSelectors)) : mCSB_dragger,
				eds=o.advanced.extraDraggableSelectors ? $(!_canAccessIFrame() || top.document).add($(o.advanced.extraDraggableSelectors)) : $(!_canAccessIFrame() || top.document);
			mCSB_dragger.bind("contextmenu."+namespace,function(e){
				e.preventDefault(); //prevent right click
			}).bind("mousedown."+namespace+" touchstart."+namespace+" pointerdown."+namespace+" MSPointerDown."+namespace,function(e){
				e.stopImmediatePropagation();
				e.preventDefault();
				if(!_mouseBtnLeft(e)){return;} /* left mouse button only */
				touchActive=true;
				if(oldIE){document.onselectstart=function(){return false;}} /* disable text selection for IE < 9 */
				_iframe.call(mCSB_container,false); /* enable scrollbar dragging over iframes by disabling their events */
				_stop($this);
				draggable=$(this);
				var offset=draggable.offset(),y=_coordinates(e)[0]-offset.top,x=_coordinates(e)[1]-offset.left,
					h=draggable.height()+offset.top,w=draggable.width()+offset.left;
				if(y<h && y>0 && x<w && x>0){
					dragY=y; 
					dragX=x;
				}
				_onDragClasses(draggable,"active",o.autoExpandScrollbar); 
			}).bind("touchmove."+namespace,function(e){
				e.stopImmediatePropagation();
				e.preventDefault();
				var offset=draggable.offset(),y=_coordinates(e)[0]-offset.top,x=_coordinates(e)[1]-offset.left;
				_drag(dragY,dragX,y,x);
			});
			$(document).add(eds).bind("mousemove."+namespace+" pointermove."+namespace+" MSPointerMove."+namespace,function(e){
				if(draggable){
					var offset=draggable.offset(),y=_coordinates(e)[0]-offset.top,x=_coordinates(e)[1]-offset.left;
					if(dragY===y && dragX===x){return;} /* has it really moved? */
					_drag(dragY,dragX,y,x);
				}
			}).add(rds).bind("mouseup."+namespace+" touchend."+namespace+" pointerup."+namespace+" MSPointerUp."+namespace,function(e){
				if(draggable){
					_onDragClasses(draggable,"active",o.autoExpandScrollbar); 
					draggable=null;
				}
				touchActive=false;
				if(oldIE){document.onselectstart=null;} /* enable text selection for IE < 9 */
				_iframe.call(mCSB_container,true); /* enable iframes events */
			});
			function _drag(dragY,dragX,y,x){
				mCSB_container[0].idleTimer=o.scrollInertia<233 ? 250 : 0;
				if(draggable.attr("id")===draggerId[1]){
					var dir="x",to=((draggable[0].offsetLeft-dragX)+x)*d.scrollRatio.x;
				}else{
					var dir="y",to=((draggable[0].offsetTop-dragY)+y)*d.scrollRatio.y;
				}
				_scrollTo($this,to.toString(),{dir:dir,drag:true});
			}
		},
		/* -------------------- */
		
		
		/* 
		TOUCH SWIPE EVENTS
		scrolls content via touch swipe 
		Emulates the native touch-swipe scrolling with momentum found in iOS, Android and WP devices 
		*/
		_contentDraggable=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				namespace=pluginPfx+"_"+d.idx,
				mCustomScrollBox=$("#mCSB_"+d.idx),
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				mCSB_dragger=[$("#mCSB_"+d.idx+"_dragger_vertical"),$("#mCSB_"+d.idx+"_dragger_horizontal")],
				draggable,dragY,dragX,touchStartY,touchStartX,touchMoveY=[],touchMoveX=[],startTime,runningTime,endTime,distance,speed,amount,
				durA=0,durB,overwrite=o.axis==="yx" ? "none" : "all",touchIntent=[],touchDrag,docDrag,
				iframe=mCSB_container.find("iframe"),
				events=[
					"touchstart."+namespace+" pointerdown."+namespace+" MSPointerDown."+namespace, //start
					"touchmove."+namespace+" pointermove."+namespace+" MSPointerMove."+namespace, //move
					"touchend."+namespace+" pointerup."+namespace+" MSPointerUp."+namespace //end
				],
				touchAction=document.body.style.touchAction!==undefined && document.body.style.touchAction!=="";
			mCSB_container.bind(events[0],function(e){
				_onTouchstart(e);
			}).bind(events[1],function(e){
				_onTouchmove(e);
			});
			mCustomScrollBox.bind(events[0],function(e){
				_onTouchstart2(e);
			}).bind(events[2],function(e){
				_onTouchend(e);
			});
			if(iframe.length){
				iframe.each(function(){
					$(this).bind("load",function(){
						/* bind events on accessible iframes */
						if(_canAccessIFrame(this)){
							$(this.contentDocument || this.contentWindow.document).bind(events[0],function(e){
								_onTouchstart(e);
								_onTouchstart2(e);
							}).bind(events[1],function(e){
								_onTouchmove(e);
							}).bind(events[2],function(e){
								_onTouchend(e);
							});
						}
					});
				});
			}
			function _onTouchstart(e){
				if(!_pointerTouch(e) || touchActive || _coordinates(e)[2]){touchable=0; return;}
				touchable=1; touchDrag=0; docDrag=0; draggable=1;
				$this.removeClass("mCS_touch_action");
				var offset=mCSB_container.offset();
				dragY=_coordinates(e)[0]-offset.top;
				dragX=_coordinates(e)[1]-offset.left;
				touchIntent=[_coordinates(e)[0],_coordinates(e)[1]];
			}
			function _onTouchmove(e){
				if(!_pointerTouch(e) || touchActive || _coordinates(e)[2]){return;}
				if(!o.documentTouchScroll){e.preventDefault();} 
				e.stopImmediatePropagation();
				if(docDrag && !touchDrag){return;}
				if(draggable){
					runningTime=_getTime();
					var offset=mCustomScrollBox.offset(),y=_coordinates(e)[0]-offset.top,x=_coordinates(e)[1]-offset.left,
						easing="mcsLinearOut";
					touchMoveY.push(y);
					touchMoveX.push(x);
					touchIntent[2]=Math.abs(_coordinates(e)[0]-touchIntent[0]); touchIntent[3]=Math.abs(_coordinates(e)[1]-touchIntent[1]);
					if(d.overflowed[0]){
						var limit=mCSB_dragger[0].parent().height()-mCSB_dragger[0].height(),
							prevent=((dragY-y)>0 && (y-dragY)>-(limit*d.scrollRatio.y) && (touchIntent[3]*2<touchIntent[2] || o.axis==="yx"));
					}
					if(d.overflowed[1]){
						var limitX=mCSB_dragger[1].parent().width()-mCSB_dragger[1].width(),
							preventX=((dragX-x)>0 && (x-dragX)>-(limitX*d.scrollRatio.x) && (touchIntent[2]*2<touchIntent[3] || o.axis==="yx"));
					}
					if(prevent || preventX){ /* prevent native document scrolling */
						if(!touchAction){e.preventDefault();} 
						touchDrag=1;
					}else{
						docDrag=1;
						$this.addClass("mCS_touch_action");
					}
					if(touchAction){e.preventDefault();} 
					amount=o.axis==="yx" ? [(dragY-y),(dragX-x)] : o.axis==="x" ? [null,(dragX-x)] : [(dragY-y),null];
					mCSB_container[0].idleTimer=250;
					if(d.overflowed[0]){_drag(amount[0],durA,easing,"y","all",true);}
					if(d.overflowed[1]){_drag(amount[1],durA,easing,"x",overwrite,true);}
				}
			}
			function _onTouchstart2(e){
				if(!_pointerTouch(e) || touchActive || _coordinates(e)[2]){touchable=0; return;}
				touchable=1;
				e.stopImmediatePropagation();
				_stop($this);
				startTime=_getTime();
				var offset=mCustomScrollBox.offset();
				touchStartY=_coordinates(e)[0]-offset.top;
				touchStartX=_coordinates(e)[1]-offset.left;
				touchMoveY=[]; touchMoveX=[];
			}
			function _onTouchend(e){
				if(!_pointerTouch(e) || touchActive || _coordinates(e)[2]){return;}
				draggable=0;
				e.stopImmediatePropagation();
				touchDrag=0; docDrag=0;
				endTime=_getTime();
				var offset=mCustomScrollBox.offset(),y=_coordinates(e)[0]-offset.top,x=_coordinates(e)[1]-offset.left;
				if((endTime-runningTime)>30){return;}
				speed=1000/(endTime-startTime);
				var easing="mcsEaseOut",slow=speed<2.5,
					diff=slow ? [touchMoveY[touchMoveY.length-2],touchMoveX[touchMoveX.length-2]] : [0,0];
				distance=slow ? [(y-diff[0]),(x-diff[1])] : [y-touchStartY,x-touchStartX];
				var absDistance=[Math.abs(distance[0]),Math.abs(distance[1])];
				speed=slow ? [Math.abs(distance[0]/4),Math.abs(distance[1]/4)] : [speed,speed];
				var a=[
					Math.abs(mCSB_container[0].offsetTop)-(distance[0]*_m((absDistance[0]/speed[0]),speed[0])),
					Math.abs(mCSB_container[0].offsetLeft)-(distance[1]*_m((absDistance[1]/speed[1]),speed[1]))
				];
				amount=o.axis==="yx" ? [a[0],a[1]] : o.axis==="x" ? [null,a[1]] : [a[0],null];
				durB=[(absDistance[0]*4)+o.scrollInertia,(absDistance[1]*4)+o.scrollInertia];
				var md=parseInt(o.contentTouchScroll) || 0; /* absolute minimum distance required */
				amount[0]=absDistance[0]>md ? amount[0] : 0;
				amount[1]=absDistance[1]>md ? amount[1] : 0;
				if(d.overflowed[0]){_drag(amount[0],durB[0],easing,"y",overwrite,false);}
				if(d.overflowed[1]){_drag(amount[1],durB[1],easing,"x",overwrite,false);}
			}
			function _m(ds,s){
				var r=[s*1.5,s*2,s/1.5,s/2];
				if(ds>90){
					return s>4 ? r[0] : r[3];
				}else if(ds>60){
					return s>3 ? r[3] : r[2];
				}else if(ds>30){
					return s>8 ? r[1] : s>6 ? r[0] : s>4 ? s : r[2];
				}else{
					return s>8 ? s : r[3];
				}
			}
			function _drag(amount,dur,easing,dir,overwrite,drag){
				if(!amount){return;}
				_scrollTo($this,amount.toString(),{dur:dur,scrollEasing:easing,dir:dir,overwrite:overwrite,drag:drag});
			}
		},
		/* -------------------- */
		
		
		/* 
		SELECT TEXT EVENTS 
		scrolls content when text is selected 
		*/
		_selectable=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,seq=d.sequential,
				namespace=pluginPfx+"_"+d.idx,
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				wrapper=mCSB_container.parent(),
				action;
			mCSB_container.bind("mousedown."+namespace,function(e){
				if(touchable){return;}
				if(!action){action=1; touchActive=true;}
			}).add(document).bind("mousemove."+namespace,function(e){
				if(!touchable && action && _sel()){
					var offset=mCSB_container.offset(),
						y=_coordinates(e)[0]-offset.top+mCSB_container[0].offsetTop,x=_coordinates(e)[1]-offset.left+mCSB_container[0].offsetLeft;
					if(y>0 && y<wrapper.height() && x>0 && x<wrapper.width()){
						if(seq.step){_seq("off",null,"stepped");}
					}else{
						if(o.axis!=="x" && d.overflowed[0]){
							if(y<0){
								_seq("on",38);
							}else if(y>wrapper.height()){
								_seq("on",40);
							}
						}
						if(o.axis!=="y" && d.overflowed[1]){
							if(x<0){
								_seq("on",37);
							}else if(x>wrapper.width()){
								_seq("on",39);
							}
						}
					}
				}
			}).bind("mouseup."+namespace+" dragend."+namespace,function(e){
				if(touchable){return;}
				if(action){action=0; _seq("off",null);}
				touchActive=false;
			});
			function _sel(){
				return 	window.getSelection ? window.getSelection().toString() : 
						document.selection && document.selection.type!="Control" ? document.selection.createRange().text : 0;
			}
			function _seq(a,c,s){
				seq.type=s && action ? "stepped" : "stepless";
				seq.scrollAmount=10;
				_sequentialScroll($this,a,c,"mcsLinearOut",s ? 60 : null);
			}
		},
		/* -------------------- */
		
		
		/* 
		MOUSE WHEEL EVENT
		scrolls content via mouse-wheel 
		via mouse-wheel plugin (https://github.com/brandonaaron/jquery-mousewheel)
		*/
		_mousewheel=function(){
			if(!$(this).data(pluginPfx)){return;} /* Check if the scrollbar is ready to use mousewheel events (issue: #185) */
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				namespace=pluginPfx+"_"+d.idx,
				mCustomScrollBox=$("#mCSB_"+d.idx),
				mCSB_dragger=[$("#mCSB_"+d.idx+"_dragger_vertical"),$("#mCSB_"+d.idx+"_dragger_horizontal")],
				iframe=$("#mCSB_"+d.idx+"_container").find("iframe");
			if(iframe.length){
				iframe.each(function(){
					$(this).bind("load",function(){
						/* bind events on accessible iframes */
						if(_canAccessIFrame(this)){
							$(this.contentDocument || this.contentWindow.document).bind("mousewheel."+namespace,function(e,delta){
								_onMousewheel(e,delta);
							});
						}
					});
				});
			}
			mCustomScrollBox.bind("mousewheel."+namespace,function(e,delta){
				_onMousewheel(e,delta);
			});
			function _onMousewheel(e,delta){
				_stop($this);
				if(_disableMousewheel($this,e.target)){return;} /* disables mouse-wheel when hovering specific elements */
				var deltaFactor=o.mouseWheel.deltaFactor!=="auto" ? parseInt(o.mouseWheel.deltaFactor) : (oldIE && e.deltaFactor<100) ? 100 : e.deltaFactor || 100,
					dur=o.scrollInertia;
				if(o.axis==="x" || o.mouseWheel.axis==="x"){
					var dir="x",
						px=[Math.round(deltaFactor*d.scrollRatio.x),parseInt(o.mouseWheel.scrollAmount)],
						amount=o.mouseWheel.scrollAmount!=="auto" ? px[1] : px[0]>=mCustomScrollBox.width() ? mCustomScrollBox.width()*0.9 : px[0],
						contentPos=Math.abs($("#mCSB_"+d.idx+"_container")[0].offsetLeft),
						draggerPos=mCSB_dragger[1][0].offsetLeft,
						limit=mCSB_dragger[1].parent().width()-mCSB_dragger[1].width(),
						dlt=o.mouseWheel.axis==="y" ? (e.deltaY || delta) : e.deltaX;
				}else{
					var dir="y",
						px=[Math.round(deltaFactor*d.scrollRatio.y),parseInt(o.mouseWheel.scrollAmount)],
						amount=o.mouseWheel.scrollAmount!=="auto" ? px[1] : px[0]>=mCustomScrollBox.height() ? mCustomScrollBox.height()*0.9 : px[0],
						contentPos=Math.abs($("#mCSB_"+d.idx+"_container")[0].offsetTop),
						draggerPos=mCSB_dragger[0][0].offsetTop,
						limit=mCSB_dragger[0].parent().height()-mCSB_dragger[0].height(),
						dlt=e.deltaY || delta;
				}
				if((dir==="y" && !d.overflowed[0]) || (dir==="x" && !d.overflowed[1])){return;}
				if(o.mouseWheel.invert || e.webkitDirectionInvertedFromDevice){dlt=-dlt;}
				if(o.mouseWheel.normalizeDelta){dlt=dlt<0 ? -1 : 1;}
				if((dlt>0 && draggerPos!==0) || (dlt<0 && draggerPos!==limit) || o.mouseWheel.preventDefault){
					e.stopImmediatePropagation();
					e.preventDefault();
				}
				if(e.deltaFactor<5 && !o.mouseWheel.normalizeDelta){
					//very low deltaFactor values mean some kind of delta acceleration (e.g. osx trackpad), so adjusting scrolling accordingly
					amount=e.deltaFactor; dur=17;
				}
				_scrollTo($this,(contentPos-(dlt*amount)).toString(),{dir:dir,dur:dur});
			}
		},
		/* -------------------- */
		
		
		/* checks if iframe can be accessed */
		_canAccessIFrameCache=new Object(),
		_canAccessIFrame=function(iframe){
		    var result=false,cacheKey=false,html=null;
		    if(iframe===undefined){
				cacheKey="#empty";
		    }else if($(iframe).attr("id")!==undefined){
				cacheKey=$(iframe).attr("id");
		    }
			if(cacheKey!==false && _canAccessIFrameCache[cacheKey]!==undefined){
				return _canAccessIFrameCache[cacheKey];
			}
			if(!iframe){
				try{
					var doc=top.document;
					html=doc.body.innerHTML;
				}catch(err){/* do nothing */}
				result=(html!==null);
			}else{
				try{
					var doc=iframe.contentDocument || iframe.contentWindow.document;
					html=doc.body.innerHTML;
				}catch(err){/* do nothing */}
				result=(html!==null);
			}
			if(cacheKey!==false){_canAccessIFrameCache[cacheKey]=result;}
			return result;
		},
		/* -------------------- */
		
		
		/* switches iframe's pointer-events property (drag, mousewheel etc. over cross-domain iframes) */
		_iframe=function(evt){
			var el=this.find("iframe");
			if(!el.length){return;} /* check if content contains iframes */
			var val=!evt ? "none" : "auto";
			el.css("pointer-events",val); /* for IE11, iframe's display property should not be "block" */
		},
		/* -------------------- */
		
		
		/* disables mouse-wheel when hovering specific elements like select, datalist etc. */
		_disableMousewheel=function(el,target){
			var tag=target.nodeName.toLowerCase(),
				tags=el.data(pluginPfx).opt.mouseWheel.disableOver,
				/* elements that require focus */
				focusTags=["select","textarea"];
			return $.inArray(tag,tags) > -1 && !($.inArray(tag,focusTags) > -1 && !$(target).is(":focus"));
		},
		/* -------------------- */
		
		
		/* 
		DRAGGER RAIL CLICK EVENT
		scrolls content via dragger rail 
		*/
		_draggerRail=function(){
			var $this=$(this),d=$this.data(pluginPfx),
				namespace=pluginPfx+"_"+d.idx,
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				wrapper=mCSB_container.parent(),
				mCSB_draggerContainer=$(".mCSB_"+d.idx+"_scrollbar ."+classes[12]),
				clickable;
			mCSB_draggerContainer.bind("mousedown."+namespace+" touchstart."+namespace+" pointerdown."+namespace+" MSPointerDown."+namespace,function(e){
				touchActive=true;
				if(!$(e.target).hasClass("mCSB_dragger")){clickable=1;}
			}).bind("touchend."+namespace+" pointerup."+namespace+" MSPointerUp."+namespace,function(e){
				touchActive=false;
			}).bind("click."+namespace,function(e){
				if(!clickable){return;}
				clickable=0;
				if($(e.target).hasClass(classes[12]) || $(e.target).hasClass("mCSB_draggerRail")){
					_stop($this);
					var el=$(this),mCSB_dragger=el.find(".mCSB_dragger");
					if(el.parent(".mCSB_scrollTools_horizontal").length>0){
						if(!d.overflowed[1]){return;}
						var dir="x",
							clickDir=e.pageX>mCSB_dragger.offset().left ? -1 : 1,
							to=Math.abs(mCSB_container[0].offsetLeft)-(clickDir*(wrapper.width()*0.9));
					}else{
						if(!d.overflowed[0]){return;}
						var dir="y",
							clickDir=e.pageY>mCSB_dragger.offset().top ? -1 : 1,
							to=Math.abs(mCSB_container[0].offsetTop)-(clickDir*(wrapper.height()*0.9));
					}
					_scrollTo($this,to.toString(),{dir:dir,scrollEasing:"mcsEaseInOut"});
				}
			});
		},
		/* -------------------- */
		
		
		/* 
		FOCUS EVENT
		scrolls content via element focus (e.g. clicking an input, pressing TAB key etc.)
		*/
		_focus=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				namespace=pluginPfx+"_"+d.idx,
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				wrapper=mCSB_container.parent();
			mCSB_container.bind("focusin."+namespace,function(e){
				var el=$(document.activeElement),
					nested=mCSB_container.find(".mCustomScrollBox").length,
					dur=0;
				if(!el.is(o.advanced.autoScrollOnFocus)){return;}
				_stop($this);
				clearTimeout($this[0]._focusTimeout);
				$this[0]._focusTimer=nested ? (dur+17)*nested : 0;
				$this[0]._focusTimeout=setTimeout(function(){
					var	to=[_childPos(el)[0],_childPos(el)[1]],
						contentPos=[mCSB_container[0].offsetTop,mCSB_container[0].offsetLeft],
						isVisible=[
							(contentPos[0]+to[0]>=0 && contentPos[0]+to[0]<wrapper.height()-el.outerHeight(false)),
							(contentPos[1]+to[1]>=0 && contentPos[0]+to[1]<wrapper.width()-el.outerWidth(false))
						],
						overwrite=(o.axis==="yx" && !isVisible[0] && !isVisible[1]) ? "none" : "all";
					if(o.axis!=="x" && !isVisible[0]){
						_scrollTo($this,to[0].toString(),{dir:"y",scrollEasing:"mcsEaseInOut",overwrite:overwrite,dur:dur});
					}
					if(o.axis!=="y" && !isVisible[1]){
						_scrollTo($this,to[1].toString(),{dir:"x",scrollEasing:"mcsEaseInOut",overwrite:overwrite,dur:dur});
					}
				},$this[0]._focusTimer);
			});
		},
		/* -------------------- */
		
		
		/* sets content wrapper scrollTop/scrollLeft always to 0 */
		_wrapperScroll=function(){
			var $this=$(this),d=$this.data(pluginPfx),
				namespace=pluginPfx+"_"+d.idx,
				wrapper=$("#mCSB_"+d.idx+"_container").parent();
			wrapper.bind("scroll."+namespace,function(e){
				if(wrapper.scrollTop()!==0 || wrapper.scrollLeft()!==0){
					$(".mCSB_"+d.idx+"_scrollbar").css("visibility","hidden"); /* hide scrollbar(s) */
				}
			});
		},
		/* -------------------- */
		
		
		/* 
		BUTTONS EVENTS
		scrolls content via up, down, left and right buttons 
		*/
		_buttons=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,seq=d.sequential,
				namespace=pluginPfx+"_"+d.idx,
				sel=".mCSB_"+d.idx+"_scrollbar",
				btn=$(sel+">a");
			btn.bind("contextmenu."+namespace,function(e){
				e.preventDefault(); //prevent right click
			}).bind("mousedown."+namespace+" touchstart."+namespace+" pointerdown."+namespace+" MSPointerDown."+namespace+" mouseup."+namespace+" touchend."+namespace+" pointerup."+namespace+" MSPointerUp."+namespace+" mouseout."+namespace+" pointerout."+namespace+" MSPointerOut."+namespace+" click."+namespace,function(e){
				e.preventDefault();
				if(!_mouseBtnLeft(e)){return;} /* left mouse button only */
				var btnClass=$(this).attr("class");
				seq.type=o.scrollButtons.scrollType;
				switch(e.type){
					case "mousedown": case "touchstart": case "pointerdown": case "MSPointerDown":
						if(seq.type==="stepped"){return;}
						touchActive=true;
						d.tweenRunning=false;
						_seq("on",btnClass);
						break;
					case "mouseup": case "touchend": case "pointerup": case "MSPointerUp":
					case "mouseout": case "pointerout": case "MSPointerOut":
						if(seq.type==="stepped"){return;}
						touchActive=false;
						if(seq.dir){_seq("off",btnClass);}
						break;
					case "click":
						if(seq.type!=="stepped" || d.tweenRunning){return;}
						_seq("on",btnClass);
						break;
				}
				function _seq(a,c){
					seq.scrollAmount=o.scrollButtons.scrollAmount;
					_sequentialScroll($this,a,c);
				}
			});
		},
		/* -------------------- */
		
		
		/* 
		KEYBOARD EVENTS
		scrolls content via keyboard 
		Keys: up arrow, down arrow, left arrow, right arrow, PgUp, PgDn, Home, End
		*/
		_keyboard=function(){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,seq=d.sequential,
				namespace=pluginPfx+"_"+d.idx,
				mCustomScrollBox=$("#mCSB_"+d.idx),
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				wrapper=mCSB_container.parent(),
				editables="input,textarea,select,datalist,keygen,[contenteditable='true']",
				iframe=mCSB_container.find("iframe"),
				events=["blur."+namespace+" keydown."+namespace+" keyup."+namespace];
			if(iframe.length){
				iframe.each(function(){
					$(this).bind("load",function(){
						/* bind events on accessible iframes */
						if(_canAccessIFrame(this)){
							$(this.contentDocument || this.contentWindow.document).bind(events[0],function(e){
								_onKeyboard(e);
							});
						}
					});
				});
			}
			mCustomScrollBox.attr("tabindex","0").bind(events[0],function(e){
				_onKeyboard(e);
			});
			function _onKeyboard(e){
				switch(e.type){
					case "blur":
						if(d.tweenRunning && seq.dir){_seq("off",null);}
						break;
					case "keydown": case "keyup":
						var code=e.keyCode ? e.keyCode : e.which,action="on";
						if((o.axis!=="x" && (code===38 || code===40)) || (o.axis!=="y" && (code===37 || code===39))){
							/* up (38), down (40), left (37), right (39) arrows */
							if(((code===38 || code===40) && !d.overflowed[0]) || ((code===37 || code===39) && !d.overflowed[1])){return;}
							if(e.type==="keyup"){action="off";}
							if(!$(document.activeElement).is(editables)){
								e.preventDefault();
								e.stopImmediatePropagation();
								_seq(action,code);
							}
						}else if(code===33 || code===34){
							/* PgUp (33), PgDn (34) */
							if(d.overflowed[0] || d.overflowed[1]){
								e.preventDefault();
								e.stopImmediatePropagation();
							}
							if(e.type==="keyup"){
								_stop($this);
								var keyboardDir=code===34 ? -1 : 1;
								if(o.axis==="x" || (o.axis==="yx" && d.overflowed[1] && !d.overflowed[0])){
									var dir="x",to=Math.abs(mCSB_container[0].offsetLeft)-(keyboardDir*(wrapper.width()*0.9));
								}else{
									var dir="y",to=Math.abs(mCSB_container[0].offsetTop)-(keyboardDir*(wrapper.height()*0.9));
								}
								_scrollTo($this,to.toString(),{dir:dir,scrollEasing:"mcsEaseInOut"});
							}
						}else if(code===35 || code===36){
							/* End (35), Home (36) */
							if(!$(document.activeElement).is(editables)){
								if(d.overflowed[0] || d.overflowed[1]){
									e.preventDefault();
									e.stopImmediatePropagation();
								}
								if(e.type==="keyup"){
									if(o.axis==="x" || (o.axis==="yx" && d.overflowed[1] && !d.overflowed[0])){
										var dir="x",to=code===35 ? Math.abs(wrapper.width()-mCSB_container.outerWidth(false)) : 0;
									}else{
										var dir="y",to=code===35 ? Math.abs(wrapper.height()-mCSB_container.outerHeight(false)) : 0;
									}
									_scrollTo($this,to.toString(),{dir:dir,scrollEasing:"mcsEaseInOut"});
								}
							}
						}
						break;
				}
				function _seq(a,c){
					seq.type=o.keyboard.scrollType;
					seq.scrollAmount=o.keyboard.scrollAmount;
					if(seq.type==="stepped" && d.tweenRunning){return;}
					_sequentialScroll($this,a,c);
				}
			}
		},
		/* -------------------- */
		
		
		/* scrolls content sequentially (used when scrolling via buttons, keyboard arrows etc.) */
		_sequentialScroll=function(el,action,trigger,e,s){
			var d=el.data(pluginPfx),o=d.opt,seq=d.sequential,
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				once=seq.type==="stepped" ? true : false,
				steplessSpeed=o.scrollInertia < 26 ? 26 : o.scrollInertia, /* 26/1.5=17 */
				steppedSpeed=o.scrollInertia < 1 ? 17 : o.scrollInertia;
			switch(action){
				case "on":
					seq.dir=[
						(trigger===classes[16] || trigger===classes[15] || trigger===39 || trigger===37 ? "x" : "y"),
						(trigger===classes[13] || trigger===classes[15] || trigger===38 || trigger===37 ? -1 : 1)
					];
					_stop(el);
					if(_isNumeric(trigger) && seq.type==="stepped"){return;}
					_on(once);
					break;
				case "off":
					_off();
					if(once || (d.tweenRunning && seq.dir)){
						_on(true);
					}
					break;
			}
			
			/* starts sequence */
			function _on(once){
				if(o.snapAmount){seq.scrollAmount=!(o.snapAmount instanceof Array) ? o.snapAmount : seq.dir[0]==="x" ? o.snapAmount[1] : o.snapAmount[0];} /* scrolling snapping */
				var c=seq.type!=="stepped", /* continuous scrolling */
					t=s ? s : !once ? 1000/60 : c ? steplessSpeed/1.5 : steppedSpeed, /* timer */
					m=!once ? 2.5 : c ? 7.5 : 40, /* multiplier */
					contentPos=[Math.abs(mCSB_container[0].offsetTop),Math.abs(mCSB_container[0].offsetLeft)],
					ratio=[d.scrollRatio.y>10 ? 10 : d.scrollRatio.y,d.scrollRatio.x>10 ? 10 : d.scrollRatio.x],
					amount=seq.dir[0]==="x" ? contentPos[1]+(seq.dir[1]*(ratio[1]*m)) : contentPos[0]+(seq.dir[1]*(ratio[0]*m)),
					px=seq.dir[0]==="x" ? contentPos[1]+(seq.dir[1]*parseInt(seq.scrollAmount)) : contentPos[0]+(seq.dir[1]*parseInt(seq.scrollAmount)),
					to=seq.scrollAmount!=="auto" ? px : amount,
					easing=e ? e : !once ? "mcsLinear" : c ? "mcsLinearOut" : "mcsEaseInOut",
					onComplete=!once ? false : true;
				if(once && t<17){
					to=seq.dir[0]==="x" ? contentPos[1] : contentPos[0];
				}
				_scrollTo(el,to.toString(),{dir:seq.dir[0],scrollEasing:easing,dur:t,onComplete:onComplete});
				if(once){
					seq.dir=false;
					return;
				}
				clearTimeout(seq.step);
				seq.step=setTimeout(function(){
					_on();
				},t);
			}
			/* stops sequence */
			function _off(){
				clearTimeout(seq.step);
				_delete(seq,"step");
				_stop(el);
			}
		},
		/* -------------------- */
		
		
		/* returns a yx array from value */
		_arr=function(val){
			var o=$(this).data(pluginPfx).opt,vals=[];
			if(typeof val==="function"){val=val();} /* check if the value is a single anonymous function */
			/* check if value is object or array, its length and create an array with yx values */
			if(!(val instanceof Array)){ /* object value (e.g. {y:"100",x:"100"}, 100 etc.) */
				vals[0]=val.y ? val.y : val.x || o.axis==="x" ? null : val;
				vals[1]=val.x ? val.x : val.y || o.axis==="y" ? null : val;
			}else{ /* array value (e.g. [100,100]) */
				vals=val.length>1 ? [val[0],val[1]] : o.axis==="x" ? [null,val[0]] : [val[0],null];
			}
			/* check if array values are anonymous functions */
			if(typeof vals[0]==="function"){vals[0]=vals[0]();}
			if(typeof vals[1]==="function"){vals[1]=vals[1]();}
			return vals;
		},
		/* -------------------- */
		
		
		/* translates values (e.g. "top", 100, "100px", "#id") to actual scroll-to positions */
		_to=function(val,dir){
			if(val==null || typeof val=="undefined"){return;}
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				wrapper=mCSB_container.parent(),
				t=typeof val;
			if(!dir){dir=o.axis==="x" ? "x" : "y";}
			var contentLength=dir==="x" ? mCSB_container.outerWidth(false)-wrapper.width() : mCSB_container.outerHeight(false)-wrapper.height(),
				contentPos=dir==="x" ? mCSB_container[0].offsetLeft : mCSB_container[0].offsetTop,
				cssProp=dir==="x" ? "left" : "top";
			switch(t){
				case "function": /* this currently is not used. Consider removing it */
					return val();
					break;
				case "object": /* js/jquery object */
					var obj=val.jquery ? val : $(val);
					if(!obj.length){return;}
					return dir==="x" ? _childPos(obj)[1] : _childPos(obj)[0];
					break;
				case "string": case "number":
					if(_isNumeric(val)){ /* numeric value */
						return Math.abs(val);
					}else if(val.indexOf("%")!==-1){ /* percentage value */
						return Math.abs(contentLength*parseInt(val)/100);
					}else if(val.indexOf("-=")!==-1){ /* decrease value */
						return Math.abs(contentPos-parseInt(val.split("-=")[1]));
					}else if(val.indexOf("+=")!==-1){ /* inrease value */
						var p=(contentPos+parseInt(val.split("+=")[1]));
						return p>=0 ? 0 : Math.abs(p);
					}else if(val.indexOf("px")!==-1 && _isNumeric(val.split("px")[0])){ /* pixels string value (e.g. "100px") */
						return Math.abs(val.split("px")[0]);
					}else{
						if(val==="top" || val==="left"){ /* special strings */
							return 0;
						}else if(val==="bottom"){
							return Math.abs(wrapper.height()-mCSB_container.outerHeight(false));
						}else if(val==="right"){
							return Math.abs(wrapper.width()-mCSB_container.outerWidth(false));
						}else if(val==="first" || val==="last"){
							var obj=mCSB_container.find(":"+val);
							return dir==="x" ? _childPos(obj)[1] : _childPos(obj)[0];
						}else{
							if($(val).length){ /* jquery selector */
								return dir==="x" ? _childPos($(val))[1] : _childPos($(val))[0];
							}else{ /* other values (e.g. "100em") */
								mCSB_container.css(cssProp,val);
								methods.update.call(null,$this[0]);
								return;
							}
						}
					}
					break;
			}
		},
		/* -------------------- */
		
		
		/* calls the update method automatically */
		_autoUpdate=function(rem){
			var $this=$(this),d=$this.data(pluginPfx),o=d.opt,
				mCSB_container=$("#mCSB_"+d.idx+"_container");
			if(rem){
				/* 
				removes autoUpdate timer 
				usage: _autoUpdate.call(this,"remove");
				*/
				clearTimeout(mCSB_container[0].autoUpdate);
				_delete(mCSB_container[0],"autoUpdate");
				return;
			}
			upd();
			function upd(){
				clearTimeout(mCSB_container[0].autoUpdate);
				if($this.parents("html").length===0){
					/* check element in dom tree */
					$this=null;
					return;
				}
				mCSB_container[0].autoUpdate=setTimeout(function(){
					/* update on specific selector(s) length and size change */
					if(o.advanced.updateOnSelectorChange){
						d.poll.change.n=sizesSum();
						if(d.poll.change.n!==d.poll.change.o){
							d.poll.change.o=d.poll.change.n;
							doUpd(3);
							return;
						}
					}
					/* update on main element and scrollbar size changes */
					if(o.advanced.updateOnContentResize){
						d.poll.size.n=$this[0].scrollHeight+$this[0].scrollWidth+mCSB_container[0].offsetHeight+$this[0].offsetHeight+$this[0].offsetWidth;
						if(d.poll.size.n!==d.poll.size.o){
							d.poll.size.o=d.poll.size.n;
							doUpd(1);
							return;
						}
					}
					/* update on image load */
					if(o.advanced.updateOnImageLoad){
						if(!(o.advanced.updateOnImageLoad==="auto" && o.axis==="y")){ //by default, it doesn't run on vertical content
							d.poll.img.n=mCSB_container.find("img").length;
							if(d.poll.img.n!==d.poll.img.o){
								d.poll.img.o=d.poll.img.n;
								mCSB_container.find("img").each(function(){
									imgLoader(this);
								});
								return;
							}
						}
					}
					if(o.advanced.updateOnSelectorChange || o.advanced.updateOnContentResize || o.advanced.updateOnImageLoad){upd();}
				},o.advanced.autoUpdateTimeout);
			}
			/* a tiny image loader */
			function imgLoader(el){
				if($(el).hasClass(classes[2])){doUpd(); return;}
				var img=new Image();
				function createDelegate(contextObject,delegateMethod){
					return function(){return delegateMethod.apply(contextObject,arguments);}
				}
				function imgOnLoad(){
					this.onload=null;
					$(el).addClass(classes[2]);
					doUpd(2);
				}
				img.onload=createDelegate(img,imgOnLoad);
				img.src=el.src;
			}
			/* returns the total height and width sum of all elements matching the selector */
			function sizesSum(){
				if(o.advanced.updateOnSelectorChange===true){o.advanced.updateOnSelectorChange="*";}
				var total=0,sel=mCSB_container.find(o.advanced.updateOnSelectorChange);
				if(o.advanced.updateOnSelectorChange && sel.length>0){sel.each(function(){total+=this.offsetHeight+this.offsetWidth;});}
				return total;
			}
			/* calls the update method */
			function doUpd(cb){
				clearTimeout(mCSB_container[0].autoUpdate);
				methods.update.call(null,$this[0],cb);
			}
		},
		/* -------------------- */
		
		
		/* snaps scrolling to a multiple of a pixels number */
		_snapAmount=function(to,amount,offset){
			return (Math.round(to/amount)*amount-offset); 
		},
		/* -------------------- */
		
		
		/* stops content and scrollbar animations */
		_stop=function(el){
			var d=el.data(pluginPfx),
				sel=$("#mCSB_"+d.idx+"_container,#mCSB_"+d.idx+"_container_wrapper,#mCSB_"+d.idx+"_dragger_vertical,#mCSB_"+d.idx+"_dragger_horizontal");
			sel.each(function(){
				_stopTween.call(this);
			});
		},
		/* -------------------- */
		
		
		/* 
		ANIMATES CONTENT 
		This is where the actual scrolling happens
		*/
		_scrollTo=function(el,to,options){
			var d=el.data(pluginPfx),o=d.opt,
				defaults={
					trigger:"internal",
					dir:"y",
					scrollEasing:"mcsEaseOut",
					drag:false,
					dur:o.scrollInertia,
					overwrite:"all",
					callbacks:true,
					onStart:true,
					onUpdate:true,
					onComplete:true
				},
				options=$.extend(defaults,options),
				dur=[options.dur,(options.drag ? 0 : options.dur)],
				mCustomScrollBox=$("#mCSB_"+d.idx),
				mCSB_container=$("#mCSB_"+d.idx+"_container"),
				wrapper=mCSB_container.parent(),
				totalScrollOffsets=o.callbacks.onTotalScrollOffset ? _arr.call(el,o.callbacks.onTotalScrollOffset) : [0,0],
				totalScrollBackOffsets=o.callbacks.onTotalScrollBackOffset ? _arr.call(el,o.callbacks.onTotalScrollBackOffset) : [0,0];
			d.trigger=options.trigger;
			if(wrapper.scrollTop()!==0 || wrapper.scrollLeft()!==0){ /* always reset scrollTop/Left */
				$(".mCSB_"+d.idx+"_scrollbar").css("visibility","visible");
				wrapper.scrollTop(0).scrollLeft(0);
			}
			if(to==="_resetY" && !d.contentReset.y){
				/* callbacks: onOverflowYNone */
				if(_cb("onOverflowYNone")){o.callbacks.onOverflowYNone.call(el[0]);}
				d.contentReset.y=1;
			}
			if(to==="_resetX" && !d.contentReset.x){
				/* callbacks: onOverflowXNone */
				if(_cb("onOverflowXNone")){o.callbacks.onOverflowXNone.call(el[0]);}
				d.contentReset.x=1;
			}
			if(to==="_resetY" || to==="_resetX"){return;}
			if((d.contentReset.y || !el[0].mcs) && d.overflowed[0]){
				/* callbacks: onOverflowY */
				if(_cb("onOverflowY")){o.callbacks.onOverflowY.call(el[0]);}
				d.contentReset.x=null;
			}
			if((d.contentReset.x || !el[0].mcs) && d.overflowed[1]){
				/* callbacks: onOverflowX */
				if(_cb("onOverflowX")){o.callbacks.onOverflowX.call(el[0]);}
				d.contentReset.x=null;
			}
			if(o.snapAmount){ /* scrolling snapping */
				var snapAmount=!(o.snapAmount instanceof Array) ? o.snapAmount : options.dir==="x" ? o.snapAmount[1] : o.snapAmount[0];
				to=_snapAmount(to,snapAmount,o.snapOffset);
			}
			switch(options.dir){
				case "x":
					var mCSB_dragger=$("#mCSB_"+d.idx+"_dragger_horizontal"),
						property="left",
						contentPos=mCSB_container[0].offsetLeft,
						limit=[
							mCustomScrollBox.width()-mCSB_container.outerWidth(false),
							mCSB_dragger.parent().width()-mCSB_dragger.width()
						],
						scrollTo=[to,to===0 ? 0 : (to/d.scrollRatio.x)],
						tso=totalScrollOffsets[1],
						tsbo=totalScrollBackOffsets[1],
						totalScrollOffset=tso>0 ? tso/d.scrollRatio.x : 0,
						totalScrollBackOffset=tsbo>0 ? tsbo/d.scrollRatio.x : 0;
					break;
				case "y":
					var mCSB_dragger=$("#mCSB_"+d.idx+"_dragger_vertical"),
						property="top",
						contentPos=mCSB_container[0].offsetTop,
						limit=[
							mCustomScrollBox.height()-mCSB_container.outerHeight(false),
							mCSB_dragger.parent().height()-mCSB_dragger.height()
						],
						scrollTo=[to,to===0 ? 0 : (to/d.scrollRatio.y)],
						tso=totalScrollOffsets[0],
						tsbo=totalScrollBackOffsets[0],
						totalScrollOffset=tso>0 ? tso/d.scrollRatio.y : 0,
						totalScrollBackOffset=tsbo>0 ? tsbo/d.scrollRatio.y : 0;
					break;
			}
			if(scrollTo[1]<0 || (scrollTo[0]===0 && scrollTo[1]===0)){
				scrollTo=[0,0];
			}else if(scrollTo[1]>=limit[1]){
				scrollTo=[limit[0],limit[1]];
			}else{
				scrollTo[0]=-scrollTo[0];
			}
			if(!el[0].mcs){
				_mcs();  /* init mcs object (once) to make it available before callbacks */
				if(_cb("onInit")){o.callbacks.onInit.call(el[0]);} /* callbacks: onInit */
			}
			clearTimeout(mCSB_container[0].onCompleteTimeout);
			_tweenTo(mCSB_dragger[0],property,Math.round(scrollTo[1]),dur[1],options.scrollEasing);
			if(!d.tweenRunning && ((contentPos===0 && scrollTo[0]>=0) || (contentPos===limit[0] && scrollTo[0]<=limit[0]))){return;}
			_tweenTo(mCSB_container[0],property,Math.round(scrollTo[0]),dur[0],options.scrollEasing,options.overwrite,{
				onStart:function(){
					if(options.callbacks && options.onStart && !d.tweenRunning){
						/* callbacks: onScrollStart */
						if(_cb("onScrollStart")){_mcs(); o.callbacks.onScrollStart.call(el[0]);}
						d.tweenRunning=true;
						_onDragClasses(mCSB_dragger);
						d.cbOffsets=_cbOffsets();
					}
				},onUpdate:function(){
					if(options.callbacks && options.onUpdate){
						/* callbacks: whileScrolling */
						if(_cb("whileScrolling")){_mcs(); o.callbacks.whileScrolling.call(el[0]);}
					}
				},onComplete:function(){
					if(options.callbacks && options.onComplete){
						if(o.axis==="yx"){clearTimeout(mCSB_container[0].onCompleteTimeout);}
						var t=mCSB_container[0].idleTimer || 0;
						mCSB_container[0].onCompleteTimeout=setTimeout(function(){
							/* callbacks: onScroll, onTotalScroll, onTotalScrollBack */
							if(_cb("onScroll")){_mcs(); o.callbacks.onScroll.call(el[0]);}
							if(_cb("onTotalScroll") && scrollTo[1]>=limit[1]-totalScrollOffset && d.cbOffsets[0]){_mcs(); o.callbacks.onTotalScroll.call(el[0]);}
							if(_cb("onTotalScrollBack") && scrollTo[1]<=totalScrollBackOffset && d.cbOffsets[1]){_mcs(); o.callbacks.onTotalScrollBack.call(el[0]);}
							d.tweenRunning=false;
							mCSB_container[0].idleTimer=0;
							_onDragClasses(mCSB_dragger,"hide");
						},t);
					}
				}
			});
			/* checks if callback function exists */
			function _cb(cb){
				return d && o.callbacks[cb] && typeof o.callbacks[cb]==="function";
			}
			/* checks whether callback offsets always trigger */
			function _cbOffsets(){
				return [o.callbacks.alwaysTriggerOffsets || contentPos>=limit[0]+tso,o.callbacks.alwaysTriggerOffsets || contentPos<=-tsbo];
			}
			/* 
			populates object with useful values for the user 
			values: 
				content: this.mcs.content
				content top position: this.mcs.top 
				content left position: this.mcs.left 
				dragger top position: this.mcs.draggerTop 
				dragger left position: this.mcs.draggerLeft 
				scrolling y percentage: this.mcs.topPct 
				scrolling x percentage: this.mcs.leftPct 
				scrolling direction: this.mcs.direction
			*/
			function _mcs(){
				var cp=[mCSB_container[0].offsetTop,mCSB_container[0].offsetLeft], /* content position */
					dp=[mCSB_dragger[0].offsetTop,mCSB_dragger[0].offsetLeft], /* dragger position */
					cl=[mCSB_container.outerHeight(false),mCSB_container.outerWidth(false)], /* content length */
					pl=[mCustomScrollBox.height(),mCustomScrollBox.width()]; /* content parent length */
				el[0].mcs={
					content:mCSB_container, /* original content wrapper as jquery object */
					top:cp[0],left:cp[1],draggerTop:dp[0],draggerLeft:dp[1],
					topPct:Math.round((100*Math.abs(cp[0]))/(Math.abs(cl[0])-pl[0])),leftPct:Math.round((100*Math.abs(cp[1]))/(Math.abs(cl[1])-pl[1])),
					direction:options.dir
				};
				/* 
				this refers to the original element containing the scrollbar(s)
				usage: this.mcs.top, this.mcs.leftPct etc. 
				*/
			}
		},
		/* -------------------- */
		
		
		/* 
		CUSTOM JAVASCRIPT ANIMATION TWEEN 
		Lighter and faster than jquery animate() and css transitions 
		Animates top/left properties and includes easings 
		*/
		_tweenTo=function(el,prop,to,duration,easing,overwrite,callbacks){
			if(!el._mTween){el._mTween={top:{},left:{}};}
			var callbacks=callbacks || {},
				onStart=callbacks.onStart || function(){},onUpdate=callbacks.onUpdate || function(){},onComplete=callbacks.onComplete || function(){},
				startTime=_getTime(),_delay,progress=0,from=el.offsetTop,elStyle=el.style,_request,tobj=el._mTween[prop];
			if(prop==="left"){from=el.offsetLeft;}
			var diff=to-from;
			tobj.stop=0;
			if(overwrite!=="none"){_cancelTween();}
			_startTween();
			function _step(){
				if(tobj.stop){return;}
				if(!progress){onStart.call();}
				progress=_getTime()-startTime;
				_tween();
				if(progress>=tobj.time){
					tobj.time=(progress>tobj.time) ? progress+_delay-(progress-tobj.time) : progress+_delay-1;
					if(tobj.time<progress+1){tobj.time=progress+1;}
				}
				if(tobj.time<duration){tobj.id=_request(_step);}else{onComplete.call();}
			}
			function _tween(){
				if(duration>0){
					tobj.currVal=_ease(tobj.time,from,diff,duration,easing);
					elStyle[prop]=Math.round(tobj.currVal)+"px";
				}else{
					elStyle[prop]=to+"px";
				}
				onUpdate.call();
			}
			function _startTween(){
				_delay=1000/60;
				tobj.time=progress+_delay;
				_request=(!window.requestAnimationFrame) ? function(f){_tween(); return setTimeout(f,0.01);} : window.requestAnimationFrame;
				tobj.id=_request(_step);
			}
			function _cancelTween(){
				if(tobj.id==null){return;}
				if(!window.requestAnimationFrame){clearTimeout(tobj.id);
				}else{window.cancelAnimationFrame(tobj.id);}
				tobj.id=null;
			}
			function _ease(t,b,c,d,type){
				switch(type){
					case "linear": case "mcsLinear":
						return c*t/d + b;
						break;
					case "mcsLinearOut":
						t/=d; t--; return c * Math.sqrt(1 - t*t) + b;
						break;
					case "easeInOutSmooth":
						t/=d/2;
						if(t<1) return c/2*t*t + b;
						t--;
						return -c/2 * (t*(t-2) - 1) + b;
						break;
					case "easeInOutStrong":
						t/=d/2;
						if(t<1) return c/2 * Math.pow( 2, 10 * (t - 1) ) + b;
						t--;
						return c/2 * ( -Math.pow( 2, -10 * t) + 2 ) + b;
						break;
					case "easeInOut": case "mcsEaseInOut":
						t/=d/2;
						if(t<1) return c/2*t*t*t + b;
						t-=2;
						return c/2*(t*t*t + 2) + b;
						break;
					case "easeOutSmooth":
						t/=d; t--;
						return -c * (t*t*t*t - 1) + b;
						break;
					case "easeOutStrong":
						return c * ( -Math.pow( 2, -10 * t/d ) + 1 ) + b;
						break;
					case "easeOut": case "mcsEaseOut": default:
						var ts=(t/=d)*t,tc=ts*t;
						return b+c*(0.499999999999997*tc*ts + -2.5*ts*ts + 5.5*tc + -6.5*ts + 4*t);
				}
			}
		},
		/* -------------------- */
		
		
		/* returns current time */
		_getTime=function(){
			if(window.performance && window.performance.now){
				return window.performance.now();
			}else{
				if(window.performance && window.performance.webkitNow){
					return window.performance.webkitNow();
				}else{
					if(Date.now){return Date.now();}else{return new Date().getTime();}
				}
			}
		},
		/* -------------------- */
		
		
		/* stops a tween */
		_stopTween=function(){
			var el=this;
			if(!el._mTween){el._mTween={top:{},left:{}};}
			var props=["top","left"];
			for(var i=0; i<props.length; i++){
				var prop=props[i];
				if(el._mTween[prop].id){
					if(!window.requestAnimationFrame){clearTimeout(el._mTween[prop].id);
					}else{window.cancelAnimationFrame(el._mTween[prop].id);}
					el._mTween[prop].id=null;
					el._mTween[prop].stop=1;
				}
			}
		},
		/* -------------------- */
		
		
		/* deletes a property (avoiding the exception thrown by IE) */
		_delete=function(c,m){
			try{delete c[m];}catch(e){c[m]=null;}
		},
		/* -------------------- */
		
		
		/* detects left mouse button */
		_mouseBtnLeft=function(e){
			return !(e.which && e.which!==1);
		},
		/* -------------------- */
		
		
		/* detects if pointer type event is touch */
		_pointerTouch=function(e){
			var t=e.originalEvent.pointerType;
			return !(t && t!=="touch" && t!==2);
		},
		/* -------------------- */
		
		
		/* checks if value is numeric */
		_isNumeric=function(val){
			return !isNaN(parseFloat(val)) && isFinite(val);
		},
		/* -------------------- */
		
		
		/* returns element position according to content */
		_childPos=function(el){
			var p=el.parents(".mCSB_container");
			return [el.offset().top-p.offset().top,el.offset().left-p.offset().left];
		},
		/* -------------------- */
		
		
		/* checks if browser tab is hidden/inactive via Page Visibility API */
		_isTabHidden=function(){
			var prop=_getHiddenProp();
			if(!prop) return false;
			return document[prop];
			function _getHiddenProp(){
				var pfx=["webkit","moz","ms","o"];
				if("hidden" in document) return "hidden"; //natively supported
				for(var i=0; i<pfx.length; i++){ //prefixed
				    if((pfx[i]+"Hidden") in document) 
				        return pfx[i]+"Hidden";
				}
				return null; //not supported
			}
		};
		/* -------------------- */
		
	
	
	
	
	/* 
	----------------------------------------
	PLUGIN SETUP 
	----------------------------------------
	*/
	
	/* plugin constructor functions */
	$.fn[pluginNS]=function(method){ /* usage: $(selector).mCustomScrollbar(); */
		if(methods[method]){
			return methods[method].apply(this,Array.prototype.slice.call(arguments,1));
		}else if(typeof method==="object" || !method){
			return methods.init.apply(this,arguments);
		}else{
			$.error("Method "+method+" does not exist");
		}
	};
	$[pluginNS]=function(method){ /* usage: $.mCustomScrollbar(); */
		if(methods[method]){
			return methods[method].apply(this,Array.prototype.slice.call(arguments,1));
		}else if(typeof method==="object" || !method){
			return methods.init.apply(this,arguments);
		}else{
			$.error("Method "+method+" does not exist");
		}
	};
	
	/* 
	allow setting plugin default options. 
	usage: $.mCustomScrollbar.defaults.scrollInertia=500; 
	to apply any changed default options on default selectors (below), use inside document ready fn 
	e.g.: $(document).ready(function(){ $.mCustomScrollbar.defaults.scrollInertia=500; });
	*/
	$[pluginNS].defaults=defaults;
	
	/* 
	add window object (window.mCustomScrollbar) 
	usage: if(window.mCustomScrollbar){console.log("custom scrollbar plugin loaded");}
	*/
	window[pluginNS]=true;
	
	$(window).bind("load",function(){
		
		$(defaultSelector)[pluginNS](); /* add scrollbars automatically on default selector */
		
		/* extend jQuery expressions */
		$.extend($.expr[":"],{
			/* checks if element is within scrollable viewport */
			mcsInView:$.expr[":"].mcsInView || function(el){
				var $el=$(el),content=$el.parents(".mCSB_container"),wrapper,cPos;
				if(!content.length){return;}
				wrapper=content.parent();
				cPos=[content[0].offsetTop,content[0].offsetLeft];
				return 	cPos[0]+_childPos($el)[0]>=0 && cPos[0]+_childPos($el)[0]<wrapper.height()-$el.outerHeight(false) && 
						cPos[1]+_childPos($el)[1]>=0 && cPos[1]+_childPos($el)[1]<wrapper.width()-$el.outerWidth(false);
			},
			/* checks if element or part of element is in view of scrollable viewport */
			mcsInSight:$.expr[":"].mcsInSight || function(el,i,m){
				var $el=$(el),elD,content=$el.parents(".mCSB_container"),wrapperView,pos,wrapperViewPct,
					pctVals=m[3]==="exact" ? [[1,0],[1,0]] : [[0.9,0.1],[0.6,0.4]];
				if(!content.length){return;}
				elD=[$el.outerHeight(false),$el.outerWidth(false)];
				pos=[content[0].offsetTop+_childPos($el)[0],content[0].offsetLeft+_childPos($el)[1]];
				wrapperView=[content.parent()[0].offsetHeight,content.parent()[0].offsetWidth];
				wrapperViewPct=[elD[0]<wrapperView[0] ? pctVals[0] : pctVals[1],elD[1]<wrapperView[1] ? pctVals[0] : pctVals[1]];
				return 	pos[0]-(wrapperView[0]*wrapperViewPct[0][0])<0 && pos[0]+elD[0]-(wrapperView[0]*wrapperViewPct[0][1])>=0 && 
						pos[1]-(wrapperView[1]*wrapperViewPct[1][0])<0 && pos[1]+elD[1]-(wrapperView[1]*wrapperViewPct[1][1])>=0;
			},
			/* checks if element is overflowed having visible scrollbar(s) */
			mcsOverflow:$.expr[":"].mcsOverflow || function(el){
				var d=$(el).data(pluginPfx);
				if(!d){return;}
				return d.overflowed[0] || d.overflowed[1];
			}
		});
	
	});

}))}));
(function(factory) {
  /* global define */
  /* istanbul ignore next */
  if ( typeof define === 'function' && define.amd ) {
    define(['jquery'], factory);
  } else if ( typeof module === 'object' && module.exports ) {
    // Node/CommonJS
    module.exports = function( root, jQuery ) {
      if ( jQuery === undefined ) {
        if ( typeof window !== 'undefined' ) {
          jQuery = require('jquery');
        } else {
          jQuery = require('jquery')(root);
        }
      }
      factory(jQuery);
      return jQuery;
    };
  } else {
    // Browser globals
    factory(jQuery);
  }
}(function($) {
  'use strict';

  var $doc = $(document);
  var $win = $(window);

  var pluginName = 'selectric';
  var classList = 'Input Items Open Disabled TempShow HideSelect Wrapper Focus Hover Responsive Above Below Scroll Group GroupLabel';
  var eventNamespaceSuffix = '.sl';

  var chars = ['a', 'e', 'i', 'o', 'u', 'n', 'c', 'y'];
  var diacritics = [
    /[\xE0-\xE5]/g, // a
    /[\xE8-\xEB]/g, // e
    /[\xEC-\xEF]/g, // i
    /[\xF2-\xF6]/g, // o
    /[\xF9-\xFC]/g, // u
    /[\xF1]/g,      // n
    /[\xE7]/g,      // c
    /[\xFD-\xFF]/g  // y
  ];

  /**
   * Create an instance of Selectric
   *
   * @constructor
   * @param {Node} element - The &lt;select&gt; element
   * @param {object}  opts - Options
   */
  var Selectric = function(element, opts) {
    var _this = this;

    _this.element = element;
    _this.$element = $(element);

    _this.state = {
      multiple       : !!_this.$element.attr('multiple'),
      enabled        : false,
      opened         : false,
      currValue      : -1,
      selectedIdx    : -1,
      highlightedIdx : -1
    };

    _this.eventTriggers = {
      open    : _this.open,
      close   : _this.close,
      destroy : _this.destroy,
      refresh : _this.refresh,
      init    : _this.init
    };

    _this.init(opts);
  };

  Selectric.prototype = {
    utils: {
      /**
       * Detect mobile browser
       *
       * @return {boolean}
       */
      isMobile: function() {
        return /android|ip(hone|od|ad)/i.test(navigator.userAgent);
      },

      /**
       * Escape especial characters in string (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions)
       *
       * @param  {string} str - The string to be escaped
       * @return {string}       The string with the special characters escaped
       */
      escapeRegExp: function(str) {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
      },

      /**
       * Replace diacritics
       *
       * @param  {string} str - The string to replace the diacritics
       * @return {string}       The string with diacritics replaced with ascii characters
       */
      replaceDiacritics: function(str) {
        var k = diacritics.length;

        while (k--) {
          str = str.toLowerCase().replace(diacritics[k], chars[k]);
        }

        return str;
      },

      /**
       * Format string
       * https://gist.github.com/atesgoral/984375
       *
       * @param  {string} f - String to be formated
       * @return {string}     String formated
       */
      format: function(f) {
        var a = arguments; // store outer arguments
        return ('' + f) // force format specifier to String
          .replace( // replace tokens in format specifier
            /\{(?:(\d+)|(\w+))\}/g, // match {token} references
            function(
              s, // the matched string (ignored)
              i, // an argument index
              p // a property name
            ) {
              return p && a[1] // if property name and first argument exist
                ? a[1][p] // return property from first argument
                : a[i]; // assume argument index and return i-th argument
            });
      },

      /**
       * Get the next enabled item in the options list.
       *
       * @param  {object} selectItems - The options object.
       * @param  {number}    selected - Index of the currently selected option.
       * @return {object}               The next enabled item.
       */
      nextEnabledItem: function(selectItems, selected) {
        while ( selectItems[ selected = (selected + 1) % selectItems.length ].disabled ) {
          // empty
        }
        return selected;
      },

      /**
       * Get the previous enabled item in the options list.
       *
       * @param  {object} selectItems - The options object.
       * @param  {number}    selected - Index of the currently selected option.
       * @return {object}               The previous enabled item.
       */
      previousEnabledItem: function(selectItems, selected) {
        while ( selectItems[ selected = (selected > 0 ? selected : selectItems.length) - 1 ].disabled ) {
          // empty
        }
        return selected;
      },

      /**
       * Transform camelCase string to dash-case.
       *
       * @param  {string} str - The camelCased string.
       * @return {string}       The string transformed to dash-case.
       */
      toDash: function(str) {
        return str.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase();
      },

      /**
       * Calls the events registered with function name.
       *
       * @param {string}    fn - The name of the function.
       * @param {number} scope - Scope that should be set on the function.
       */
      triggerCallback: function(fn, scope) {
        var elm = scope.element;
        var func = scope.options['on' + fn];
        var args = [elm].concat([].slice.call(arguments).slice(1));

        if ( $.isFunction(func) ) {
          func.apply(elm, args);
        }

        $(elm).trigger(pluginName + '-' + this.toDash(fn), args);
      },

      /**
       * Transform array list to concatenated string and remove empty values
       * @param  {array} arr - Class list
       * @return {string}      Concatenated string
       */
      arrayToClassname: function(arr) {
        var newArr = $.grep(arr, function(item) {
          return !!item;
        });

        return $.trim(newArr.join(' '));
      }
    },

    /** Initializes */
    init: function(opts) {
      var _this = this;

      // Set options
      _this.options = $.extend(true, {}, $.fn[pluginName].defaults, _this.options, opts);

      _this.utils.triggerCallback('BeforeInit', _this);

      // Preserve data
      _this.destroy(true);

      // Disable on mobile browsers
      if ( _this.options.disableOnMobile && _this.utils.isMobile() ) {
        _this.disableOnMobile = true;
        return;
      }

      // Get classes
      _this.classes = _this.getClassNames();

      // Create elements
      var input              = $('<input/>', { 'class': _this.classes.input, 'readonly': _this.utils.isMobile() });
      var items              = $('<div/>',   { 'class': _this.classes.items, 'tabindex': -1 });
      var itemsScroll        = $('<div/>',   { 'class': _this.classes.scroll });
      var wrapper            = $('<div/>',   { 'class': _this.classes.prefix, 'html': _this.options.arrowButtonMarkup });
      var label              = $('<span/>',  { 'class': 'label' });
      var outerWrapper       = _this.$element.wrap('<div/>').parent().append(wrapper.prepend(label), items, input);
      var hideSelectWrapper  = $('<div/>',   { 'class': _this.classes.hideselect });

      _this.elements = {
        input        : input,
        items        : items,
        itemsScroll  : itemsScroll,
        wrapper      : wrapper,
        label        : label,
        outerWrapper : outerWrapper
      };

      if ( _this.options.nativeOnMobile && _this.utils.isMobile() ) {
        _this.elements.input = undefined;
        hideSelectWrapper.addClass(_this.classes.prefix + '-is-native');

        _this.$element.on('change', function() {
          _this.refresh();
        });
      }

      _this.$element
        .on(_this.eventTriggers)
        .wrap(hideSelectWrapper);

      _this.originalTabindex = _this.$element.prop('tabindex');
      _this.$element.prop('tabindex', -1);

      _this.populate();
      _this.activate();

      _this.utils.triggerCallback('Init', _this);
    },

    /** Activates the plugin */
    activate: function() {
      var _this = this;
      var hiddenChildren = _this.elements.items.closest(':visible').children(':hidden').addClass(_this.classes.tempshow);
      var originalWidth = _this.$element.width();

      hiddenChildren.removeClass(_this.classes.tempshow);

      _this.utils.triggerCallback('BeforeActivate', _this);

      _this.elements.outerWrapper.prop('class',
        _this.utils.arrayToClassname([
          _this.classes.wrapper,
          _this.$element.prop('class').replace(/\S+/g, _this.classes.prefix + '-$&'),
          _this.options.responsive ? _this.classes.responsive : ''
        ])
      );

      if ( _this.options.inheritOriginalWidth && originalWidth > 0 ) {
        _this.elements.outerWrapper.width(originalWidth);
      }

      _this.unbindEvents();

      if ( !_this.$element.prop('disabled') ) {
        _this.state.enabled = true;

        // Not disabled, so... Removing disabled class
        _this.elements.outerWrapper.removeClass(_this.classes.disabled);

        // Remove styles from items box
        // Fix incorrect height when refreshed is triggered with fewer options
        _this.$li = _this.elements.items.removeAttr('style').find('li');

        _this.bindEvents();
      } else {
        _this.elements.outerWrapper.addClass(_this.classes.disabled);

        if ( _this.elements.input ) {
          _this.elements.input.prop('disabled', true);
        }
      }

      _this.utils.triggerCallback('Activate', _this);
    },

    /**
     * Generate classNames for elements
     *
     * @return {object} Classes object
     */
    getClassNames: function() {
      var _this = this;
      var customClass = _this.options.customClass;
      var classesObj = {};

      $.each(classList.split(' '), function(i, currClass) {
        var c = customClass.prefix + currClass;
        classesObj[currClass.toLowerCase()] = customClass.camelCase ? c : _this.utils.toDash(c);
      });

      classesObj.prefix = customClass.prefix;

      return classesObj;
    },

    /** Set the label text */
    setLabel: function() {
      var _this = this;
      var labelBuilder = _this.options.labelBuilder;

      if ( _this.state.multiple ) {
        // Make sure currentValues is an array
        var currentValues = $.isArray(_this.state.currValue) ? _this.state.currValue : [_this.state.currValue];
        // I'm not happy with this, but currentValues can be an empty
        // array and we need to fallback to the default option.
        currentValues = currentValues.length === 0 ? [0] : currentValues;

        var labelMarkup = $.map(currentValues, function(value) {
          return $.grep(_this.lookupItems, function(item) {
            return item.index === value;
          })[0]; // we don't want nested arrays here
        });

        labelMarkup = $.grep(labelMarkup, function(item) {
          // Hide default (please choose) if more then one element were selected.
          // If no option value were given value is set to option text by default
          if ( labelMarkup.length > 1 || labelMarkup.length === 0 ) {
            return $.trim(item.value) !== '';
          }
          return item;
        });

        labelMarkup = $.map(labelMarkup, function(item) {
          return $.isFunction(labelBuilder)
            ? labelBuilder(item)
            : _this.utils.format(labelBuilder, item);
        });

        // Limit the amount of selected values shown in label
        if ( _this.options.multiple.maxLabelEntries ) {
          if ( labelMarkup.length >= _this.options.multiple.maxLabelEntries + 1 ) {
            labelMarkup = labelMarkup.slice(0, _this.options.multiple.maxLabelEntries);
            labelMarkup.push(
              $.isFunction(labelBuilder)
                ? labelBuilder({ text: '...' })
                : _this.utils.format(labelBuilder, { text: '...' }));
          } else {
            labelMarkup.slice(labelMarkup.length - 1);
          }
        }
        _this.elements.label.html(labelMarkup.join(_this.options.multiple.separator));

      } else {
        var currItem = _this.lookupItems[_this.state.currValue];

        _this.elements.label.html(
          $.isFunction(labelBuilder)
            ? labelBuilder(currItem)
            : _this.utils.format(labelBuilder, currItem)
        );
      }
    },

    /** Get and save the available options */
    populate: function() {
      var _this = this;
      var $options = _this.$element.children();
      var $justOptions = _this.$element.find('option');
      var $selected = $justOptions.filter(':selected');
      var selectedIndex = $justOptions.index($selected);
      var currIndex = 0;
      var emptyValue = (_this.state.multiple ? [] : 0);

      if ( $selected.length > 1 && _this.state.multiple ) {
        selectedIndex = [];
        $selected.each(function() {
          selectedIndex.push($(this).index());
        });
      }

      _this.state.currValue = (~selectedIndex ? selectedIndex : emptyValue);
      _this.state.selectedIdx = _this.state.currValue;
      _this.state.highlightedIdx = _this.state.currValue;
      _this.items = [];
      _this.lookupItems = [];

      if ( $options.length ) {
        // Build options markup
        $options.each(function(i) {
          var $elm = $(this);

          if ( $elm.is('optgroup') ) {

            var optionsGroup = {
              element       : $elm,
              label         : $elm.prop('label'),
              groupDisabled : $elm.prop('disabled'),
              items         : []
            };

            $elm.children().each(function(i) {
              var $elm = $(this);

              optionsGroup.items[i] = _this.getItemData(currIndex, $elm, optionsGroup.groupDisabled || $elm.prop('disabled'));

              _this.lookupItems[currIndex] = optionsGroup.items[i];

              currIndex++;
            });

            _this.items[i] = optionsGroup;

          } else {

            _this.items[i] = _this.getItemData(currIndex, $elm, $elm.prop('disabled'));

            _this.lookupItems[currIndex] = _this.items[i];

            currIndex++;

          }
        });

        _this.setLabel();
        _this.elements.items.append( _this.elements.itemsScroll.html( _this.getItemsMarkup(_this.items) ) );
      }
    },

    /**
     * Generate items object data
     * @param  {integer} index      - Current item index
     * @param  {node}    $elm       - Current element node
     * @param  {boolean} isDisabled - Current element disabled state
     * @return {object}               Item object
     */
    getItemData: function(index, $elm, isDisabled) {
      var _this = this;

      return {
        index     : index,
        element   : $elm,
        value     : $elm.val(),
        className : $elm.prop('class'),
        text      : $elm.html(),
        slug      : $.trim(_this.utils.replaceDiacritics($elm.html())),
        alt       : $elm.attr('data-alt'),
        selected  : $elm.prop('selected'),
        disabled  : isDisabled
      };
    },

    /**
     * Generate options markup
     *
     * @param  {object} items - Object containing all available options
     * @return {string}         HTML for the options box
     */
    getItemsMarkup: function(items) {
      var _this = this;
      var markup = '<ul>';

      if ( $.isFunction(_this.options.listBuilder) && _this.options.listBuilder ) {
        items = _this.options.listBuilder(items);
      }

      $.each(items, function(i, elm) {
        if ( elm.label !== undefined ) {

          markup += _this.utils.format('<ul class="{1}"><li class="{2}">{3}</li>',
            _this.utils.arrayToClassname([
              _this.classes.group,
              elm.groupDisabled ? 'disabled' : '',
              elm.element.prop('class')
            ]),
            _this.classes.grouplabel,
            elm.element.prop('label')
          );

          $.each(elm.items, function(i, elm) {
            markup += _this.getItemMarkup(elm.index, elm);
          });

          markup += '</ul>';

        } else {

          markup += _this.getItemMarkup(elm.index, elm);

        }
      });

      return markup + '</ul>';
    },

    /**
     * Generate every option markup
     *
     * @param  {number} index    - Index of current item
     * @param  {object} itemData - Current item
     * @return {string}            HTML for the option
     */
    getItemMarkup: function(index, itemData) {
      var _this = this;
      var itemBuilder = _this.options.optionsItemBuilder;
      // limit access to item data to provide a simple interface
      // to most relevant options.
      var filteredItemData = {
        value: itemData.value,
        text : itemData.text,
        slug : itemData.slug,
        index: itemData.index
      };

      return _this.utils.format('<li data-index="{1}" class="{2}">{3}</li>',
        index,
        _this.utils.arrayToClassname([
          itemData.className,
          index === _this.items.length - 1  ? 'last'     : '',
          itemData.disabled                 ? 'disabled' : '',
          itemData.selected                 ? 'selected' : ''
        ]),
        $.isFunction(itemBuilder)
          ? _this.utils.format(itemBuilder(itemData, this.$element, index), itemData)
          : _this.utils.format(itemBuilder, filteredItemData)
      );
    },

    /** Remove events on the elements */
    unbindEvents: function() {
      var _this = this;

      _this.elements.wrapper
        .add(_this.$element)
        .add(_this.elements.outerWrapper)
        .add(_this.elements.input)
        .off(eventNamespaceSuffix);
    },

    /** Bind events on the elements */
    bindEvents: function() {
      var _this = this;

      _this.elements.outerWrapper.on('mouseenter' + eventNamespaceSuffix + ' mouseleave' + eventNamespaceSuffix, function(e) {
        $(this).toggleClass(_this.classes.hover, e.type === 'mouseenter');

        // Delay close effect when openOnHover is true
        if ( _this.options.openOnHover ) {
          clearTimeout(_this.closeTimer);

          if ( e.type === 'mouseleave' ) {
            _this.closeTimer = setTimeout($.proxy(_this.close, _this), _this.options.hoverIntentTimeout);
          } else {
            _this.open();
          }
        }
      });

      // Toggle open/close
      _this.elements.wrapper.on('click' + eventNamespaceSuffix, function(e) {
        _this.state.opened ? _this.close() : _this.open(e);
      });

      // Translate original element focus event to dummy input.
      // Disabled on mobile devices because the default option list isn't
      // shown due the fact that hidden input gets focused
      if ( !(_this.options.nativeOnMobile && _this.utils.isMobile()) ) {
        _this.$element.on('focus' + eventNamespaceSuffix, function() {
          _this.elements.input.focus();
        });

        _this.elements.input
          .prop({ tabindex: _this.originalTabindex, disabled: false })
          .on('keydown' + eventNamespaceSuffix, $.proxy(_this.handleKeys, _this))
          .on('focusin' + eventNamespaceSuffix, function(e) {
            _this.elements.outerWrapper.addClass(_this.classes.focus);

            // Prevent the flicker when focusing out and back again in the browser window
            _this.elements.input.one('blur', function() {
              _this.elements.input.blur();
            });

            if ( _this.options.openOnFocus && !_this.state.opened ) {
              _this.open(e);
            }
          })
          .on('focusout' + eventNamespaceSuffix, function() {
            _this.elements.outerWrapper.removeClass(_this.classes.focus);
          })
          .on('input propertychange', function() {
            var val = _this.elements.input.val();
            var searchRegExp = new RegExp('^' + _this.utils.escapeRegExp(val), 'i');

            // Clear search
            clearTimeout(_this.resetStr);
            _this.resetStr = setTimeout(function() {
              _this.elements.input.val('');
            }, _this.options.keySearchTimeout);

            if ( val.length ) {
              // Search in select options
              $.each(_this.items, function(i, elm) {
                if (elm.disabled) {
                  return;
                }
                if (searchRegExp.test(elm.text) || searchRegExp.test(elm.slug)) {
                  _this.highlight(i);
                  return false;
                }
                if (!elm.alt) {
                  return;
                }
                var altItems = elm.alt.split('|');
                for (var ai = 0; ai < altItems.length; ai++) {
                  if (!altItems[ai]) {
                    break;
                  }
                  if (searchRegExp.test(altItems[ai].trim())) {
                    _this.highlight(i);
                    return false;
                  }
                }
              });
            }
          });
      }

      _this.$li.on({
        // Prevent <input> blur on Chrome
        mousedown: function(e) {
          e.preventDefault();
          e.stopPropagation();
        },
        click: function() {
          _this.select($(this).data('index'));

          // Chrome doesn't close options box if select is wrapped with a label
          // We need to 'return false' to avoid that
          return false;
        }
      });
    },

    /**
     * Behavior when keyboard keys is pressed
     *
     * @param {object} e - Event object
     */
    handleKeys: function(e) {
      var _this = this;
      var key = e.which;
      var keys = _this.options.keys;

      var isPrevKey = $.inArray(key, keys.previous) > -1;
      var isNextKey = $.inArray(key, keys.next) > -1;
      var isSelectKey = $.inArray(key, keys.select) > -1;
      var isOpenKey = $.inArray(key, keys.open) > -1;
      var idx = _this.state.highlightedIdx;
      var isFirstOrLastItem = (isPrevKey && idx === 0) || (isNextKey && (idx + 1) === _this.items.length);
      var goToItem = 0;

      // Enter / Space
      if ( key === 13 || key === 32 ) {
        e.preventDefault();
      }

      // If it's a directional key
      if ( isPrevKey || isNextKey ) {
        if ( !_this.options.allowWrap && isFirstOrLastItem ) {
          return;
        }

        if ( isPrevKey ) {
          goToItem = _this.utils.previousEnabledItem(_this.lookupItems, idx);
        }

        if ( isNextKey ) {
          goToItem = _this.utils.nextEnabledItem(_this.lookupItems, idx);
        }

        _this.highlight(goToItem);
      }

      // Tab / Enter / ESC
      if ( isSelectKey && _this.state.opened ) {
        _this.select(idx);

        if ( !_this.state.multiple || !_this.options.multiple.keepMenuOpen ) {
          _this.close();
        }

        return;
      }

      // Space / Enter / Left / Up / Right / Down
      if ( isOpenKey && !_this.state.opened ) {
        _this.open();
      }
    },

    /** Update the items object */
    refresh: function() {
      var _this = this;

      _this.populate();
      _this.activate();
      _this.utils.triggerCallback('Refresh', _this);
    },

    /** Set options box width/height */
    setOptionsDimensions: function() {
      var _this = this;

      // Calculate options box height
      // Set a temporary class on the hidden parent of the element
      var hiddenChildren = _this.elements.items.closest(':visible').children(':hidden').addClass(_this.classes.tempshow);
      var maxHeight = _this.options.maxHeight;
      var itemsWidth = _this.elements.items.outerWidth();
      var wrapperWidth = _this.elements.wrapper.outerWidth() - (itemsWidth - _this.elements.items.width());

      // Set the dimensions, minimum is wrapper width, expand for long items if option is true
      if ( !_this.options.expandToItemText || wrapperWidth > itemsWidth ) {
        _this.finalWidth = wrapperWidth;
      } else {
        // Make sure the scrollbar width is included
        _this.elements.items.css('overflow', 'scroll');

        // Set a really long width for _this.elements.outerWrapper
        _this.elements.outerWrapper.width(9e4);
        _this.finalWidth = _this.elements.items.width();
        // Set scroll bar to auto
        _this.elements.items.css('overflow', '');
        _this.elements.outerWrapper.width('');
      }

      _this.elements.items.width(_this.finalWidth).height() > maxHeight && _this.elements.items.height(maxHeight);

      // Remove the temporary class
      hiddenChildren.removeClass(_this.classes.tempshow);
    },

    /** Detect if the options box is inside the window */
    isInViewport: function() {
      var _this = this;

      if (_this.options.forceRenderAbove === true) {
        _this.elements.outerWrapper.addClass(_this.classes.above);
      } else if (_this.options.forceRenderBelow === true) {
        _this.elements.outerWrapper.addClass(_this.classes.below);
      } else {
        var scrollTop = $win.scrollTop();
        var winHeight = $win.height();
        var uiPosX = _this.elements.outerWrapper.offset().top;
        var uiHeight = _this.elements.outerWrapper.outerHeight();

        var fitsDown = (uiPosX + uiHeight + _this.itemsHeight) <= (scrollTop + winHeight);
        var fitsAbove = (uiPosX - _this.itemsHeight) > scrollTop;

        // If it does not fit below, only render it
        // above it fit's there.
        // It's acceptable that the user needs to
        // scroll the viewport to see the cut off UI
        var renderAbove = !fitsDown && fitsAbove;
        var renderBelow = !renderAbove;

        _this.elements.outerWrapper.toggleClass(_this.classes.above, renderAbove);
        _this.elements.outerWrapper.toggleClass(_this.classes.below, renderBelow);
      }
    },

    /**
     * Detect if currently selected option is visible and scroll the options box to show it
     *
     * @param {Number|Array} index - Index of the selected items
     */
    detectItemVisibility: function(index) {
      var _this = this;
      var $filteredLi = _this.$li.filter('[data-index]');

      if ( _this.state.multiple ) {
        // If index is an array, we can assume a multiple select and we
        // want to scroll to the uppermost selected item!
        // Math.min.apply(Math, index) returns the lowest entry in an Array.
        index = ($.isArray(index) && index.length === 0) ? 0 : index;
        index = $.isArray(index) ? Math.min.apply(Math, index) : index;
      }

      var liHeight = $filteredLi.eq(index).outerHeight();
      var liTop = $filteredLi[index].offsetTop;
      var itemsScrollTop = _this.elements.itemsScroll.scrollTop();
      var scrollT = liTop + liHeight * 2;

      _this.elements.itemsScroll.scrollTop(
        scrollT > itemsScrollTop + _this.itemsHeight ? scrollT - _this.itemsHeight :
          liTop - liHeight < itemsScrollTop ? liTop - liHeight :
            itemsScrollTop
      );
    },

    /**
     * Open the select options box
     *
     * @param {Event} e - Event
     */
    open: function(e) {
      var _this = this;

      if ( _this.options.nativeOnMobile && _this.utils.isMobile()) {
        return false;
      }

      _this.utils.triggerCallback('BeforeOpen', _this);

      if ( e ) {
        e.preventDefault();
        if (_this.options.stopPropagation) {
          e.stopPropagation();
        }
      }

      if ( _this.state.enabled ) {
        _this.setOptionsDimensions();

        // Find any other opened instances of select and close it
        $('.' + _this.classes.hideselect, '.' + _this.classes.open).children()[pluginName]('close');

        _this.state.opened = true;
        _this.itemsHeight = _this.elements.items.outerHeight();
        _this.itemsInnerHeight = _this.elements.items.height();

        // Toggle options box visibility
        _this.elements.outerWrapper.addClass(_this.classes.open);

        // Give dummy input focus
        _this.elements.input.val('');
        if ( e && e.type !== 'focusin' ) {
          _this.elements.input.focus();
        }

        // Delayed binds events on Document to make label clicks work
        setTimeout(function() {
          $doc
            .on('click' + eventNamespaceSuffix, $.proxy(_this.close, _this))
            .on('scroll' + eventNamespaceSuffix, $.proxy(_this.isInViewport, _this));
        }, 1);

        _this.isInViewport();

        // Prevent window scroll when using mouse wheel inside items box
        if ( _this.options.preventWindowScroll ) {
          /* istanbul ignore next */
          $doc.on('mousewheel' + eventNamespaceSuffix + ' DOMMouseScroll' + eventNamespaceSuffix, '.' + _this.classes.scroll, function(e) {
            var orgEvent = e.originalEvent;
            var scrollTop = $(this).scrollTop();
            var deltaY = 0;

            if ( 'detail'      in orgEvent ) { deltaY = orgEvent.detail * -1; }
            if ( 'wheelDelta'  in orgEvent ) { deltaY = orgEvent.wheelDelta;  }
            if ( 'wheelDeltaY' in orgEvent ) { deltaY = orgEvent.wheelDeltaY; }
            if ( 'deltaY'      in orgEvent ) { deltaY = orgEvent.deltaY * -1; }

            if ( scrollTop === (this.scrollHeight - _this.itemsInnerHeight) && deltaY < 0 || scrollTop === 0 && deltaY > 0 ) {
              e.preventDefault();
            }
          });
        }

        _this.detectItemVisibility(_this.state.selectedIdx);

        _this.highlight(_this.state.multiple ? -1 : _this.state.selectedIdx);

        _this.utils.triggerCallback('Open', _this);
      }
    },

    /** Close the select options box */
    close: function() {
      var _this = this;

      _this.utils.triggerCallback('BeforeClose', _this);

      // Remove custom events on document
      $doc.off(eventNamespaceSuffix);

      // Remove visible class to hide options box
      _this.elements.outerWrapper.removeClass(_this.classes.open);

      _this.state.opened = false;

      _this.utils.triggerCallback('Close', _this);
    },

    /** Select current option and change the label */
    change: function() {
      var _this = this;

      _this.utils.triggerCallback('BeforeChange', _this);

      if ( _this.state.multiple ) {
        // Reset old selected
        $.each(_this.lookupItems, function(idx) {
          _this.lookupItems[idx].selected = false;
          _this.$element.find('option').prop('selected', false);
        });

        // Set new selected
        $.each(_this.state.selectedIdx, function(idx, value) {
          _this.lookupItems[value].selected = true;
          _this.$element.find('option').eq(value).prop('selected', true);
        });

        _this.state.currValue = _this.state.selectedIdx;

        _this.setLabel();

        _this.utils.triggerCallback('Change', _this);
      } else if ( _this.state.currValue !== _this.state.selectedIdx ) {
        // Apply changed value to original select
        _this.$element
          .prop('selectedIndex', _this.state.currValue = _this.state.selectedIdx)
          .data('value', _this.lookupItems[_this.state.selectedIdx].text);

        // Change label text
        _this.setLabel();

        _this.utils.triggerCallback('Change', _this);
      }
    },

    /**
     * Highlight option
     * @param {number} index - Index of the options that will be highlighted
     */
    highlight: function(index) {
      var _this = this;
      var $filteredLi = _this.$li.filter('[data-index]').removeClass('highlighted');

      _this.utils.triggerCallback('BeforeHighlight', _this);

      // Parameter index is required and should not be a disabled item
      if ( index === undefined || index === -1 || _this.lookupItems[index].disabled ) {
        return;
      }

      $filteredLi
        .eq(_this.state.highlightedIdx = index)
        .addClass('highlighted');

      _this.detectItemVisibility(index);

      _this.utils.triggerCallback('Highlight', _this);
    },

    /**
     * Select option
     *
     * @param {number} index - Index of the option that will be selected
     */
    select: function(index) {
      var _this = this;
      var $filteredLi = _this.$li.filter('[data-index]');

      _this.utils.triggerCallback('BeforeSelect', _this, index);

      // Parameter index is required and should not be a disabled item
      if ( index === undefined || index === -1 || _this.lookupItems[index].disabled ) {
        return;
      }

      if ( _this.state.multiple ) {
        // Make sure selectedIdx is an array
        _this.state.selectedIdx = $.isArray(_this.state.selectedIdx) ? _this.state.selectedIdx : [_this.state.selectedIdx];

        var hasSelectedIndex = $.inArray(index, _this.state.selectedIdx);
        if ( hasSelectedIndex !== -1 ) {
          _this.state.selectedIdx.splice(hasSelectedIndex, 1);
        } else {
          _this.state.selectedIdx.push(index);
        }

        $filteredLi
          .removeClass('selected')
          .filter(function(index) {
            return $.inArray(index, _this.state.selectedIdx) !== -1;
          })
          .addClass('selected');
      } else {
        $filteredLi
          .removeClass('selected')
          .eq(_this.state.selectedIdx = index)
          .addClass('selected');
      }

      if ( !_this.state.multiple || !_this.options.multiple.keepMenuOpen ) {
        _this.close();
      }

      _this.change();

      _this.utils.triggerCallback('Select', _this, index);
    },

    /**
     * Unbind and remove
     *
     * @param {boolean} preserveData - Check if the data on the element should be removed too
     */
    destroy: function(preserveData) {
      var _this = this;

      if ( _this.state && _this.state.enabled ) {
        _this.elements.items.add(_this.elements.wrapper).add(_this.elements.input).remove();

        if ( !preserveData ) {
          _this.$element.removeData(pluginName).removeData('value');
        }

        _this.$element.prop('tabindex', _this.originalTabindex).off(eventNamespaceSuffix).off(_this.eventTriggers).unwrap().unwrap();

        _this.state.enabled = false;
      }
    }
  };

  // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations
  $.fn[pluginName] = function(args) {
    return this.each(function() {
      var data = $.data(this, pluginName);

      if ( data && !data.disableOnMobile ) {
        (typeof args === 'string' && data[args]) ? data[args]() : data.init(args);
      } else {
        $.data(this, pluginName, new Selectric(this, args));
      }
    });
  };

  /**
   * Default plugin options
   *
   * @type {object}
   */
  $.fn[pluginName].defaults = {
    onChange             : function(elm) { $(elm).change(); },
    maxHeight            : 300,
    keySearchTimeout     : 500,
    arrowButtonMarkup    : '<b class="button">&#x25be;</b>',
    disableOnMobile      : false,
    nativeOnMobile       : true,
    openOnFocus          : true,
    openOnHover          : false,
    hoverIntentTimeout   : 500,
    expandToItemText     : false,
    responsive           : false,
    preventWindowScroll  : true,
    inheritOriginalWidth : false,
    allowWrap            : true,
    forceRenderAbove     : false,
    forceRenderBelow     : false,
    stopPropagation      : true,
    optionsItemBuilder   : '{text}', // function(itemData, element, index)
    labelBuilder         : '{text}', // function(currItem)
    listBuilder          : false,    // function(items)
    keys                 : {
      previous : [37, 38],                 // Left / Up
      next     : [39, 40],                 // Right / Down
      select   : [9, 13, 27],              // Tab / Enter / Escape
      open     : [13, 32, 37, 38, 39, 40], // Enter / Space / Left / Up / Right / Down
      close    : [9, 27]                   // Tab / Escape
    },
    customClass          : {
      prefix: pluginName,
      camelCase: false
    },
    multiple              : {
      separator: ', ',
      keepMenuOpen: true,
      maxLabelEntries: false
    }
  };
}));

"object"==typeof navigator&&function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define("Plyr",t):(e=e||self).Plyr=t()}(this,(function(){"use strict";!function(){if("undefined"!=typeof window)try{var e=new window.CustomEvent("test",{cancelable:!0});if(e.preventDefault(),!0!==e.defaultPrevented)throw new Error("Could not prevent default")}catch(e){var t=function(e,t){var n,i;return(t=t||{}).bubbles=!!t.bubbles,t.cancelable=!!t.cancelable,(n=document.createEvent("CustomEvent")).initCustomEvent(e,t.bubbles,t.cancelable,t.detail),i=n.preventDefault,n.preventDefault=function(){i.call(this);try{Object.defineProperty(this,"defaultPrevented",{get:function(){return!0}})}catch(e){this.defaultPrevented=!0}},n};t.prototype=window.Event.prototype,window.CustomEvent=t}}();var e="undefined"!=typeof globalThis?globalThis:"undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:{};function t(e,t){return e(t={exports:{}},t.exports),t.exports}var n=function(e){return e&&e.Math==Math&&e},i=n("object"==typeof globalThis&&globalThis)||n("object"==typeof window&&window)||n("object"==typeof self&&self)||n("object"==typeof e&&e)||Function("return this")(),r=function(e){try{return!!e()}catch(e){return!0}},a=!r((function(){return 7!=Object.defineProperty({},1,{get:function(){return 7}})[1]})),o={}.propertyIsEnumerable,s=Object.getOwnPropertyDescriptor,l={f:s&&!o.call({1:2},1)?function(e){var t=s(this,e);return!!t&&t.enumerable}:o},c=function(e,t){return{enumerable:!(1&e),configurable:!(2&e),writable:!(4&e),value:t}},u={}.toString,h=function(e){return u.call(e).slice(8,-1)},f="".split,d=r((function(){return!Object("z").propertyIsEnumerable(0)}))?function(e){return"String"==h(e)?f.call(e,""):Object(e)}:Object,p=function(e){if(null==e)throw TypeError("Can't call method on "+e);return e},m=function(e){return d(p(e))},g=function(e){return"object"==typeof e?null!==e:"function"==typeof e},v=function(e,t){if(!g(e))return e;var n,i;if(t&&"function"==typeof(n=e.toString)&&!g(i=n.call(e)))return i;if("function"==typeof(n=e.valueOf)&&!g(i=n.call(e)))return i;if(!t&&"function"==typeof(n=e.toString)&&!g(i=n.call(e)))return i;throw TypeError("Can't convert object to primitive value")},y={}.hasOwnProperty,b=function(e,t){return y.call(e,t)},w=i.document,k=g(w)&&g(w.createElement),T=function(e){return k?w.createElement(e):{}},S=!a&&!r((function(){return 7!=Object.defineProperty(T("div"),"a",{get:function(){return 7}}).a})),E=Object.getOwnPropertyDescriptor,A={f:a?E:function(e,t){if(e=m(e),t=v(t,!0),S)try{return E(e,t)}catch(e){}if(b(e,t))return c(!l.f.call(e,t),e[t])}},P=function(e){if(!g(e))throw TypeError(String(e)+" is not an object");return e},x=Object.defineProperty,C={f:a?x:function(e,t,n){if(P(e),t=v(t,!0),P(n),S)try{return x(e,t,n)}catch(e){}if("get"in n||"set"in n)throw TypeError("Accessors not supported");return"value"in n&&(e[t]=n.value),e}},O=a?function(e,t,n){return C.f(e,t,c(1,n))}:function(e,t,n){return e[t]=n,e},I=function(e,t){try{O(i,e,t)}catch(n){i[e]=t}return t},L=i["__core-js_shared__"]||I("__core-js_shared__",{}),j=Function.toString;"function"!=typeof L.inspectSource&&(L.inspectSource=function(e){return j.call(e)});var N,R,M,_=L.inspectSource,U=i.WeakMap,D="function"==typeof U&&/native code/.test(_(U)),F=t((function(e){(e.exports=function(e,t){return L[e]||(L[e]=void 0!==t?t:{})})("versions",[]).push({version:"3.6.5",mode:"global",copyright:"© 2020 Denis Pushkarev (zloirock.ru)"})})),q=0,H=Math.random(),B=function(e){return"Symbol("+String(void 0===e?"":e)+")_"+(++q+H).toString(36)},V=F("keys"),z=function(e){return V[e]||(V[e]=B(e))},W={},K=i.WeakMap;if(D){var $=new K,Y=$.get,G=$.has,X=$.set;N=function(e,t){return X.call($,e,t),t},R=function(e){return Y.call($,e)||{}},M=function(e){return G.call($,e)}}else{var Q=z("state");W[Q]=!0,N=function(e,t){return O(e,Q,t),t},R=function(e){return b(e,Q)?e[Q]:{}},M=function(e){return b(e,Q)}}var J,Z={set:N,get:R,has:M,enforce:function(e){return M(e)?R(e):N(e,{})},getterFor:function(e){return function(t){var n;if(!g(t)||(n=R(t)).type!==e)throw TypeError("Incompatible receiver, "+e+" required");return n}}},ee=t((function(e){var t=Z.get,n=Z.enforce,r=String(String).split("String");(e.exports=function(e,t,a,o){var s=!!o&&!!o.unsafe,l=!!o&&!!o.enumerable,c=!!o&&!!o.noTargetGet;"function"==typeof a&&("string"!=typeof t||b(a,"name")||O(a,"name",t),n(a).source=r.join("string"==typeof t?t:"")),e!==i?(s?!c&&e[t]&&(l=!0):delete e[t],l?e[t]=a:O(e,t,a)):l?e[t]=a:I(t,a)})(Function.prototype,"toString",(function(){return"function"==typeof this&&t(this).source||_(this)}))})),te=i,ne=function(e){return"function"==typeof e?e:void 0},ie=function(e,t){return arguments.length<2?ne(te[e])||ne(i[e]):te[e]&&te[e][t]||i[e]&&i[e][t]},re=Math.ceil,ae=Math.floor,oe=function(e){return isNaN(e=+e)?0:(e>0?ae:re)(e)},se=Math.min,le=function(e){return e>0?se(oe(e),9007199254740991):0},ce=Math.max,ue=Math.min,he=function(e,t){var n=oe(e);return n<0?ce(n+t,0):ue(n,t)},fe=function(e){return function(t,n,i){var r,a=m(t),o=le(a.length),s=he(i,o);if(e&&n!=n){for(;o>s;)if((r=a[s++])!=r)return!0}else for(;o>s;s++)if((e||s in a)&&a[s]===n)return e||s||0;return!e&&-1}},de={includes:fe(!0),indexOf:fe(!1)},pe=de.indexOf,me=function(e,t){var n,i=m(e),r=0,a=[];for(n in i)!b(W,n)&&b(i,n)&&a.push(n);for(;t.length>r;)b(i,n=t[r++])&&(~pe(a,n)||a.push(n));return a},ge=["constructor","hasOwnProperty","isPrototypeOf","propertyIsEnumerable","toLocaleString","toString","valueOf"],ve=ge.concat("length","prototype"),ye={f:Object.getOwnPropertyNames||function(e){return me(e,ve)}},be={f:Object.getOwnPropertySymbols},we=ie("Reflect","ownKeys")||function(e){var t=ye.f(P(e)),n=be.f;return n?t.concat(n(e)):t},ke=function(e,t){for(var n=we(t),i=C.f,r=A.f,a=0;a<n.length;a++){var o=n[a];b(e,o)||i(e,o,r(t,o))}},Te=/#|\.prototype\./,Se=function(e,t){var n=Ae[Ee(e)];return n==xe||n!=Pe&&("function"==typeof t?r(t):!!t)},Ee=Se.normalize=function(e){return String(e).replace(Te,".").toLowerCase()},Ae=Se.data={},Pe=Se.NATIVE="N",xe=Se.POLYFILL="P",Ce=Se,Oe=A.f,Ie=function(e,t){var n,r,a,o,s,l=e.target,c=e.global,u=e.stat;if(n=c?i:u?i[l]||I(l,{}):(i[l]||{}).prototype)for(r in t){if(o=t[r],a=e.noTargetGet?(s=Oe(n,r))&&s.value:n[r],!Ce(c?r:l+(u?".":"#")+r,e.forced)&&void 0!==a){if(typeof o==typeof a)continue;ke(o,a)}(e.sham||a&&a.sham)&&O(o,"sham",!0),ee(n,r,o,e)}},Le=!!Object.getOwnPropertySymbols&&!r((function(){return!String(Symbol())})),je=Le&&!Symbol.sham&&"symbol"==typeof Symbol.iterator,Ne=Array.isArray||function(e){return"Array"==h(e)},Re=function(e){return Object(p(e))},Me=Object.keys||function(e){return me(e,ge)},_e=a?Object.defineProperties:function(e,t){P(e);for(var n,i=Me(t),r=i.length,a=0;r>a;)C.f(e,n=i[a++],t[n]);return e},Ue=ie("document","documentElement"),De=z("IE_PROTO"),Fe=function(){},qe=function(e){return"<script>"+e+"<\/script>"},He=function(){try{J=document.domain&&new ActiveXObject("htmlfile")}catch(e){}var e,t;He=J?function(e){e.write(qe("")),e.close();var t=e.parentWindow.Object;return e=null,t}(J):((t=T("iframe")).style.display="none",Ue.appendChild(t),t.src=String("javascript:"),(e=t.contentWindow.document).open(),e.write(qe("document.F=Object")),e.close(),e.F);for(var n=ge.length;n--;)delete He.prototype[ge[n]];return He()};W[De]=!0;var Be=Object.create||function(e,t){var n;return null!==e?(Fe.prototype=P(e),n=new Fe,Fe.prototype=null,n[De]=e):n=He(),void 0===t?n:_e(n,t)},Ve=ye.f,ze={}.toString,We="object"==typeof window&&window&&Object.getOwnPropertyNames?Object.getOwnPropertyNames(window):[],Ke={f:function(e){return We&&"[object Window]"==ze.call(e)?function(e){try{return Ve(e)}catch(e){return We.slice()}}(e):Ve(m(e))}},$e=F("wks"),Ye=i.Symbol,Ge=je?Ye:Ye&&Ye.withoutSetter||B,Xe=function(e){return b($e,e)||(Le&&b(Ye,e)?$e[e]=Ye[e]:$e[e]=Ge("Symbol."+e)),$e[e]},Qe={f:Xe},Je=C.f,Ze=function(e){var t=te.Symbol||(te.Symbol={});b(t,e)||Je(t,e,{value:Qe.f(e)})},et=C.f,tt=Xe("toStringTag"),nt=function(e,t,n){e&&!b(e=n?e:e.prototype,tt)&&et(e,tt,{configurable:!0,value:t})},it=function(e){if("function"!=typeof e)throw TypeError(String(e)+" is not a function");return e},rt=function(e,t,n){if(it(e),void 0===t)return e;switch(n){case 0:return function(){return e.call(t)};case 1:return function(n){return e.call(t,n)};case 2:return function(n,i){return e.call(t,n,i)};case 3:return function(n,i,r){return e.call(t,n,i,r)}}return function(){return e.apply(t,arguments)}},at=Xe("species"),ot=function(e,t){var n;return Ne(e)&&("function"!=typeof(n=e.constructor)||n!==Array&&!Ne(n.prototype)?g(n)&&null===(n=n[at])&&(n=void 0):n=void 0),new(void 0===n?Array:n)(0===t?0:t)},st=[].push,lt=function(e){var t=1==e,n=2==e,i=3==e,r=4==e,a=6==e,o=5==e||a;return function(s,l,c,u){for(var h,f,p=Re(s),m=d(p),g=rt(l,c,3),v=le(m.length),y=0,b=u||ot,w=t?b(s,v):n?b(s,0):void 0;v>y;y++)if((o||y in m)&&(f=g(h=m[y],y,p),e))if(t)w[y]=f;else if(f)switch(e){case 3:return!0;case 5:return h;case 6:return y;case 2:st.call(w,h)}else if(r)return!1;return a?-1:i||r?r:w}},ct={forEach:lt(0),map:lt(1),filter:lt(2),some:lt(3),every:lt(4),find:lt(5),findIndex:lt(6)},ut=ct.forEach,ht=z("hidden"),ft=Xe("toPrimitive"),dt=Z.set,pt=Z.getterFor("Symbol"),mt=Object.prototype,gt=i.Symbol,vt=ie("JSON","stringify"),yt=A.f,bt=C.f,wt=Ke.f,kt=l.f,Tt=F("symbols"),St=F("op-symbols"),Et=F("string-to-symbol-registry"),At=F("symbol-to-string-registry"),Pt=F("wks"),xt=i.QObject,Ct=!xt||!xt.prototype||!xt.prototype.findChild,Ot=a&&r((function(){return 7!=Be(bt({},"a",{get:function(){return bt(this,"a",{value:7}).a}})).a}))?function(e,t,n){var i=yt(mt,t);i&&delete mt[t],bt(e,t,n),i&&e!==mt&&bt(mt,t,i)}:bt,It=function(e,t){var n=Tt[e]=Be(gt.prototype);return dt(n,{type:"Symbol",tag:e,description:t}),a||(n.description=t),n},Lt=je?function(e){return"symbol"==typeof e}:function(e){return Object(e)instanceof gt},jt=function(e,t,n){e===mt&&jt(St,t,n),P(e);var i=v(t,!0);return P(n),b(Tt,i)?(n.enumerable?(b(e,ht)&&e[ht][i]&&(e[ht][i]=!1),n=Be(n,{enumerable:c(0,!1)})):(b(e,ht)||bt(e,ht,c(1,{})),e[ht][i]=!0),Ot(e,i,n)):bt(e,i,n)},Nt=function(e,t){P(e);var n=m(t),i=Me(n).concat(Ut(n));return ut(i,(function(t){a&&!Rt.call(n,t)||jt(e,t,n[t])})),e},Rt=function(e){var t=v(e,!0),n=kt.call(this,t);return!(this===mt&&b(Tt,t)&&!b(St,t))&&(!(n||!b(this,t)||!b(Tt,t)||b(this,ht)&&this[ht][t])||n)},Mt=function(e,t){var n=m(e),i=v(t,!0);if(n!==mt||!b(Tt,i)||b(St,i)){var r=yt(n,i);return!r||!b(Tt,i)||b(n,ht)&&n[ht][i]||(r.enumerable=!0),r}},_t=function(e){var t=wt(m(e)),n=[];return ut(t,(function(e){b(Tt,e)||b(W,e)||n.push(e)})),n},Ut=function(e){var t=e===mt,n=wt(t?St:m(e)),i=[];return ut(n,(function(e){!b(Tt,e)||t&&!b(mt,e)||i.push(Tt[e])})),i};if(Le||(ee((gt=function(){if(this instanceof gt)throw TypeError("Symbol is not a constructor");var e=arguments.length&&void 0!==arguments[0]?String(arguments[0]):void 0,t=B(e),n=function(e){this===mt&&n.call(St,e),b(this,ht)&&b(this[ht],t)&&(this[ht][t]=!1),Ot(this,t,c(1,e))};return a&&Ct&&Ot(mt,t,{configurable:!0,set:n}),It(t,e)}).prototype,"toString",(function(){return pt(this).tag})),ee(gt,"withoutSetter",(function(e){return It(B(e),e)})),l.f=Rt,C.f=jt,A.f=Mt,ye.f=Ke.f=_t,be.f=Ut,Qe.f=function(e){return It(Xe(e),e)},a&&(bt(gt.prototype,"description",{configurable:!0,get:function(){return pt(this).description}}),ee(mt,"propertyIsEnumerable",Rt,{unsafe:!0}))),Ie({global:!0,wrap:!0,forced:!Le,sham:!Le},{Symbol:gt}),ut(Me(Pt),(function(e){Ze(e)})),Ie({target:"Symbol",stat:!0,forced:!Le},{for:function(e){var t=String(e);if(b(Et,t))return Et[t];var n=gt(t);return Et[t]=n,At[n]=t,n},keyFor:function(e){if(!Lt(e))throw TypeError(e+" is not a symbol");if(b(At,e))return At[e]},useSetter:function(){Ct=!0},useSimple:function(){Ct=!1}}),Ie({target:"Object",stat:!0,forced:!Le,sham:!a},{create:function(e,t){return void 0===t?Be(e):Nt(Be(e),t)},defineProperty:jt,defineProperties:Nt,getOwnPropertyDescriptor:Mt}),Ie({target:"Object",stat:!0,forced:!Le},{getOwnPropertyNames:_t,getOwnPropertySymbols:Ut}),Ie({target:"Object",stat:!0,forced:r((function(){be.f(1)}))},{getOwnPropertySymbols:function(e){return be.f(Re(e))}}),vt){var Dt=!Le||r((function(){var e=gt();return"[null]"!=vt([e])||"{}"!=vt({a:e})||"{}"!=vt(Object(e))}));Ie({target:"JSON",stat:!0,forced:Dt},{stringify:function(e,t,n){for(var i,r=[e],a=1;arguments.length>a;)r.push(arguments[a++]);if(i=t,(g(t)||void 0!==e)&&!Lt(e))return Ne(t)||(t=function(e,t){if("function"==typeof i&&(t=i.call(this,e,t)),!Lt(t))return t}),r[1]=t,vt.apply(null,r)}})}gt.prototype[ft]||O(gt.prototype,ft,gt.prototype.valueOf),nt(gt,"Symbol"),W[ht]=!0;var Ft=C.f,qt=i.Symbol;if(a&&"function"==typeof qt&&(!("description"in qt.prototype)||void 0!==qt().description)){var Ht={},Bt=function(){var e=arguments.length<1||void 0===arguments[0]?void 0:String(arguments[0]),t=this instanceof Bt?new qt(e):void 0===e?qt():qt(e);return""===e&&(Ht[t]=!0),t};ke(Bt,qt);var Vt=Bt.prototype=qt.prototype;Vt.constructor=Bt;var zt=Vt.toString,Wt="Symbol(test)"==String(qt("test")),Kt=/^Symbol\((.*)\)[^)]+$/;Ft(Vt,"description",{configurable:!0,get:function(){var e=g(this)?this.valueOf():this,t=zt.call(e);if(b(Ht,e))return"";var n=Wt?t.slice(7,-1):t.replace(Kt,"$1");return""===n?void 0:n}}),Ie({global:!0,forced:!0},{Symbol:Bt})}Ze("iterator");var $t=function(e,t){var n=[][e];return!!n&&r((function(){n.call(null,t||function(){throw 1},1)}))},Yt=Object.defineProperty,Gt={},Xt=function(e){throw e},Qt=function(e,t){if(b(Gt,e))return Gt[e];t||(t={});var n=[][e],i=!!b(t,"ACCESSORS")&&t.ACCESSORS,o=b(t,0)?t[0]:Xt,s=b(t,1)?t[1]:void 0;return Gt[e]=!!n&&!r((function(){if(i&&!a)return!0;var e={length:-1};i?Yt(e,1,{enumerable:!0,get:Xt}):e[1]=1,n.call(e,o,s)}))},Jt=ct.forEach,Zt=$t("forEach"),en=Qt("forEach"),tn=Zt&&en?[].forEach:function(e){return Jt(this,e,arguments.length>1?arguments[1]:void 0)};Ie({target:"Array",proto:!0,forced:[].forEach!=tn},{forEach:tn});var nn=de.indexOf,rn=[].indexOf,an=!!rn&&1/[1].indexOf(1,-0)<0,on=$t("indexOf"),sn=Qt("indexOf",{ACCESSORS:!0,1:0});Ie({target:"Array",proto:!0,forced:an||!on||!sn},{indexOf:function(e){return an?rn.apply(this,arguments)||0:nn(this,e,arguments.length>1?arguments[1]:void 0)}});var ln=Xe("unscopables"),cn=Array.prototype;null==cn[ln]&&C.f(cn,ln,{configurable:!0,value:Be(null)});var un,hn,fn,dn=function(e){cn[ln][e]=!0},pn={},mn=!r((function(){function e(){}return e.prototype.constructor=null,Object.getPrototypeOf(new e)!==e.prototype})),gn=z("IE_PROTO"),vn=Object.prototype,yn=mn?Object.getPrototypeOf:function(e){return e=Re(e),b(e,gn)?e[gn]:"function"==typeof e.constructor&&e instanceof e.constructor?e.constructor.prototype:e instanceof Object?vn:null},bn=Xe("iterator"),wn=!1;[].keys&&("next"in(fn=[].keys())?(hn=yn(yn(fn)))!==Object.prototype&&(un=hn):wn=!0),null==un&&(un={}),b(un,bn)||O(un,bn,(function(){return this}));var kn={IteratorPrototype:un,BUGGY_SAFARI_ITERATORS:wn},Tn=kn.IteratorPrototype,Sn=function(){return this},En=function(e,t,n){var i=t+" Iterator";return e.prototype=Be(Tn,{next:c(1,n)}),nt(e,i,!1),pn[i]=Sn,e},An=Object.setPrototypeOf||("__proto__"in{}?function(){var e,t=!1,n={};try{(e=Object.getOwnPropertyDescriptor(Object.prototype,"__proto__").set).call(n,[]),t=n instanceof Array}catch(e){}return function(n,i){return P(n),function(e){if(!g(e)&&null!==e)throw TypeError("Can't set "+String(e)+" as a prototype")}(i),t?e.call(n,i):n.__proto__=i,n}}():void 0),Pn=kn.IteratorPrototype,xn=kn.BUGGY_SAFARI_ITERATORS,Cn=Xe("iterator"),On=function(){return this},In=function(e,t,n,i,r,a,o){En(n,t,i);var s,l,c,u=function(e){if(e===r&&m)return m;if(!xn&&e in d)return d[e];switch(e){case"keys":case"values":case"entries":return function(){return new n(this,e)}}return function(){return new n(this)}},h=t+" Iterator",f=!1,d=e.prototype,p=d[Cn]||d["@@iterator"]||r&&d[r],m=!xn&&p||u(r),g="Array"==t&&d.entries||p;if(g&&(s=yn(g.call(new e)),Pn!==Object.prototype&&s.next&&(yn(s)!==Pn&&(An?An(s,Pn):"function"!=typeof s[Cn]&&O(s,Cn,On)),nt(s,h,!0))),"values"==r&&p&&"values"!==p.name&&(f=!0,m=function(){return p.call(this)}),d[Cn]!==m&&O(d,Cn,m),pn[t]=m,r)if(l={values:u("values"),keys:a?m:u("keys"),entries:u("entries")},o)for(c in l)(xn||f||!(c in d))&&ee(d,c,l[c]);else Ie({target:t,proto:!0,forced:xn||f},l);return l},Ln=Z.set,jn=Z.getterFor("Array Iterator"),Nn=In(Array,"Array",(function(e,t){Ln(this,{type:"Array Iterator",target:m(e),index:0,kind:t})}),(function(){var e=jn(this),t=e.target,n=e.kind,i=e.index++;return!t||i>=t.length?(e.target=void 0,{value:void 0,done:!0}):"keys"==n?{value:i,done:!1}:"values"==n?{value:t[i],done:!1}:{value:[i,t[i]],done:!1}}),"values");pn.Arguments=pn.Array,dn("keys"),dn("values"),dn("entries");var Rn=[].join,Mn=d!=Object,_n=$t("join",",");Ie({target:"Array",proto:!0,forced:Mn||!_n},{join:function(e){return Rn.call(m(this),void 0===e?",":e)}});var Un,Dn,Fn=function(e,t,n){var i=v(t);i in e?C.f(e,i,c(0,n)):e[i]=n},qn=ie("navigator","userAgent")||"",Hn=i.process,Bn=Hn&&Hn.versions,Vn=Bn&&Bn.v8;Vn?Dn=(Un=Vn.split("."))[0]+Un[1]:qn&&(!(Un=qn.match(/Edge\/(\d+)/))||Un[1]>=74)&&(Un=qn.match(/Chrome\/(\d+)/))&&(Dn=Un[1]);var zn=Dn&&+Dn,Wn=Xe("species"),Kn=function(e){return zn>=51||!r((function(){var t=[];return(t.constructor={})[Wn]=function(){return{foo:1}},1!==t[e](Boolean).foo}))},$n=Kn("slice"),Yn=Qt("slice",{ACCESSORS:!0,0:0,1:2}),Gn=Xe("species"),Xn=[].slice,Qn=Math.max;Ie({target:"Array",proto:!0,forced:!$n||!Yn},{slice:function(e,t){var n,i,r,a=m(this),o=le(a.length),s=he(e,o),l=he(void 0===t?o:t,o);if(Ne(a)&&("function"!=typeof(n=a.constructor)||n!==Array&&!Ne(n.prototype)?g(n)&&null===(n=n[Gn])&&(n=void 0):n=void 0,n===Array||void 0===n))return Xn.call(a,s,l);for(i=new(void 0===n?Array:n)(Qn(l-s,0)),r=0;s<l;s++,r++)s in a&&Fn(i,r,a[s]);return i.length=r,i}});var Jn={};Jn[Xe("toStringTag")]="z";var Zn="[object z]"===String(Jn),ei=Xe("toStringTag"),ti="Arguments"==h(function(){return arguments}()),ni=Zn?h:function(e){var t,n,i;return void 0===e?"Undefined":null===e?"Null":"string"==typeof(n=function(e,t){try{return e[t]}catch(e){}}(t=Object(e),ei))?n:ti?h(t):"Object"==(i=h(t))&&"function"==typeof t.callee?"Arguments":i},ii=Zn?{}.toString:function(){return"[object "+ni(this)+"]"};Zn||ee(Object.prototype,"toString",ii,{unsafe:!0});var ri=function(){var e=P(this),t="";return e.global&&(t+="g"),e.ignoreCase&&(t+="i"),e.multiline&&(t+="m"),e.dotAll&&(t+="s"),e.unicode&&(t+="u"),e.sticky&&(t+="y"),t};function ai(e,t){return RegExp(e,t)}var oi={UNSUPPORTED_Y:r((function(){var e=ai("a","y");return e.lastIndex=2,null!=e.exec("abcd")})),BROKEN_CARET:r((function(){var e=ai("^r","gy");return e.lastIndex=2,null!=e.exec("str")}))},si=RegExp.prototype.exec,li=String.prototype.replace,ci=si,ui=function(){var e=/a/,t=/b*/g;return si.call(e,"a"),si.call(t,"a"),0!==e.lastIndex||0!==t.lastIndex}(),hi=oi.UNSUPPORTED_Y||oi.BROKEN_CARET,fi=void 0!==/()??/.exec("")[1];(ui||fi||hi)&&(ci=function(e){var t,n,i,r,a=this,o=hi&&a.sticky,s=ri.call(a),l=a.source,c=0,u=e;return o&&(-1===(s=s.replace("y","")).indexOf("g")&&(s+="g"),u=String(e).slice(a.lastIndex),a.lastIndex>0&&(!a.multiline||a.multiline&&"\n"!==e[a.lastIndex-1])&&(l="(?: "+l+")",u=" "+u,c++),n=new RegExp("^(?:"+l+")",s)),fi&&(n=new RegExp("^"+l+"$(?!\\s)",s)),ui&&(t=a.lastIndex),i=si.call(o?n:a,u),o?i?(i.input=i.input.slice(c),i[0]=i[0].slice(c),i.index=a.lastIndex,a.lastIndex+=i[0].length):a.lastIndex=0:ui&&i&&(a.lastIndex=a.global?i.index+i[0].length:t),fi&&i&&i.length>1&&li.call(i[0],n,(function(){for(r=1;r<arguments.length-2;r++)void 0===arguments[r]&&(i[r]=void 0)})),i});var di=ci;Ie({target:"RegExp",proto:!0,forced:/./.exec!==di},{exec:di});var pi=RegExp.prototype,mi=pi.toString,gi=r((function(){return"/a/b"!=mi.call({source:"a",flags:"b"})})),vi="toString"!=mi.name;(gi||vi)&&ee(RegExp.prototype,"toString",(function(){var e=P(this),t=String(e.source),n=e.flags;return"/"+t+"/"+String(void 0===n&&e instanceof RegExp&&!("flags"in pi)?ri.call(e):n)}),{unsafe:!0});var yi=function(e){return function(t,n){var i,r,a=String(p(t)),o=oe(n),s=a.length;return o<0||o>=s?e?"":void 0:(i=a.charCodeAt(o))<55296||i>56319||o+1===s||(r=a.charCodeAt(o+1))<56320||r>57343?e?a.charAt(o):i:e?a.slice(o,o+2):r-56320+(i-55296<<10)+65536}},bi={codeAt:yi(!1),charAt:yi(!0)},wi=bi.charAt,ki=Z.set,Ti=Z.getterFor("String Iterator");In(String,"String",(function(e){ki(this,{type:"String Iterator",string:String(e),index:0})}),(function(){var e,t=Ti(this),n=t.string,i=t.index;return i>=n.length?{value:void 0,done:!0}:(e=wi(n,i),t.index+=e.length,{value:e,done:!1})}));var Si=Xe("species"),Ei=!r((function(){var e=/./;return e.exec=function(){var e=[];return e.groups={a:"7"},e},"7"!=="".replace(e,"$<a>")})),Ai="$0"==="a".replace(/./,"$0"),Pi=Xe("replace"),xi=!!/./[Pi]&&""===/./[Pi]("a","$0"),Ci=!r((function(){var e=/(?:)/,t=e.exec;e.exec=function(){return t.apply(this,arguments)};var n="ab".split(e);return 2!==n.length||"a"!==n[0]||"b"!==n[1]})),Oi=function(e,t,n,i){var a=Xe(e),o=!r((function(){var t={};return t[a]=function(){return 7},7!=""[e](t)})),s=o&&!r((function(){var t=!1,n=/a/;return"split"===e&&((n={}).constructor={},n.constructor[Si]=function(){return n},n.flags="",n[a]=/./[a]),n.exec=function(){return t=!0,null},n[a](""),!t}));if(!o||!s||"replace"===e&&(!Ei||!Ai||xi)||"split"===e&&!Ci){var l=/./[a],c=n(a,""[e],(function(e,t,n,i,r){return t.exec===di?o&&!r?{done:!0,value:l.call(t,n,i)}:{done:!0,value:e.call(n,t,i)}:{done:!1}}),{REPLACE_KEEPS_$0:Ai,REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE:xi}),u=c[0],h=c[1];ee(String.prototype,e,u),ee(RegExp.prototype,a,2==t?function(e,t){return h.call(e,this,t)}:function(e){return h.call(e,this)})}i&&O(RegExp.prototype[a],"sham",!0)},Ii=bi.charAt,Li=function(e,t,n){return t+(n?Ii(e,t).length:1)},ji=function(e,t){var n=e.exec;if("function"==typeof n){var i=n.call(e,t);if("object"!=typeof i)throw TypeError("RegExp exec method returned something other than an Object or null");return i}if("RegExp"!==h(e))throw TypeError("RegExp#exec called on incompatible receiver");return di.call(e,t)},Ni=Math.max,Ri=Math.min,Mi=Math.floor,_i=/\$([$&'`]|\d\d?|<[^>]*>)/g,Ui=/\$([$&'`]|\d\d?)/g;Oi("replace",2,(function(e,t,n,i){var r=i.REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE,a=i.REPLACE_KEEPS_$0,o=r?"$":"$0";return[function(n,i){var r=p(this),a=null==n?void 0:n[e];return void 0!==a?a.call(n,r,i):t.call(String(r),n,i)},function(e,i){if(!r&&a||"string"==typeof i&&-1===i.indexOf(o)){var l=n(t,e,this,i);if(l.done)return l.value}var c=P(e),u=String(this),h="function"==typeof i;h||(i=String(i));var f=c.global;if(f){var d=c.unicode;c.lastIndex=0}for(var p=[];;){var m=ji(c,u);if(null===m)break;if(p.push(m),!f)break;""===String(m[0])&&(c.lastIndex=Li(u,le(c.lastIndex),d))}for(var g,v="",y=0,b=0;b<p.length;b++){m=p[b];for(var w=String(m[0]),k=Ni(Ri(oe(m.index),u.length),0),T=[],S=1;S<m.length;S++)T.push(void 0===(g=m[S])?g:String(g));var E=m.groups;if(h){var A=[w].concat(T,k,u);void 0!==E&&A.push(E);var x=String(i.apply(void 0,A))}else x=s(w,u,k,T,E,i);k>=y&&(v+=u.slice(y,k)+x,y=k+w.length)}return v+u.slice(y)}];function s(e,n,i,r,a,o){var s=i+e.length,l=r.length,c=Ui;return void 0!==a&&(a=Re(a),c=_i),t.call(o,c,(function(t,o){var c;switch(o.charAt(0)){case"$":return"$";case"&":return e;case"`":return n.slice(0,i);case"'":return n.slice(s);case"<":c=a[o.slice(1,-1)];break;default:var u=+o;if(0===u)return t;if(u>l){var h=Mi(u/10);return 0===h?t:h<=l?void 0===r[h-1]?o.charAt(1):r[h-1]+o.charAt(1):t}c=r[u-1]}return void 0===c?"":c}))}}));var Di=Object.is||function(e,t){return e===t?0!==e||1/e==1/t:e!=e&&t!=t};Oi("search",1,(function(e,t,n){return[function(t){var n=p(this),i=null==t?void 0:t[e];return void 0!==i?i.call(t,n):new RegExp(t)[e](String(n))},function(e){var i=n(t,e,this);if(i.done)return i.value;var r=P(e),a=String(this),o=r.lastIndex;Di(o,0)||(r.lastIndex=0);var s=ji(r,a);return Di(r.lastIndex,o)||(r.lastIndex=o),null===s?-1:s.index}]}));var Fi=Xe("match"),qi=function(e){var t;return g(e)&&(void 0!==(t=e[Fi])?!!t:"RegExp"==h(e))},Hi=Xe("species"),Bi=function(e,t){var n,i=P(e).constructor;return void 0===i||null==(n=P(i)[Hi])?t:it(n)},Vi=[].push,zi=Math.min,Wi=!r((function(){return!RegExp(4294967295,"y")}));Oi("split",2,(function(e,t,n){var i;return i="c"=="abbc".split(/(b)*/)[1]||4!="test".split(/(?:)/,-1).length||2!="ab".split(/(?:ab)*/).length||4!=".".split(/(.?)(.?)/).length||".".split(/()()/).length>1||"".split(/.?/).length?function(e,n){var i=String(p(this)),r=void 0===n?4294967295:n>>>0;if(0===r)return[];if(void 0===e)return[i];if(!qi(e))return t.call(i,e,r);for(var a,o,s,l=[],c=(e.ignoreCase?"i":"")+(e.multiline?"m":"")+(e.unicode?"u":"")+(e.sticky?"y":""),u=0,h=new RegExp(e.source,c+"g");(a=di.call(h,i))&&!((o=h.lastIndex)>u&&(l.push(i.slice(u,a.index)),a.length>1&&a.index<i.length&&Vi.apply(l,a.slice(1)),s=a[0].length,u=o,l.length>=r));)h.lastIndex===a.index&&h.lastIndex++;return u===i.length?!s&&h.test("")||l.push(""):l.push(i.slice(u)),l.length>r?l.slice(0,r):l}:"0".split(void 0,0).length?function(e,n){return void 0===e&&0===n?[]:t.call(this,e,n)}:t,[function(t,n){var r=p(this),a=null==t?void 0:t[e];return void 0!==a?a.call(t,r,n):i.call(String(r),t,n)},function(e,r){var a=n(i,e,this,r,i!==t);if(a.done)return a.value;var o=P(e),s=String(this),l=Bi(o,RegExp),c=o.unicode,u=(o.ignoreCase?"i":"")+(o.multiline?"m":"")+(o.unicode?"u":"")+(Wi?"y":"g"),h=new l(Wi?o:"^(?:"+o.source+")",u),f=void 0===r?4294967295:r>>>0;if(0===f)return[];if(0===s.length)return null===ji(h,s)?[s]:[];for(var d=0,p=0,m=[];p<s.length;){h.lastIndex=Wi?p:0;var g,v=ji(h,Wi?s:s.slice(p));if(null===v||(g=zi(le(h.lastIndex+(Wi?0:p)),s.length))===d)p=Li(s,p,c);else{if(m.push(s.slice(d,p)),m.length===f)return m;for(var y=1;y<=v.length-1;y++)if(m.push(v[y]),m.length===f)return m;p=d=g}}return m.push(s.slice(d)),m}]}),!Wi);var Ki={CSSRuleList:0,CSSStyleDeclaration:0,CSSValueList:0,ClientRectList:0,DOMRectList:0,DOMStringList:0,DOMTokenList:1,DataTransferItemList:0,FileList:0,HTMLAllCollection:0,HTMLCollection:0,HTMLFormElement:0,HTMLSelectElement:0,MediaList:0,MimeTypeArray:0,NamedNodeMap:0,NodeList:1,PaintRequestList:0,Plugin:0,PluginArray:0,SVGLengthList:0,SVGNumberList:0,SVGPathSegList:0,SVGPointList:0,SVGStringList:0,SVGTransformList:0,SourceBufferList:0,StyleSheetList:0,TextTrackCueList:0,TextTrackList:0,TouchList:0};for(var $i in Ki){var Yi=i[$i],Gi=Yi&&Yi.prototype;if(Gi&&Gi.forEach!==tn)try{O(Gi,"forEach",tn)}catch(e){Gi.forEach=tn}}var Xi=Xe("iterator"),Qi=Xe("toStringTag"),Ji=Nn.values;for(var Zi in Ki){var er=i[Zi],tr=er&&er.prototype;if(tr){if(tr[Xi]!==Ji)try{O(tr,Xi,Ji)}catch(e){tr[Xi]=Ji}if(tr[Qi]||O(tr,Qi,Zi),Ki[Zi])for(var nr in Nn)if(tr[nr]!==Nn[nr])try{O(tr,nr,Nn[nr])}catch(e){tr[nr]=Nn[nr]}}}var ir=Xe("iterator"),rr=!r((function(){var e=new URL("b?a=1&b=2&c=3","http://a"),t=e.searchParams,n="";return e.pathname="c%20d",t.forEach((function(e,i){t.delete("b"),n+=i+e})),!t.sort||"http://a/c%20d?a=1&c=3"!==e.href||"3"!==t.get("c")||"a=1"!==String(new URLSearchParams("?a=1"))||!t[ir]||"a"!==new URL("https://a@b").username||"b"!==new URLSearchParams(new URLSearchParams("a=b")).get("a")||"xn--e1aybc"!==new URL("http://тест").host||"#%D0%B1"!==new URL("http://a#б").hash||"a1c3"!==n||"x"!==new URL("http://x",void 0).host})),ar=function(e,t,n){if(!(e instanceof t))throw TypeError("Incorrect "+(n?n+" ":"")+"invocation");return e},or=Object.assign,sr=Object.defineProperty,lr=!or||r((function(){if(a&&1!==or({b:1},or(sr({},"a",{enumerable:!0,get:function(){sr(this,"b",{value:3,enumerable:!1})}}),{b:2})).b)return!0;var e={},t={},n=Symbol();return e[n]=7,"abcdefghijklmnopqrst".split("").forEach((function(e){t[e]=e})),7!=or({},e)[n]||"abcdefghijklmnopqrst"!=Me(or({},t)).join("")}))?function(e,t){for(var n=Re(e),i=arguments.length,r=1,o=be.f,s=l.f;i>r;)for(var c,u=d(arguments[r++]),h=o?Me(u).concat(o(u)):Me(u),f=h.length,p=0;f>p;)c=h[p++],a&&!s.call(u,c)||(n[c]=u[c]);return n}:or,cr=function(e,t,n,i){try{return i?t(P(n)[0],n[1]):t(n)}catch(t){var r=e.return;throw void 0!==r&&P(r.call(e)),t}},ur=Xe("iterator"),hr=Array.prototype,fr=function(e){return void 0!==e&&(pn.Array===e||hr[ur]===e)},dr=Xe("iterator"),pr=function(e){if(null!=e)return e[dr]||e["@@iterator"]||pn[ni(e)]},mr=function(e){var t,n,i,r,a,o,s=Re(e),l="function"==typeof this?this:Array,c=arguments.length,u=c>1?arguments[1]:void 0,h=void 0!==u,f=pr(s),d=0;if(h&&(u=rt(u,c>2?arguments[2]:void 0,2)),null==f||l==Array&&fr(f))for(n=new l(t=le(s.length));t>d;d++)o=h?u(s[d],d):s[d],Fn(n,d,o);else for(a=(r=f.call(s)).next,n=new l;!(i=a.call(r)).done;d++)o=h?cr(r,u,[i.value,d],!0):i.value,Fn(n,d,o);return n.length=d,n},gr=/[^\0-\u007E]/,vr=/[.\u3002\uFF0E\uFF61]/g,yr="Overflow: input needs wider integers to process",br=Math.floor,wr=String.fromCharCode,kr=function(e){return e+22+75*(e<26)},Tr=function(e,t,n){var i=0;for(e=n?br(e/700):e>>1,e+=br(e/t);e>455;i+=36)e=br(e/35);return br(i+36*e/(e+38))},Sr=function(e){var t,n,i=[],r=(e=function(e){for(var t=[],n=0,i=e.length;n<i;){var r=e.charCodeAt(n++);if(r>=55296&&r<=56319&&n<i){var a=e.charCodeAt(n++);56320==(64512&a)?t.push(((1023&r)<<10)+(1023&a)+65536):(t.push(r),n--)}else t.push(r)}return t}(e)).length,a=128,o=0,s=72;for(t=0;t<e.length;t++)(n=e[t])<128&&i.push(wr(n));var l=i.length,c=l;for(l&&i.push("-");c<r;){var u=2147483647;for(t=0;t<e.length;t++)(n=e[t])>=a&&n<u&&(u=n);var h=c+1;if(u-a>br((2147483647-o)/h))throw RangeError(yr);for(o+=(u-a)*h,a=u,t=0;t<e.length;t++){if((n=e[t])<a&&++o>2147483647)throw RangeError(yr);if(n==a){for(var f=o,d=36;;d+=36){var p=d<=s?1:d>=s+26?26:d-s;if(f<p)break;var m=f-p,g=36-p;i.push(wr(kr(p+m%g))),f=br(m/g)}i.push(wr(kr(f))),s=Tr(o,h,c==l),o=0,++c}}++o,++a}return i.join("")},Er=function(e,t,n){for(var i in t)ee(e,i,t[i],n);return e},Ar=function(e){var t=pr(e);if("function"!=typeof t)throw TypeError(String(e)+" is not iterable");return P(t.call(e))},Pr=ie("fetch"),xr=ie("Headers"),Cr=Xe("iterator"),Or=Z.set,Ir=Z.getterFor("URLSearchParams"),Lr=Z.getterFor("URLSearchParamsIterator"),jr=/\+/g,Nr=Array(4),Rr=function(e){return Nr[e-1]||(Nr[e-1]=RegExp("((?:%[\\da-f]{2}){"+e+"})","gi"))},Mr=function(e){try{return decodeURIComponent(e)}catch(t){return e}},_r=function(e){var t=e.replace(jr," "),n=4;try{return decodeURIComponent(t)}catch(e){for(;n;)t=t.replace(Rr(n--),Mr);return t}},Ur=/[!'()~]|%20/g,Dr={"!":"%21","'":"%27","(":"%28",")":"%29","~":"%7E","%20":"+"},Fr=function(e){return Dr[e]},qr=function(e){return encodeURIComponent(e).replace(Ur,Fr)},Hr=function(e,t){if(t)for(var n,i,r=t.split("&"),a=0;a<r.length;)(n=r[a++]).length&&(i=n.split("="),e.push({key:_r(i.shift()),value:_r(i.join("="))}))},Br=function(e){this.entries.length=0,Hr(this.entries,e)},Vr=function(e,t){if(e<t)throw TypeError("Not enough arguments")},zr=En((function(e,t){Or(this,{type:"URLSearchParamsIterator",iterator:Ar(Ir(e).entries),kind:t})}),"Iterator",(function(){var e=Lr(this),t=e.kind,n=e.iterator.next(),i=n.value;return n.done||(n.value="keys"===t?i.key:"values"===t?i.value:[i.key,i.value]),n})),Wr=function(){ar(this,Wr,"URLSearchParams");var e,t,n,i,r,a,o,s,l,c=arguments.length>0?arguments[0]:void 0,u=this,h=[];if(Or(u,{type:"URLSearchParams",entries:h,updateURL:function(){},updateSearchParams:Br}),void 0!==c)if(g(c))if("function"==typeof(e=pr(c)))for(n=(t=e.call(c)).next;!(i=n.call(t)).done;){if((o=(a=(r=Ar(P(i.value))).next).call(r)).done||(s=a.call(r)).done||!a.call(r).done)throw TypeError("Expected sequence with length 2");h.push({key:o.value+"",value:s.value+""})}else for(l in c)b(c,l)&&h.push({key:l,value:c[l]+""});else Hr(h,"string"==typeof c?"?"===c.charAt(0)?c.slice(1):c:c+"")},Kr=Wr.prototype;Er(Kr,{append:function(e,t){Vr(arguments.length,2);var n=Ir(this);n.entries.push({key:e+"",value:t+""}),n.updateURL()},delete:function(e){Vr(arguments.length,1);for(var t=Ir(this),n=t.entries,i=e+"",r=0;r<n.length;)n[r].key===i?n.splice(r,1):r++;t.updateURL()},get:function(e){Vr(arguments.length,1);for(var t=Ir(this).entries,n=e+"",i=0;i<t.length;i++)if(t[i].key===n)return t[i].value;return null},getAll:function(e){Vr(arguments.length,1);for(var t=Ir(this).entries,n=e+"",i=[],r=0;r<t.length;r++)t[r].key===n&&i.push(t[r].value);return i},has:function(e){Vr(arguments.length,1);for(var t=Ir(this).entries,n=e+"",i=0;i<t.length;)if(t[i++].key===n)return!0;return!1},set:function(e,t){Vr(arguments.length,1);for(var n,i=Ir(this),r=i.entries,a=!1,o=e+"",s=t+"",l=0;l<r.length;l++)(n=r[l]).key===o&&(a?r.splice(l--,1):(a=!0,n.value=s));a||r.push({key:o,value:s}),i.updateURL()},sort:function(){var e,t,n,i=Ir(this),r=i.entries,a=r.slice();for(r.length=0,n=0;n<a.length;n++){for(e=a[n],t=0;t<n;t++)if(r[t].key>e.key){r.splice(t,0,e);break}t===n&&r.push(e)}i.updateURL()},forEach:function(e){for(var t,n=Ir(this).entries,i=rt(e,arguments.length>1?arguments[1]:void 0,3),r=0;r<n.length;)i((t=n[r++]).value,t.key,this)},keys:function(){return new zr(this,"keys")},values:function(){return new zr(this,"values")},entries:function(){return new zr(this,"entries")}},{enumerable:!0}),ee(Kr,Cr,Kr.entries),ee(Kr,"toString",(function(){for(var e,t=Ir(this).entries,n=[],i=0;i<t.length;)e=t[i++],n.push(qr(e.key)+"="+qr(e.value));return n.join("&")}),{enumerable:!0}),nt(Wr,"URLSearchParams"),Ie({global:!0,forced:!rr},{URLSearchParams:Wr}),rr||"function"!=typeof Pr||"function"!=typeof xr||Ie({global:!0,enumerable:!0,forced:!0},{fetch:function(e){var t,n,i,r=[e];return arguments.length>1&&(t=arguments[1],g(t)&&(n=t.body,"URLSearchParams"===ni(n)&&((i=t.headers?new xr(t.headers):new xr).has("content-type")||i.set("content-type","application/x-www-form-urlencoded;charset=UTF-8"),t=Be(t,{body:c(0,String(n)),headers:c(0,i)}))),r.push(t)),Pr.apply(this,r)}});var $r,Yr={URLSearchParams:Wr,getState:Ir},Gr=bi.codeAt,Xr=i.URL,Qr=Yr.URLSearchParams,Jr=Yr.getState,Zr=Z.set,ea=Z.getterFor("URL"),ta=Math.floor,na=Math.pow,ia=/[A-Za-z]/,ra=/[\d+-.A-Za-z]/,aa=/\d/,oa=/^(0x|0X)/,sa=/^[0-7]+$/,la=/^\d+$/,ca=/^[\dA-Fa-f]+$/,ua=/[\u0000\u0009\u000A\u000D #%/:?@[\\]]/,ha=/[\u0000\u0009\u000A\u000D #/:?@[\\]]/,fa=/^[\u0000-\u001F ]+|[\u0000-\u001F ]+$/g,da=/[\u0009\u000A\u000D]/g,pa=function(e,t){var n,i,r;if("["==t.charAt(0)){if("]"!=t.charAt(t.length-1))return"Invalid host";if(!(n=ga(t.slice(1,-1))))return"Invalid host";e.host=n}else if(Ea(e)){if(t=function(e){var t,n,i=[],r=e.toLowerCase().replace(vr,".").split(".");for(t=0;t<r.length;t++)n=r[t],i.push(gr.test(n)?"xn--"+Sr(n):n);return i.join(".")}(t),ua.test(t))return"Invalid host";if(null===(n=ma(t)))return"Invalid host";e.host=n}else{if(ha.test(t))return"Invalid host";for(n="",i=mr(t),r=0;r<i.length;r++)n+=Ta(i[r],ya);e.host=n}},ma=function(e){var t,n,i,r,a,o,s,l=e.split(".");if(l.length&&""==l[l.length-1]&&l.pop(),(t=l.length)>4)return e;for(n=[],i=0;i<t;i++){if(""==(r=l[i]))return e;if(a=10,r.length>1&&"0"==r.charAt(0)&&(a=oa.test(r)?16:8,r=r.slice(8==a?1:2)),""===r)o=0;else{if(!(10==a?la:8==a?sa:ca).test(r))return e;o=parseInt(r,a)}n.push(o)}for(i=0;i<t;i++)if(o=n[i],i==t-1){if(o>=na(256,5-t))return null}else if(o>255)return null;for(s=n.pop(),i=0;i<n.length;i++)s+=n[i]*na(256,3-i);return s},ga=function(e){var t,n,i,r,a,o,s,l=[0,0,0,0,0,0,0,0],c=0,u=null,h=0,f=function(){return e.charAt(h)};if(":"==f()){if(":"!=e.charAt(1))return;h+=2,u=++c}for(;f();){if(8==c)return;if(":"!=f()){for(t=n=0;n<4&&ca.test(f());)t=16*t+parseInt(f(),16),h++,n++;if("."==f()){if(0==n)return;if(h-=n,c>6)return;for(i=0;f();){if(r=null,i>0){if(!("."==f()&&i<4))return;h++}if(!aa.test(f()))return;for(;aa.test(f());){if(a=parseInt(f(),10),null===r)r=a;else{if(0==r)return;r=10*r+a}if(r>255)return;h++}l[c]=256*l[c]+r,2!=++i&&4!=i||c++}if(4!=i)return;break}if(":"==f()){if(h++,!f())return}else if(f())return;l[c++]=t}else{if(null!==u)return;h++,u=++c}}if(null!==u)for(o=c-u,c=7;0!=c&&o>0;)s=l[c],l[c--]=l[u+o-1],l[u+--o]=s;else if(8!=c)return;return l},va=function(e){var t,n,i,r;if("number"==typeof e){for(t=[],n=0;n<4;n++)t.unshift(e%256),e=ta(e/256);return t.join(".")}if("object"==typeof e){for(t="",i=function(e){for(var t=null,n=1,i=null,r=0,a=0;a<8;a++)0!==e[a]?(r>n&&(t=i,n=r),i=null,r=0):(null===i&&(i=a),++r);return r>n&&(t=i,n=r),t}(e),n=0;n<8;n++)r&&0===e[n]||(r&&(r=!1),i===n?(t+=n?":":"::",r=!0):(t+=e[n].toString(16),n<7&&(t+=":")));return"["+t+"]"}return e},ya={},ba=lr({},ya,{" ":1,'"':1,"<":1,">":1,"`":1}),wa=lr({},ba,{"#":1,"?":1,"{":1,"}":1}),ka=lr({},wa,{"/":1,":":1,";":1,"=":1,"@":1,"[":1,"\\":1,"]":1,"^":1,"|":1}),Ta=function(e,t){var n=Gr(e,0);return n>32&&n<127&&!b(t,e)?e:encodeURIComponent(e)},Sa={ftp:21,file:null,http:80,https:443,ws:80,wss:443},Ea=function(e){return b(Sa,e.scheme)},Aa=function(e){return""!=e.username||""!=e.password},Pa=function(e){return!e.host||e.cannotBeABaseURL||"file"==e.scheme},xa=function(e,t){var n;return 2==e.length&&ia.test(e.charAt(0))&&(":"==(n=e.charAt(1))||!t&&"|"==n)},Ca=function(e){var t;return e.length>1&&xa(e.slice(0,2))&&(2==e.length||"/"===(t=e.charAt(2))||"\\"===t||"?"===t||"#"===t)},Oa=function(e){var t=e.path,n=t.length;!n||"file"==e.scheme&&1==n&&xa(t[0],!0)||t.pop()},Ia=function(e){return"."===e||"%2e"===e.toLowerCase()},La={},ja={},Na={},Ra={},Ma={},_a={},Ua={},Da={},Fa={},qa={},Ha={},Ba={},Va={},za={},Wa={},Ka={},$a={},Ya={},Ga={},Xa={},Qa={},Ja=function(e,t,n,i){var r,a,o,s,l,c=n||La,u=0,h="",f=!1,d=!1,p=!1;for(n||(e.scheme="",e.username="",e.password="",e.host=null,e.port=null,e.path=[],e.query=null,e.fragment=null,e.cannotBeABaseURL=!1,t=t.replace(fa,"")),t=t.replace(da,""),r=mr(t);u<=r.length;){switch(a=r[u],c){case La:if(!a||!ia.test(a)){if(n)return"Invalid scheme";c=Na;continue}h+=a.toLowerCase(),c=ja;break;case ja:if(a&&(ra.test(a)||"+"==a||"-"==a||"."==a))h+=a.toLowerCase();else{if(":"!=a){if(n)return"Invalid scheme";h="",c=Na,u=0;continue}if(n&&(Ea(e)!=b(Sa,h)||"file"==h&&(Aa(e)||null!==e.port)||"file"==e.scheme&&!e.host))return;if(e.scheme=h,n)return void(Ea(e)&&Sa[e.scheme]==e.port&&(e.port=null));h="","file"==e.scheme?c=za:Ea(e)&&i&&i.scheme==e.scheme?c=Ra:Ea(e)?c=Da:"/"==r[u+1]?(c=Ma,u++):(e.cannotBeABaseURL=!0,e.path.push(""),c=Ga)}break;case Na:if(!i||i.cannotBeABaseURL&&"#"!=a)return"Invalid scheme";if(i.cannotBeABaseURL&&"#"==a){e.scheme=i.scheme,e.path=i.path.slice(),e.query=i.query,e.fragment="",e.cannotBeABaseURL=!0,c=Qa;break}c="file"==i.scheme?za:_a;continue;case Ra:if("/"!=a||"/"!=r[u+1]){c=_a;continue}c=Fa,u++;break;case Ma:if("/"==a){c=qa;break}c=Ya;continue;case _a:if(e.scheme=i.scheme,a==$r)e.username=i.username,e.password=i.password,e.host=i.host,e.port=i.port,e.path=i.path.slice(),e.query=i.query;else if("/"==a||"\\"==a&&Ea(e))c=Ua;else if("?"==a)e.username=i.username,e.password=i.password,e.host=i.host,e.port=i.port,e.path=i.path.slice(),e.query="",c=Xa;else{if("#"!=a){e.username=i.username,e.password=i.password,e.host=i.host,e.port=i.port,e.path=i.path.slice(),e.path.pop(),c=Ya;continue}e.username=i.username,e.password=i.password,e.host=i.host,e.port=i.port,e.path=i.path.slice(),e.query=i.query,e.fragment="",c=Qa}break;case Ua:if(!Ea(e)||"/"!=a&&"\\"!=a){if("/"!=a){e.username=i.username,e.password=i.password,e.host=i.host,e.port=i.port,c=Ya;continue}c=qa}else c=Fa;break;case Da:if(c=Fa,"/"!=a||"/"!=h.charAt(u+1))continue;u++;break;case Fa:if("/"!=a&&"\\"!=a){c=qa;continue}break;case qa:if("@"==a){f&&(h="%40"+h),f=!0,o=mr(h);for(var m=0;m<o.length;m++){var g=o[m];if(":"!=g||p){var v=Ta(g,ka);p?e.password+=v:e.username+=v}else p=!0}h=""}else if(a==$r||"/"==a||"?"==a||"#"==a||"\\"==a&&Ea(e)){if(f&&""==h)return"Invalid authority";u-=mr(h).length+1,h="",c=Ha}else h+=a;break;case Ha:case Ba:if(n&&"file"==e.scheme){c=Ka;continue}if(":"!=a||d){if(a==$r||"/"==a||"?"==a||"#"==a||"\\"==a&&Ea(e)){if(Ea(e)&&""==h)return"Invalid host";if(n&&""==h&&(Aa(e)||null!==e.port))return;if(s=pa(e,h))return s;if(h="",c=$a,n)return;continue}"["==a?d=!0:"]"==a&&(d=!1),h+=a}else{if(""==h)return"Invalid host";if(s=pa(e,h))return s;if(h="",c=Va,n==Ba)return}break;case Va:if(!aa.test(a)){if(a==$r||"/"==a||"?"==a||"#"==a||"\\"==a&&Ea(e)||n){if(""!=h){var y=parseInt(h,10);if(y>65535)return"Invalid port";e.port=Ea(e)&&y===Sa[e.scheme]?null:y,h=""}if(n)return;c=$a;continue}return"Invalid port"}h+=a;break;case za:if(e.scheme="file","/"==a||"\\"==a)c=Wa;else{if(!i||"file"!=i.scheme){c=Ya;continue}if(a==$r)e.host=i.host,e.path=i.path.slice(),e.query=i.query;else if("?"==a)e.host=i.host,e.path=i.path.slice(),e.query="",c=Xa;else{if("#"!=a){Ca(r.slice(u).join(""))||(e.host=i.host,e.path=i.path.slice(),Oa(e)),c=Ya;continue}e.host=i.host,e.path=i.path.slice(),e.query=i.query,e.fragment="",c=Qa}}break;case Wa:if("/"==a||"\\"==a){c=Ka;break}i&&"file"==i.scheme&&!Ca(r.slice(u).join(""))&&(xa(i.path[0],!0)?e.path.push(i.path[0]):e.host=i.host),c=Ya;continue;case Ka:if(a==$r||"/"==a||"\\"==a||"?"==a||"#"==a){if(!n&&xa(h))c=Ya;else if(""==h){if(e.host="",n)return;c=$a}else{if(s=pa(e,h))return s;if("localhost"==e.host&&(e.host=""),n)return;h="",c=$a}continue}h+=a;break;case $a:if(Ea(e)){if(c=Ya,"/"!=a&&"\\"!=a)continue}else if(n||"?"!=a)if(n||"#"!=a){if(a!=$r&&(c=Ya,"/"!=a))continue}else e.fragment="",c=Qa;else e.query="",c=Xa;break;case Ya:if(a==$r||"/"==a||"\\"==a&&Ea(e)||!n&&("?"==a||"#"==a)){if(".."===(l=(l=h).toLowerCase())||"%2e."===l||".%2e"===l||"%2e%2e"===l?(Oa(e),"/"==a||"\\"==a&&Ea(e)||e.path.push("")):Ia(h)?"/"==a||"\\"==a&&Ea(e)||e.path.push(""):("file"==e.scheme&&!e.path.length&&xa(h)&&(e.host&&(e.host=""),h=h.charAt(0)+":"),e.path.push(h)),h="","file"==e.scheme&&(a==$r||"?"==a||"#"==a))for(;e.path.length>1&&""===e.path[0];)e.path.shift();"?"==a?(e.query="",c=Xa):"#"==a&&(e.fragment="",c=Qa)}else h+=Ta(a,wa);break;case Ga:"?"==a?(e.query="",c=Xa):"#"==a?(e.fragment="",c=Qa):a!=$r&&(e.path[0]+=Ta(a,ya));break;case Xa:n||"#"!=a?a!=$r&&("'"==a&&Ea(e)?e.query+="%27":e.query+="#"==a?"%23":Ta(a,ya)):(e.fragment="",c=Qa);break;case Qa:a!=$r&&(e.fragment+=Ta(a,ba))}u++}},Za=function(e){var t,n,i=ar(this,Za,"URL"),r=arguments.length>1?arguments[1]:void 0,o=String(e),s=Zr(i,{type:"URL"});if(void 0!==r)if(r instanceof Za)t=ea(r);else if(n=Ja(t={},String(r)))throw TypeError(n);if(n=Ja(s,o,null,t))throw TypeError(n);var l=s.searchParams=new Qr,c=Jr(l);c.updateSearchParams(s.query),c.updateURL=function(){s.query=String(l)||null},a||(i.href=to.call(i),i.origin=no.call(i),i.protocol=io.call(i),i.username=ro.call(i),i.password=ao.call(i),i.host=oo.call(i),i.hostname=so.call(i),i.port=lo.call(i),i.pathname=co.call(i),i.search=uo.call(i),i.searchParams=ho.call(i),i.hash=fo.call(i))},eo=Za.prototype,to=function(){var e=ea(this),t=e.scheme,n=e.username,i=e.password,r=e.host,a=e.port,o=e.path,s=e.query,l=e.fragment,c=t+":";return null!==r?(c+="//",Aa(e)&&(c+=n+(i?":"+i:"")+"@"),c+=va(r),null!==a&&(c+=":"+a)):"file"==t&&(c+="//"),c+=e.cannotBeABaseURL?o[0]:o.length?"/"+o.join("/"):"",null!==s&&(c+="?"+s),null!==l&&(c+="#"+l),c},no=function(){var e=ea(this),t=e.scheme,n=e.port;if("blob"==t)try{return new URL(t.path[0]).origin}catch(e){return"null"}return"file"!=t&&Ea(e)?t+"://"+va(e.host)+(null!==n?":"+n:""):"null"},io=function(){return ea(this).scheme+":"},ro=function(){return ea(this).username},ao=function(){return ea(this).password},oo=function(){var e=ea(this),t=e.host,n=e.port;return null===t?"":null===n?va(t):va(t)+":"+n},so=function(){var e=ea(this).host;return null===e?"":va(e)},lo=function(){var e=ea(this).port;return null===e?"":String(e)},co=function(){var e=ea(this),t=e.path;return e.cannotBeABaseURL?t[0]:t.length?"/"+t.join("/"):""},uo=function(){var e=ea(this).query;return e?"?"+e:""},ho=function(){return ea(this).searchParams},fo=function(){var e=ea(this).fragment;return e?"#"+e:""},po=function(e,t){return{get:e,set:t,configurable:!0,enumerable:!0}};if(a&&_e(eo,{href:po(to,(function(e){var t=ea(this),n=String(e),i=Ja(t,n);if(i)throw TypeError(i);Jr(t.searchParams).updateSearchParams(t.query)})),origin:po(no),protocol:po(io,(function(e){var t=ea(this);Ja(t,String(e)+":",La)})),username:po(ro,(function(e){var t=ea(this),n=mr(String(e));if(!Pa(t)){t.username="";for(var i=0;i<n.length;i++)t.username+=Ta(n[i],ka)}})),password:po(ao,(function(e){var t=ea(this),n=mr(String(e));if(!Pa(t)){t.password="";for(var i=0;i<n.length;i++)t.password+=Ta(n[i],ka)}})),host:po(oo,(function(e){var t=ea(this);t.cannotBeABaseURL||Ja(t,String(e),Ha)})),hostname:po(so,(function(e){var t=ea(this);t.cannotBeABaseURL||Ja(t,String(e),Ba)})),port:po(lo,(function(e){var t=ea(this);Pa(t)||(""==(e=String(e))?t.port=null:Ja(t,e,Va))})),pathname:po(co,(function(e){var t=ea(this);t.cannotBeABaseURL||(t.path=[],Ja(t,e+"",$a))})),search:po(uo,(function(e){var t=ea(this);""==(e=String(e))?t.query=null:("?"==e.charAt(0)&&(e=e.slice(1)),t.query="",Ja(t,e,Xa)),Jr(t.searchParams).updateSearchParams(t.query)})),searchParams:po(ho),hash:po(fo,(function(e){var t=ea(this);""!=(e=String(e))?("#"==e.charAt(0)&&(e=e.slice(1)),t.fragment="",Ja(t,e,Qa)):t.fragment=null}))}),ee(eo,"toJSON",(function(){return to.call(this)}),{enumerable:!0}),ee(eo,"toString",(function(){return to.call(this)}),{enumerable:!0}),Xr){var mo=Xr.createObjectURL,go=Xr.revokeObjectURL;mo&&ee(Za,"createObjectURL",(function(e){return mo.apply(Xr,arguments)})),go&&ee(Za,"revokeObjectURL",(function(e){return go.apply(Xr,arguments)}))}function vo(e){return(vo="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e})(e)}function yo(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function bo(e,t){for(var n=0;n<t.length;n++){var i=t[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(e,i.key,i)}}function wo(e,t,n){return t&&bo(e.prototype,t),n&&bo(e,n),e}function ko(e,t,n){return t in e?Object.defineProperty(e,t,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[t]=n,e}function To(e,t){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var i=Object.getOwnPropertySymbols(e);t&&(i=i.filter((function(t){return Object.getOwnPropertyDescriptor(e,t).enumerable}))),n.push.apply(n,i)}return n}function So(e){for(var t=1;t<arguments.length;t++){var n=null!=arguments[t]?arguments[t]:{};t%2?To(Object(n),!0).forEach((function(t){ko(e,t,n[t])})):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):To(Object(n)).forEach((function(t){Object.defineProperty(e,t,Object.getOwnPropertyDescriptor(n,t))}))}return e}function Eo(e,t){if(null==e)return{};var n,i,r=function(e,t){if(null==e)return{};var n,i,r={},a=Object.keys(e);for(i=0;i<a.length;i++)n=a[i],t.indexOf(n)>=0||(r[n]=e[n]);return r}(e,t);if(Object.getOwnPropertySymbols){var a=Object.getOwnPropertySymbols(e);for(i=0;i<a.length;i++)n=a[i],t.indexOf(n)>=0||Object.prototype.propertyIsEnumerable.call(e,n)&&(r[n]=e[n])}return r}function Ao(e,t){return function(e){if(Array.isArray(e))return e}(e)||function(e,t){if("undefined"==typeof Symbol||!(Symbol.iterator in Object(e)))return;var n=[],i=!0,r=!1,a=void 0;try{for(var o,s=e[Symbol.iterator]();!(i=(o=s.next()).done)&&(n.push(o.value),!t||n.length!==t);i=!0);}catch(e){r=!0,a=e}finally{try{i||null==s.return||s.return()}finally{if(r)throw a}}return n}(e,t)||xo(e,t)||function(){throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}()}function Po(e){return function(e){if(Array.isArray(e))return Co(e)}(e)||function(e){if("undefined"!=typeof Symbol&&Symbol.iterator in Object(e))return Array.from(e)}(e)||xo(e)||function(){throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}()}function xo(e,t){if(e){if("string"==typeof e)return Co(e,t);var n=Object.prototype.toString.call(e).slice(8,-1);return"Object"===n&&e.constructor&&(n=e.constructor.name),"Map"===n||"Set"===n?Array.from(e):"Arguments"===n||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)?Co(e,t):void 0}}function Co(e,t){(null==t||t>e.length)&&(t=e.length);for(var n=0,i=new Array(t);n<t;n++)i[n]=e[n];return i}nt(Za,"URL"),Ie({global:!0,forced:!rr,sham:!a},{URL:Za}),function(e){var t=function(){try{return!!Symbol.iterator}catch(e){return!1}}(),n=function(e){var n={next:function(){var t=e.shift();return{done:void 0===t,value:t}}};return t&&(n[Symbol.iterator]=function(){return n}),n},i=function(e){return encodeURIComponent(e).replace(/%20/g,"+")},r=function(e){return decodeURIComponent(String(e).replace(/\+/g," "))};(function(){try{var t=e.URLSearchParams;return"a=1"===new t("?a=1").toString()&&"function"==typeof t.prototype.set}catch(e){return!1}})()||function(){var r=function e(t){Object.defineProperty(this,"_entries",{writable:!0,value:{}});var n=vo(t);if("undefined"===n);else if("string"===n)""!==t&&this._fromString(t);else if(t instanceof e){var i=this;t.forEach((function(e,t){i.append(t,e)}))}else{if(null===t||"object"!==n)throw new TypeError("Unsupported input's type for URLSearchParams");if("[object Array]"===Object.prototype.toString.call(t))for(var r=0;r<t.length;r++){var a=t[r];if("[object Array]"!==Object.prototype.toString.call(a)&&2===a.length)throw new TypeError("Expected [string, any] as entry at index "+r+" of URLSearchParams's input");this.append(a[0],a[1])}else for(var o in t)t.hasOwnProperty(o)&&this.append(o,t[o])}},a=r.prototype;a.append=function(e,t){e in this._entries?this._entries[e].push(String(t)):this._entries[e]=[String(t)]},a.delete=function(e){delete this._entries[e]},a.get=function(e){return e in this._entries?this._entries[e][0]:null},a.getAll=function(e){return e in this._entries?this._entries[e].slice(0):[]},a.has=function(e){return e in this._entries},a.set=function(e,t){this._entries[e]=[String(t)]},a.forEach=function(e,t){var n;for(var i in this._entries)if(this._entries.hasOwnProperty(i)){n=this._entries[i];for(var r=0;r<n.length;r++)e.call(t,n[r],i,this)}},a.keys=function(){var e=[];return this.forEach((function(t,n){e.push(n)})),n(e)},a.values=function(){var e=[];return this.forEach((function(t){e.push(t)})),n(e)},a.entries=function(){var e=[];return this.forEach((function(t,n){e.push([n,t])})),n(e)},t&&(a[Symbol.iterator]=a.entries),a.toString=function(){var e=[];return this.forEach((function(t,n){e.push(i(n)+"="+i(t))})),e.join("&")},e.URLSearchParams=r}();var a=e.URLSearchParams.prototype;"function"!=typeof a.sort&&(a.sort=function(){var e=this,t=[];this.forEach((function(n,i){t.push([i,n]),e._entries||e.delete(i)})),t.sort((function(e,t){return e[0]<t[0]?-1:e[0]>t[0]?1:0})),e._entries&&(e._entries={});for(var n=0;n<t.length;n++)this.append(t[n][0],t[n][1])}),"function"!=typeof a._fromString&&Object.defineProperty(a,"_fromString",{enumerable:!1,configurable:!1,writable:!1,value:function(e){if(this._entries)this._entries={};else{var t=[];this.forEach((function(e,n){t.push(n)}));for(var n=0;n<t.length;n++)this.delete(t[n])}var i,a=(e=e.replace(/^\?/,"")).split("&");for(n=0;n<a.length;n++)i=a[n].split("="),this.append(r(i[0]),i.length>1?r(i[1]):"")}})}(void 0!==e?e:"undefined"!=typeof window?window:"undefined"!=typeof self?self:e),function(e){if(function(){try{var t=new e.URL("b","http://a");return t.pathname="c d","http://a/c%20d"===t.href&&t.searchParams}catch(e){return!1}}()||function(){var t=e.URL,n=function(t,n){"string"!=typeof t&&(t=String(t));var i,r=document;if(n&&(void 0===e.location||n!==e.location.href)){(i=(r=document.implementation.createHTMLDocument("")).createElement("base")).href=n,r.head.appendChild(i);try{if(0!==i.href.indexOf(n))throw new Error(i.href)}catch(e){throw new Error("URL unable to set base "+n+" due to "+e)}}var a=r.createElement("a");if(a.href=t,i&&(r.body.appendChild(a),a.href=a.href),":"===a.protocol||!/:/.test(a.href))throw new TypeError("Invalid URL");Object.defineProperty(this,"_anchorElement",{value:a});var o=new e.URLSearchParams(this.search),s=!0,l=!0,c=this;["append","delete","set"].forEach((function(e){var t=o[e];o[e]=function(){t.apply(o,arguments),s&&(l=!1,c.search=o.toString(),l=!0)}})),Object.defineProperty(this,"searchParams",{value:o,enumerable:!0});var u=void 0;Object.defineProperty(this,"_updateSearchParams",{enumerable:!1,configurable:!1,writable:!1,value:function(){this.search!==u&&(u=this.search,l&&(s=!1,this.searchParams._fromString(this.search),s=!0))}})},i=n.prototype;["hash","host","hostname","port","protocol"].forEach((function(e){!function(e){Object.defineProperty(i,e,{get:function(){return this._anchorElement[e]},set:function(t){this._anchorElement[e]=t},enumerable:!0})}(e)})),Object.defineProperty(i,"search",{get:function(){return this._anchorElement.search},set:function(e){this._anchorElement.search=e,this._updateSearchParams()},enumerable:!0}),Object.defineProperties(i,{toString:{get:function(){var e=this;return function(){return e.href}}},href:{get:function(){return this._anchorElement.href.replace(/\?$/,"")},set:function(e){this._anchorElement.href=e,this._updateSearchParams()},enumerable:!0},pathname:{get:function(){return this._anchorElement.pathname.replace(/(^\/?)/,"/")},set:function(e){this._anchorElement.pathname=e},enumerable:!0},origin:{get:function(){var e={"http:":80,"https:":443,"ftp:":21}[this._anchorElement.protocol],t=this._anchorElement.port!=e&&""!==this._anchorElement.port;return this._anchorElement.protocol+"//"+this._anchorElement.hostname+(t?":"+this._anchorElement.port:"")},enumerable:!0},password:{get:function(){return""},set:function(e){},enumerable:!0},username:{get:function(){return""},set:function(e){},enumerable:!0}}),n.createObjectURL=function(e){return t.createObjectURL.apply(t,arguments)},n.revokeObjectURL=function(e){return t.revokeObjectURL.apply(t,arguments)},e.URL=n}(),void 0!==e.location&&!("origin"in e.location)){var t=function(){return e.location.protocol+"//"+e.location.hostname+(e.location.port?":"+e.location.port:"")};try{Object.defineProperty(e.location,"origin",{get:t,enumerable:!0})}catch(n){setInterval((function(){e.location.origin=t()}),100)}}}(void 0!==e?e:"undefined"!=typeof window?window:"undefined"!=typeof self?self:e);var Oo=Xe("isConcatSpreadable"),Io=zn>=51||!r((function(){var e=[];return e[Oo]=!1,e.concat()[0]!==e})),Lo=Kn("concat"),jo=function(e){if(!g(e))return!1;var t=e[Oo];return void 0!==t?!!t:Ne(e)};Ie({target:"Array",proto:!0,forced:!Io||!Lo},{concat:function(e){var t,n,i,r,a,o=Re(this),s=ot(o,0),l=0;for(t=-1,i=arguments.length;t<i;t++)if(a=-1===t?o:arguments[t],jo(a)){if(l+(r=le(a.length))>9007199254740991)throw TypeError("Maximum allowed index exceeded");for(n=0;n<r;n++,l++)n in a&&Fn(s,l,a[n])}else{if(l>=9007199254740991)throw TypeError("Maximum allowed index exceeded");Fn(s,l++,a)}return s.length=l,s}});var No=ct.filter,Ro=Kn("filter"),Mo=Qt("filter");Ie({target:"Array",proto:!0,forced:!Ro||!Mo},{filter:function(e){return No(this,e,arguments.length>1?arguments[1]:void 0)}});var _o=ct.find,Uo=!0,Do=Qt("find");"find"in[]&&Array(1).find((function(){Uo=!1})),Ie({target:"Array",proto:!0,forced:Uo||!Do},{find:function(e){return _o(this,e,arguments.length>1?arguments[1]:void 0)}}),dn("find");var Fo=Xe("iterator"),qo=!1;try{var Ho=0,Bo={next:function(){return{done:!!Ho++}},return:function(){qo=!0}};Bo[Fo]=function(){return this},Array.from(Bo,(function(){throw 2}))}catch(e){}var Vo=function(e,t){if(!t&&!qo)return!1;var n=!1;try{var i={};i[Fo]=function(){return{next:function(){return{done:n=!0}}}},e(i)}catch(e){}return n},zo=!Vo((function(e){Array.from(e)}));Ie({target:"Array",stat:!0,forced:zo},{from:mr});var Wo=de.includes,Ko=Qt("indexOf",{ACCESSORS:!0,1:0});Ie({target:"Array",proto:!0,forced:!Ko},{includes:function(e){return Wo(this,e,arguments.length>1?arguments[1]:void 0)}}),dn("includes");var $o=ct.map,Yo=Kn("map"),Go=Qt("map");Ie({target:"Array",proto:!0,forced:!Yo||!Go},{map:function(e){return $o(this,e,arguments.length>1?arguments[1]:void 0)}});var Xo=function(e,t,n){var i,r;return An&&"function"==typeof(i=t.constructor)&&i!==n&&g(r=i.prototype)&&r!==n.prototype&&An(e,r),e},Qo="\t\n\v\f\r                　\u2028\u2029\ufeff",Jo="["+Qo+"]",Zo=RegExp("^"+Jo+Jo+"*"),es=RegExp(Jo+Jo+"*$"),ts=function(e){return function(t){var n=String(p(t));return 1&e&&(n=n.replace(Zo,"")),2&e&&(n=n.replace(es,"")),n}},ns={start:ts(1),end:ts(2),trim:ts(3)},is=ye.f,rs=A.f,as=C.f,os=ns.trim,ss=i.Number,ls=ss.prototype,cs="Number"==h(Be(ls)),us=function(e){var t,n,i,r,a,o,s,l,c=v(e,!1);if("string"==typeof c&&c.length>2)if(43===(t=(c=os(c)).charCodeAt(0))||45===t){if(88===(n=c.charCodeAt(2))||120===n)return NaN}else if(48===t){switch(c.charCodeAt(1)){case 66:case 98:i=2,r=49;break;case 79:case 111:i=8,r=55;break;default:return+c}for(o=(a=c.slice(2)).length,s=0;s<o;s++)if((l=a.charCodeAt(s))<48||l>r)return NaN;return parseInt(a,i)}return+c};if(Ce("Number",!ss(" 0o1")||!ss("0b1")||ss("+0x1"))){for(var hs,fs=function(e){var t=arguments.length<1?0:e,n=this;return n instanceof fs&&(cs?r((function(){ls.valueOf.call(n)})):"Number"!=h(n))?Xo(new ss(us(t)),n,fs):us(t)},ds=a?is(ss):"MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","),ps=0;ds.length>ps;ps++)b(ss,hs=ds[ps])&&!b(fs,hs)&&as(fs,hs,rs(ss,hs));fs.prototype=ls,ls.constructor=fs,ee(i,"Number",fs)}var ms=r((function(){Me(1)}));Ie({target:"Object",stat:!0,forced:ms},{keys:function(e){return Me(Re(e))}});var gs=function(e){if(qi(e))throw TypeError("The method doesn't accept regular expressions");return e},vs=Xe("match"),ys=function(e){var t=/./;try{"/./"[e](t)}catch(n){try{return t[vs]=!1,"/./"[e](t)}catch(e){}}return!1};Ie({target:"String",proto:!0,forced:!ys("includes")},{includes:function(e){return!!~String(p(this)).indexOf(gs(e),arguments.length>1?arguments[1]:void 0)}});var bs=!r((function(){return Object.isExtensible(Object.preventExtensions({}))})),ws=t((function(e){var t=C.f,n=B("meta"),i=0,r=Object.isExtensible||function(){return!0},a=function(e){t(e,n,{value:{objectID:"O"+ ++i,weakData:{}}})},o=e.exports={REQUIRED:!1,fastKey:function(e,t){if(!g(e))return"symbol"==typeof e?e:("string"==typeof e?"S":"P")+e;if(!b(e,n)){if(!r(e))return"F";if(!t)return"E";a(e)}return e[n].objectID},getWeakData:function(e,t){if(!b(e,n)){if(!r(e))return!0;if(!t)return!1;a(e)}return e[n].weakData},onFreeze:function(e){return bs&&o.REQUIRED&&r(e)&&!b(e,n)&&a(e),e}};W[n]=!0})),ks=(ws.REQUIRED,ws.fastKey,ws.getWeakData,ws.onFreeze,t((function(e){var t=function(e,t){this.stopped=e,this.result=t};(e.exports=function(e,n,i,r,a){var o,s,l,c,u,h,f,d=rt(n,i,r?2:1);if(a)o=e;else{if("function"!=typeof(s=pr(e)))throw TypeError("Target is not iterable");if(fr(s)){for(l=0,c=le(e.length);c>l;l++)if((u=r?d(P(f=e[l])[0],f[1]):d(e[l]))&&u instanceof t)return u;return new t(!1)}o=s.call(e)}for(h=o.next;!(f=h.call(o)).done;)if("object"==typeof(u=cr(o,d,f.value,r))&&u&&u instanceof t)return u;return new t(!1)}).stop=function(e){return new t(!0,e)}}))),Ts=ws.getWeakData,Ss=Z.set,Es=Z.getterFor,As=ct.find,Ps=ct.findIndex,xs=0,Cs=function(e){return e.frozen||(e.frozen=new Os)},Os=function(){this.entries=[]},Is=function(e,t){return As(e.entries,(function(e){return e[0]===t}))};Os.prototype={get:function(e){var t=Is(this,e);if(t)return t[1]},has:function(e){return!!Is(this,e)},set:function(e,t){var n=Is(this,e);n?n[1]=t:this.entries.push([e,t])},delete:function(e){var t=Ps(this.entries,(function(t){return t[0]===e}));return~t&&this.entries.splice(t,1),!!~t}};var Ls={getConstructor:function(e,t,n,i){var r=e((function(e,a){ar(e,r,t),Ss(e,{type:t,id:xs++,frozen:void 0}),null!=a&&ks(a,e[i],e,n)})),a=Es(t),o=function(e,t,n){var i=a(e),r=Ts(P(t),!0);return!0===r?Cs(i).set(t,n):r[i.id]=n,e};return Er(r.prototype,{delete:function(e){var t=a(this);if(!g(e))return!1;var n=Ts(e);return!0===n?Cs(t).delete(e):n&&b(n,t.id)&&delete n[t.id]},has:function(e){var t=a(this);if(!g(e))return!1;var n=Ts(e);return!0===n?Cs(t).has(e):n&&b(n,t.id)}}),Er(r.prototype,n?{get:function(e){var t=a(this);if(g(e)){var n=Ts(e);return!0===n?Cs(t).get(e):n?n[t.id]:void 0}},set:function(e,t){return o(this,e,t)}}:{add:function(e){return o(this,e,!0)}}),r}},js=(t((function(e){var t,n=Z.enforce,a=!i.ActiveXObject&&"ActiveXObject"in i,o=Object.isExtensible,s=function(e){return function(){return e(this,arguments.length?arguments[0]:void 0)}},l=e.exports=function(e,t,n){var a=-1!==e.indexOf("Map"),o=-1!==e.indexOf("Weak"),s=a?"set":"add",l=i[e],c=l&&l.prototype,u=l,h={},f=function(e){var t=c[e];ee(c,e,"add"==e?function(e){return t.call(this,0===e?0:e),this}:"delete"==e?function(e){return!(o&&!g(e))&&t.call(this,0===e?0:e)}:"get"==e?function(e){return o&&!g(e)?void 0:t.call(this,0===e?0:e)}:"has"==e?function(e){return!(o&&!g(e))&&t.call(this,0===e?0:e)}:function(e,n){return t.call(this,0===e?0:e,n),this})};if(Ce(e,"function"!=typeof l||!(o||c.forEach&&!r((function(){(new l).entries().next()})))))u=n.getConstructor(t,e,a,s),ws.REQUIRED=!0;else if(Ce(e,!0)){var d=new u,p=d[s](o?{}:-0,1)!=d,m=r((function(){d.has(1)})),v=Vo((function(e){new l(e)})),y=!o&&r((function(){for(var e=new l,t=5;t--;)e[s](t,t);return!e.has(-0)}));v||((u=t((function(t,n){ar(t,u,e);var i=Xo(new l,t,u);return null!=n&&ks(n,i[s],i,a),i}))).prototype=c,c.constructor=u),(m||y)&&(f("delete"),f("has"),a&&f("get")),(y||p)&&f(s),o&&c.clear&&delete c.clear}return h[e]=u,Ie({global:!0,forced:u!=l},h),nt(u,e),o||n.setStrong(u,e,a),u}("WeakMap",s,Ls);if(D&&a){t=Ls.getConstructor(s,"WeakMap",!0),ws.REQUIRED=!0;var c=l.prototype,u=c.delete,h=c.has,f=c.get,d=c.set;Er(c,{delete:function(e){if(g(e)&&!o(e)){var i=n(this);return i.frozen||(i.frozen=new t),u.call(this,e)||i.frozen.delete(e)}return u.call(this,e)},has:function(e){if(g(e)&&!o(e)){var i=n(this);return i.frozen||(i.frozen=new t),h.call(this,e)||i.frozen.has(e)}return h.call(this,e)},get:function(e){if(g(e)&&!o(e)){var i=n(this);return i.frozen||(i.frozen=new t),h.call(this,e)?f.call(this,e):i.frozen.get(e)}return f.call(this,e)},set:function(e,i){if(g(e)&&!o(e)){var r=n(this);r.frozen||(r.frozen=new t),h.call(this,e)?d.call(this,e,i):r.frozen.set(e,i)}else d.call(this,e,i);return this}})}})),ct.every),Ns=$t("every"),Rs=Qt("every");Ie({target:"Array",proto:!0,forced:!Ns||!Rs},{every:function(e){return js(this,e,arguments.length>1?arguments[1]:void 0)}}),Ie({target:"Object",stat:!0,forced:Object.assign!==lr},{assign:lr});var Ms=ns.trim;Ie({target:"String",proto:!0,forced:function(e){return r((function(){return!!Qo[e]()||"​᠎"!="​᠎"[e]()||Qo[e].name!==e}))}("trim")},{trim:function(){return Ms(this)}});var _s=ct.some,Us=$t("some"),Ds=Qt("some");Ie({target:"Array",proto:!0,forced:!Us||!Ds},{some:function(e){return _s(this,e,arguments.length>1?arguments[1]:void 0)}});var Fs="".repeat||function(e){var t=String(p(this)),n="",i=oe(e);if(i<0||i==1/0)throw RangeError("Wrong number of repetitions");for(;i>0;(i>>>=1)&&(t+=t))1&i&&(n+=t);return n},qs=1..toFixed,Hs=Math.floor,Bs=function(e,t,n){return 0===t?n:t%2==1?Bs(e,t-1,n*e):Bs(e*e,t/2,n)},Vs=qs&&("0.000"!==8e-5.toFixed(3)||"1"!==.9.toFixed(0)||"1.25"!==1.255.toFixed(2)||"1000000000000000128"!==(0xde0b6b3a7640080).toFixed(0))||!r((function(){qs.call({})}));Ie({target:"Number",proto:!0,forced:Vs},{toFixed:function(e){var t,n,i,r,a=function(e){if("number"!=typeof e&&"Number"!=h(e))throw TypeError("Incorrect invocation");return+e}(this),o=oe(e),s=[0,0,0,0,0,0],l="",c="0",u=function(e,t){for(var n=-1,i=t;++n<6;)i+=e*s[n],s[n]=i%1e7,i=Hs(i/1e7)},f=function(e){for(var t=6,n=0;--t>=0;)n+=s[t],s[t]=Hs(n/e),n=n%e*1e7},d=function(){for(var e=6,t="";--e>=0;)if(""!==t||0===e||0!==s[e]){var n=String(s[e]);t=""===t?n:t+Fs.call("0",7-n.length)+n}return t};if(o<0||o>20)throw RangeError("Incorrect fraction digits");if(a!=a)return"NaN";if(a<=-1e21||a>=1e21)return String(a);if(a<0&&(l="-",a=-a),a>1e-21)if(n=(t=function(e){for(var t=0,n=e;n>=4096;)t+=12,n/=4096;for(;n>=2;)t+=1,n/=2;return t}(a*Bs(2,69,1))-69)<0?a*Bs(2,-t,1):a/Bs(2,t,1),n*=4503599627370496,(t=52-t)>0){for(u(0,n),i=o;i>=7;)u(1e7,0),i-=7;for(u(Bs(10,i,1),0),i=t-1;i>=23;)f(1<<23),i-=23;f(1<<i),u(1,1),f(2),c=d()}else u(0,n),u(1<<-t,0),c=d()+Fs.call("0",o);return c=o>0?l+((r=c.length)<=o?"0."+Fs.call("0",o-r)+c:c.slice(0,r-o)+"."+c.slice(r-o)):l+c}});var zs=l.f,Ws=function(e){return function(t){for(var n,i=m(t),r=Me(i),o=r.length,s=0,l=[];o>s;)n=r[s++],a&&!zs.call(i,n)||l.push(e?[n,i[n]]:i[n]);return l}},Ks={entries:Ws(!0),values:Ws(!1)},$s=Ks.entries;Ie({target:"Object",stat:!0},{entries:function(e){return $s(e)}});var Ys=Ks.values;Ie({target:"Object",stat:!0},{values:function(e){return Ys(e)}}),Ie({target:"Number",stat:!0},{isNaN:function(e){return e!=e}});var Gs=A.f,Xs=r((function(){Gs(1)}));function Qs(e,t){for(var n=0;n<t.length;n++){var i=t[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(e,i.key,i)}}function Js(e,t,n){return t in e?Object.defineProperty(e,t,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[t]=n,e}function Zs(e,t){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var i=Object.getOwnPropertySymbols(e);t&&(i=i.filter((function(t){return Object.getOwnPropertyDescriptor(e,t).enumerable}))),n.push.apply(n,i)}return n}function el(e){for(var t=1;t<arguments.length;t++){var n=null!=arguments[t]?arguments[t]:{};t%2?Zs(Object(n),!0).forEach((function(t){Js(e,t,n[t])})):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):Zs(Object(n)).forEach((function(t){Object.defineProperty(e,t,Object.getOwnPropertyDescriptor(n,t))}))}return e}Ie({target:"Object",stat:!0,forced:!a||Xs,sham:!a},{getOwnPropertyDescriptor:function(e,t){return Gs(m(e),t)}}),Ie({target:"Object",stat:!0,sham:!a},{getOwnPropertyDescriptors:function(e){for(var t,n,i=m(e),r=A.f,a=we(i),o={},s=0;a.length>s;)void 0!==(n=r(i,t=a[s++]))&&Fn(o,t,n);return o}}),Oi("match",1,(function(e,t,n){return[function(t){var n=p(this),i=null==t?void 0:t[e];return void 0!==i?i.call(t,n):new RegExp(t)[e](String(n))},function(e){var i=n(t,e,this);if(i.done)return i.value;var r=P(e),a=String(this);if(!r.global)return ji(r,a);var o=r.unicode;r.lastIndex=0;for(var s,l=[],c=0;null!==(s=ji(r,a));){var u=String(s[0]);l[c]=u,""===u&&(r.lastIndex=Li(a,le(r.lastIndex),o)),c++}return 0===c?null:l}]}));var tl={addCSS:!0,thumbWidth:15,watch:!0};function nl(e,t){return function(){return Array.from(document.querySelectorAll(t)).includes(this)}.call(e,t)}var il=function(e){return null!=e?e.constructor:null},rl=function(e,t){return!!(e&&t&&e instanceof t)},al=function(e){return null==e},ol=function(e){return il(e)===Object},sl=function(e){return il(e)===String},ll=function(e){return Array.isArray(e)},cl=function(e){return rl(e,NodeList)},ul=sl,hl=ll,fl=cl,dl=function(e){return rl(e,Element)},pl=function(e){return rl(e,Event)},ml=function(e){return al(e)||(sl(e)||ll(e)||cl(e))&&!e.length||ol(e)&&!Object.keys(e).length};function gl(e,t){if(1>t){var n=function(e){var t="".concat(e).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);return t?Math.max(0,(t[1]?t[1].length:0)-(t[2]?+t[2]:0)):0}(t);return parseFloat(e.toFixed(n))}return Math.round(e/t)*t}var vl,yl,bl,wl=function(){function e(t,n){(function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")})(this,e),dl(t)?this.element=t:ul(t)&&(this.element=document.querySelector(t)),dl(this.element)&&ml(this.element.rangeTouch)&&(this.config=el({},tl,{},n),this.init())}return function(e,t,n){t&&Qs(e.prototype,t),n&&Qs(e,n)}(e,[{key:"init",value:function(){e.enabled&&(this.config.addCSS&&(this.element.style.userSelect="none",this.element.style.webKitUserSelect="none",this.element.style.touchAction="manipulation"),this.listeners(!0),this.element.rangeTouch=this)}},{key:"destroy",value:function(){e.enabled&&(this.config.addCSS&&(this.element.style.userSelect="",this.element.style.webKitUserSelect="",this.element.style.touchAction=""),this.listeners(!1),this.element.rangeTouch=null)}},{key:"listeners",value:function(e){var t=this,n=e?"addEventListener":"removeEventListener";["touchstart","touchmove","touchend"].forEach((function(e){t.element[n](e,(function(e){return t.set(e)}),!1)}))}},{key:"get",value:function(t){if(!e.enabled||!pl(t))return null;var n,i=t.target,r=t.changedTouches[0],a=parseFloat(i.getAttribute("min"))||0,o=parseFloat(i.getAttribute("max"))||100,s=parseFloat(i.getAttribute("step"))||1,l=i.getBoundingClientRect(),c=100/l.width*(this.config.thumbWidth/2)/100;return 0>(n=100/l.width*(r.clientX-l.left))?n=0:100<n&&(n=100),50>n?n-=(100-2*n)*c:50<n&&(n+=2*(n-50)*c),a+gl(n/100*(o-a),s)}},{key:"set",value:function(t){e.enabled&&pl(t)&&!t.target.disabled&&(t.preventDefault(),t.target.value=this.get(t),function(e,t){if(e&&t){var n=new Event(t,{bubbles:!0});e.dispatchEvent(n)}}(t.target,"touchend"===t.type?"change":"input"))}}],[{key:"setup",value:function(t){var n=1<arguments.length&&void 0!==arguments[1]?arguments[1]:{},i=null;if(ml(t)||ul(t)?i=Array.from(document.querySelectorAll(ul(t)?t:'input[type="range"]')):dl(t)?i=[t]:fl(t)?i=Array.from(t):hl(t)&&(i=t.filter(dl)),ml(i))return null;var r=el({},tl,{},n);if(ul(t)&&r.watch){var a=new MutationObserver((function(n){Array.from(n).forEach((function(n){Array.from(n.addedNodes).forEach((function(n){dl(n)&&nl(n,t)&&new e(n,r)}))}))}));a.observe(document.body,{childList:!0,subtree:!0})}return i.map((function(t){return new e(t,n)}))}},{key:"enabled",get:function(){return"ontouchstart"in document.documentElement}}]),e}(),kl=i.Promise,Tl=Xe("species"),Sl=function(e){var t=ie(e),n=C.f;a&&t&&!t[Tl]&&n(t,Tl,{configurable:!0,get:function(){return this}})},El=/(iphone|ipod|ipad).*applewebkit/i.test(qn),Al=i.location,Pl=i.setImmediate,xl=i.clearImmediate,Cl=i.process,Ol=i.MessageChannel,Il=i.Dispatch,Ll=0,jl={},Nl=function(e){if(jl.hasOwnProperty(e)){var t=jl[e];delete jl[e],t()}},Rl=function(e){return function(){Nl(e)}},Ml=function(e){Nl(e.data)},_l=function(e){i.postMessage(e+"",Al.protocol+"//"+Al.host)};Pl&&xl||(Pl=function(e){for(var t=[],n=1;arguments.length>n;)t.push(arguments[n++]);return jl[++Ll]=function(){("function"==typeof e?e:Function(e)).apply(void 0,t)},vl(Ll),Ll},xl=function(e){delete jl[e]},"process"==h(Cl)?vl=function(e){Cl.nextTick(Rl(e))}:Il&&Il.now?vl=function(e){Il.now(Rl(e))}:Ol&&!El?(bl=(yl=new Ol).port2,yl.port1.onmessage=Ml,vl=rt(bl.postMessage,bl,1)):!i.addEventListener||"function"!=typeof postMessage||i.importScripts||r(_l)||"file:"===Al.protocol?vl="onreadystatechange"in T("script")?function(e){Ue.appendChild(T("script")).onreadystatechange=function(){Ue.removeChild(this),Nl(e)}}:function(e){setTimeout(Rl(e),0)}:(vl=_l,i.addEventListener("message",Ml,!1)));var Ul,Dl,Fl,ql,Hl,Bl,Vl,zl,Wl={set:Pl,clear:xl},Kl=A.f,$l=Wl.set,Yl=i.MutationObserver||i.WebKitMutationObserver,Gl=i.process,Xl=i.Promise,Ql="process"==h(Gl),Jl=Kl(i,"queueMicrotask"),Zl=Jl&&Jl.value;Zl||(Ul=function(){var e,t;for(Ql&&(e=Gl.domain)&&e.exit();Dl;){t=Dl.fn,Dl=Dl.next;try{t()}catch(e){throw Dl?ql():Fl=void 0,e}}Fl=void 0,e&&e.enter()},Ql?ql=function(){Gl.nextTick(Ul)}:Yl&&!El?(Hl=!0,Bl=document.createTextNode(""),new Yl(Ul).observe(Bl,{characterData:!0}),ql=function(){Bl.data=Hl=!Hl}):Xl&&Xl.resolve?(Vl=Xl.resolve(void 0),zl=Vl.then,ql=function(){zl.call(Vl,Ul)}):ql=function(){$l.call(i,Ul)});var ec,tc,nc,ic,rc=Zl||function(e){var t={fn:e,next:void 0};Fl&&(Fl.next=t),Dl||(Dl=t,ql()),Fl=t},ac=function(e){var t,n;this.promise=new e((function(e,i){if(void 0!==t||void 0!==n)throw TypeError("Bad Promise constructor");t=e,n=i})),this.resolve=it(t),this.reject=it(n)},oc={f:function(e){return new ac(e)}},sc=function(e,t){if(P(e),g(t)&&t.constructor===e)return t;var n=oc.f(e);return(0,n.resolve)(t),n.promise},lc=function(e){try{return{error:!1,value:e()}}catch(e){return{error:!0,value:e}}},cc=Wl.set,uc=Xe("species"),hc="Promise",fc=Z.get,dc=Z.set,pc=Z.getterFor(hc),mc=kl,gc=i.TypeError,vc=i.document,yc=i.process,bc=ie("fetch"),wc=oc.f,kc=wc,Tc="process"==h(yc),Sc=!!(vc&&vc.createEvent&&i.dispatchEvent),Ec=Ce(hc,(function(){if(!(_(mc)!==String(mc))){if(66===zn)return!0;if(!Tc&&"function"!=typeof PromiseRejectionEvent)return!0}if(zn>=51&&/native code/.test(mc))return!1;var e=mc.resolve(1),t=function(e){e((function(){}),(function(){}))};return(e.constructor={})[uc]=t,!(e.then((function(){}))instanceof t)})),Ac=Ec||!Vo((function(e){mc.all(e).catch((function(){}))})),Pc=function(e){var t;return!(!g(e)||"function"!=typeof(t=e.then))&&t},xc=function(e,t,n){if(!t.notified){t.notified=!0;var i=t.reactions;rc((function(){for(var r=t.value,a=1==t.state,o=0;i.length>o;){var s,l,c,u=i[o++],h=a?u.ok:u.fail,f=u.resolve,d=u.reject,p=u.domain;try{h?(a||(2===t.rejection&&Lc(e,t),t.rejection=1),!0===h?s=r:(p&&p.enter(),s=h(r),p&&(p.exit(),c=!0)),s===u.promise?d(gc("Promise-chain cycle")):(l=Pc(s))?l.call(s,f,d):f(s)):d(r)}catch(e){p&&!c&&p.exit(),d(e)}}t.reactions=[],t.notified=!1,n&&!t.rejection&&Oc(e,t)}))}},Cc=function(e,t,n){var r,a;Sc?((r=vc.createEvent("Event")).promise=t,r.reason=n,r.initEvent(e,!1,!0),i.dispatchEvent(r)):r={promise:t,reason:n},(a=i["on"+e])?a(r):"unhandledrejection"===e&&function(e,t){var n=i.console;n&&n.error&&(1===arguments.length?n.error(e):n.error(e,t))}("Unhandled promise rejection",n)},Oc=function(e,t){cc.call(i,(function(){var n,i=t.value;if(Ic(t)&&(n=lc((function(){Tc?yc.emit("unhandledRejection",i,e):Cc("unhandledrejection",e,i)})),t.rejection=Tc||Ic(t)?2:1,n.error))throw n.value}))},Ic=function(e){return 1!==e.rejection&&!e.parent},Lc=function(e,t){cc.call(i,(function(){Tc?yc.emit("rejectionHandled",e):Cc("rejectionhandled",e,t.value)}))},jc=function(e,t,n,i){return function(r){e(t,n,r,i)}},Nc=function(e,t,n,i){t.done||(t.done=!0,i&&(t=i),t.value=n,t.state=2,xc(e,t,!0))},Rc=function(e,t,n,i){if(!t.done){t.done=!0,i&&(t=i);try{if(e===n)throw gc("Promise can't be resolved itself");var r=Pc(n);r?rc((function(){var i={done:!1};try{r.call(n,jc(Rc,e,i,t),jc(Nc,e,i,t))}catch(n){Nc(e,i,n,t)}})):(t.value=n,t.state=1,xc(e,t,!1))}catch(n){Nc(e,{done:!1},n,t)}}};Ec&&(mc=function(e){ar(this,mc,hc),it(e),ec.call(this);var t=fc(this);try{e(jc(Rc,this,t),jc(Nc,this,t))}catch(e){Nc(this,t,e)}},(ec=function(e){dc(this,{type:hc,done:!1,notified:!1,parent:!1,reactions:[],rejection:!1,state:0,value:void 0})}).prototype=Er(mc.prototype,{then:function(e,t){var n=pc(this),i=wc(Bi(this,mc));return i.ok="function"!=typeof e||e,i.fail="function"==typeof t&&t,i.domain=Tc?yc.domain:void 0,n.parent=!0,n.reactions.push(i),0!=n.state&&xc(this,n,!1),i.promise},catch:function(e){return this.then(void 0,e)}}),tc=function(){var e=new ec,t=fc(e);this.promise=e,this.resolve=jc(Rc,e,t),this.reject=jc(Nc,e,t)},oc.f=wc=function(e){return e===mc||e===nc?new tc(e):kc(e)},"function"==typeof kl&&(ic=kl.prototype.then,ee(kl.prototype,"then",(function(e,t){var n=this;return new mc((function(e,t){ic.call(n,e,t)})).then(e,t)}),{unsafe:!0}),"function"==typeof bc&&Ie({global:!0,enumerable:!0,forced:!0},{fetch:function(e){return sc(mc,bc.apply(i,arguments))}}))),Ie({global:!0,wrap:!0,forced:Ec},{Promise:mc}),nt(mc,hc,!1),Sl(hc),nc=ie(hc),Ie({target:hc,stat:!0,forced:Ec},{reject:function(e){var t=wc(this);return t.reject.call(void 0,e),t.promise}}),Ie({target:hc,stat:!0,forced:Ec},{resolve:function(e){return sc(this,e)}}),Ie({target:hc,stat:!0,forced:Ac},{all:function(e){var t=this,n=wc(t),i=n.resolve,r=n.reject,a=lc((function(){var n=it(t.resolve),a=[],o=0,s=1;ks(e,(function(e){var l=o++,c=!1;a.push(void 0),s++,n.call(t,e).then((function(e){c||(c=!0,a[l]=e,--s||i(a))}),r)})),--s||i(a)}));return a.error&&r(a.value),n.promise},race:function(e){var t=this,n=wc(t),i=n.reject,r=lc((function(){var r=it(t.resolve);ks(e,(function(e){r.call(t,e).then(n.resolve,i)}))}));return r.error&&i(r.value),n.promise}});var Mc,_c=A.f,Uc="".startsWith,Dc=Math.min,Fc=ys("startsWith"),qc=!(Fc||(Mc=_c(String.prototype,"startsWith"),!Mc||Mc.writable));Ie({target:"String",proto:!0,forced:!qc&&!Fc},{startsWith:function(e){var t=String(p(this));gs(e);var n=le(Dc(arguments.length>1?arguments[1]:void 0,t.length)),i=String(e);return Uc?Uc.call(t,i,n):t.slice(n,n+i.length)===i}});var Hc,Bc,Vc,zc=function(e){return null!=e?e.constructor:null},Wc=function(e,t){return Boolean(e&&t&&e instanceof t)},Kc=function(e){return null==e},$c=function(e){return zc(e)===Object},Yc=function(e){return zc(e)===String},Gc=function(e){return zc(e)===Function},Xc=function(e){return Array.isArray(e)},Qc=function(e){return Wc(e,NodeList)},Jc=function(e){return Kc(e)||(Yc(e)||Xc(e)||Qc(e))&&!e.length||$c(e)&&!Object.keys(e).length},Zc=Kc,eu=$c,tu=function(e){return zc(e)===Number&&!Number.isNaN(e)},nu=Yc,iu=function(e){return zc(e)===Boolean},ru=Gc,au=Xc,ou=Qc,su=function(e){return Wc(e,Element)},lu=function(e){return Wc(e,Event)},cu=function(e){return Wc(e,KeyboardEvent)},uu=function(e){return Wc(e,TextTrack)||!Kc(e)&&Yc(e.kind)},hu=function(e){return Wc(e,Promise)&&Gc(e.then)},fu=function(e){if(Wc(e,window.URL))return!0;if(!Yc(e))return!1;var t=e;e.startsWith("http://")&&e.startsWith("https://")||(t="http://".concat(e));try{return!Jc(new URL(t).hostname)}catch(e){return!1}},du=Jc,pu=(Hc=document.createElement("span"),Bc={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"},Vc=Object.keys(Bc).find((function(e){return void 0!==Hc.style[e]})),!!nu(Vc)&&Bc[Vc]);function mu(e,t){setTimeout((function(){try{e.hidden=!0,e.offsetHeight,e.hidden=!1}catch(e){}}),t)}var gu={isIE:
/* @cc_on!@ */
!!document.documentMode,isEdge:window.navigator.userAgent.includes("Edge"),isWebkit:"WebkitAppearance"in document.documentElement.style&&!/Edge/.test(navigator.userAgent),isIPhone:/(iPhone|iPod)/gi.test(navigator.platform),isIos:/(iPad|iPhone|iPod)/gi.test(navigator.platform)},vu=function(e){return function(t,n,i,r){it(n);var a=Re(t),o=d(a),s=le(a.length),l=e?s-1:0,c=e?-1:1;if(i<2)for(;;){if(l in o){r=o[l],l+=c;break}if(l+=c,e?l<0:s<=l)throw TypeError("Reduce of empty array with no initial value")}for(;e?l>=0:s>l;l+=c)l in o&&(r=n(r,o[l],l,a));return r}},yu={left:vu(!1),right:vu(!0)}.left,bu=$t("reduce"),wu=Qt("reduce",{1:0});function ku(e,t){return t.split(".").reduce((function(e,t){return e&&e[t]}),e)}function Tu(){for(var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},t=arguments.length,n=new Array(t>1?t-1:0),i=1;i<t;i++)n[i-1]=arguments[i];if(!n.length)return e;var r=n.shift();return eu(r)?(Object.keys(r).forEach((function(t){eu(r[t])?(Object.keys(e).includes(t)||Object.assign(e,ko({},t,{})),Tu(e[t],r[t])):Object.assign(e,ko({},t,r[t]))})),Tu.apply(void 0,[e].concat(n))):e}function Su(e,t){var n=e.length?e:[e];Array.from(n).reverse().forEach((function(e,n){var i=n>0?t.cloneNode(!0):t,r=e.parentNode,a=e.nextSibling;i.appendChild(e),a?r.insertBefore(i,a):r.appendChild(i)}))}function Eu(e,t){su(e)&&!du(t)&&Object.entries(t).filter((function(e){var t=Ao(e,2)[1];return!Zc(t)})).forEach((function(t){var n=Ao(t,2),i=n[0],r=n[1];return e.setAttribute(i,r)}))}function Au(e,t,n){var i=document.createElement(e);return eu(t)&&Eu(i,t),nu(n)&&(i.innerText=n),i}function Pu(e,t,n,i){su(t)&&t.appendChild(Au(e,n,i))}function xu(e){ou(e)||au(e)?Array.from(e).forEach(xu):su(e)&&su(e.parentNode)&&e.parentNode.removeChild(e)}function Cu(e){if(su(e))for(var t=e.childNodes.length;t>0;)e.removeChild(e.lastChild),t-=1}function Ou(e,t){return su(t)&&su(t.parentNode)&&su(e)?(t.parentNode.replaceChild(e,t),e):null}function Iu(e,t){if(!nu(e)||du(e))return{};var n={},i=Tu({},t);return e.split(",").forEach((function(e){var t=e.trim(),r=t.replace(".",""),a=t.replace(/[[\]]/g,"").split("="),o=Ao(a,1)[0],s=a.length>1?a[1].replace(/["']/g,""):"";switch(t.charAt(0)){case".":nu(i.class)?n.class="".concat(i.class," ").concat(r):n.class=r;break;case"#":n.id=t.replace("#","");break;case"[":n[o]=s}})),Tu(i,n)}function Lu(e,t){if(su(e)){var n=t;iu(n)||(n=!e.hidden),e.hidden=n}}function ju(e,t,n){if(ou(e))return Array.from(e).map((function(e){return ju(e,t,n)}));if(su(e)){var i="toggle";return void 0!==n&&(i=n?"add":"remove"),e.classList[i](t),e.classList.contains(t)}return!1}function Nu(e,t){return su(e)&&e.classList.contains(t)}function Ru(e,t){var n=Element.prototype;return(n.matches||n.webkitMatchesSelector||n.mozMatchesSelector||n.msMatchesSelector||function(){return Array.from(document.querySelectorAll(t)).includes(this)}).call(e,t)}function Mu(e){return this.elements.container.querySelectorAll(e)}function _u(e){return this.elements.container.querySelector(e)}function Uu(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null,t=arguments.length>1&&void 0!==arguments[1]&&arguments[1];su(e)&&(e.focus({preventScroll:!0}),t&&ju(e,this.config.classNames.tabFocus))}Ie({target:"Array",proto:!0,forced:!bu||!wu},{reduce:function(e){return yu(this,e,arguments.length,arguments.length>1?arguments[1]:void 0)}});var Du,Fu={"audio/ogg":"vorbis","audio/wav":"1","video/webm":"vp8, vorbis","video/mp4":"avc1.42E01E, mp4a.40.2","video/ogg":"theora"},qu={audio:"canPlayType"in document.createElement("audio"),video:"canPlayType"in document.createElement("video"),check:function(e,t,n){var i=gu.isIPhone&&n&&qu.playsinline,r=qu[e]||"html5"!==t;return{api:r,ui:r&&qu.rangeInput&&("video"!==e||!gu.isIPhone||i)}},pip:!(gu.isIPhone||!ru(Au("video").webkitSetPresentationMode)&&(!document.pictureInPictureEnabled||Au("video").disablePictureInPicture)),airplay:ru(window.WebKitPlaybackTargetAvailabilityEvent),playsinline:"playsInline"in document.createElement("video"),mime:function(e){if(du(e))return!1;var t=Ao(e.split("/"),1)[0],n=e;if(!this.isHTML5||t!==this.type)return!1;Object.keys(Fu).includes(n)&&(n+='; codecs="'.concat(Fu[e],'"'));try{return Boolean(n&&this.media.canPlayType(n).replace(/no/,""))}catch(e){return!1}},textTracks:"textTracks"in document.createElement("video"),rangeInput:(Du=document.createElement("input"),Du.type="range","range"===Du.type),touch:"ontouchstart"in document.documentElement,transitions:!1!==pu,reducedMotion:"matchMedia"in window&&window.matchMedia("(prefers-reduced-motion)").matches},Hu=function(){var e=!1;try{var t=Object.defineProperty({},"passive",{get:function(){return e=!0,null}});window.addEventListener("test",null,t),window.removeEventListener("test",null,t)}catch(e){}return e}();function Bu(e,t,n){var i=this,r=arguments.length>3&&void 0!==arguments[3]&&arguments[3],a=!(arguments.length>4&&void 0!==arguments[4])||arguments[4],o=arguments.length>5&&void 0!==arguments[5]&&arguments[5];if(e&&"addEventListener"in e&&!du(t)&&ru(n)){var s=t.split(" "),l=o;Hu&&(l={passive:a,capture:o}),s.forEach((function(t){i&&i.eventListeners&&r&&i.eventListeners.push({element:e,type:t,callback:n,options:l}),e[r?"addEventListener":"removeEventListener"](t,n,l)}))}}function Vu(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"",n=arguments.length>2?arguments[2]:void 0,i=!(arguments.length>3&&void 0!==arguments[3])||arguments[3],r=arguments.length>4&&void 0!==arguments[4]&&arguments[4];Bu.call(this,e,t,n,!0,i,r)}function zu(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"",n=arguments.length>2?arguments[2]:void 0,i=!(arguments.length>3&&void 0!==arguments[3])||arguments[3],r=arguments.length>4&&void 0!==arguments[4]&&arguments[4];Bu.call(this,e,t,n,!1,i,r)}function Wu(e){var t=this,n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"",i=arguments.length>2?arguments[2]:void 0,r=!(arguments.length>3&&void 0!==arguments[3])||arguments[3],a=arguments.length>4&&void 0!==arguments[4]&&arguments[4],o=function o(){zu(e,n,o,r,a);for(var s=arguments.length,l=new Array(s),c=0;c<s;c++)l[c]=arguments[c];i.apply(t,l)};Bu.call(this,e,n,o,!0,r,a)}function Ku(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"",n=arguments.length>2&&void 0!==arguments[2]&&arguments[2],i=arguments.length>3&&void 0!==arguments[3]?arguments[3]:{};if(su(e)&&!du(t)){var r=new CustomEvent(t,{bubbles:n,detail:So(So({},i),{},{plyr:this})});e.dispatchEvent(r)}}function $u(){this&&this.eventListeners&&(this.eventListeners.forEach((function(e){var t=e.element,n=e.type,i=e.callback,r=e.options;t.removeEventListener(n,i,r)})),this.eventListeners=[])}function Yu(){var e=this;return new Promise((function(t){return e.ready?setTimeout(t,0):Vu.call(e,e.elements.container,"ready",t)})).then((function(){}))}function Gu(e){hu(e)&&e.then(null,(function(){}))}function Xu(e){return!!(au(e)||nu(e)&&e.includes(":"))&&(au(e)?e:e.split(":")).map(Number).every(tu)}function Qu(e){if(!au(e)||!e.every(tu))return null;var t=Ao(e,2),n=t[0],i=t[1],r=function e(t,n){return 0===n?t:e(n,t%n)}(n,i);return[n/r,i/r]}function Ju(e){var t=function(e){return Xu(e)?e.split(":").map(Number):null},n=t(e);if(null===n&&(n=t(this.config.ratio)),null===n&&!du(this.embed)&&au(this.embed.ratio)&&(n=this.embed.ratio),null===n&&this.isHTML5){var i=this.media;n=Qu([i.videoWidth,i.videoHeight])}return n}function Zu(e){if(!this.isVideo)return{};var t=this.elements.wrapper,n=Ju.call(this,e),i=Ao(au(n)?n:[0,0],2),r=100/i[0]*i[1];if(t.style.paddingBottom="".concat(r,"%"),this.isVimeo&&!this.config.vimeo.premium&&this.supported.ui){var a=100/this.media.offsetWidth*parseInt(window.getComputedStyle(this.media).paddingBottom,10),o=(a-r)/(a/50);this.media.style.transform="translateY(-".concat(o,"%)")}else this.isHTML5&&t.classList.toggle(this.config.classNames.videoFixedRatio,null!==n);return{padding:r,ratio:n}}var eh={getSources:function(){var e=this;return this.isHTML5?Array.from(this.media.querySelectorAll("source")).filter((function(t){var n=t.getAttribute("type");return!!du(n)||qu.mime.call(e,n)})):[]},getQualityOptions:function(){return this.config.quality.forced?this.config.quality.options:eh.getSources.call(this).map((function(e){return Number(e.getAttribute("size"))})).filter(Boolean)},setup:function(){if(this.isHTML5){var e=this;e.options.speed=e.config.speed.options,du(this.config.ratio)||Zu.call(e),Object.defineProperty(e.media,"quality",{get:function(){var t=eh.getSources.call(e).find((function(t){return t.getAttribute("src")===e.source}));return t&&Number(t.getAttribute("size"))},set:function(t){if(e.quality!==t){if(e.config.quality.forced&&ru(e.config.quality.onChange))e.config.quality.onChange(t);else{var n=eh.getSources.call(e).find((function(e){return Number(e.getAttribute("size"))===t}));if(!n)return;var i=e.media,r=i.currentTime,a=i.paused,o=i.preload,s=i.readyState,l=i.playbackRate;e.media.src=n.getAttribute("src"),("none"!==o||s)&&(e.once("loadedmetadata",(function(){e.speed=l,e.currentTime=r,a||Gu(e.play())})),e.media.load())}Ku.call(e,e.media,"qualitychange",!1,{quality:t})}}})}},cancelRequests:function(){this.isHTML5&&(xu(eh.getSources.call(this)),this.media.setAttribute("src",this.config.blankVideo),this.media.load(),this.debug.log("Cancelled network requests"))}};function th(e){return au(e)?e.filter((function(t,n){return e.indexOf(t)===n})):e}var nh=C.f,ih=ye.f,rh=Z.set,ah=Xe("match"),oh=i.RegExp,sh=oh.prototype,lh=/a/g,ch=/a/g,uh=new oh(lh)!==lh,hh=oi.UNSUPPORTED_Y;if(a&&Ce("RegExp",!uh||hh||r((function(){return ch[ah]=!1,oh(lh)!=lh||oh(ch)==ch||"/a/i"!=oh(lh,"i")})))){for(var fh=function(e,t){var n,i=this instanceof fh,r=qi(e),a=void 0===t;if(!i&&r&&e.constructor===fh&&a)return e;uh?r&&!a&&(e=e.source):e instanceof fh&&(a&&(t=ri.call(e)),e=e.source),hh&&(n=!!t&&t.indexOf("y")>-1)&&(t=t.replace(/y/g,""));var o=Xo(uh?new oh(e,t):oh(e,t),i?this:sh,fh);return hh&&n&&rh(o,{sticky:n}),o},dh=function(e){e in fh||nh(fh,e,{configurable:!0,get:function(){return oh[e]},set:function(t){oh[e]=t}})},ph=ih(oh),mh=0;ph.length>mh;)dh(ph[mh++]);sh.constructor=fh,fh.prototype=sh,ee(i,"RegExp",fh)}function gh(e){for(var t=arguments.length,n=new Array(t>1?t-1:0),i=1;i<t;i++)n[i-1]=arguments[i];return du(e)?e:e.toString().replace(/{(\d+)}/g,(function(e,t){return n[t].toString()}))}Sl("RegExp");var vh=function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"",t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"",n=arguments.length>2&&void 0!==arguments[2]?arguments[2]:"";return e.replace(new RegExp(t.toString().replace(/([.*+?^=!:${}()|[\]/\\])/g,"\\$1"),"g"),n.toString())},yh=function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"";return e.toString().replace(/\w\S*/g,(function(e){return e.charAt(0).toUpperCase()+e.substr(1).toLowerCase()}))};function bh(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"",t=e.toString();return t=vh(t,"-"," "),t=vh(t,"_"," "),t=yh(t),vh(t," ","")}function wh(e){var t=document.createElement("div");return t.appendChild(e),t.innerHTML}var kh={pip:"PIP",airplay:"AirPlay",html5:"HTML5",vimeo:"Vimeo",youtube:"YouTube"},Th=function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"",t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{};if(du(e)||du(t))return"";var n=ku(t.i18n,e);if(du(n))return Object.keys(kh).includes(e)?kh[e]:"";var i={"{seektime}":t.seekTime,"{title}":t.title};return Object.entries(i).forEach((function(e){var t=Ao(e,2),i=t[0],r=t[1];n=vh(n,i,r)})),n},Sh=function(){function e(t){yo(this,e),this.enabled=t.config.storage.enabled,this.key=t.config.storage.key}return wo(e,[{key:"get",value:function(t){if(!e.supported||!this.enabled)return null;var n=window.localStorage.getItem(this.key);if(du(n))return null;var i=JSON.parse(n);return nu(t)&&t.length?i[t]:i}},{key:"set",value:function(t){if(e.supported&&this.enabled&&eu(t)){var n=this.get();du(n)&&(n={}),Tu(n,t),window.localStorage.setItem(this.key,JSON.stringify(n))}}}],[{key:"supported",get:function(){try{if(!("localStorage"in window))return!1;return window.localStorage.setItem("___test","___test"),window.localStorage.removeItem("___test"),!0}catch(e){return!1}}}]),e}();function Eh(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"text";return new Promise((function(n,i){try{var r=new XMLHttpRequest;if(!("withCredentials"in r))return;r.addEventListener("load",(function(){if("text"===t)try{n(JSON.parse(r.responseText))}catch(e){n(r.responseText)}else n(r.response)})),r.addEventListener("error",(function(){throw new Error(r.status)})),r.open("GET",e,!0),r.responseType=t,r.send()}catch(e){i(e)}}))}function Ah(e,t){if(nu(e)){var n=nu(t),i=function(){return null!==document.getElementById(t)},r=function(e,t){e.innerHTML=t,n&&i()||document.body.insertAdjacentElement("afterbegin",e)};if(!n||!i()){var a=Sh.supported,o=document.createElement("div");if(o.setAttribute("hidden",""),n&&o.setAttribute("id",t),a){var s=window.localStorage.getItem("".concat("cache","-").concat(t));if(null!==s){var l=JSON.parse(s);r(o,l.content)}}Eh(e).then((function(e){du(e)||(a&&window.localStorage.setItem("".concat("cache","-").concat(t),JSON.stringify({content:e})),r(o,e))})).catch((function(){}))}}}var Ph=Math.ceil,xh=Math.floor;Ie({target:"Math",stat:!0},{trunc:function(e){return(e>0?xh:Ph)(e)}});var Ch=function(e){return Math.trunc(e/60/60%60,10)},Oh=function(e){return Math.trunc(e/60%60,10)},Ih=function(e){return Math.trunc(e%60,10)};function Lh(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0,t=arguments.length>1&&void 0!==arguments[1]&&arguments[1],n=arguments.length>2&&void 0!==arguments[2]&&arguments[2];if(!tu(e))return Lh(void 0,t,n);var i=function(e){return"0".concat(e).slice(-2)},r=Ch(e),a=Oh(e),o=Ih(e);return r=t||r>0?"".concat(r,":"):"","".concat(n&&e>0?"-":"").concat(r).concat(i(a),":").concat(i(o))}var jh={getIconUrl:function(){var e=new URL(this.config.iconUrl,window.location).host!==window.location.host||gu.isIE&&!window.svg4everybody;return{url:this.config.iconUrl,cors:e}},findElements:function(){try{return this.elements.controls=_u.call(this,this.config.selectors.controls.wrapper),this.elements.buttons={play:Mu.call(this,this.config.selectors.buttons.play),pause:_u.call(this,this.config.selectors.buttons.pause),restart:_u.call(this,this.config.selectors.buttons.restart),rewind:_u.call(this,this.config.selectors.buttons.rewind),fastForward:_u.call(this,this.config.selectors.buttons.fastForward),mute:_u.call(this,this.config.selectors.buttons.mute),pip:_u.call(this,this.config.selectors.buttons.pip),airplay:_u.call(this,this.config.selectors.buttons.airplay),settings:_u.call(this,this.config.selectors.buttons.settings),captions:_u.call(this,this.config.selectors.buttons.captions),fullscreen:_u.call(this,this.config.selectors.buttons.fullscreen)},this.elements.progress=_u.call(this,this.config.selectors.progress),this.elements.inputs={seek:_u.call(this,this.config.selectors.inputs.seek),volume:_u.call(this,this.config.selectors.inputs.volume)},this.elements.display={buffer:_u.call(this,this.config.selectors.display.buffer),currentTime:_u.call(this,this.config.selectors.display.currentTime),duration:_u.call(this,this.config.selectors.display.duration)},su(this.elements.progress)&&(this.elements.display.seekTooltip=this.elements.progress.querySelector(".".concat(this.config.classNames.tooltip))),!0}catch(e){return this.debug.warn("It looks like there is a problem with your custom controls HTML",e),this.toggleNativeControls(!0),!1}},createIcon:function(e,t){var n=jh.getIconUrl.call(this),i="".concat(n.cors?"":n.url,"#").concat(this.config.iconPrefix),r=document.createElementNS("http://www.w3.org/2000/svg","svg");Eu(r,Tu(t,{"aria-hidden":"true",focusable:"false"}));var a=document.createElementNS("http://www.w3.org/2000/svg","use"),o="".concat(i,"-").concat(e);return"href"in a&&a.setAttributeNS("http://www.w3.org/1999/xlink","href",o),a.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href",o),r.appendChild(a),r},createLabel:function(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},n=Th(e,this.config),i=So(So({},t),{},{class:[t.class,this.config.classNames.hidden].filter(Boolean).join(" ")});return Au("span",i,n)},createBadge:function(e){if(du(e))return null;var t=Au("span",{class:this.config.classNames.menu.value});return t.appendChild(Au("span",{class:this.config.classNames.menu.badge},e)),t},createButton:function(e,t){var n=this,i=Tu({},t),r=function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"",t=e.toString();return(t=bh(t)).charAt(0).toLowerCase()+t.slice(1)}(e),a={element:"button",toggle:!1,label:null,icon:null,labelPressed:null,iconPressed:null};switch(["element","icon","label"].forEach((function(e){Object.keys(i).includes(e)&&(a[e]=i[e],delete i[e])})),"button"!==a.element||Object.keys(i).includes("type")||(i.type="button"),Object.keys(i).includes("class")?i.class.split(" ").some((function(e){return e===n.config.classNames.control}))||Tu(i,{class:"".concat(i.class," ").concat(this.config.classNames.control)}):i.class=this.config.classNames.control,e){case"play":a.toggle=!0,a.label="play",a.labelPressed="pause",a.icon="play",a.iconPressed="pause";break;case"mute":a.toggle=!0,a.label="mute",a.labelPressed="unmute",a.icon="volume",a.iconPressed="muted";break;case"captions":a.toggle=!0,a.label="enableCaptions",a.labelPressed="disableCaptions",a.icon="captions-off",a.iconPressed="captions-on";break;case"fullscreen":a.toggle=!0,a.label="enterFullscreen",a.labelPressed="exitFullscreen",a.icon="enter-fullscreen",a.iconPressed="exit-fullscreen";break;case"play-large":i.class+=" ".concat(this.config.classNames.control,"--overlaid"),r="play",a.label="play",a.icon="play";break;default:du(a.label)&&(a.label=r),du(a.icon)&&(a.icon=e)}var o=Au(a.element);return a.toggle?(o.appendChild(jh.createIcon.call(this,a.iconPressed,{class:"icon--pressed"})),o.appendChild(jh.createIcon.call(this,a.icon,{class:"icon--not-pressed"})),o.appendChild(jh.createLabel.call(this,a.labelPressed,{class:"label--pressed"})),o.appendChild(jh.createLabel.call(this,a.label,{class:"label--not-pressed"}))):(o.appendChild(jh.createIcon.call(this,a.icon)),o.appendChild(jh.createLabel.call(this,a.label))),Tu(i,Iu(this.config.selectors.buttons[r],i)),Eu(o,i),"play"===r?(au(this.elements.buttons[r])||(this.elements.buttons[r]=[]),this.elements.buttons[r].push(o)):this.elements.buttons[r]=o,o},createRange:function(e,t){var n=Au("input",Tu(Iu(this.config.selectors.inputs[e]),{type:"range",min:0,max:100,step:.01,value:0,autocomplete:"off",role:"slider","aria-label":Th(e,this.config),"aria-valuemin":0,"aria-valuemax":100,"aria-valuenow":0},t));return this.elements.inputs[e]=n,jh.updateRangeFill.call(this,n),wl.setup(n),n},createProgress:function(e,t){var n=Au("progress",Tu(Iu(this.config.selectors.display[e]),{min:0,max:100,value:0,role:"progressbar","aria-hidden":!0},t));if("volume"!==e){n.appendChild(Au("span",null,"0"));var i={played:"played",buffer:"buffered"}[e],r=i?Th(i,this.config):"";n.innerText="% ".concat(r.toLowerCase())}return this.elements.display[e]=n,n},createTime:function(e,t){var n=Iu(this.config.selectors.display[e],t),i=Au("div",Tu(n,{class:"".concat(n.class?n.class:""," ").concat(this.config.classNames.display.time," ").trim(),"aria-label":Th(e,this.config)}),"00:00");return this.elements.display[e]=i,i},bindMenuItemShortcuts:function(e,t){var n=this;Vu.call(this,e,"keydown keyup",(function(i){if([32,38,39,40].includes(i.which)&&(i.preventDefault(),i.stopPropagation(),"keydown"!==i.type)){var r,a=Ru(e,'[role="menuitemradio"]');if(!a&&[32,39].includes(i.which))jh.showMenuPanel.call(n,t,!0);else 32!==i.which&&(40===i.which||a&&39===i.which?(r=e.nextElementSibling,su(r)||(r=e.parentNode.firstElementChild)):(r=e.previousElementSibling,su(r)||(r=e.parentNode.lastElementChild)),Uu.call(n,r,!0))}}),!1),Vu.call(this,e,"keyup",(function(e){13===e.which&&jh.focusFirstMenuItem.call(n,null,!0)}))},createMenuItem:function(e){var t=this,n=e.value,i=e.list,r=e.type,a=e.title,o=e.badge,s=void 0===o?null:o,l=e.checked,c=void 0!==l&&l,u=Iu(this.config.selectors.inputs[r]),h=Au("button",Tu(u,{type:"button",role:"menuitemradio",class:"".concat(this.config.classNames.control," ").concat(u.class?u.class:"").trim(),"aria-checked":c,value:n})),f=Au("span");f.innerHTML=a,su(s)&&f.appendChild(s),h.appendChild(f),Object.defineProperty(h,"checked",{enumerable:!0,get:function(){return"true"===h.getAttribute("aria-checked")},set:function(e){e&&Array.from(h.parentNode.children).filter((function(e){return Ru(e,'[role="menuitemradio"]')})).forEach((function(e){return e.setAttribute("aria-checked","false")})),h.setAttribute("aria-checked",e?"true":"false")}}),this.listeners.bind(h,"click keyup",(function(e){if(!cu(e)||32===e.which){switch(e.preventDefault(),e.stopPropagation(),h.checked=!0,r){case"language":t.currentTrack=Number(n);break;case"quality":t.quality=n;break;case"speed":t.speed=parseFloat(n)}jh.showMenuPanel.call(t,"home",cu(e))}}),r,!1),jh.bindMenuItemShortcuts.call(this,h,r),i.appendChild(h)},formatTime:function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0,t=arguments.length>1&&void 0!==arguments[1]&&arguments[1];if(!tu(e))return e;var n=Ch(this.duration)>0;return Lh(e,n,t)},updateTimeDisplay:function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null,t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,n=arguments.length>2&&void 0!==arguments[2]&&arguments[2];su(e)&&tu(t)&&(e.innerText=jh.formatTime(t,n))},updateVolume:function(){this.supported.ui&&(su(this.elements.inputs.volume)&&jh.setRange.call(this,this.elements.inputs.volume,this.muted?0:this.volume),su(this.elements.buttons.mute)&&(this.elements.buttons.mute.pressed=this.muted||0===this.volume))},setRange:function(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0;su(e)&&(e.value=t,jh.updateRangeFill.call(this,e))},updateProgress:function(e){var t=this;if(this.supported.ui&&lu(e)){var n=0;if(e)switch(e.type){case"timeupdate":case"seeking":case"seeked":n=function(e,t){return 0===e||0===t||Number.isNaN(e)||Number.isNaN(t)?0:(e/t*100).toFixed(2)}(this.currentTime,this.duration),"timeupdate"===e.type&&jh.setRange.call(this,this.elements.inputs.seek,n);break;case"playing":case"progress":!function(e,n){var i=tu(n)?n:0,r=su(e)?e:t.elements.display.buffer;if(su(r)){r.value=i;var a=r.getElementsByTagName("span")[0];su(a)&&(a.childNodes[0].nodeValue=i)}}(this.elements.display.buffer,100*this.buffered)}}},updateRangeFill:function(e){var t=lu(e)?e.target:e;if(su(t)&&"range"===t.getAttribute("type")){if(Ru(t,this.config.selectors.inputs.seek)){t.setAttribute("aria-valuenow",this.currentTime);var n=jh.formatTime(this.currentTime),i=jh.formatTime(this.duration),r=Th("seekLabel",this.config);t.setAttribute("aria-valuetext",r.replace("{currentTime}",n).replace("{duration}",i))}else if(Ru(t,this.config.selectors.inputs.volume)){var a=100*t.value;t.setAttribute("aria-valuenow",a),t.setAttribute("aria-valuetext","".concat(a.toFixed(1),"%"))}else t.setAttribute("aria-valuenow",t.value);gu.isWebkit&&t.style.setProperty("--value","".concat(t.value/t.max*100,"%"))}},updateSeekTooltip:function(e){var t=this;if(this.config.tooltips.seek&&su(this.elements.inputs.seek)&&su(this.elements.display.seekTooltip)&&0!==this.duration){var n="".concat(this.config.classNames.tooltip,"--visible"),i=function(e){return ju(t.elements.display.seekTooltip,n,e)};if(this.touch)i(!1);else{var r=0,a=this.elements.progress.getBoundingClientRect();if(lu(e))r=100/a.width*(e.pageX-a.left);else{if(!Nu(this.elements.display.seekTooltip,n))return;r=parseFloat(this.elements.display.seekTooltip.style.left,10)}r<0?r=0:r>100&&(r=100),jh.updateTimeDisplay.call(this,this.elements.display.seekTooltip,this.duration/100*r),this.elements.display.seekTooltip.style.left="".concat(r,"%"),lu(e)&&["mouseenter","mouseleave"].includes(e.type)&&i("mouseenter"===e.type)}}},timeUpdate:function(e){var t=!su(this.elements.display.duration)&&this.config.invertTime;jh.updateTimeDisplay.call(this,this.elements.display.currentTime,t?this.duration-this.currentTime:this.currentTime,t),e&&"timeupdate"===e.type&&this.media.seeking||jh.updateProgress.call(this,e)},durationUpdate:function(){if(this.supported.ui&&(this.config.invertTime||!this.currentTime)){if(this.duration>=Math.pow(2,32))return Lu(this.elements.display.currentTime,!0),void Lu(this.elements.progress,!0);su(this.elements.inputs.seek)&&this.elements.inputs.seek.setAttribute("aria-valuemax",this.duration);var e=su(this.elements.display.duration);!e&&this.config.displayDuration&&this.paused&&jh.updateTimeDisplay.call(this,this.elements.display.currentTime,this.duration),e&&jh.updateTimeDisplay.call(this,this.elements.display.duration,this.duration),jh.updateSeekTooltip.call(this)}},toggleMenuButton:function(e,t){Lu(this.elements.settings.buttons[e],!t)},updateSetting:function(e,t,n){var i=this.elements.settings.panels[e],r=null,a=t;if("captions"===e)r=this.currentTrack;else{if(r=du(n)?this[e]:n,du(r)&&(r=this.config[e].default),!du(this.options[e])&&!this.options[e].includes(r))return void this.debug.warn("Unsupported value of '".concat(r,"' for ").concat(e));if(!this.config[e].options.includes(r))return void this.debug.warn("Disabled value of '".concat(r,"' for ").concat(e))}if(su(a)||(a=i&&i.querySelector('[role="menu"]')),su(a)){this.elements.settings.buttons[e].querySelector(".".concat(this.config.classNames.menu.value)).innerHTML=jh.getLabel.call(this,e,r);var o=a&&a.querySelector('[value="'.concat(r,'"]'));su(o)&&(o.checked=!0)}},getLabel:function(e,t){switch(e){case"speed":return 1===t?Th("normal",this.config):"".concat(t,"&times;");case"quality":if(tu(t)){var n=Th("qualityLabel.".concat(t),this.config);return n.length?n:"".concat(t,"p")}return yh(t);case"captions":return Mh.getLabel.call(this);default:return null}},setQualityMenu:function(e){var t=this;if(su(this.elements.settings.panels.quality)){var n=this.elements.settings.panels.quality.querySelector('[role="menu"]');au(e)&&(this.options.quality=th(e).filter((function(e){return t.config.quality.options.includes(e)})));var i=!du(this.options.quality)&&this.options.quality.length>1;if(jh.toggleMenuButton.call(this,"quality",i),Cu(n),jh.checkMenu.call(this),i){var r=function(e){var n=Th("qualityBadge.".concat(e),t.config);return n.length?jh.createBadge.call(t,n):null};this.options.quality.sort((function(e,n){var i=t.config.quality.options;return i.indexOf(e)>i.indexOf(n)?1:-1})).forEach((function(e){jh.createMenuItem.call(t,{value:e,list:n,type:"quality",title:jh.getLabel.call(t,"quality",e),badge:r(e)})})),jh.updateSetting.call(this,"quality",n)}}},setCaptionsMenu:function(){var e=this;if(su(this.elements.settings.panels.captions)){var t=this.elements.settings.panels.captions.querySelector('[role="menu"]'),n=Mh.getTracks.call(this),i=Boolean(n.length);if(jh.toggleMenuButton.call(this,"captions",i),Cu(t),jh.checkMenu.call(this),i){var r=n.map((function(n,i){return{value:i,checked:e.captions.toggled&&e.currentTrack===i,title:Mh.getLabel.call(e,n),badge:n.language&&jh.createBadge.call(e,n.language.toUpperCase()),list:t,type:"language"}}));r.unshift({value:-1,checked:!this.captions.toggled,title:Th("disabled",this.config),list:t,type:"language"}),r.forEach(jh.createMenuItem.bind(this)),jh.updateSetting.call(this,"captions",t)}}},setSpeedMenu:function(){var e=this;if(su(this.elements.settings.panels.speed)){var t=this.elements.settings.panels.speed.querySelector('[role="menu"]');this.options.speed=this.options.speed.filter((function(t){return t>=e.minimumSpeed&&t<=e.maximumSpeed}));var n=!du(this.options.speed)&&this.options.speed.length>1;jh.toggleMenuButton.call(this,"speed",n),Cu(t),jh.checkMenu.call(this),n&&(this.options.speed.forEach((function(n){jh.createMenuItem.call(e,{value:n,list:t,type:"speed",title:jh.getLabel.call(e,"speed",n)})})),jh.updateSetting.call(this,"speed",t))}},checkMenu:function(){var e=this.elements.settings.buttons,t=!du(e)&&Object.values(e).some((function(e){return!e.hidden}));Lu(this.elements.settings.menu,!t)},focusFirstMenuItem:function(e){var t=arguments.length>1&&void 0!==arguments[1]&&arguments[1];if(!this.elements.settings.popup.hidden){var n=e;su(n)||(n=Object.values(this.elements.settings.panels).find((function(e){return!e.hidden})));var i=n.querySelector('[role^="menuitem"]');Uu.call(this,i,t)}},toggleMenu:function(e){var t=this.elements.settings.popup,n=this.elements.buttons.settings;if(su(t)&&su(n)){var i=t.hidden,r=i;if(iu(e))r=e;else if(cu(e)&&27===e.which)r=!1;else if(lu(e)){var a=ru(e.composedPath)?e.composedPath()[0]:e.target,o=t.contains(a);if(o||!o&&e.target!==n&&r)return}n.setAttribute("aria-expanded",r),Lu(t,!r),ju(this.elements.container,this.config.classNames.menu.open,r),r&&cu(e)?jh.focusFirstMenuItem.call(this,null,!0):r||i||Uu.call(this,n,cu(e))}},getMenuSize:function(e){var t=e.cloneNode(!0);t.style.position="absolute",t.style.opacity=0,t.removeAttribute("hidden"),e.parentNode.appendChild(t);var n=t.scrollWidth,i=t.scrollHeight;return xu(t),{width:n,height:i}},showMenuPanel:function(){var e=this,t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"",n=arguments.length>1&&void 0!==arguments[1]&&arguments[1],i=this.elements.container.querySelector("#plyr-settings-".concat(this.id,"-").concat(t));if(su(i)){var r=i.parentNode,a=Array.from(r.children).find((function(e){return!e.hidden}));if(qu.transitions&&!qu.reducedMotion){r.style.width="".concat(a.scrollWidth,"px"),r.style.height="".concat(a.scrollHeight,"px");var o=jh.getMenuSize.call(this,i),s=function t(n){n.target===r&&["width","height"].includes(n.propertyName)&&(r.style.width="",r.style.height="",zu.call(e,r,pu,t))};Vu.call(this,r,pu,s),r.style.width="".concat(o.width,"px"),r.style.height="".concat(o.height,"px")}Lu(a,!0),Lu(i,!1),jh.focusFirstMenuItem.call(this,i,n)}},setDownloadUrl:function(){var e=this.elements.buttons.download;su(e)&&e.setAttribute("href",this.download)},create:function(e){var t=this,n=jh.bindMenuItemShortcuts,i=jh.createButton,r=jh.createProgress,a=jh.createRange,o=jh.createTime,s=jh.setQualityMenu,l=jh.setSpeedMenu,c=jh.showMenuPanel;this.elements.controls=null,au(this.config.controls)&&this.config.controls.includes("play-large")&&this.elements.container.appendChild(i.call(this,"play-large"));var u=Au("div",Iu(this.config.selectors.controls.wrapper));this.elements.controls=u;var h={class:"plyr__controls__item"};return th(au(this.config.controls)?this.config.controls:[]).forEach((function(s){if("restart"===s&&u.appendChild(i.call(t,"restart",h)),"rewind"===s&&u.appendChild(i.call(t,"rewind",h)),"play"===s&&u.appendChild(i.call(t,"play",h)),"fast-forward"===s&&u.appendChild(i.call(t,"fast-forward",h)),"progress"===s){var l=Au("div",{class:"".concat(h.class," plyr__progress__container")}),f=Au("div",Iu(t.config.selectors.progress));if(f.appendChild(a.call(t,"seek",{id:"plyr-seek-".concat(e.id)})),f.appendChild(r.call(t,"buffer")),t.config.tooltips.seek){var d=Au("span",{class:t.config.classNames.tooltip},"00:00");f.appendChild(d),t.elements.display.seekTooltip=d}t.elements.progress=f,l.appendChild(t.elements.progress),u.appendChild(l)}if("current-time"===s&&u.appendChild(o.call(t,"currentTime",h)),"duration"===s&&u.appendChild(o.call(t,"duration",h)),"mute"===s||"volume"===s){var p=t.elements.volume;if(su(p)&&u.contains(p)||(p=Au("div",Tu({},h,{class:"".concat(h.class," plyr__volume").trim()})),t.elements.volume=p,u.appendChild(p)),"mute"===s&&p.appendChild(i.call(t,"mute")),"volume"===s&&!gu.isIos){var m={max:1,step:.05,value:t.config.volume};p.appendChild(a.call(t,"volume",Tu(m,{id:"plyr-volume-".concat(e.id)})))}}if("captions"===s&&u.appendChild(i.call(t,"captions",h)),"settings"===s&&!du(t.config.settings)){var g=Au("div",Tu({},h,{class:"".concat(h.class," plyr__menu").trim(),hidden:""}));g.appendChild(i.call(t,"settings",{"aria-haspopup":!0,"aria-controls":"plyr-settings-".concat(e.id),"aria-expanded":!1}));var v=Au("div",{class:"plyr__menu__container",id:"plyr-settings-".concat(e.id),hidden:""}),y=Au("div"),b=Au("div",{id:"plyr-settings-".concat(e.id,"-home")}),w=Au("div",{role:"menu"});b.appendChild(w),y.appendChild(b),t.elements.settings.panels.home=b,t.config.settings.forEach((function(i){var r=Au("button",Tu(Iu(t.config.selectors.buttons.settings),{type:"button",class:"".concat(t.config.classNames.control," ").concat(t.config.classNames.control,"--forward"),role:"menuitem","aria-haspopup":!0,hidden:""}));n.call(t,r,i),Vu.call(t,r,"click",(function(){c.call(t,i,!1)}));var a=Au("span",null,Th(i,t.config)),o=Au("span",{class:t.config.classNames.menu.value});o.innerHTML=e[i],a.appendChild(o),r.appendChild(a),w.appendChild(r);var s=Au("div",{id:"plyr-settings-".concat(e.id,"-").concat(i),hidden:""}),l=Au("button",{type:"button",class:"".concat(t.config.classNames.control," ").concat(t.config.classNames.control,"--back")});l.appendChild(Au("span",{"aria-hidden":!0},Th(i,t.config))),l.appendChild(Au("span",{class:t.config.classNames.hidden},Th("menuBack",t.config))),Vu.call(t,s,"keydown",(function(e){37===e.which&&(e.preventDefault(),e.stopPropagation(),c.call(t,"home",!0))}),!1),Vu.call(t,l,"click",(function(){c.call(t,"home",!1)})),s.appendChild(l),s.appendChild(Au("div",{role:"menu"})),y.appendChild(s),t.elements.settings.buttons[i]=r,t.elements.settings.panels[i]=s})),v.appendChild(y),g.appendChild(v),u.appendChild(g),t.elements.settings.popup=v,t.elements.settings.menu=g}if("pip"===s&&qu.pip&&u.appendChild(i.call(t,"pip",h)),"airplay"===s&&qu.airplay&&u.appendChild(i.call(t,"airplay",h)),"download"===s){var k=Tu({},h,{element:"a",href:t.download,target:"_blank"});t.isHTML5&&(k.download="");var T=t.config.urls.download;!fu(T)&&t.isEmbed&&Tu(k,{icon:"logo-".concat(t.provider),label:t.provider}),u.appendChild(i.call(t,"download",k))}"fullscreen"===s&&u.appendChild(i.call(t,"fullscreen",h))})),this.isHTML5&&s.call(this,eh.getQualityOptions.call(this)),l.call(this),u},inject:function(){var e=this;if(this.config.loadSprite){var t=jh.getIconUrl.call(this);t.cors&&Ah(t.url,"sprite-plyr")}this.id=Math.floor(1e4*Math.random());var n=null;this.elements.controls=null;var i={id:this.id,seektime:this.config.seekTime,title:this.config.title},r=!0;ru(this.config.controls)&&(this.config.controls=this.config.controls.call(this,i)),this.config.controls||(this.config.controls=[]),su(this.config.controls)||nu(this.config.controls)?n=this.config.controls:(n=jh.create.call(this,{id:this.id,seektime:this.config.seekTime,speed:this.speed,quality:this.quality,captions:Mh.getLabel.call(this)}),r=!1);var a,o;if(r&&nu(this.config.controls)&&(a=n,Object.entries(i).forEach((function(e){var t=Ao(e,2),n=t[0],i=t[1];a=vh(a,"{".concat(n,"}"),i)})),n=a),nu(this.config.selectors.controls.container)&&(o=document.querySelector(this.config.selectors.controls.container)),su(o)||(o=this.elements.container),o[su(n)?"insertAdjacentElement":"insertAdjacentHTML"]("afterbegin",n),su(this.elements.controls)||jh.findElements.call(this),!du(this.elements.buttons)){var s=function(t){var n=e.config.classNames.controlPressed;Object.defineProperty(t,"pressed",{enumerable:!0,get:function(){return Nu(t,n)},set:function(){var e=arguments.length>0&&void 0!==arguments[0]&&arguments[0];ju(t,n,e)}})};Object.values(this.elements.buttons).filter(Boolean).forEach((function(e){au(e)||ou(e)?Array.from(e).filter(Boolean).forEach(s):s(e)}))}if(gu.isEdge&&mu(o),this.config.tooltips.controls){var l=this.config,c=l.classNames,u=l.selectors,h="".concat(u.controls.wrapper," ").concat(u.labels," .").concat(c.hidden),f=Mu.call(this,h);Array.from(f).forEach((function(t){ju(t,e.config.classNames.hidden,!1),ju(t,e.config.classNames.tooltip,!0)}))}}};function Nh(e){var t=!(arguments.length>1&&void 0!==arguments[1])||arguments[1],n=e;if(t){var i=document.createElement("a");i.href=n,n=i.href}try{return new URL(n)}catch(e){return null}}function Rh(e){var t=new URLSearchParams;return eu(e)&&Object.entries(e).forEach((function(e){var n=Ao(e,2),i=n[0],r=n[1];t.set(i,r)})),t}var Mh={setup:function(){if(this.supported.ui)if(!this.isVideo||this.isYouTube||this.isHTML5&&!qu.textTracks)au(this.config.controls)&&this.config.controls.includes("settings")&&this.config.settings.includes("captions")&&jh.setCaptionsMenu.call(this);else{if(su(this.elements.captions)||(this.elements.captions=Au("div",Iu(this.config.selectors.captions)),function(e,t){su(e)&&su(t)&&t.parentNode.insertBefore(e,t.nextSibling)}(this.elements.captions,this.elements.wrapper)),gu.isIE&&window.URL){var e=this.media.querySelectorAll("track");Array.from(e).forEach((function(e){var t=e.getAttribute("src"),n=Nh(t);null!==n&&n.hostname!==window.location.href.hostname&&["http:","https:"].includes(n.protocol)&&Eh(t,"blob").then((function(t){e.setAttribute("src",window.URL.createObjectURL(t))})).catch((function(){xu(e)}))}))}var t=th((navigator.languages||[navigator.language||navigator.userLanguage||"en"]).map((function(e){return e.split("-")[0]}))),n=(this.storage.get("language")||this.config.captions.language||"auto").toLowerCase();if("auto"===n)n=Ao(t,1)[0];var i=this.storage.get("captions");if(iu(i)||(i=this.config.captions.active),Object.assign(this.captions,{toggled:!1,active:i,language:n,languages:t}),this.isHTML5){var r=this.config.captions.update?"addtrack removetrack":"removetrack";Vu.call(this,this.media.textTracks,r,Mh.update.bind(this))}setTimeout(Mh.update.bind(this),0)}},update:function(){var e=this,t=Mh.getTracks.call(this,!0),n=this.captions,i=n.active,r=n.language,a=n.meta,o=n.currentTrackNode,s=Boolean(t.find((function(e){return e.language===r})));this.isHTML5&&this.isVideo&&t.filter((function(e){return!a.get(e)})).forEach((function(t){e.debug.log("Track added",t),a.set(t,{default:"showing"===t.mode}),"showing"===t.mode&&(t.mode="hidden"),Vu.call(e,t,"cuechange",(function(){return Mh.updateCues.call(e)}))})),(s&&this.language!==r||!t.includes(o))&&(Mh.setLanguage.call(this,r),Mh.toggle.call(this,i&&s)),ju(this.elements.container,this.config.classNames.captions.enabled,!du(t)),au(this.config.controls)&&this.config.controls.includes("settings")&&this.config.settings.includes("captions")&&jh.setCaptionsMenu.call(this)},toggle:function(e){var t=this,n=!(arguments.length>1&&void 0!==arguments[1])||arguments[1];if(this.supported.ui){var i=this.captions.toggled,r=this.config.classNames.captions.active,a=Zc(e)?!i:e;if(a!==i){if(n||(this.captions.active=a,this.storage.set({captions:a})),!this.language&&a&&!n){var o=Mh.getTracks.call(this),s=Mh.findTrack.call(this,[this.captions.language].concat(Po(this.captions.languages)),!0);return this.captions.language=s.language,void Mh.set.call(this,o.indexOf(s))}this.elements.buttons.captions&&(this.elements.buttons.captions.pressed=a),ju(this.elements.container,r,a),this.captions.toggled=a,jh.updateSetting.call(this,"captions"),Ku.call(this,this.media,a?"captionsenabled":"captionsdisabled")}setTimeout((function(){a&&t.captions.toggled&&(t.captions.currentTrackNode.mode="hidden")}))}},set:function(e){var t=!(arguments.length>1&&void 0!==arguments[1])||arguments[1],n=Mh.getTracks.call(this);if(-1!==e)if(tu(e))if(e in n){if(this.captions.currentTrack!==e){this.captions.currentTrack=e;var i=n[e],r=i||{},a=r.language;this.captions.currentTrackNode=i,jh.updateSetting.call(this,"captions"),t||(this.captions.language=a,this.storage.set({language:a})),this.isVimeo&&this.embed.enableTextTrack(a),Ku.call(this,this.media,"languagechange")}Mh.toggle.call(this,!0,t),this.isHTML5&&this.isVideo&&Mh.updateCues.call(this)}else this.debug.warn("Track not found",e);else this.debug.warn("Invalid caption argument",e);else Mh.toggle.call(this,!1,t)},setLanguage:function(e){var t=!(arguments.length>1&&void 0!==arguments[1])||arguments[1];if(nu(e)){var n=e.toLowerCase();this.captions.language=n;var i=Mh.getTracks.call(this),r=Mh.findTrack.call(this,[n]);Mh.set.call(this,i.indexOf(r),t)}else this.debug.warn("Invalid language argument",e)},getTracks:function(){var e=this,t=arguments.length>0&&void 0!==arguments[0]&&arguments[0],n=Array.from((this.media||{}).textTracks||[]);return n.filter((function(n){return!e.isHTML5||t||e.captions.meta.has(n)})).filter((function(e){return["captions","subtitles"].includes(e.kind)}))},findTrack:function(e){var t,n=this,i=arguments.length>1&&void 0!==arguments[1]&&arguments[1],r=Mh.getTracks.call(this),a=function(e){return Number((n.captions.meta.get(e)||{}).default)},o=Array.from(r).sort((function(e,t){return a(t)-a(e)}));return e.every((function(e){return!(t=o.find((function(t){return t.language===e})))})),t||(i?o[0]:void 0)},getCurrentTrack:function(){return Mh.getTracks.call(this)[this.currentTrack]},getLabel:function(e){var t=e;return!uu(t)&&qu.textTracks&&this.captions.toggled&&(t=Mh.getCurrentTrack.call(this)),uu(t)?du(t.label)?du(t.language)?Th("enabled",this.config):e.language.toUpperCase():t.label:Th("disabled",this.config)},updateCues:function(e){if(this.supported.ui)if(su(this.elements.captions))if(Zc(e)||Array.isArray(e)){var t=e;if(!t){var n=Mh.getCurrentTrack.call(this);t=Array.from((n||{}).activeCues||[]).map((function(e){return e.getCueAsHTML()})).map(wh)}var i=t.map((function(e){return e.trim()})).join("\n");if(i!==this.elements.captions.innerHTML){Cu(this.elements.captions);var r=Au("span",Iu(this.config.selectors.caption));r.innerHTML=i,this.elements.captions.appendChild(r),Ku.call(this,this.media,"cuechange")}}else this.debug.warn("updateCues: Invalid input",e);else this.debug.warn("No captions element to render to")}},_h={enabled:!0,title:"",debug:!1,autoplay:!1,autopause:!0,playsinline:!0,seekTime:10,volume:1,muted:!1,duration:null,displayDuration:!0,invertTime:!0,toggleInvert:!0,ratio:null,clickToPlay:!0,hideControls:!0,resetOnEnd:!1,disableContextMenu:!0,loadSprite:!0,iconPrefix:"plyr",iconUrl:"https://cdn.plyr.io/3.6.2/plyr.svg",blankVideo:"https://cdn.plyr.io/static/blank.mp4",quality:{default:576,options:[4320,2880,2160,1440,1080,720,576,480,360,240],forced:!1,onChange:null},loop:{active:!1},speed:{selected:1,options:[.5,.75,1,1.25,1.5,1.75,2,4]},keyboard:{focused:!0,global:!1},tooltips:{controls:!1,seek:!0},captions:{active:!1,language:"auto",update:!1},fullscreen:{enabled:!0,fallback:!0,iosNative:!1},storage:{enabled:!0,key:"plyr"},controls:["play-large","play","progress","current-time","mute","volume","captions","settings","pip","airplay","fullscreen"],settings:["captions","quality","speed"],i18n:{restart:"Restart",rewind:"Rewind {seektime}s",play:"Play",pause:"Pause",fastForward:"Forward {seektime}s",seek:"Seek",seekLabel:"{currentTime} of {duration}",played:"Played",buffered:"Buffered",currentTime:"Current time",duration:"Duration",volume:"Volume",mute:"Mute",unmute:"Unmute",enableCaptions:"Enable captions",disableCaptions:"Disable captions",download:"Download",enterFullscreen:"Enter fullscreen",exitFullscreen:"Exit fullscreen",frameTitle:"Player for {title}",captions:"Captions",settings:"Settings",pip:"PIP",menuBack:"Go back to previous menu",speed:"Speed",normal:"Normal",quality:"Quality",loop:"Loop",start:"Start",end:"End",all:"All",reset:"Reset",disabled:"Disabled",enabled:"Enabled",advertisement:"Ad",qualityBadge:{2160:"4K",1440:"HD",1080:"HD",720:"HD",576:"SD",480:"SD"}},urls:{download:null,vimeo:{sdk:"https://player.vimeo.com/api/player.js",iframe:"https://player.vimeo.com/video/{0}?{1}",api:"https://vimeo.com/api/v2/video/{0}.json"},youtube:{sdk:"https://www.youtube.com/iframe_api",api:"https://noembed.com/embed?url=https://www.youtube.com/watch?v={0}"},googleIMA:{sdk:"https://imasdk.googleapis.com/js/sdkloader/ima3.js"}},listeners:{seek:null,play:null,pause:null,restart:null,rewind:null,fastForward:null,mute:null,volume:null,captions:null,download:null,fullscreen:null,pip:null,airplay:null,speed:null,quality:null,loop:null,language:null},events:["ended","progress","stalled","playing","waiting","canplay","canplaythrough","loadstart","loadeddata","loadedmetadata","timeupdate","volumechange","play","pause","error","seeking","seeked","emptied","ratechange","cuechange","download","enterfullscreen","exitfullscreen","captionsenabled","captionsdisabled","languagechange","controlshidden","controlsshown","ready","statechange","qualitychange","adsloaded","adscontentpause","adscontentresume","adstarted","adsmidpoint","adscomplete","adsallcomplete","adsimpression","adsclick"],selectors:{editable:"input, textarea, select, [contenteditable]",container:".plyr",controls:{container:null,wrapper:".plyr__controls"},labels:"[data-plyr]",buttons:{play:'[data-plyr="play"]',pause:'[data-plyr="pause"]',restart:'[data-plyr="restart"]',rewind:'[data-plyr="rewind"]',fastForward:'[data-plyr="fast-forward"]',mute:'[data-plyr="mute"]',captions:'[data-plyr="captions"]',download:'[data-plyr="download"]',fullscreen:'[data-plyr="fullscreen"]',pip:'[data-plyr="pip"]',airplay:'[data-plyr="airplay"]',settings:'[data-plyr="settings"]',loop:'[data-plyr="loop"]'},inputs:{seek:'[data-plyr="seek"]',volume:'[data-plyr="volume"]',speed:'[data-plyr="speed"]',language:'[data-plyr="language"]',quality:'[data-plyr="quality"]'},display:{currentTime:".plyr__time--current",duration:".plyr__time--duration",buffer:".plyr__progress__buffer",loop:".plyr__progress__loop",volume:".plyr__volume--display"},progress:".plyr__progress",captions:".plyr__captions",caption:".plyr__caption"},classNames:{type:"plyr--{0}",provider:"plyr--{0}",video:"plyr__video-wrapper",embed:"plyr__video-embed",videoFixedRatio:"plyr__video-wrapper--fixed-ratio",embedContainer:"plyr__video-embed__container",poster:"plyr__poster",posterEnabled:"plyr__poster-enabled",ads:"plyr__ads",control:"plyr__control",controlPressed:"plyr__control--pressed",playing:"plyr--playing",paused:"plyr--paused",stopped:"plyr--stopped",loading:"plyr--loading",hover:"plyr--hover",tooltip:"plyr__tooltip",cues:"plyr__cues",hidden:"plyr__sr-only",hideControls:"plyr--hide-controls",isIos:"plyr--is-ios",isTouch:"plyr--is-touch",uiSupported:"plyr--full-ui",noTransition:"plyr--no-transition",display:{time:"plyr__time"},menu:{value:"plyr__menu__value",badge:"plyr__badge",open:"plyr--menu-open"},captions:{enabled:"plyr--captions-enabled",active:"plyr--captions-active"},fullscreen:{enabled:"plyr--fullscreen-enabled",fallback:"plyr--fullscreen-fallback"},pip:{supported:"plyr--pip-supported",active:"plyr--pip-active"},airplay:{supported:"plyr--airplay-supported",active:"plyr--airplay-active"},tabFocus:"plyr__tab-focus",previewThumbnails:{thumbContainer:"plyr__preview-thumb",thumbContainerShown:"plyr__preview-thumb--is-shown",imageContainer:"plyr__preview-thumb__image-container",timeContainer:"plyr__preview-thumb__time-container",scrubbingContainer:"plyr__preview-scrubbing",scrubbingContainerShown:"plyr__preview-scrubbing--is-shown"}},attributes:{embed:{provider:"data-plyr-provider",id:"data-plyr-embed-id"}},ads:{enabled:!1,publisherId:"",tagUrl:""},previewThumbnails:{enabled:!1,src:""},vimeo:{byline:!1,portrait:!1,title:!1,speed:!0,transparent:!1,premium:!1,referrerPolicy:null},youtube:{noCookie:!0,rel:0,showinfo:0,iv_load_policy:3,modestbranding:1}},Uh="picture-in-picture",Dh="inline",Fh={html5:"html5",youtube:"youtube",vimeo:"vimeo"},qh="audio",Hh="video";var Bh=function(){},Vh=function(){function e(){var t=arguments.length>0&&void 0!==arguments[0]&&arguments[0];yo(this,e),this.enabled=window.console&&t,this.enabled&&this.log("Debugging enabled")}return wo(e,[{key:"log",get:function(){return this.enabled?Function.prototype.bind.call(console.log,console):Bh}},{key:"warn",get:function(){return this.enabled?Function.prototype.bind.call(console.warn,console):Bh}},{key:"error",get:function(){return this.enabled?Function.prototype.bind.call(console.error,console):Bh}}]),e}(),zh=function(){function e(t){var n=this;yo(this,e),this.player=t,this.prefix=e.prefix,this.property=e.property,this.scrollPosition={x:0,y:0},this.forceFallback="force"===t.config.fullscreen.fallback,this.player.elements.fullscreen=t.config.fullscreen.container&&function(e,t){return(Element.prototype.closest||function(){var e=this;do{if(Ru.matches(e,t))return e;e=e.parentElement||e.parentNode}while(null!==e&&1===e.nodeType);return null}).call(e,t)}(this.player.elements.container,t.config.fullscreen.container),Vu.call(this.player,document,"ms"===this.prefix?"MSFullscreenChange":"".concat(this.prefix,"fullscreenchange"),(function(){n.onChange()})),Vu.call(this.player,this.player.elements.container,"dblclick",(function(e){su(n.player.elements.controls)&&n.player.elements.controls.contains(e.target)||n.toggle()})),Vu.call(this,this.player.elements.container,"keydown",(function(e){return n.trapFocus(e)})),this.update()}return wo(e,[{key:"onChange",value:function(){if(this.enabled){var e=this.player.elements.buttons.fullscreen;su(e)&&(e.pressed=this.active),Ku.call(this.player,this.target,this.active?"enterfullscreen":"exitfullscreen",!0)}}},{key:"toggleFallback",value:function(){var e=arguments.length>0&&void 0!==arguments[0]&&arguments[0];if(e?this.scrollPosition={x:window.scrollX||0,y:window.scrollY||0}:window.scrollTo(this.scrollPosition.x,this.scrollPosition.y),document.body.style.overflow=e?"hidden":"",ju(this.target,this.player.config.classNames.fullscreen.fallback,e),gu.isIos){var t=document.head.querySelector('meta[name="viewport"]'),n="viewport-fit=cover";t||(t=document.createElement("meta")).setAttribute("name","viewport");var i=nu(t.content)&&t.content.includes(n);e?(this.cleanupViewport=!i,i||(t.content+=",".concat(n))):this.cleanupViewport&&(t.content=t.content.split(",").filter((function(e){return e.trim()!==n})).join(","))}this.onChange()}},{key:"trapFocus",value:function(e){if(!gu.isIos&&this.active&&"Tab"===e.key&&9===e.keyCode){var t=document.activeElement,n=Mu.call(this.player,"a[href], button:not(:disabled), input:not(:disabled), [tabindex]"),i=Ao(n,1)[0],r=n[n.length-1];t!==r||e.shiftKey?t===i&&e.shiftKey&&(r.focus(),e.preventDefault()):(i.focus(),e.preventDefault())}}},{key:"update",value:function(){var t;this.enabled?(t=this.forceFallback?"Fallback (forced)":e.native?"Native":"Fallback",this.player.debug.log("".concat(t," fullscreen enabled"))):this.player.debug.log("Fullscreen not supported and fallback disabled");ju(this.player.elements.container,this.player.config.classNames.fullscreen.enabled,this.enabled)}},{key:"enter",value:function(){this.enabled&&(gu.isIos&&this.player.config.fullscreen.iosNative?this.target.webkitEnterFullscreen():!e.native||this.forceFallback?this.toggleFallback(!0):this.prefix?du(this.prefix)||this.target["".concat(this.prefix,"Request").concat(this.property)]():this.target.requestFullscreen({navigationUI:"hide"}))}},{key:"exit",value:function(){if(this.enabled)if(gu.isIos&&this.player.config.fullscreen.iosNative)this.target.webkitExitFullscreen(),Gu(this.player.play());else if(!e.native||this.forceFallback)this.toggleFallback(!1);else if(this.prefix){if(!du(this.prefix)){var t="moz"===this.prefix?"Cancel":"Exit";document["".concat(this.prefix).concat(t).concat(this.property)]()}}else(document.cancelFullScreen||document.exitFullscreen).call(document)}},{key:"toggle",value:function(){this.active?this.exit():this.enter()}},{key:"usingNative",get:function(){return e.native&&!this.forceFallback}},{key:"enabled",get:function(){return(e.native||this.player.config.fullscreen.fallback)&&this.player.config.fullscreen.enabled&&this.player.supported.ui&&this.player.isVideo}},{key:"active",get:function(){if(!this.enabled)return!1;if(!e.native||this.forceFallback)return Nu(this.target,this.player.config.classNames.fullscreen.fallback);var t=this.prefix?document["".concat(this.prefix).concat(this.property,"Element")]:document.fullscreenElement;return t&&t.shadowRoot?t===this.target.getRootNode().host:t===this.target}},{key:"target",get:function(){return gu.isIos&&this.player.config.fullscreen.iosNative?this.player.media:this.player.elements.fullscreen||this.player.elements.container}}],[{key:"native",get:function(){return!!(document.fullscreenEnabled||document.webkitFullscreenEnabled||document.mozFullScreenEnabled||document.msFullscreenEnabled)}},{key:"prefix",get:function(){if(ru(document.exitFullscreen))return"";var e="";return["webkit","moz","ms"].some((function(t){return!(!ru(document["".concat(t,"ExitFullscreen")])&&!ru(document["".concat(t,"CancelFullScreen")]))&&(e=t,!0)})),e}},{key:"property",get:function(){return"moz"===this.prefix?"FullScreen":"Fullscreen"}}]),e}(),Wh=Math.sign||function(e){return 0==(e=+e)||e!=e?e:e<0?-1:1};function Kh(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:1;return new Promise((function(n,i){var r=new Image,a=function(){delete r.onload,delete r.onerror,(r.naturalWidth>=t?n:i)(r)};Object.assign(r,{onload:a,onerror:a,src:e})}))}Ie({target:"Math",stat:!0},{sign:Wh});var $h={addStyleHook:function(){ju(this.elements.container,this.config.selectors.container.replace(".",""),!0),ju(this.elements.container,this.config.classNames.uiSupported,this.supported.ui)},toggleNativeControls:function(){var e=arguments.length>0&&void 0!==arguments[0]&&arguments[0];e&&this.isHTML5?this.media.setAttribute("controls",""):this.media.removeAttribute("controls")},build:function(){var e=this;if(this.listeners.media(),!this.supported.ui)return this.debug.warn("Basic support only for ".concat(this.provider," ").concat(this.type)),void $h.toggleNativeControls.call(this,!0);su(this.elements.controls)||(jh.inject.call(this),this.listeners.controls()),$h.toggleNativeControls.call(this),this.isHTML5&&Mh.setup.call(this),this.volume=null,this.muted=null,this.loop=null,this.quality=null,this.speed=null,jh.updateVolume.call(this),jh.timeUpdate.call(this),$h.checkPlaying.call(this),ju(this.elements.container,this.config.classNames.pip.supported,qu.pip&&this.isHTML5&&this.isVideo),ju(this.elements.container,this.config.classNames.airplay.supported,qu.airplay&&this.isHTML5),ju(this.elements.container,this.config.classNames.isIos,gu.isIos),ju(this.elements.container,this.config.classNames.isTouch,this.touch),this.ready=!0,setTimeout((function(){Ku.call(e,e.media,"ready")}),0),$h.setTitle.call(this),this.poster&&$h.setPoster.call(this,this.poster,!1).catch((function(){})),this.config.duration&&jh.durationUpdate.call(this)},setTitle:function(){var e=Th("play",this.config);if(nu(this.config.title)&&!du(this.config.title)&&(e+=", ".concat(this.config.title)),Array.from(this.elements.buttons.play||[]).forEach((function(t){t.setAttribute("aria-label",e)})),this.isEmbed){var t=_u.call(this,"iframe");if(!su(t))return;var n=du(this.config.title)?"video":this.config.title,i=Th("frameTitle",this.config);t.setAttribute("title",i.replace("{title}",n))}},togglePoster:function(e){ju(this.elements.container,this.config.classNames.posterEnabled,e)},setPoster:function(e){var t=this,n=!(arguments.length>1&&void 0!==arguments[1])||arguments[1];return n&&this.poster?Promise.reject(new Error("Poster already set")):(this.media.setAttribute("data-poster",e),Yu.call(this).then((function(){return Kh(e)})).catch((function(n){throw e===t.poster&&$h.togglePoster.call(t,!1),n})).then((function(){if(e!==t.poster)throw new Error("setPoster cancelled by later call to setPoster")})).then((function(){return Object.assign(t.elements.poster.style,{backgroundImage:"url('".concat(e,"')"),backgroundSize:""}),$h.togglePoster.call(t,!0),e})))},checkPlaying:function(e){var t=this;ju(this.elements.container,this.config.classNames.playing,this.playing),ju(this.elements.container,this.config.classNames.paused,this.paused),ju(this.elements.container,this.config.classNames.stopped,this.stopped),Array.from(this.elements.buttons.play||[]).forEach((function(e){Object.assign(e,{pressed:t.playing}),e.setAttribute("aria-label",Th(t.playing?"pause":"play",t.config))})),lu(e)&&"timeupdate"===e.type||$h.toggleControls.call(this)},checkLoading:function(e){var t=this;this.loading=["stalled","waiting"].includes(e.type),clearTimeout(this.timers.loading),this.timers.loading=setTimeout((function(){ju(t.elements.container,t.config.classNames.loading,t.loading),$h.toggleControls.call(t)}),this.loading?250:0)},toggleControls:function(e){var t=this.elements.controls;if(t&&this.config.hideControls){var n=this.touch&&this.lastSeekTime+2e3>Date.now();this.toggleControls(Boolean(e||this.loading||this.paused||t.pressed||t.hover||n))}},migrateStyles:function(){var e=this;Object.values(So({},this.media.style)).filter((function(e){return!du(e)&&e.startsWith("--plyr")})).forEach((function(t){e.elements.container.style.setProperty(t,e.media.style.getPropertyValue(t)),e.media.style.removeProperty(t)})),du(this.media.style)&&this.media.removeAttribute("style")}},Yh=function(){function e(t){yo(this,e),this.player=t,this.lastKey=null,this.focusTimer=null,this.lastKeyDown=null,this.handleKey=this.handleKey.bind(this),this.toggleMenu=this.toggleMenu.bind(this),this.setTabFocus=this.setTabFocus.bind(this),this.firstTouch=this.firstTouch.bind(this)}return wo(e,[{key:"handleKey",value:function(e){var t=this.player,n=t.elements,i=e.keyCode?e.keyCode:e.which,r="keydown"===e.type,a=r&&i===this.lastKey;if(!(e.altKey||e.ctrlKey||e.metaKey||e.shiftKey)&&tu(i)){if(r){var o=document.activeElement;if(su(o)){var s=t.config.selectors.editable;if(o!==n.inputs.seek&&Ru(o,s))return;if(32===e.which&&Ru(o,'button, [role^="menuitem"]'))return}switch([32,37,38,39,40,48,49,50,51,52,53,54,56,57,67,70,73,75,76,77,79].includes(i)&&(e.preventDefault(),e.stopPropagation()),i){case 48:case 49:case 50:case 51:case 52:case 53:case 54:case 55:case 56:case 57:a||(t.currentTime=t.duration/10*(i-48));break;case 32:case 75:a||Gu(t.togglePlay());break;case 38:t.increaseVolume(.1);break;case 40:t.decreaseVolume(.1);break;case 77:a||(t.muted=!t.muted);break;case 39:t.forward();break;case 37:t.rewind();break;case 70:t.fullscreen.toggle();break;case 67:a||t.toggleCaptions();break;case 76:t.loop=!t.loop}27===i&&!t.fullscreen.usingNative&&t.fullscreen.active&&t.fullscreen.toggle(),this.lastKey=i}else this.lastKey=null}}},{key:"toggleMenu",value:function(e){jh.toggleMenu.call(this.player,e)}},{key:"firstTouch",value:function(){var e=this.player,t=e.elements;e.touch=!0,ju(t.container,e.config.classNames.isTouch,!0)}},{key:"setTabFocus",value:function(e){var t=this.player,n=t.elements;if(clearTimeout(this.focusTimer),"keydown"!==e.type||9===e.which){"keydown"===e.type&&(this.lastKeyDown=e.timeStamp);var i,r=e.timeStamp-this.lastKeyDown<=20;if("focus"!==e.type||r)i=t.config.classNames.tabFocus,ju(Mu.call(t,".".concat(i)),i,!1),"focusout"!==e.type&&(this.focusTimer=setTimeout((function(){var e=document.activeElement;n.container.contains(e)&&ju(document.activeElement,t.config.classNames.tabFocus,!0)}),10))}}},{key:"global",value:function(){var e=!(arguments.length>0&&void 0!==arguments[0])||arguments[0],t=this.player;t.config.keyboard.global&&Bu.call(t,window,"keydown keyup",this.handleKey,e,!1),Bu.call(t,document.body,"click",this.toggleMenu,e),Wu.call(t,document.body,"touchstart",this.firstTouch),Bu.call(t,document.body,"keydown focus blur focusout",this.setTabFocus,e,!1,!0)}},{key:"container",value:function(){var e=this.player,t=e.config,n=e.elements,i=e.timers;!t.keyboard.global&&t.keyboard.focused&&Vu.call(e,n.container,"keydown keyup",this.handleKey,!1),Vu.call(e,n.container,"mousemove mouseleave touchstart touchmove enterfullscreen exitfullscreen",(function(t){var r=n.controls;r&&"enterfullscreen"===t.type&&(r.pressed=!1,r.hover=!1);var a=0;["touchstart","touchmove","mousemove"].includes(t.type)&&($h.toggleControls.call(e,!0),a=e.touch?3e3:2e3),clearTimeout(i.controls),i.controls=setTimeout((function(){return $h.toggleControls.call(e,!1)}),a)}));var r=function(t){if(!t)return Zu.call(e);var i=n.container.getBoundingClientRect(),r=i.width,a=i.height;return Zu.call(e,"".concat(r,":").concat(a))},a=function(){clearTimeout(i.resized),i.resized=setTimeout(r,50)};Vu.call(e,n.container,"enterfullscreen exitfullscreen",(function(t){var i=e.fullscreen,o=i.target,s=i.usingNative;if(o===n.container&&(e.isEmbed||!du(e.config.ratio))){var l="enterfullscreen"===t.type,c=r(l);c.padding;!function(t,n,i){if(e.isVimeo&&!e.config.vimeo.premium){var r=e.elements.wrapper.firstChild,a=Ao(t,2)[1],o=Ao(Ju.call(e),2),s=o[0],l=o[1];r.style.maxWidth=i?"".concat(a/l*s,"px"):null,r.style.margin=i?"0 auto":null}}(c.ratio,0,l),s||(l?Vu.call(e,window,"resize",a):zu.call(e,window,"resize",a))}}))}},{key:"media",value:function(){var e=this,t=this.player,n=t.elements;if(Vu.call(t,t.media,"timeupdate seeking seeked",(function(e){return jh.timeUpdate.call(t,e)})),Vu.call(t,t.media,"durationchange loadeddata loadedmetadata",(function(e){return jh.durationUpdate.call(t,e)})),Vu.call(t,t.media,"ended",(function(){t.isHTML5&&t.isVideo&&t.config.resetOnEnd&&(t.restart(),t.pause())})),Vu.call(t,t.media,"progress playing seeking seeked",(function(e){return jh.updateProgress.call(t,e)})),Vu.call(t,t.media,"volumechange",(function(e){return jh.updateVolume.call(t,e)})),Vu.call(t,t.media,"playing play pause ended emptied timeupdate",(function(e){return $h.checkPlaying.call(t,e)})),Vu.call(t,t.media,"waiting canplay seeked playing",(function(e){return $h.checkLoading.call(t,e)})),t.supported.ui&&t.config.clickToPlay&&!t.isAudio){var i=_u.call(t,".".concat(t.config.classNames.video));if(!su(i))return;Vu.call(t,n.container,"click",(function(r){([n.container,i].includes(r.target)||i.contains(r.target))&&(t.touch&&t.config.hideControls||(t.ended?(e.proxy(r,t.restart,"restart"),e.proxy(r,(function(){Gu(t.play())}),"play")):e.proxy(r,(function(){Gu(t.togglePlay())}),"play")))}))}t.supported.ui&&t.config.disableContextMenu&&Vu.call(t,n.wrapper,"contextmenu",(function(e){e.preventDefault()}),!1),Vu.call(t,t.media,"volumechange",(function(){t.storage.set({volume:t.volume,muted:t.muted})})),Vu.call(t,t.media,"ratechange",(function(){jh.updateSetting.call(t,"speed"),t.storage.set({speed:t.speed})})),Vu.call(t,t.media,"qualitychange",(function(e){jh.updateSetting.call(t,"quality",null,e.detail.quality)})),Vu.call(t,t.media,"ready qualitychange",(function(){jh.setDownloadUrl.call(t)}));var r=t.config.events.concat(["keyup","keydown"]).join(" ");Vu.call(t,t.media,r,(function(e){var i=e.detail,r=void 0===i?{}:i;"error"===e.type&&(r=t.media.error),Ku.call(t,n.container,e.type,!0,r)}))}},{key:"proxy",value:function(e,t,n){var i=this.player,r=i.config.listeners[n],a=!0;ru(r)&&(a=r.call(i,e)),!1!==a&&ru(t)&&t.call(i,e)}},{key:"bind",value:function(e,t,n,i){var r=this,a=!(arguments.length>4&&void 0!==arguments[4])||arguments[4],o=this.player,s=o.config.listeners[i],l=ru(s);Vu.call(o,e,t,(function(e){return r.proxy(e,n,i)}),a&&!l)}},{key:"controls",value:function(){var e=this,t=this.player,n=t.elements,i=gu.isIE?"change":"input";if(n.buttons.play&&Array.from(n.buttons.play).forEach((function(n){e.bind(n,"click",(function(){Gu(t.togglePlay())}),"play")})),this.bind(n.buttons.restart,"click",t.restart,"restart"),this.bind(n.buttons.rewind,"click",t.rewind,"rewind"),this.bind(n.buttons.fastForward,"click",t.forward,"fastForward"),this.bind(n.buttons.mute,"click",(function(){t.muted=!t.muted}),"mute"),this.bind(n.buttons.captions,"click",(function(){return t.toggleCaptions()})),this.bind(n.buttons.download,"click",(function(){Ku.call(t,t.media,"download")}),"download"),this.bind(n.buttons.fullscreen,"click",(function(){t.fullscreen.toggle()}),"fullscreen"),this.bind(n.buttons.pip,"click",(function(){t.pip="toggle"}),"pip"),this.bind(n.buttons.airplay,"click",t.airplay,"airplay"),this.bind(n.buttons.settings,"click",(function(e){e.stopPropagation(),e.preventDefault(),jh.toggleMenu.call(t,e)}),null,!1),this.bind(n.buttons.settings,"keyup",(function(e){var n=e.which;[13,32].includes(n)&&(13!==n?(e.preventDefault(),e.stopPropagation(),jh.toggleMenu.call(t,e)):jh.focusFirstMenuItem.call(t,null,!0))}),null,!1),this.bind(n.settings.menu,"keydown",(function(e){27===e.which&&jh.toggleMenu.call(t,e)})),this.bind(n.inputs.seek,"mousedown mousemove",(function(e){var t=n.progress.getBoundingClientRect(),i=100/t.width*(e.pageX-t.left);e.currentTarget.setAttribute("seek-value",i)})),this.bind(n.inputs.seek,"mousedown mouseup keydown keyup touchstart touchend",(function(e){var n=e.currentTarget,i=e.keyCode?e.keyCode:e.which;if(!cu(e)||39===i||37===i){t.lastSeekTime=Date.now();var r=n.hasAttribute("play-on-seeked"),a=["mouseup","touchend","keyup"].includes(e.type);r&&a?(n.removeAttribute("play-on-seeked"),Gu(t.play())):!a&&t.playing&&(n.setAttribute("play-on-seeked",""),t.pause())}})),gu.isIos){var r=Mu.call(t,'input[type="range"]');Array.from(r).forEach((function(t){return e.bind(t,i,(function(e){return mu(e.target)}))}))}this.bind(n.inputs.seek,i,(function(e){var n=e.currentTarget,i=n.getAttribute("seek-value");du(i)&&(i=n.value),n.removeAttribute("seek-value"),t.currentTime=i/n.max*t.duration}),"seek"),this.bind(n.progress,"mouseenter mouseleave mousemove",(function(e){return jh.updateSeekTooltip.call(t,e)})),this.bind(n.progress,"mousemove touchmove",(function(e){var n=t.previewThumbnails;n&&n.loaded&&n.startMove(e)})),this.bind(n.progress,"mouseleave touchend click",(function(){var e=t.previewThumbnails;e&&e.loaded&&e.endMove(!1,!0)})),this.bind(n.progress,"mousedown touchstart",(function(e){var n=t.previewThumbnails;n&&n.loaded&&n.startScrubbing(e)})),this.bind(n.progress,"mouseup touchend",(function(e){var n=t.previewThumbnails;n&&n.loaded&&n.endScrubbing(e)})),gu.isWebkit&&Array.from(Mu.call(t,'input[type="range"]')).forEach((function(n){e.bind(n,"input",(function(e){return jh.updateRangeFill.call(t,e.target)}))})),t.config.toggleInvert&&!su(n.display.duration)&&this.bind(n.display.currentTime,"click",(function(){0!==t.currentTime&&(t.config.invertTime=!t.config.invertTime,jh.timeUpdate.call(t))})),this.bind(n.inputs.volume,i,(function(e){t.volume=e.target.value}),"volume"),this.bind(n.controls,"mouseenter mouseleave",(function(e){n.controls.hover=!t.touch&&"mouseenter"===e.type})),n.fullscreen&&Array.from(n.fullscreen.children).filter((function(e){return!e.contains(n.container)})).forEach((function(i){e.bind(i,"mouseenter mouseleave",(function(e){n.controls.hover=!t.touch&&"mouseenter"===e.type}))})),this.bind(n.controls,"mousedown mouseup touchstart touchend touchcancel",(function(e){n.controls.pressed=["mousedown","touchstart"].includes(e.type)})),this.bind(n.controls,"focusin",(function(){var i=t.config,r=t.timers;ju(n.controls,i.classNames.noTransition,!0),$h.toggleControls.call(t,!0),setTimeout((function(){ju(n.controls,i.classNames.noTransition,!1)}),0);var a=e.touch?3e3:4e3;clearTimeout(r.controls),r.controls=setTimeout((function(){return $h.toggleControls.call(t,!1)}),a)})),this.bind(n.inputs.volume,"wheel",(function(e){var n=e.webkitDirectionInvertedFromDevice,i=Ao([e.deltaX,-e.deltaY].map((function(e){return n?-e:e})),2),r=i[0],a=i[1],o=Math.sign(Math.abs(r)>Math.abs(a)?r:a);t.increaseVolume(o/50);var s=t.media.volume;(1===o&&s<1||-1===o&&s>0)&&e.preventDefault()}),"volume",!1)}}]),e}(),Gh=Kn("splice"),Xh=Qt("splice",{ACCESSORS:!0,0:0,1:2}),Qh=Math.max,Jh=Math.min;Ie({target:"Array",proto:!0,forced:!Gh||!Xh},{splice:function(e,t){var n,i,r,a,o,s,l=Re(this),c=le(l.length),u=he(e,c),h=arguments.length;if(0===h?n=i=0:1===h?(n=0,i=c-u):(n=h-2,i=Jh(Qh(oe(t),0),c-u)),c+n-i>9007199254740991)throw TypeError("Maximum allowed length exceeded");for(r=ot(l,i),a=0;a<i;a++)(o=u+a)in l&&Fn(r,a,l[o]);if(r.length=i,n<i){for(a=u;a<c-i;a++)s=a+n,(o=a+i)in l?l[s]=l[o]:delete l[s];for(a=c;a>c-i+n;a--)delete l[a-1]}else if(n>i)for(a=c-i;a>u;a--)s=a+n-1,(o=a+i-1)in l?l[s]=l[o]:delete l[s];for(a=0;a<n;a++)l[a+u]=arguments[a+2];return l.length=c-i+n,r}});var Zh=t((function(e,t){e.exports=function(){var e=function(){},t={},n={},i={};function r(e,t){if(e){var r=i[e];if(n[e]=t,r)for(;r.length;)r[0](e,t),r.splice(0,1)}}function a(t,n){t.call&&(t={success:t}),n.length?(t.error||e)(n):(t.success||e)(t)}function o(t,n,i,r){var a,s,l=document,c=i.async,u=(i.numRetries||0)+1,h=i.before||e,f=t.replace(/[\?|#].*$/,""),d=t.replace(/^(css|img)!/,"");r=r||0,/(^css!|\.css$)/.test(f)?((s=l.createElement("link")).rel="stylesheet",s.href=d,(a="hideFocus"in s)&&s.relList&&(a=0,s.rel="preload",s.as="style")):/(^img!|\.(png|gif|jpg|svg|webp)$)/.test(f)?(s=l.createElement("img")).src=d:((s=l.createElement("script")).src=t,s.async=void 0===c||c),s.onload=s.onerror=s.onbeforeload=function(e){var l=e.type[0];if(a)try{s.sheet.cssText.length||(l="e")}catch(e){18!=e.code&&(l="e")}if("e"==l){if((r+=1)<u)return o(t,n,i,r)}else if("preload"==s.rel&&"style"==s.as)return s.rel="stylesheet";n(t,l,e.defaultPrevented)},!1!==h(t,s)&&l.head.appendChild(s)}function s(e,n,i){var s,l;if(n&&n.trim&&(s=n),l=(s?i:n)||{},s){if(s in t)throw"LoadJS";t[s]=!0}function c(t,n){!function(e,t,n){var i,r,a=(e=e.push?e:[e]).length,s=a,l=[];for(i=function(e,n,i){if("e"==n&&l.push(e),"b"==n){if(!i)return;l.push(e)}--a||t(l)},r=0;r<s;r++)o(e[r],i,n)}(e,(function(e){a(l,e),t&&a({success:t,error:n},e),r(s,e)}),l)}if(l.returnPromise)return new Promise(c);c()}return s.ready=function(e,t){return function(e,t){e=e.push?e:[e];var r,a,o,s=[],l=e.length,c=l;for(r=function(e,n){n.length&&s.push(e),--c||t(s)};l--;)a=e[l],(o=n[a])?r(a,o):(i[a]=i[a]||[]).push(r)}(e,(function(e){a(t,e)})),s},s.done=function(e){r(e,[])},s.reset=function(){t={},n={},i={}},s.isDefined=function(e){return e in t},s}()}));function ef(e){return new Promise((function(t,n){Zh(e,{success:t,error:n})}))}function tf(e){e&&!this.embed.hasPlayed&&(this.embed.hasPlayed=!0),this.media.paused===e&&(this.media.paused=!e,Ku.call(this,this.media,e?"play":"pause"))}var nf={setup:function(){var e=this;ju(e.elements.wrapper,e.config.classNames.embed,!0),e.options.speed=e.config.speed.options,Zu.call(e),eu(window.Vimeo)?nf.ready.call(e):ef(e.config.urls.vimeo.sdk).then((function(){nf.ready.call(e)})).catch((function(t){e.debug.warn("Vimeo SDK (player.js) failed to load",t)}))},ready:function(){var e=this,t=this,n=t.config.vimeo,i=n.premium,r=n.referrerPolicy,a=Eo(n,["premium","referrerPolicy"]);i&&Object.assign(a,{controls:!1,sidedock:!1});var o=Rh(So({loop:t.config.loop.active,autoplay:t.autoplay,muted:t.muted,gesture:"media",playsinline:!this.config.fullscreen.iosNative},a)),s=t.media.getAttribute("src");du(s)&&(s=t.media.getAttribute(t.config.attributes.embed.id));var l,c=du(l=s)?null:tu(Number(l))?l:l.match(/^.*(vimeo.com\/|video\/)(\d+).*/)?RegExp.$2:l,u=Au("iframe"),h=gh(t.config.urls.vimeo.iframe,c,o);u.setAttribute("src",h),u.setAttribute("allowfullscreen",""),u.setAttribute("allow","autoplay,fullscreen,picture-in-picture"),du(r)||u.setAttribute("referrerPolicy",r);var f=t.poster;if(i)u.setAttribute("data-poster",f),t.media=Ou(u,t.media);else{var d=Au("div",{class:t.config.classNames.embedContainer,"data-poster":f});d.appendChild(u),t.media=Ou(d,t.media)}Eh(gh(t.config.urls.vimeo.api,c),"json").then((function(e){if(!du(e)){var n=new URL(e[0].thumbnail_large);n.pathname="".concat(n.pathname.split("_")[0],".jpg"),$h.setPoster.call(t,n.href).catch((function(){}))}})),t.embed=new window.Vimeo.Player(u,{autopause:t.config.autopause,muted:t.muted}),t.media.paused=!0,t.media.currentTime=0,t.supported.ui&&t.embed.disableTextTrack(),t.media.play=function(){return tf.call(t,!0),t.embed.play()},t.media.pause=function(){return tf.call(t,!1),t.embed.pause()},t.media.stop=function(){t.pause(),t.currentTime=0};var p=t.media.currentTime;Object.defineProperty(t.media,"currentTime",{get:function(){return p},set:function(e){var n=t.embed,i=t.media,r=t.paused,a=t.volume,o=r&&!n.hasPlayed;i.seeking=!0,Ku.call(t,i,"seeking"),Promise.resolve(o&&n.setVolume(0)).then((function(){return n.setCurrentTime(e)})).then((function(){return o&&n.pause()})).then((function(){return o&&n.setVolume(a)})).catch((function(){}))}});var m=t.config.speed.selected;Object.defineProperty(t.media,"playbackRate",{get:function(){return m},set:function(e){t.embed.setPlaybackRate(e).then((function(){m=e,Ku.call(t,t.media,"ratechange")})).catch((function(){t.options.speed=[1]}))}});var g=t.config.volume;Object.defineProperty(t.media,"volume",{get:function(){return g},set:function(e){t.embed.setVolume(e).then((function(){g=e,Ku.call(t,t.media,"volumechange")}))}});var v=t.config.muted;Object.defineProperty(t.media,"muted",{get:function(){return v},set:function(e){var n=!!iu(e)&&e;t.embed.setVolume(n?0:t.config.volume).then((function(){v=n,Ku.call(t,t.media,"volumechange")}))}});var y,b=t.config.loop;Object.defineProperty(t.media,"loop",{get:function(){return b},set:function(e){var n=iu(e)?e:t.config.loop.active;t.embed.setLoop(n).then((function(){b=n}))}}),t.embed.getVideoUrl().then((function(e){y=e,jh.setDownloadUrl.call(t)})).catch((function(t){e.debug.warn(t)})),Object.defineProperty(t.media,"currentSrc",{get:function(){return y}}),Object.defineProperty(t.media,"ended",{get:function(){return t.currentTime===t.duration}}),Promise.all([t.embed.getVideoWidth(),t.embed.getVideoHeight()]).then((function(n){var i=Ao(n,2),r=i[0],a=i[1];t.embed.ratio=[r,a],Zu.call(e)})),t.embed.setAutopause(t.config.autopause).then((function(e){t.config.autopause=e})),t.embed.getVideoTitle().then((function(n){t.config.title=n,$h.setTitle.call(e)})),t.embed.getCurrentTime().then((function(e){p=e,Ku.call(t,t.media,"timeupdate")})),t.embed.getDuration().then((function(e){t.media.duration=e,Ku.call(t,t.media,"durationchange")})),t.embed.getTextTracks().then((function(e){t.media.textTracks=e,Mh.setup.call(t)})),t.embed.on("cuechange",(function(e){var n=e.cues,i=(void 0===n?[]:n).map((function(e){return function(e){var t=document.createDocumentFragment(),n=document.createElement("div");return t.appendChild(n),n.innerHTML=e,t.firstChild.innerText}(e.text)}));Mh.updateCues.call(t,i)})),t.embed.on("loaded",(function(){(t.embed.getPaused().then((function(e){tf.call(t,!e),e||Ku.call(t,t.media,"playing")})),su(t.embed.element)&&t.supported.ui)&&t.embed.element.setAttribute("tabindex",-1)})),t.embed.on("bufferstart",(function(){Ku.call(t,t.media,"waiting")})),t.embed.on("bufferend",(function(){Ku.call(t,t.media,"playing")})),t.embed.on("play",(function(){tf.call(t,!0),Ku.call(t,t.media,"playing")})),t.embed.on("pause",(function(){tf.call(t,!1)})),t.embed.on("timeupdate",(function(e){t.media.seeking=!1,p=e.seconds,Ku.call(t,t.media,"timeupdate")})),t.embed.on("progress",(function(e){t.media.buffered=e.percent,Ku.call(t,t.media,"progress"),1===parseInt(e.percent,10)&&Ku.call(t,t.media,"canplaythrough"),t.embed.getDuration().then((function(e){e!==t.media.duration&&(t.media.duration=e,Ku.call(t,t.media,"durationchange"))}))})),t.embed.on("seeked",(function(){t.media.seeking=!1,Ku.call(t,t.media,"seeked")})),t.embed.on("ended",(function(){t.media.paused=!0,Ku.call(t,t.media,"ended")})),t.embed.on("error",(function(e){t.media.error=e,Ku.call(t,t.media,"error")})),setTimeout((function(){return $h.build.call(t)}),0)}};function rf(e){e&&!this.embed.hasPlayed&&(this.embed.hasPlayed=!0),this.media.paused===e&&(this.media.paused=!e,Ku.call(this,this.media,e?"play":"pause"))}function af(e){return e.noCookie?"https://www.youtube-nocookie.com":"http:"===window.location.protocol?"http://www.youtube.com":void 0}var of={setup:function(){var e=this;if(ju(this.elements.wrapper,this.config.classNames.embed,!0),eu(window.YT)&&ru(window.YT.Player))of.ready.call(this);else{var t=window.onYouTubeIframeAPIReady;window.onYouTubeIframeAPIReady=function(){ru(t)&&t(),of.ready.call(e)},ef(this.config.urls.youtube.sdk).catch((function(t){e.debug.warn("YouTube API failed to load",t)}))}},getTitle:function(e){var t=this;Eh(gh(this.config.urls.youtube.api,e)).then((function(e){if(eu(e)){var n=e.title,i=e.height,r=e.width;t.config.title=n,$h.setTitle.call(t),t.embed.ratio=[r,i]}Zu.call(t)})).catch((function(){Zu.call(t)}))},ready:function(){var e=this,t=e.media&&e.media.getAttribute("id");if(du(t)||!t.startsWith("youtube-")){var n=e.media.getAttribute("src");du(n)&&(n=e.media.getAttribute(this.config.attributes.embed.id));var i,r,a=du(i=n)?null:i.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/)?RegExp.$2:i,o=(r=e.provider,"".concat(r,"-").concat(Math.floor(1e4*Math.random()))),s=Au("div",{id:o,"data-poster":e.poster});e.media=Ou(s,e.media);var l=function(e){return"https://i.ytimg.com/vi/".concat(a,"/").concat(e,"default.jpg")};Kh(l("maxres"),121).catch((function(){return Kh(l("sd"),121)})).catch((function(){return Kh(l("hq"))})).then((function(t){return $h.setPoster.call(e,t.src)})).then((function(t){t.includes("maxres")||(e.elements.poster.style.backgroundSize="cover")})).catch((function(){}));var c=e.config.youtube;e.embed=new window.YT.Player(o,{videoId:a,host:af(c),playerVars:Tu({},{autoplay:e.config.autoplay?1:0,hl:e.config.hl,controls:e.supported.ui?0:1,disablekb:1,playsinline:e.config.fullscreen.iosNative?0:1,cc_load_policy:e.captions.active?1:0,cc_lang_pref:e.config.captions.language,widget_referrer:window?window.location.href:null},c),events:{onError:function(t){if(!e.media.error){var n=t.data,i={2:"The request contains an invalid parameter value. For example, this error occurs if you specify a video ID that does not have 11 characters, or if the video ID contains invalid characters, such as exclamation points or asterisks.",5:"The requested content cannot be played in an HTML5 player or another error related to the HTML5 player has occurred.",100:"The video requested was not found. This error occurs when a video has been removed (for any reason) or has been marked as private.",101:"The owner of the requested video does not allow it to be played in embedded players.",150:"The owner of the requested video does not allow it to be played in embedded players."}[n]||"An unknown error occured";e.media.error={code:n,message:i},Ku.call(e,e.media,"error")}},onPlaybackRateChange:function(t){var n=t.target;e.media.playbackRate=n.getPlaybackRate(),Ku.call(e,e.media,"ratechange")},onReady:function(t){if(!ru(e.media.play)){var n=t.target;of.getTitle.call(e,a),e.media.play=function(){rf.call(e,!0),n.playVideo()},e.media.pause=function(){rf.call(e,!1),n.pauseVideo()},e.media.stop=function(){n.stopVideo()},e.media.duration=n.getDuration(),e.media.paused=!0,e.media.currentTime=0,Object.defineProperty(e.media,"currentTime",{get:function(){return Number(n.getCurrentTime())},set:function(t){e.paused&&!e.embed.hasPlayed&&e.embed.mute(),e.media.seeking=!0,Ku.call(e,e.media,"seeking"),n.seekTo(t)}}),Object.defineProperty(e.media,"playbackRate",{get:function(){return n.getPlaybackRate()},set:function(e){n.setPlaybackRate(e)}});var i=e.config.volume;Object.defineProperty(e.media,"volume",{get:function(){return i},set:function(t){i=t,n.setVolume(100*i),Ku.call(e,e.media,"volumechange")}});var r=e.config.muted;Object.defineProperty(e.media,"muted",{get:function(){return r},set:function(t){var i=iu(t)?t:r;r=i,n[i?"mute":"unMute"](),Ku.call(e,e.media,"volumechange")}}),Object.defineProperty(e.media,"currentSrc",{get:function(){return n.getVideoUrl()}}),Object.defineProperty(e.media,"ended",{get:function(){return e.currentTime===e.duration}});var o=n.getAvailablePlaybackRates();e.options.speed=o.filter((function(t){return e.config.speed.options.includes(t)})),e.supported.ui&&e.media.setAttribute("tabindex",-1),Ku.call(e,e.media,"timeupdate"),Ku.call(e,e.media,"durationchange"),clearInterval(e.timers.buffering),e.timers.buffering=setInterval((function(){e.media.buffered=n.getVideoLoadedFraction(),(null===e.media.lastBuffered||e.media.lastBuffered<e.media.buffered)&&Ku.call(e,e.media,"progress"),e.media.lastBuffered=e.media.buffered,1===e.media.buffered&&(clearInterval(e.timers.buffering),Ku.call(e,e.media,"canplaythrough"))}),200),setTimeout((function(){return $h.build.call(e)}),50)}},onStateChange:function(t){var n=t.target;switch(clearInterval(e.timers.playing),e.media.seeking&&[1,2].includes(t.data)&&(e.media.seeking=!1,Ku.call(e,e.media,"seeked")),t.data){case-1:Ku.call(e,e.media,"timeupdate"),e.media.buffered=n.getVideoLoadedFraction(),Ku.call(e,e.media,"progress");break;case 0:rf.call(e,!1),e.media.loop?(n.stopVideo(),n.playVideo()):Ku.call(e,e.media,"ended");break;case 1:e.config.autoplay||!e.media.paused||e.embed.hasPlayed?(rf.call(e,!0),Ku.call(e,e.media,"playing"),e.timers.playing=setInterval((function(){Ku.call(e,e.media,"timeupdate")}),50),e.media.duration!==n.getDuration()&&(e.media.duration=n.getDuration(),Ku.call(e,e.media,"durationchange"))):e.media.pause();break;case 2:e.muted||e.embed.unMute(),rf.call(e,!1);break;case 3:Ku.call(e,e.media,"waiting")}Ku.call(e,e.elements.container,"statechange",!1,{code:t.data})}}})}}},sf={setup:function(){this.media?(ju(this.elements.container,this.config.classNames.type.replace("{0}",this.type),!0),ju(this.elements.container,this.config.classNames.provider.replace("{0}",this.provider),!0),this.isEmbed&&ju(this.elements.container,this.config.classNames.type.replace("{0}","video"),!0),this.isVideo&&(this.elements.wrapper=Au("div",{class:this.config.classNames.video}),Su(this.media,this.elements.wrapper),this.elements.poster=Au("div",{class:this.config.classNames.poster}),this.elements.wrapper.appendChild(this.elements.poster)),this.isHTML5?eh.setup.call(this):this.isYouTube?of.setup.call(this):this.isVimeo&&nf.setup.call(this)):this.debug.warn("No media element found!")}},lf=function(){function e(t){var n=this;yo(this,e),this.player=t,this.config=t.config.ads,this.playing=!1,this.initialized=!1,this.elements={container:null,displayContainer:null},this.manager=null,this.loader=null,this.cuePoints=null,this.events={},this.safetyTimer=null,this.countdownTimer=null,this.managerPromise=new Promise((function(e,t){n.on("loaded",e),n.on("error",t)})),this.load()}return wo(e,[{key:"load",value:function(){var e=this;this.enabled&&(eu(window.google)&&eu(window.google.ima)?this.ready():ef(this.player.config.urls.googleIMA.sdk).then((function(){e.ready()})).catch((function(){e.trigger("error",new Error("Google IMA SDK failed to load"))})))}},{key:"ready",value:function(){var e,t=this;this.enabled||((e=this).manager&&e.manager.destroy(),e.elements.displayContainer&&e.elements.displayContainer.destroy(),e.elements.container.remove()),this.startSafetyTimer(12e3,"ready()"),this.managerPromise.then((function(){t.clearSafetyTimer("onAdsManagerLoaded()")})),this.listeners(),this.setupIMA()}},{key:"setupIMA",value:function(){var e=this;this.elements.container=Au("div",{class:this.player.config.classNames.ads}),this.player.elements.container.appendChild(this.elements.container),google.ima.settings.setVpaidMode(google.ima.ImaSdkSettings.VpaidMode.ENABLED),google.ima.settings.setLocale(this.player.config.ads.language),google.ima.settings.setDisableCustomPlaybackForIOS10Plus(this.player.config.playsinline),this.elements.displayContainer=new google.ima.AdDisplayContainer(this.elements.container,this.player.media),this.loader=new google.ima.AdsLoader(this.elements.displayContainer),this.loader.addEventListener(google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,(function(t){return e.onAdsManagerLoaded(t)}),!1),this.loader.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR,(function(t){return e.onAdError(t)}),!1),this.requestAds()}},{key:"requestAds",value:function(){var e=this.player.elements.container;try{var t=new google.ima.AdsRequest;t.adTagUrl=this.tagUrl,t.linearAdSlotWidth=e.offsetWidth,t.linearAdSlotHeight=e.offsetHeight,t.nonLinearAdSlotWidth=e.offsetWidth,t.nonLinearAdSlotHeight=e.offsetHeight,t.forceNonLinearFullSlot=!1,t.setAdWillPlayMuted(!this.player.muted),this.loader.requestAds(t)}catch(e){this.onAdError(e)}}},{key:"pollCountdown",value:function(){var e=this,t=arguments.length>0&&void 0!==arguments[0]&&arguments[0];if(!t)return clearInterval(this.countdownTimer),void this.elements.container.removeAttribute("data-badge-text");var n=function(){var t=Lh(Math.max(e.manager.getRemainingTime(),0)),n="".concat(Th("advertisement",e.player.config)," - ").concat(t);e.elements.container.setAttribute("data-badge-text",n)};this.countdownTimer=setInterval(n,100)}},{key:"onAdsManagerLoaded",value:function(e){var t=this;if(this.enabled){var n=new google.ima.AdsRenderingSettings;n.restoreCustomPlaybackStateOnAdBreakComplete=!0,n.enablePreloading=!0,this.manager=e.getAdsManager(this.player,n),this.cuePoints=this.manager.getCuePoints(),this.manager.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR,(function(e){return t.onAdError(e)})),Object.keys(google.ima.AdEvent.Type).forEach((function(e){t.manager.addEventListener(google.ima.AdEvent.Type[e],(function(e){return t.onAdEvent(e)}))})),this.trigger("loaded")}}},{key:"addCuePoints",value:function(){var e=this;du(this.cuePoints)||this.cuePoints.forEach((function(t){if(0!==t&&-1!==t&&t<e.player.duration){var n=e.player.elements.progress;if(su(n)){var i=100/e.player.duration*t,r=Au("span",{class:e.player.config.classNames.cues});r.style.left="".concat(i.toString(),"%"),n.appendChild(r)}}}))}},{key:"onAdEvent",value:function(e){var t=this,n=this.player.elements.container,i=e.getAd(),r=e.getAdData();switch(function(e){Ku.call(t.player,t.player.media,"ads".concat(e.replace(/_/g,"").toLowerCase()))}(e.type),e.type){case google.ima.AdEvent.Type.LOADED:this.trigger("loaded"),this.pollCountdown(!0),i.isLinear()||(i.width=n.offsetWidth,i.height=n.offsetHeight);break;case google.ima.AdEvent.Type.STARTED:this.manager.setVolume(this.player.volume);break;case google.ima.AdEvent.Type.ALL_ADS_COMPLETED:this.player.ended?this.loadAds():this.loader.contentComplete();break;case google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED:this.pauseContent();break;case google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED:this.pollCountdown(),this.resumeContent();break;case google.ima.AdEvent.Type.LOG:r.adError&&this.player.debug.warn("Non-fatal ad error: ".concat(r.adError.getMessage()))}}},{key:"onAdError",value:function(e){this.cancel(),this.player.debug.warn("Ads error",e)}},{key:"listeners",value:function(){var e,t=this,n=this.player.elements.container;this.player.on("canplay",(function(){t.addCuePoints()})),this.player.on("ended",(function(){t.loader.contentComplete()})),this.player.on("timeupdate",(function(){e=t.player.currentTime})),this.player.on("seeked",(function(){var n=t.player.currentTime;du(t.cuePoints)||t.cuePoints.forEach((function(i,r){e<i&&i<n&&(t.manager.discardAdBreak(),t.cuePoints.splice(r,1))}))})),window.addEventListener("resize",(function(){t.manager&&t.manager.resize(n.offsetWidth,n.offsetHeight,google.ima.ViewMode.NORMAL)}))}},{key:"play",value:function(){var e=this,t=this.player.elements.container;this.managerPromise||this.resumeContent(),this.managerPromise.then((function(){e.manager.setVolume(e.player.volume),e.elements.displayContainer.initialize();try{e.initialized||(e.manager.init(t.offsetWidth,t.offsetHeight,google.ima.ViewMode.NORMAL),e.manager.start()),e.initialized=!0}catch(t){e.onAdError(t)}})).catch((function(){}))}},{key:"resumeContent",value:function(){this.elements.container.style.zIndex="",this.playing=!1,Gu(this.player.media.play())}},{key:"pauseContent",value:function(){this.elements.container.style.zIndex=3,this.playing=!0,this.player.media.pause()}},{key:"cancel",value:function(){this.initialized&&this.resumeContent(),this.trigger("error"),this.loadAds()}},{key:"loadAds",value:function(){var e=this;this.managerPromise.then((function(){e.manager&&e.manager.destroy(),e.managerPromise=new Promise((function(t){e.on("loaded",t),e.player.debug.log(e.manager)})),e.initialized=!1,e.requestAds()})).catch((function(){}))}},{key:"trigger",value:function(e){for(var t=this,n=arguments.length,i=new Array(n>1?n-1:0),r=1;r<n;r++)i[r-1]=arguments[r];var a=this.events[e];au(a)&&a.forEach((function(e){ru(e)&&e.apply(t,i)}))}},{key:"on",value:function(e,t){return au(this.events[e])||(this.events[e]=[]),this.events[e].push(t),this}},{key:"startSafetyTimer",value:function(e,t){var n=this;this.player.debug.log("Safety timer invoked from: ".concat(t)),this.safetyTimer=setTimeout((function(){n.cancel(),n.clearSafetyTimer("startSafetyTimer()")}),e)}},{key:"clearSafetyTimer",value:function(e){Zc(this.safetyTimer)||(this.player.debug.log("Safety timer cleared from: ".concat(e)),clearTimeout(this.safetyTimer),this.safetyTimer=null)}},{key:"enabled",get:function(){var e=this.config;return this.player.isHTML5&&this.player.isVideo&&e.enabled&&(!du(e.publisherId)||fu(e.tagUrl))}},{key:"tagUrl",get:function(){var e=this.config;if(fu(e.tagUrl))return e.tagUrl;var t={AV_PUBLISHERID:"58c25bb0073ef448b1087ad6",AV_CHANNELID:"5a0458dc28a06145e4519d21",AV_URL:window.location.hostname,cb:Date.now(),AV_WIDTH:640,AV_HEIGHT:480,AV_CDIM2:e.publisherId};return"".concat("https://go.aniview.com/api/adserver6/vast/","?").concat(Rh(t))}}]),e}(),cf=ct.findIndex,uf=!0,hf=Qt("findIndex");"findIndex"in[]&&Array(1).findIndex((function(){uf=!1})),Ie({target:"Array",proto:!0,forced:uf||!hf},{findIndex:function(e){return cf(this,e,arguments.length>1?arguments[1]:void 0)}}),dn("findIndex");var ff=Math.min,df=[].lastIndexOf,pf=!!df&&1/[1].lastIndexOf(1,-0)<0,mf=$t("lastIndexOf"),gf=Qt("indexOf",{ACCESSORS:!0,1:0}),vf=pf||!mf||!gf?function(e){if(pf)return df.apply(this,arguments)||0;var t=m(this),n=le(t.length),i=n-1;for(arguments.length>1&&(i=ff(i,oe(arguments[1]))),i<0&&(i=n+i);i>=0;i--)if(i in t&&t[i]===e)return i||0;return-1}:df;Ie({target:"Array",proto:!0,forced:vf!==[].lastIndexOf},{lastIndexOf:vf});var yf=function(e,t){var n={};return e>t.width/t.height?(n.width=t.width,n.height=1/e*t.width):(n.height=t.height,n.width=e*t.height),n},bf=function(){function e(t){yo(this,e),this.player=t,this.thumbnails=[],this.loaded=!1,this.lastMouseMoveTime=Date.now(),this.mouseDown=!1,this.loadedImages=[],this.elements={thumb:{},scrubbing:{}},this.load()}return wo(e,[{key:"load",value:function(){var e=this;this.player.elements.display.seekTooltip&&(this.player.elements.display.seekTooltip.hidden=this.enabled),this.enabled&&this.getThumbnails().then((function(){e.enabled&&(e.render(),e.determineContainerAutoSizing(),e.loaded=!0)}))}},{key:"getThumbnails",value:function(){var e=this;return new Promise((function(t){var n=e.player.config.previewThumbnails.src;if(du(n))throw new Error("Missing previewThumbnails.src config attribute");var i=function(){e.thumbnails.sort((function(e,t){return e.height-t.height})),e.player.debug.log("Preview thumbnails",e.thumbnails),t()};if(ru(n))n((function(t){e.thumbnails=t,i()}));else{var r=(nu(n)?[n]:n).map((function(t){return e.getThumbnail(t)}));Promise.all(r).then(i)}}))}},{key:"getThumbnail",value:function(e){var t=this;return new Promise((function(n){Eh(e).then((function(i){var r,a,o={frames:(r=i,a=[],r.split(/\r\n\r\n|\n\n|\r\r/).forEach((function(e){var t={};e.split(/\r\n|\n|\r/).forEach((function(e){if(tu(t.startTime)){if(!du(e.trim())&&du(t.text)){var n=e.trim().split("#xywh="),i=Ao(n,1);if(t.text=i[0],n[1]){var r=Ao(n[1].split(","),4);t.x=r[0],t.y=r[1],t.w=r[2],t.h=r[3]}}}else{var a=e.match(/([0-9]{2})?:?([0-9]{2}):([0-9]{2}).([0-9]{2,3})( ?--> ?)([0-9]{2})?:?([0-9]{2}):([0-9]{2}).([0-9]{2,3})/);a&&(t.startTime=60*Number(a[1]||0)*60+60*Number(a[2])+Number(a[3])+Number("0.".concat(a[4])),t.endTime=60*Number(a[6]||0)*60+60*Number(a[7])+Number(a[8])+Number("0.".concat(a[9])))}})),t.text&&a.push(t)})),a),height:null,urlPrefix:""};o.frames[0].text.startsWith("/")||o.frames[0].text.startsWith("http://")||o.frames[0].text.startsWith("https://")||(o.urlPrefix=e.substring(0,e.lastIndexOf("/")+1));var s=new Image;s.onload=function(){o.height=s.naturalHeight,o.width=s.naturalWidth,t.thumbnails.push(o),n()},s.src=o.urlPrefix+o.frames[0].text}))}))}},{key:"startMove",value:function(e){if(this.loaded&&lu(e)&&["touchmove","mousemove"].includes(e.type)&&this.player.media.duration){if("touchmove"===e.type)this.seekTime=this.player.media.duration*(this.player.elements.inputs.seek.value/100);else{var t=this.player.elements.progress.getBoundingClientRect(),n=100/t.width*(e.pageX-t.left);this.seekTime=this.player.media.duration*(n/100),this.seekTime<0&&(this.seekTime=0),this.seekTime>this.player.media.duration-1&&(this.seekTime=this.player.media.duration-1),this.mousePosX=e.pageX,this.elements.thumb.time.innerText=Lh(this.seekTime)}this.showImageAtCurrentTime()}}},{key:"endMove",value:function(){this.toggleThumbContainer(!1,!0)}},{key:"startScrubbing",value:function(e){(Zc(e.button)||!1===e.button||0===e.button)&&(this.mouseDown=!0,this.player.media.duration&&(this.toggleScrubbingContainer(!0),this.toggleThumbContainer(!1,!0),this.showImageAtCurrentTime()))}},{key:"endScrubbing",value:function(){var e=this;this.mouseDown=!1,Math.ceil(this.lastTime)===Math.ceil(this.player.media.currentTime)?this.toggleScrubbingContainer(!1):Wu.call(this.player,this.player.media,"timeupdate",(function(){e.mouseDown||e.toggleScrubbingContainer(!1)}))}},{key:"listeners",value:function(){var e=this;this.player.on("play",(function(){e.toggleThumbContainer(!1,!0)})),this.player.on("seeked",(function(){e.toggleThumbContainer(!1)})),this.player.on("timeupdate",(function(){e.lastTime=e.player.media.currentTime}))}},{key:"render",value:function(){this.elements.thumb.container=Au("div",{class:this.player.config.classNames.previewThumbnails.thumbContainer}),this.elements.thumb.imageContainer=Au("div",{class:this.player.config.classNames.previewThumbnails.imageContainer}),this.elements.thumb.container.appendChild(this.elements.thumb.imageContainer);var e=Au("div",{class:this.player.config.classNames.previewThumbnails.timeContainer});this.elements.thumb.time=Au("span",{},"00:00"),e.appendChild(this.elements.thumb.time),this.elements.thumb.container.appendChild(e),su(this.player.elements.progress)&&this.player.elements.progress.appendChild(this.elements.thumb.container),this.elements.scrubbing.container=Au("div",{class:this.player.config.classNames.previewThumbnails.scrubbingContainer}),this.player.elements.wrapper.appendChild(this.elements.scrubbing.container)}},{key:"destroy",value:function(){this.elements.thumb.container&&this.elements.thumb.container.remove(),this.elements.scrubbing.container&&this.elements.scrubbing.container.remove()}},{key:"showImageAtCurrentTime",value:function(){var e=this;this.mouseDown?this.setScrubbingContainerSize():this.setThumbContainerSizeAndPos();var t=this.thumbnails[0].frames.findIndex((function(t){return e.seekTime>=t.startTime&&e.seekTime<=t.endTime})),n=t>=0,i=0;this.mouseDown||this.toggleThumbContainer(n),n&&(this.thumbnails.forEach((function(n,r){e.loadedImages.includes(n.frames[t].text)&&(i=r)})),t!==this.showingThumb&&(this.showingThumb=t,this.loadImage(i)))}},{key:"loadImage",value:function(){var e=this,t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0,n=this.showingThumb,i=this.thumbnails[t],r=i.urlPrefix,a=i.frames[n],o=i.frames[n].text,s=r+o;if(this.currentImageElement&&this.currentImageElement.dataset.filename===o)this.showImage(this.currentImageElement,a,t,n,o,!1),this.currentImageElement.dataset.index=n,this.removeOldImages(this.currentImageElement);else{this.loadingImage&&this.usingSprites&&(this.loadingImage.onload=null);var l=new Image;l.src=s,l.dataset.index=n,l.dataset.filename=o,this.showingThumbFilename=o,this.player.debug.log("Loading image: ".concat(s)),l.onload=function(){return e.showImage(l,a,t,n,o,!0)},this.loadingImage=l,this.removeOldImages(l)}}},{key:"showImage",value:function(e,t,n,i,r){var a=!(arguments.length>5&&void 0!==arguments[5])||arguments[5];this.player.debug.log("Showing thumb: ".concat(r,". num: ").concat(i,". qual: ").concat(n,". newimg: ").concat(a)),this.setImageSizeAndOffset(e,t),a&&(this.currentImageContainer.appendChild(e),this.currentImageElement=e,this.loadedImages.includes(r)||this.loadedImages.push(r)),this.preloadNearby(i,!0).then(this.preloadNearby(i,!1)).then(this.getHigherQuality(n,e,t,r))}},{key:"removeOldImages",value:function(e){var t=this;Array.from(this.currentImageContainer.children).forEach((function(n){if("img"===n.tagName.toLowerCase()){var i=t.usingSprites?500:1e3;if(n.dataset.index!==e.dataset.index&&!n.dataset.deleting){n.dataset.deleting=!0;var r=t.currentImageContainer;setTimeout((function(){r.removeChild(n),t.player.debug.log("Removing thumb: ".concat(n.dataset.filename))}),i)}}}))}},{key:"preloadNearby",value:function(e){var t=this,n=!(arguments.length>1&&void 0!==arguments[1])||arguments[1];return new Promise((function(i){setTimeout((function(){var r=t.thumbnails[0].frames[e].text;if(t.showingThumbFilename===r){var a;a=n?t.thumbnails[0].frames.slice(e):t.thumbnails[0].frames.slice(0,e).reverse();var o=!1;a.forEach((function(e){var n=e.text;if(n!==r&&!t.loadedImages.includes(n)){o=!0,t.player.debug.log("Preloading thumb filename: ".concat(n));var a=t.thumbnails[0].urlPrefix+n,s=new Image;s.src=a,s.onload=function(){t.player.debug.log("Preloaded thumb filename: ".concat(n)),t.loadedImages.includes(n)||t.loadedImages.push(n),i()}}})),o||i()}}),300)}))}},{key:"getHigherQuality",value:function(e,t,n,i){var r=this;if(e<this.thumbnails.length-1){var a=t.naturalHeight;this.usingSprites&&(a=n.h),a<this.thumbContainerHeight&&setTimeout((function(){r.showingThumbFilename===i&&(r.player.debug.log("Showing higher quality thumb for: ".concat(i)),r.loadImage(e+1))}),300)}}},{key:"toggleThumbContainer",value:function(){var e=arguments.length>0&&void 0!==arguments[0]&&arguments[0],t=arguments.length>1&&void 0!==arguments[1]&&arguments[1],n=this.player.config.classNames.previewThumbnails.thumbContainerShown;this.elements.thumb.container.classList.toggle(n,e),!e&&t&&(this.showingThumb=null,this.showingThumbFilename=null)}},{key:"toggleScrubbingContainer",value:function(){var e=arguments.length>0&&void 0!==arguments[0]&&arguments[0],t=this.player.config.classNames.previewThumbnails.scrubbingContainerShown;this.elements.scrubbing.container.classList.toggle(t,e),e||(this.showingThumb=null,this.showingThumbFilename=null)}},{key:"determineContainerAutoSizing",value:function(){(this.elements.thumb.imageContainer.clientHeight>20||this.elements.thumb.imageContainer.clientWidth>20)&&(this.sizeSpecifiedInCSS=!0)}},{key:"setThumbContainerSizeAndPos",value:function(){if(this.sizeSpecifiedInCSS){if(this.elements.thumb.imageContainer.clientHeight>20&&this.elements.thumb.imageContainer.clientWidth<20){var e=Math.floor(this.elements.thumb.imageContainer.clientHeight*this.thumbAspectRatio);this.elements.thumb.imageContainer.style.width="".concat(e,"px")}else if(this.elements.thumb.imageContainer.clientHeight<20&&this.elements.thumb.imageContainer.clientWidth>20){var t=Math.floor(this.elements.thumb.imageContainer.clientWidth/this.thumbAspectRatio);this.elements.thumb.imageContainer.style.height="".concat(t,"px")}}else{var n=Math.floor(this.thumbContainerHeight*this.thumbAspectRatio);this.elements.thumb.imageContainer.style.height="".concat(this.thumbContainerHeight,"px"),this.elements.thumb.imageContainer.style.width="".concat(n,"px")}this.setThumbContainerPos()}},{key:"setThumbContainerPos",value:function(){var e=this.player.elements.progress.getBoundingClientRect(),t=this.player.elements.container.getBoundingClientRect(),n=this.elements.thumb.container,i=t.left-e.left+10,r=t.right-e.left-n.clientWidth-10,a=this.mousePosX-e.left-n.clientWidth/2;a<i&&(a=i),a>r&&(a=r),n.style.left="".concat(a,"px")}},{key:"setScrubbingContainerSize",value:function(){var e=yf(this.thumbAspectRatio,{width:this.player.media.clientWidth,height:this.player.media.clientHeight}),t=e.width,n=e.height;this.elements.scrubbing.container.style.width="".concat(t,"px"),this.elements.scrubbing.container.style.height="".concat(n,"px")}},{key:"setImageSizeAndOffset",value:function(e,t){if(this.usingSprites){var n=this.thumbContainerHeight/t.h;e.style.height="".concat(e.naturalHeight*n,"px"),e.style.width="".concat(e.naturalWidth*n,"px"),e.style.left="-".concat(t.x*n,"px"),e.style.top="-".concat(t.y*n,"px")}}},{key:"enabled",get:function(){return this.player.isHTML5&&this.player.isVideo&&this.player.config.previewThumbnails.enabled}},{key:"currentImageContainer",get:function(){return this.mouseDown?this.elements.scrubbing.container:this.elements.thumb.imageContainer}},{key:"usingSprites",get:function(){return Object.keys(this.thumbnails[0].frames[0]).includes("w")}},{key:"thumbAspectRatio",get:function(){return this.usingSprites?this.thumbnails[0].frames[0].w/this.thumbnails[0].frames[0].h:this.thumbnails[0].width/this.thumbnails[0].height}},{key:"thumbContainerHeight",get:function(){return this.mouseDown?yf(this.thumbAspectRatio,{width:this.player.media.clientWidth,height:this.player.media.clientHeight}).height:this.sizeSpecifiedInCSS?this.elements.thumb.imageContainer.clientHeight:Math.floor(this.player.media.clientWidth/this.thumbAspectRatio/4)}},{key:"currentImageElement",get:function(){return this.mouseDown?this.currentScrubbingImageElement:this.currentThumbnailImageElement},set:function(e){this.mouseDown?this.currentScrubbingImageElement=e:this.currentThumbnailImageElement=e}}]),e}(),wf={insertElements:function(e,t){var n=this;nu(t)?Pu(e,this.media,{src:t}):au(t)&&t.forEach((function(t){Pu(e,n.media,t)}))},change:function(e){var t=this;ku(e,"sources.length")?(eh.cancelRequests.call(this),this.destroy.call(this,(function(){t.options.quality=[],xu(t.media),t.media=null,su(t.elements.container)&&t.elements.container.removeAttribute("class");var n=e.sources,i=e.type,r=Ao(n,1)[0],a=r.provider,o=void 0===a?Fh.html5:a,s=r.src,l="html5"===o?i:"div",c="html5"===o?{}:{src:s};Object.assign(t,{provider:o,type:i,supported:qu.check(i,o,t.config.playsinline),media:Au(l,c)}),t.elements.container.appendChild(t.media),iu(e.autoplay)&&(t.config.autoplay=e.autoplay),t.isHTML5&&(t.config.crossorigin&&t.media.setAttribute("crossorigin",""),t.config.autoplay&&t.media.setAttribute("autoplay",""),du(e.poster)||(t.poster=e.poster),t.config.loop.active&&t.media.setAttribute("loop",""),t.config.muted&&t.media.setAttribute("muted",""),t.config.playsinline&&t.media.setAttribute("playsinline","")),$h.addStyleHook.call(t),t.isHTML5&&wf.insertElements.call(t,"source",n),t.config.title=e.title,sf.setup.call(t),t.isHTML5&&Object.keys(e).includes("tracks")&&wf.insertElements.call(t,"track",e.tracks),(t.isHTML5||t.isEmbed&&!t.supported.ui)&&$h.build.call(t),t.isHTML5&&t.media.load(),du(e.previewThumbnails)||(Object.assign(t.config.previewThumbnails,e.previewThumbnails),t.previewThumbnails&&t.previewThumbnails.loaded&&(t.previewThumbnails.destroy(),t.previewThumbnails=null),t.config.previewThumbnails.enabled&&(t.previewThumbnails=new bf(t))),t.fullscreen.update()}),!0)):this.debug.warn("Invalid source format")}};var kf,Tf=function(){function e(t,n){var i=this;if(yo(this,e),this.timers={},this.ready=!1,this.loading=!1,this.failed=!1,this.touch=qu.touch,this.media=t,nu(this.media)&&(this.media=document.querySelectorAll(this.media)),(window.jQuery&&this.media instanceof jQuery||ou(this.media)||au(this.media))&&(this.media=this.media[0]),this.config=Tu({},_h,e.defaults,n||{},function(){try{return JSON.parse(i.media.getAttribute("data-plyr-config"))}catch(e){return{}}}()),this.elements={container:null,fullscreen:null,captions:null,buttons:{},display:{},progress:{},inputs:{},settings:{popup:null,menu:null,panels:{},buttons:{}}},this.captions={active:null,currentTrack:-1,meta:new WeakMap},this.fullscreen={active:!1},this.options={speed:[],quality:[]},this.debug=new Vh(this.config.debug),this.debug.log("Config",this.config),this.debug.log("Support",qu),!Zc(this.media)&&su(this.media))if(this.media.plyr)this.debug.warn("Target already setup");else if(this.config.enabled)if(qu.check().api){var r=this.media.cloneNode(!0);r.autoplay=!1,this.elements.original=r;var a=this.media.tagName.toLowerCase(),o=null,s=null;switch(a){case"div":if(o=this.media.querySelector("iframe"),su(o)){if(s=Nh(o.getAttribute("src")),this.provider=function(e){return/^(https?:\/\/)?(www\.)?(youtube\.com|youtube-nocookie\.com|youtu\.?be)\/.+$/.test(e)?Fh.youtube:/^https?:\/\/player.vimeo.com\/video\/\d{0,9}(?=\b|\/)/.test(e)?Fh.vimeo:null}(s.toString()),this.elements.container=this.media,this.media=o,this.elements.container.className="",s.search.length){var l=["1","true"];l.includes(s.searchParams.get("autoplay"))&&(this.config.autoplay=!0),l.includes(s.searchParams.get("loop"))&&(this.config.loop.active=!0),this.isYouTube?(this.config.playsinline=l.includes(s.searchParams.get("playsinline")),this.config.youtube.hl=s.searchParams.get("hl")):this.config.playsinline=!0}}else this.provider=this.media.getAttribute(this.config.attributes.embed.provider),this.media.removeAttribute(this.config.attributes.embed.provider);if(du(this.provider)||!Object.keys(Fh).includes(this.provider))return void this.debug.error("Setup failed: Invalid provider");this.type=Hh;break;case"video":case"audio":this.type=a,this.provider=Fh.html5,this.media.hasAttribute("crossorigin")&&(this.config.crossorigin=!0),this.media.hasAttribute("autoplay")&&(this.config.autoplay=!0),(this.media.hasAttribute("playsinline")||this.media.hasAttribute("webkit-playsinline"))&&(this.config.playsinline=!0),this.media.hasAttribute("muted")&&(this.config.muted=!0),this.media.hasAttribute("loop")&&(this.config.loop.active=!0);break;default:return void this.debug.error("Setup failed: unsupported type")}this.supported=qu.check(this.type,this.provider,this.config.playsinline),this.supported.api?(this.eventListeners=[],this.listeners=new Yh(this),this.storage=new Sh(this),this.media.plyr=this,su(this.elements.container)||(this.elements.container=Au("div",{tabindex:0}),Su(this.media,this.elements.container)),$h.migrateStyles.call(this),$h.addStyleHook.call(this),sf.setup.call(this),this.config.debug&&Vu.call(this,this.elements.container,this.config.events.join(" "),(function(e){i.debug.log("event: ".concat(e.type))})),this.fullscreen=new zh(this),(this.isHTML5||this.isEmbed&&!this.supported.ui)&&$h.build.call(this),this.listeners.container(),this.listeners.global(),this.config.ads.enabled&&(this.ads=new lf(this)),this.isHTML5&&this.config.autoplay&&setTimeout((function(){return Gu(i.play())}),10),this.lastSeekTime=0,this.config.previewThumbnails.enabled&&(this.previewThumbnails=new bf(this))):this.debug.error("Setup failed: no support")}else this.debug.error("Setup failed: no support");else this.debug.error("Setup failed: disabled by config");else this.debug.error("Setup failed: no suitable element passed")}return wo(e,[{key:"play",value:function(){var e=this;return ru(this.media.play)?(this.ads&&this.ads.enabled&&this.ads.managerPromise.then((function(){return e.ads.play()})).catch((function(){return Gu(e.media.play())})),this.media.play()):null}},{key:"pause",value:function(){return this.playing&&ru(this.media.pause)?this.media.pause():null}},{key:"togglePlay",value:function(e){return(iu(e)?e:!this.playing)?this.play():this.pause()}},{key:"stop",value:function(){this.isHTML5?(this.pause(),this.restart()):ru(this.media.stop)&&this.media.stop()}},{key:"restart",value:function(){this.currentTime=0}},{key:"rewind",value:function(e){this.currentTime-=tu(e)?e:this.config.seekTime}},{key:"forward",value:function(e){this.currentTime+=tu(e)?e:this.config.seekTime}},{key:"increaseVolume",value:function(e){var t=this.media.muted?0:this.volume;this.volume=t+(tu(e)?e:0)}},{key:"decreaseVolume",value:function(e){this.increaseVolume(-e)}},{key:"toggleCaptions",value:function(e){Mh.toggle.call(this,e,!1)}},{key:"airplay",value:function(){qu.airplay&&this.media.webkitShowPlaybackTargetPicker()}},{key:"toggleControls",value:function(e){if(this.supported.ui&&!this.isAudio){var t=Nu(this.elements.container,this.config.classNames.hideControls),n=void 0===e?void 0:!e,i=ju(this.elements.container,this.config.classNames.hideControls,n);if(i&&au(this.config.controls)&&this.config.controls.includes("settings")&&!du(this.config.settings)&&jh.toggleMenu.call(this,!1),i!==t){var r=i?"controlshidden":"controlsshown";Ku.call(this,this.media,r)}return!i}return!1}},{key:"on",value:function(e,t){Vu.call(this,this.elements.container,e,t)}},{key:"once",value:function(e,t){Wu.call(this,this.elements.container,e,t)}},{key:"off",value:function(e,t){zu(this.elements.container,e,t)}},{key:"destroy",value:function(e){var t=this,n=arguments.length>1&&void 0!==arguments[1]&&arguments[1];if(this.ready){var i=function(){document.body.style.overflow="",t.embed=null,n?(Object.keys(t.elements).length&&(xu(t.elements.buttons.play),xu(t.elements.captions),xu(t.elements.controls),xu(t.elements.wrapper),t.elements.buttons.play=null,t.elements.captions=null,t.elements.controls=null,t.elements.wrapper=null),ru(e)&&e()):($u.call(t),Ou(t.elements.original,t.elements.container),Ku.call(t,t.elements.original,"destroyed",!0),ru(e)&&e.call(t.elements.original),t.ready=!1,setTimeout((function(){t.elements=null,t.media=null}),200))};this.stop(),clearTimeout(this.timers.loading),clearTimeout(this.timers.controls),clearTimeout(this.timers.resized),this.isHTML5?($h.toggleNativeControls.call(this,!0),i()):this.isYouTube?(clearInterval(this.timers.buffering),clearInterval(this.timers.playing),null!==this.embed&&ru(this.embed.destroy)&&this.embed.destroy(),i()):this.isVimeo&&(null!==this.embed&&this.embed.unload().then(i),setTimeout(i,200))}}},{key:"supports",value:function(e){return qu.mime.call(this,e)}},{key:"isHTML5",get:function(){return this.provider===Fh.html5}},{key:"isEmbed",get:function(){return this.isYouTube||this.isVimeo}},{key:"isYouTube",get:function(){return this.provider===Fh.youtube}},{key:"isVimeo",get:function(){return this.provider===Fh.vimeo}},{key:"isVideo",get:function(){return this.type===Hh}},{key:"isAudio",get:function(){return this.type===qh}},{key:"playing",get:function(){return Boolean(this.ready&&!this.paused&&!this.ended)}},{key:"paused",get:function(){return Boolean(this.media.paused)}},{key:"stopped",get:function(){return Boolean(this.paused&&0===this.currentTime)}},{key:"ended",get:function(){return Boolean(this.media.ended)}},{key:"currentTime",set:function(e){if(this.duration){var t=tu(e)&&e>0;this.media.currentTime=t?Math.min(e,this.duration):0,this.debug.log("Seeking to ".concat(this.currentTime," seconds"))}},get:function(){return Number(this.media.currentTime)}},{key:"buffered",get:function(){var e=this.media.buffered;return tu(e)?e:e&&e.length&&this.duration>0?e.end(0)/this.duration:0}},{key:"seeking",get:function(){return Boolean(this.media.seeking)}},{key:"duration",get:function(){var e=parseFloat(this.config.duration),t=(this.media||{}).duration,n=tu(t)&&t!==1/0?t:0;return e||n}},{key:"volume",set:function(e){var t=e;nu(t)&&(t=Number(t)),tu(t)||(t=this.storage.get("volume")),tu(t)||(t=this.config.volume),t>1&&(t=1),t<0&&(t=0),this.config.volume=t,this.media.volume=t,!du(e)&&this.muted&&t>0&&(this.muted=!1)},get:function(){return Number(this.media.volume)}},{key:"muted",set:function(e){var t=e;iu(t)||(t=this.storage.get("muted")),iu(t)||(t=this.config.muted),this.config.muted=t,this.media.muted=t},get:function(){return Boolean(this.media.muted)}},{key:"hasAudio",get:function(){return!this.isHTML5||(!!this.isAudio||(Boolean(this.media.mozHasAudio)||Boolean(this.media.webkitAudioDecodedByteCount)||Boolean(this.media.audioTracks&&this.media.audioTracks.length)))}},{key:"speed",set:function(e){var t=this,n=null;tu(e)&&(n=e),tu(n)||(n=this.storage.get("speed")),tu(n)||(n=this.config.speed.selected);var i=this.minimumSpeed,r=this.maximumSpeed;n=function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0,t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,n=arguments.length>2&&void 0!==arguments[2]?arguments[2]:255;return Math.min(Math.max(e,t),n)}(n,i,r),this.config.speed.selected=n,setTimeout((function(){t.media.playbackRate=n}),0)},get:function(){return Number(this.media.playbackRate)}},{key:"minimumSpeed",get:function(){return this.isYouTube?Math.min.apply(Math,Po(this.options.speed)):this.isVimeo?.5:.0625}},{key:"maximumSpeed",get:function(){return this.isYouTube?Math.max.apply(Math,Po(this.options.speed)):this.isVimeo?2:16}},{key:"quality",set:function(e){var t=this.config.quality,n=this.options.quality;if(n.length){var i=[!du(e)&&Number(e),this.storage.get("quality"),t.selected,t.default].find(tu),r=!0;if(!n.includes(i)){var a=function(e,t){return au(e)&&e.length?e.reduce((function(e,n){return Math.abs(n-t)<Math.abs(e-t)?n:e})):null}(n,i);this.debug.warn("Unsupported quality option: ".concat(i,", using ").concat(a," instead")),i=a,r=!1}t.selected=i,this.media.quality=i,r&&this.storage.set({quality:i})}},get:function(){return this.media.quality}},{key:"loop",set:function(e){var t=iu(e)?e:this.config.loop.active;this.config.loop.active=t,this.media.loop=t},get:function(){return Boolean(this.media.loop)}},{key:"source",set:function(e){wf.change.call(this,e)},get:function(){return this.media.currentSrc}},{key:"download",get:function(){var e=this.config.urls.download;return fu(e)?e:this.source},set:function(e){fu(e)&&(this.config.urls.download=e,jh.setDownloadUrl.call(this))}},{key:"poster",set:function(e){this.isVideo?$h.setPoster.call(this,e,!1).catch((function(){})):this.debug.warn("Poster can only be set for video")},get:function(){return this.isVideo?this.media.getAttribute("poster")||this.media.getAttribute("data-poster"):null}},{key:"ratio",get:function(){if(!this.isVideo)return null;var e=Qu(Ju.call(this));return au(e)?e.join(":"):e},set:function(e){this.isVideo?nu(e)&&Xu(e)?(this.config.ratio=e,Zu.call(this)):this.debug.error("Invalid aspect ratio specified (".concat(e,")")):this.debug.warn("Aspect ratio can only be set for video")}},{key:"autoplay",set:function(e){var t=iu(e)?e:this.config.autoplay;this.config.autoplay=t},get:function(){return Boolean(this.config.autoplay)}},{key:"currentTrack",set:function(e){Mh.set.call(this,e,!1)},get:function(){var e=this.captions,t=e.toggled,n=e.currentTrack;return t?n:-1}},{key:"language",set:function(e){Mh.setLanguage.call(this,e,!1)},get:function(){return(Mh.getCurrentTrack.call(this)||{}).language}},{key:"pip",set:function(e){if(qu.pip){var t=iu(e)?e:!this.pip;ru(this.media.webkitSetPresentationMode)&&this.media.webkitSetPresentationMode(t?Uh:Dh),ru(this.media.requestPictureInPicture)&&(!this.pip&&t?this.media.requestPictureInPicture():this.pip&&!t&&document.exitPictureInPicture())}},get:function(){return qu.pip?du(this.media.webkitPresentationMode)?this.media===document.pictureInPictureElement:this.media.webkitPresentationMode===Uh:null}}],[{key:"supported",value:function(e,t,n){return qu.check(e,t,n)}},{key:"loadSprite",value:function(e,t){return Ah(e,t)}},{key:"setup",value:function(t){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},i=null;return nu(t)?i=Array.from(document.querySelectorAll(t)):ou(t)?i=Array.from(t):au(t)&&(i=t.filter(su)),du(i)?null:i.map((function(t){return new e(t,n)}))}}]),e}();return Tf.defaults=(kf=_h,JSON.parse(JSON.stringify(kf))),Tf}));
//# sourceMappingURL=plyr.polyfilled.min.js.map

/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.9.0
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
/* global window, document, define, jQuery, setInterval, clearInterval */
;(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {
    'use strict';
    var Slick = window.Slick || {};

    Slick = (function() {

        var instanceUid = 0;

        function Slick(element, settings) {

            var _ = this, dataSettings;

            _.defaults = {
                accessibility: true,
                adaptiveHeight: false,
                appendArrows: $(element),
                appendDots: $(element),
                arrows: true,
                asNavFor: null,
                prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
                nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
                autoplay: false,
                autoplaySpeed: 3000,
                centerMode: false,
                centerPadding: '50px',
                cssEase: 'ease',
                customPaging: function(slider, i) {
                    return $('<button type="button" />').text(i + 1);
                },
                dots: false,
                dotsClass: 'slick-dots',
                draggable: true,
                easing: 'linear',
                edgeFriction: 0.35,
                fade: false,
                focusOnSelect: false,
                focusOnChange: false,
                infinite: true,
                initialSlide: 0,
                lazyLoad: 'ondemand',
                mobileFirst: false,
                pauseOnHover: true,
                pauseOnFocus: true,
                pauseOnDotsHover: false,
                respondTo: 'window',
                responsive: null,
                rows: 1,
                rtl: false,
                slide: '',
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: true,
                swipeToSlide: false,
                touchMove: true,
                touchThreshold: 5,
                useCSS: true,
                useTransform: true,
                variableWidth: false,
                vertical: false,
                verticalSwiping: false,
                waitForAnimate: true,
                zIndex: 1000
            };

            _.initials = {
                animating: false,
                dragging: false,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                scrolling: false,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: false,
                slideOffset: 0,
                swipeLeft: null,
                swiping: false,
                $list: null,
                touchObject: {},
                transformsEnabled: false,
                unslicked: false
            };

            $.extend(_, _.initials);

            _.activeBreakpoint = null;
            _.animType = null;
            _.animProp = null;
            _.breakpoints = [];
            _.breakpointSettings = [];
            _.cssTransitions = false;
            _.focussed = false;
            _.interrupted = false;
            _.hidden = 'hidden';
            _.paused = true;
            _.positionProp = null;
            _.respondTo = null;
            _.rowCount = 1;
            _.shouldClick = true;
            _.$slider = $(element);
            _.$slidesCache = null;
            _.transformType = null;
            _.transitionType = null;
            _.visibilityChange = 'visibilitychange';
            _.windowWidth = 0;
            _.windowTimer = null;

            dataSettings = $(element).data('slick') || {};

            _.options = $.extend({}, _.defaults, settings, dataSettings);

            _.currentSlide = _.options.initialSlide;

            _.originalSettings = _.options;

            if (typeof document.mozHidden !== 'undefined') {
                _.hidden = 'mozHidden';
                _.visibilityChange = 'mozvisibilitychange';
            } else if (typeof document.webkitHidden !== 'undefined') {
                _.hidden = 'webkitHidden';
                _.visibilityChange = 'webkitvisibilitychange';
            }

            _.autoPlay = $.proxy(_.autoPlay, _);
            _.autoPlayClear = $.proxy(_.autoPlayClear, _);
            _.autoPlayIterator = $.proxy(_.autoPlayIterator, _);
            _.changeSlide = $.proxy(_.changeSlide, _);
            _.clickHandler = $.proxy(_.clickHandler, _);
            _.selectHandler = $.proxy(_.selectHandler, _);
            _.setPosition = $.proxy(_.setPosition, _);
            _.swipeHandler = $.proxy(_.swipeHandler, _);
            _.dragHandler = $.proxy(_.dragHandler, _);
            _.keyHandler = $.proxy(_.keyHandler, _);

            _.instanceUid = instanceUid++;

            // A simple way to check for HTML strings
            // Strict HTML recognition (must start with <)
            // Extracted from jQuery v1.11 source
            _.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;


            _.registerBreakpoints();
            _.init(true);

        }

        return Slick;

    }());

    Slick.prototype.activateADA = function() {
        var _ = this;

        _.$slideTrack.find('.slick-active').attr({
            'aria-hidden': 'false'
        }).find('a, input, button, select').attr({
            'tabindex': '0'
        });

    };

    Slick.prototype.addSlide = Slick.prototype.slickAdd = function(markup, index, addBefore) {

        var _ = this;

        if (typeof(index) === 'boolean') {
            addBefore = index;
            index = null;
        } else if (index < 0 || (index >= _.slideCount)) {
            return false;
        }

        _.unload();

        if (typeof(index) === 'number') {
            if (index === 0 && _.$slides.length === 0) {
                $(markup).appendTo(_.$slideTrack);
            } else if (addBefore) {
                $(markup).insertBefore(_.$slides.eq(index));
            } else {
                $(markup).insertAfter(_.$slides.eq(index));
            }
        } else {
            if (addBefore === true) {
                $(markup).prependTo(_.$slideTrack);
            } else {
                $(markup).appendTo(_.$slideTrack);
            }
        }

        _.$slides = _.$slideTrack.children(this.options.slide);

        _.$slideTrack.children(this.options.slide).detach();

        _.$slideTrack.append(_.$slides);

        _.$slides.each(function(index, element) {
            $(element).attr('data-slick-index', index);
        });

        _.$slidesCache = _.$slides;

        _.reinit();

    };

    Slick.prototype.animateHeight = function() {
        var _ = this;
        if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
            var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
            _.$list.animate({
                height: targetHeight
            }, _.options.speed);
        }
    };

    Slick.prototype.animateSlide = function(targetLeft, callback) {

        var animProps = {},
            _ = this;

        _.animateHeight();

        if (_.options.rtl === true && _.options.vertical === false) {
            targetLeft = -targetLeft;
        }
        if (_.transformsEnabled === false) {
            if (_.options.vertical === false) {
                _.$slideTrack.animate({
                    left: targetLeft
                }, _.options.speed, _.options.easing, callback);
            } else {
                _.$slideTrack.animate({
                    top: targetLeft
                }, _.options.speed, _.options.easing, callback);
            }

        } else {

            if (_.cssTransitions === false) {
                if (_.options.rtl === true) {
                    _.currentLeft = -(_.currentLeft);
                }
                $({
                    animStart: _.currentLeft
                }).animate({
                    animStart: targetLeft
                }, {
                    duration: _.options.speed,
                    easing: _.options.easing,
                    step: function(now) {
                        now = Math.ceil(now);
                        if (_.options.vertical === false) {
                            animProps[_.animType] = 'translate(' +
                                now + 'px, 0px)';
                            _.$slideTrack.css(animProps);
                        } else {
                            animProps[_.animType] = 'translate(0px,' +
                                now + 'px)';
                            _.$slideTrack.css(animProps);
                        }
                    },
                    complete: function() {
                        if (callback) {
                            callback.call();
                        }
                    }
                });

            } else {

                _.applyTransition();
                targetLeft = Math.ceil(targetLeft);

                if (_.options.vertical === false) {
                    animProps[_.animType] = 'translate3d(' + targetLeft + 'px, 0px, 0px)';
                } else {
                    animProps[_.animType] = 'translate3d(0px,' + targetLeft + 'px, 0px)';
                }
                _.$slideTrack.css(animProps);

                if (callback) {
                    setTimeout(function() {

                        _.disableTransition();

                        callback.call();
                    }, _.options.speed);
                }

            }

        }

    };

    Slick.prototype.getNavTarget = function() {

        var _ = this,
            asNavFor = _.options.asNavFor;

        if ( asNavFor && asNavFor !== null ) {
            asNavFor = $(asNavFor).not(_.$slider);
        }

        return asNavFor;

    };

    Slick.prototype.asNavFor = function(index) {

        var _ = this,
            asNavFor = _.getNavTarget();

        if ( asNavFor !== null && typeof asNavFor === 'object' ) {
            asNavFor.each(function() {
                var target = $(this).slick('getSlick');
                if(!target.unslicked) {
                    target.slideHandler(index, true);
                }
            });
        }

    };

    Slick.prototype.applyTransition = function(slide) {

        var _ = this,
            transition = {};

        if (_.options.fade === false) {
            transition[_.transitionType] = _.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
        } else {
            transition[_.transitionType] = 'opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
        }

        if (_.options.fade === false) {
            _.$slideTrack.css(transition);
        } else {
            _.$slides.eq(slide).css(transition);
        }

    };

    Slick.prototype.autoPlay = function() {

        var _ = this;

        _.autoPlayClear();

        if ( _.slideCount > _.options.slidesToShow ) {
            _.autoPlayTimer = setInterval( _.autoPlayIterator, _.options.autoplaySpeed );
        }

    };

    Slick.prototype.autoPlayClear = function() {

        var _ = this;

        if (_.autoPlayTimer) {
            clearInterval(_.autoPlayTimer);
        }

    };

    Slick.prototype.autoPlayIterator = function() {

        var _ = this,
            slideTo = _.currentSlide + _.options.slidesToScroll;

        if ( !_.paused && !_.interrupted && !_.focussed ) {

            if ( _.options.infinite === false ) {

                if ( _.direction === 1 && ( _.currentSlide + 1 ) === ( _.slideCount - 1 )) {
                    _.direction = 0;
                }

                else if ( _.direction === 0 ) {

                    slideTo = _.currentSlide - _.options.slidesToScroll;

                    if ( _.currentSlide - 1 === 0 ) {
                        _.direction = 1;
                    }

                }

            }

            _.slideHandler( slideTo );

        }

    };

    Slick.prototype.buildArrows = function() {

        var _ = this;

        if (_.options.arrows === true ) {

            _.$prevArrow = $(_.options.prevArrow).addClass('slick-arrow');
            _.$nextArrow = $(_.options.nextArrow).addClass('slick-arrow');

            if( _.slideCount > _.options.slidesToShow ) {

                _.$prevArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');
                _.$nextArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');

                if (_.htmlExpr.test(_.options.prevArrow)) {
                    _.$prevArrow.prependTo(_.options.appendArrows);
                }

                if (_.htmlExpr.test(_.options.nextArrow)) {
                    _.$nextArrow.appendTo(_.options.appendArrows);
                }

                if (_.options.infinite !== true) {
                    _.$prevArrow
                        .addClass('slick-disabled')
                        .attr('aria-disabled', 'true');
                }

            } else {

                _.$prevArrow.add( _.$nextArrow )

                    .addClass('slick-hidden')
                    .attr({
                        'aria-disabled': 'true',
                        'tabindex': '-1'
                    });

            }

        }

    };

    Slick.prototype.buildDots = function() {

        var _ = this,
            i, dot;

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$slider.addClass('slick-dotted');

            dot = $('<ul />').addClass(_.options.dotsClass);

            for (i = 0; i <= _.getDotCount(); i += 1) {
                dot.append($('<li />').append(_.options.customPaging.call(this, _, i)));
            }

            _.$dots = dot.appendTo(_.options.appendDots);

            _.$dots.find('li').first().addClass('slick-active');

        }

    };

    Slick.prototype.buildOut = function() {

        var _ = this;

        _.$slides =
            _.$slider
                .children( _.options.slide + ':not(.slick-cloned)')
                .addClass('slick-slide');

        _.slideCount = _.$slides.length;

        _.$slides.each(function(index, element) {
            $(element)
                .attr('data-slick-index', index)
                .data('originalStyling', $(element).attr('style') || '');
        });

        _.$slider.addClass('slick-slider');

        _.$slideTrack = (_.slideCount === 0) ?
            $('<div class="slick-track"/>').appendTo(_.$slider) :
            _.$slides.wrapAll('<div class="slick-track"/>').parent();

        _.$list = _.$slideTrack.wrap(
            '<div class="slick-list"/>').parent();
        _.$slideTrack.css('opacity', 0);

        if (_.options.centerMode === true || _.options.swipeToSlide === true) {
            _.options.slidesToScroll = 1;
        }

        $('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');

        _.setupInfinite();

        _.buildArrows();

        _.buildDots();

        _.updateDots();


        _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

        if (_.options.draggable === true) {
            _.$list.addClass('draggable');
        }

    };

    Slick.prototype.buildRows = function() {

        var _ = this, a, b, c, newSlides, numOfSlides, originalSlides,slidesPerSection;

        newSlides = document.createDocumentFragment();
        originalSlides = _.$slider.children();

        if(_.options.rows > 0) {

            slidesPerSection = _.options.slidesPerRow * _.options.rows;
            numOfSlides = Math.ceil(
                originalSlides.length / slidesPerSection
            );

            for(a = 0; a < numOfSlides; a++){
                var slide = document.createElement('div');
                for(b = 0; b < _.options.rows; b++) {
                    var row = document.createElement('div');
                    for(c = 0; c < _.options.slidesPerRow; c++) {
                        var target = (a * slidesPerSection + ((b * _.options.slidesPerRow) + c));
                        if (originalSlides.get(target)) {
                            row.appendChild(originalSlides.get(target));
                        }
                    }
                    slide.appendChild(row);
                }
                newSlides.appendChild(slide);
            }

            _.$slider.empty().append(newSlides);
            _.$slider.children().children().children()
                .css({
                    'width':(100 / _.options.slidesPerRow) + '%',
                    'display': 'inline-block'
                });

        }

    };

    Slick.prototype.checkResponsive = function(initial, forceUpdate) {

        var _ = this,
            breakpoint, targetBreakpoint, respondToWidth, triggerBreakpoint = false;
        var sliderWidth = _.$slider.width();
        var windowWidth = window.innerWidth || $(window).width();

        if (_.respondTo === 'window') {
            respondToWidth = windowWidth;
        } else if (_.respondTo === 'slider') {
            respondToWidth = sliderWidth;
        } else if (_.respondTo === 'min') {
            respondToWidth = Math.min(windowWidth, sliderWidth);
        }

        if ( _.options.responsive &&
            _.options.responsive.length &&
            _.options.responsive !== null) {

            targetBreakpoint = null;

            for (breakpoint in _.breakpoints) {
                if (_.breakpoints.hasOwnProperty(breakpoint)) {
                    if (_.originalSettings.mobileFirst === false) {
                        if (respondToWidth < _.breakpoints[breakpoint]) {
                            targetBreakpoint = _.breakpoints[breakpoint];
                        }
                    } else {
                        if (respondToWidth > _.breakpoints[breakpoint]) {
                            targetBreakpoint = _.breakpoints[breakpoint];
                        }
                    }
                }
            }

            if (targetBreakpoint !== null) {
                if (_.activeBreakpoint !== null) {
                    if (targetBreakpoint !== _.activeBreakpoint || forceUpdate) {
                        _.activeBreakpoint =
                            targetBreakpoint;
                        if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
                            _.unslick(targetBreakpoint);
                        } else {
                            _.options = $.extend({}, _.originalSettings,
                                _.breakpointSettings[
                                    targetBreakpoint]);
                            if (initial === true) {
                                _.currentSlide = _.options.initialSlide;
                            }
                            _.refresh(initial);
                        }
                        triggerBreakpoint = targetBreakpoint;
                    }
                } else {
                    _.activeBreakpoint = targetBreakpoint;
                    if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
                        _.unslick(targetBreakpoint);
                    } else {
                        _.options = $.extend({}, _.originalSettings,
                            _.breakpointSettings[
                                targetBreakpoint]);
                        if (initial === true) {
                            _.currentSlide = _.options.initialSlide;
                        }
                        _.refresh(initial);
                    }
                    triggerBreakpoint = targetBreakpoint;
                }
            } else {
                if (_.activeBreakpoint !== null) {
                    _.activeBreakpoint = null;
                    _.options = _.originalSettings;
                    if (initial === true) {
                        _.currentSlide = _.options.initialSlide;
                    }
                    _.refresh(initial);
                    triggerBreakpoint = targetBreakpoint;
                }
            }

            // only trigger breakpoints during an actual break. not on initialize.
            if( !initial && triggerBreakpoint !== false ) {
                _.$slider.trigger('breakpoint', [_, triggerBreakpoint]);
            }
        }

    };

    Slick.prototype.changeSlide = function(event, dontAnimate) {

        var _ = this,
            $target = $(event.currentTarget),
            indexOffset, slideOffset, unevenOffset;

        // If target is a link, prevent default action.
        if($target.is('a')) {
            event.preventDefault();
        }

        // If target is not the <li> element (ie: a child), find the <li>.
        if(!$target.is('li')) {
            $target = $target.closest('li');
        }

        unevenOffset = (_.slideCount % _.options.slidesToScroll !== 0);
        indexOffset = unevenOffset ? 0 : (_.slideCount - _.currentSlide) % _.options.slidesToScroll;

        switch (event.data.message) {

            case 'previous':
                slideOffset = indexOffset === 0 ? _.options.slidesToScroll : _.options.slidesToShow - indexOffset;
                if (_.slideCount > _.options.slidesToShow) {
                    _.slideHandler(_.currentSlide - slideOffset, false, dontAnimate);
                }
                break;

            case 'next':
                slideOffset = indexOffset === 0 ? _.options.slidesToScroll : indexOffset;
                if (_.slideCount > _.options.slidesToShow) {
                    _.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
                }
                break;

            case 'index':
                var index = event.data.index === 0 ? 0 :
                    event.data.index || $target.index() * _.options.slidesToScroll;

                _.slideHandler(_.checkNavigable(index), false, dontAnimate);
                $target.children().trigger('focus');
                break;

            default:
                return;
        }

    };

    Slick.prototype.checkNavigable = function(index) {

        var _ = this,
            navigables, prevNavigable;

        navigables = _.getNavigableIndexes();
        prevNavigable = 0;
        if (index > navigables[navigables.length - 1]) {
            index = navigables[navigables.length - 1];
        } else {
            for (var n in navigables) {
                if (index < navigables[n]) {
                    index = prevNavigable;
                    break;
                }
                prevNavigable = navigables[n];
            }
        }

        return index;
    };

    Slick.prototype.cleanUpEvents = function() {

        var _ = this;

        if (_.options.dots && _.$dots !== null) {

            $('li', _.$dots)
                .off('click.slick', _.changeSlide)
                .off('mouseenter.slick', $.proxy(_.interrupt, _, true))
                .off('mouseleave.slick', $.proxy(_.interrupt, _, false));

            if (_.options.accessibility === true) {
                _.$dots.off('keydown.slick', _.keyHandler);
            }
        }

        _.$slider.off('focus.slick blur.slick');

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
            _.$prevArrow && _.$prevArrow.off('click.slick', _.changeSlide);
            _.$nextArrow && _.$nextArrow.off('click.slick', _.changeSlide);

            if (_.options.accessibility === true) {
                _.$prevArrow && _.$prevArrow.off('keydown.slick', _.keyHandler);
                _.$nextArrow && _.$nextArrow.off('keydown.slick', _.keyHandler);
            }
        }

        _.$list.off('touchstart.slick mousedown.slick', _.swipeHandler);
        _.$list.off('touchmove.slick mousemove.slick', _.swipeHandler);
        _.$list.off('touchend.slick mouseup.slick', _.swipeHandler);
        _.$list.off('touchcancel.slick mouseleave.slick', _.swipeHandler);

        _.$list.off('click.slick', _.clickHandler);

        $(document).off(_.visibilityChange, _.visibility);

        _.cleanUpSlideEvents();

        if (_.options.accessibility === true) {
            _.$list.off('keydown.slick', _.keyHandler);
        }

        if (_.options.focusOnSelect === true) {
            $(_.$slideTrack).children().off('click.slick', _.selectHandler);
        }

        $(window).off('orientationchange.slick.slick-' + _.instanceUid, _.orientationChange);

        $(window).off('resize.slick.slick-' + _.instanceUid, _.resize);

        $('[draggable!=true]', _.$slideTrack).off('dragstart', _.preventDefault);

        $(window).off('load.slick.slick-' + _.instanceUid, _.setPosition);

    };

    Slick.prototype.cleanUpSlideEvents = function() {

        var _ = this;

        _.$list.off('mouseenter.slick', $.proxy(_.interrupt, _, true));
        _.$list.off('mouseleave.slick', $.proxy(_.interrupt, _, false));

    };

    Slick.prototype.cleanUpRows = function() {

        var _ = this, originalSlides;

        if(_.options.rows > 0) {
            originalSlides = _.$slides.children().children();
            originalSlides.removeAttr('style');
            _.$slider.empty().append(originalSlides);
        }

    };

    Slick.prototype.clickHandler = function(event) {

        var _ = this;

        if (_.shouldClick === false) {
            event.stopImmediatePropagation();
            event.stopPropagation();
            event.preventDefault();
        }

    };

    Slick.prototype.destroy = function(refresh) {

        var _ = this;

        _.autoPlayClear();

        _.touchObject = {};

        _.cleanUpEvents();

        $('.slick-cloned', _.$slider).detach();

        if (_.$dots) {
            _.$dots.remove();
        }

        if ( _.$prevArrow && _.$prevArrow.length ) {

            _.$prevArrow
                .removeClass('slick-disabled slick-arrow slick-hidden')
                .removeAttr('aria-hidden aria-disabled tabindex')
                .css('display','');

            if ( _.htmlExpr.test( _.options.prevArrow )) {
                _.$prevArrow.remove();
            }
        }

        if ( _.$nextArrow && _.$nextArrow.length ) {

            _.$nextArrow
                .removeClass('slick-disabled slick-arrow slick-hidden')
                .removeAttr('aria-hidden aria-disabled tabindex')
                .css('display','');

            if ( _.htmlExpr.test( _.options.nextArrow )) {
                _.$nextArrow.remove();
            }
        }


        if (_.$slides) {

            _.$slides
                .removeClass('slick-slide slick-active slick-center slick-visible slick-current')
                .removeAttr('aria-hidden')
                .removeAttr('data-slick-index')
                .each(function(){
                    $(this).attr('style', $(this).data('originalStyling'));
                });

            _.$slideTrack.children(this.options.slide).detach();

            _.$slideTrack.detach();

            _.$list.detach();

            _.$slider.append(_.$slides);
        }

        _.cleanUpRows();

        _.$slider.removeClass('slick-slider');
        _.$slider.removeClass('slick-initialized');
        _.$slider.removeClass('slick-dotted');

        _.unslicked = true;

        if(!refresh) {
            _.$slider.trigger('destroy', [_]);
        }

    };

    Slick.prototype.disableTransition = function(slide) {

        var _ = this,
            transition = {};

        transition[_.transitionType] = '';

        if (_.options.fade === false) {
            _.$slideTrack.css(transition);
        } else {
            _.$slides.eq(slide).css(transition);
        }

    };

    Slick.prototype.fadeSlide = function(slideIndex, callback) {

        var _ = this;

        if (_.cssTransitions === false) {

            _.$slides.eq(slideIndex).css({
                zIndex: _.options.zIndex
            });

            _.$slides.eq(slideIndex).animate({
                opacity: 1
            }, _.options.speed, _.options.easing, callback);

        } else {

            _.applyTransition(slideIndex);

            _.$slides.eq(slideIndex).css({
                opacity: 1,
                zIndex: _.options.zIndex
            });

            if (callback) {
                setTimeout(function() {

                    _.disableTransition(slideIndex);

                    callback.call();
                }, _.options.speed);
            }

        }

    };

    Slick.prototype.fadeSlideOut = function(slideIndex) {

        var _ = this;

        if (_.cssTransitions === false) {

            _.$slides.eq(slideIndex).animate({
                opacity: 0,
                zIndex: _.options.zIndex - 2
            }, _.options.speed, _.options.easing);

        } else {

            _.applyTransition(slideIndex);

            _.$slides.eq(slideIndex).css({
                opacity: 0,
                zIndex: _.options.zIndex - 2
            });

        }

    };

    Slick.prototype.filterSlides = Slick.prototype.slickFilter = function(filter) {

        var _ = this;

        if (filter !== null) {

            _.$slidesCache = _.$slides;

            _.unload();

            _.$slideTrack.children(this.options.slide).detach();

            _.$slidesCache.filter(filter).appendTo(_.$slideTrack);

            _.reinit();

        }

    };

    Slick.prototype.focusHandler = function() {

        var _ = this;

        // If any child element receives focus within the slider we need to pause the autoplay
        _.$slider
            .off('focus.slick blur.slick')
            .on(
                'focus.slick',
                '*',
                function(event) {
                    var $sf = $(this);

                    setTimeout(function() {
                        if( _.options.pauseOnFocus ) {
                            if ($sf.is(':focus')) {
                                _.focussed = true;
                                _.autoPlay();
                            }
                        }
                    }, 0);
                }
            ).on(
                'blur.slick',
                '*',
                function(event) {
                    var $sf = $(this);

                    // When a blur occurs on any elements within the slider we become unfocused
                    if( _.options.pauseOnFocus ) {
                        _.focussed = false;
                        _.autoPlay();
                    }
                }
            );
    };

    Slick.prototype.getCurrent = Slick.prototype.slickCurrentSlide = function() {

        var _ = this;
        return _.currentSlide;

    };

    Slick.prototype.getDotCount = function() {

        var _ = this;

        var breakPoint = 0;
        var counter = 0;
        var pagerQty = 0;

        if (_.options.infinite === true) {
            if (_.slideCount <= _.options.slidesToShow) {
                 ++pagerQty;
            } else {
                while (breakPoint < _.slideCount) {
                    ++pagerQty;
                    breakPoint = counter + _.options.slidesToScroll;
                    counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
                }
            }
        } else if (_.options.centerMode === true) {
            pagerQty = _.slideCount;
        } else if(!_.options.asNavFor) {
            pagerQty = 1 + Math.ceil((_.slideCount - _.options.slidesToShow) / _.options.slidesToScroll);
        }else {
            while (breakPoint < _.slideCount) {
                ++pagerQty;
                breakPoint = counter + _.options.slidesToScroll;
                counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
            }
        }

        return pagerQty - 1;

    };

    Slick.prototype.getLeft = function(slideIndex) {

        var _ = this,
            targetLeft,
            verticalHeight,
            verticalOffset = 0,
            targetSlide,
            coef;

        _.slideOffset = 0;
        verticalHeight = _.$slides.first().outerHeight(true);

        if (_.options.infinite === true) {
            if (_.slideCount > _.options.slidesToShow) {
                _.slideOffset = (_.slideWidth * _.options.slidesToShow) * -1;
                coef = -1

                if (_.options.vertical === true && _.options.centerMode === true) {
                    if (_.options.slidesToShow === 2) {
                        coef = -1.5;
                    } else if (_.options.slidesToShow === 1) {
                        coef = -2
                    }
                }
                verticalOffset = (verticalHeight * _.options.slidesToShow) * coef;
            }
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                if (slideIndex + _.options.slidesToScroll > _.slideCount && _.slideCount > _.options.slidesToShow) {
                    if (slideIndex > _.slideCount) {
                        _.slideOffset = ((_.options.slidesToShow - (slideIndex - _.slideCount)) * _.slideWidth) * -1;
                        verticalOffset = ((_.options.slidesToShow - (slideIndex - _.slideCount)) * verticalHeight) * -1;
                    } else {
                        _.slideOffset = ((_.slideCount % _.options.slidesToScroll) * _.slideWidth) * -1;
                        verticalOffset = ((_.slideCount % _.options.slidesToScroll) * verticalHeight) * -1;
                    }
                }
            }
        } else {
            if (slideIndex + _.options.slidesToShow > _.slideCount) {
                _.slideOffset = ((slideIndex + _.options.slidesToShow) - _.slideCount) * _.slideWidth;
                verticalOffset = ((slideIndex + _.options.slidesToShow) - _.slideCount) * verticalHeight;
            }
        }

        if (_.slideCount <= _.options.slidesToShow) {
            _.slideOffset = 0;
            verticalOffset = 0;
        }

        if (_.options.centerMode === true && _.slideCount <= _.options.slidesToShow) {
            _.slideOffset = ((_.slideWidth * Math.floor(_.options.slidesToShow)) / 2) - ((_.slideWidth * _.slideCount) / 2);
        } else if (_.options.centerMode === true && _.options.infinite === true) {
            _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
        } else if (_.options.centerMode === true) {
            _.slideOffset = 0;
            _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2);
        }

        if (_.options.vertical === false) {
            targetLeft = ((slideIndex * _.slideWidth) * -1) + _.slideOffset;
        } else {
            targetLeft = ((slideIndex * verticalHeight) * -1) + verticalOffset;
        }

        if (_.options.variableWidth === true) {

            if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
                targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
            } else {
                targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow);
            }

            if (_.options.rtl === true) {
                if (targetSlide[0]) {
                    targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
                } else {
                    targetLeft =  0;
                }
            } else {
                targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
            }

            if (_.options.centerMode === true) {
                if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
                    targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
                } else {
                    targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow + 1);
                }

                if (_.options.rtl === true) {
                    if (targetSlide[0]) {
                        targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
                    } else {
                        targetLeft =  0;
                    }
                } else {
                    targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
                }

                targetLeft += (_.$list.width() - targetSlide.outerWidth()) / 2;
            }
        }

        return targetLeft;

    };

    Slick.prototype.getOption = Slick.prototype.slickGetOption = function(option) {

        var _ = this;

        return _.options[option];

    };

    Slick.prototype.getNavigableIndexes = function() {

        var _ = this,
            breakPoint = 0,
            counter = 0,
            indexes = [],
            max;

        if (_.options.infinite === false) {
            max = _.slideCount;
        } else {
            breakPoint = _.options.slidesToScroll * -1;
            counter = _.options.slidesToScroll * -1;
            max = _.slideCount * 2;
        }

        while (breakPoint < max) {
            indexes.push(breakPoint);
            breakPoint = counter + _.options.slidesToScroll;
            counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
        }

        return indexes;

    };

    Slick.prototype.getSlick = function() {

        return this;

    };

    Slick.prototype.getSlideCount = function() {

        var _ = this,
            slidesTraversed, swipedSlide, swipeTarget, centerOffset;

        centerOffset = _.options.centerMode === true ? Math.floor(_.$list.width() / 2) : 0;
        swipeTarget = (_.swipeLeft * -1) + centerOffset;

        if (_.options.swipeToSlide === true) {

            _.$slideTrack.find('.slick-slide').each(function(index, slide) {

                var slideOuterWidth, slideOffset, slideRightBoundary;
                slideOuterWidth = $(slide).outerWidth();
                slideOffset = slide.offsetLeft;
                if (_.options.centerMode !== true) {
                    slideOffset += (slideOuterWidth / 2);
                }

                slideRightBoundary = slideOffset + (slideOuterWidth);

                if (swipeTarget < slideRightBoundary) {
                    swipedSlide = slide;
                    return false;
                }
            });

            slidesTraversed = Math.abs($(swipedSlide).attr('data-slick-index') - _.currentSlide) || 1;

            return slidesTraversed;

        } else {
            return _.options.slidesToScroll;
        }

    };

    Slick.prototype.goTo = Slick.prototype.slickGoTo = function(slide, dontAnimate) {

        var _ = this;

        _.changeSlide({
            data: {
                message: 'index',
                index: parseInt(slide)
            }
        }, dontAnimate);

    };

    Slick.prototype.init = function(creation) {

        var _ = this;

        if (!$(_.$slider).hasClass('slick-initialized')) {

            $(_.$slider).addClass('slick-initialized');

            _.buildRows();
            _.buildOut();
            _.setProps();
            _.startLoad();
            _.loadSlider();
            _.initializeEvents();
            _.updateArrows();
            _.updateDots();
            _.checkResponsive(true);
            _.focusHandler();

        }

        if (creation) {
            _.$slider.trigger('init', [_]);
        }

        if (_.options.accessibility === true) {
            _.initADA();
        }

        if ( _.options.autoplay ) {

            _.paused = false;
            _.autoPlay();

        }

    };

    Slick.prototype.initADA = function() {
        var _ = this,
                numDotGroups = Math.ceil(_.slideCount / _.options.slidesToShow),
                tabControlIndexes = _.getNavigableIndexes().filter(function(val) {
                    return (val >= 0) && (val < _.slideCount);
                });

        _.$slides.add(_.$slideTrack.find('.slick-cloned')).attr({
            'aria-hidden': 'true',
            'tabindex': '-1'
        }).find('a, input, button, select').attr({
            'tabindex': '-1'
        });

        if (_.$dots !== null) {
            _.$slides.not(_.$slideTrack.find('.slick-cloned')).each(function(i) {
                var slideControlIndex = tabControlIndexes.indexOf(i);

                $(this).attr({
                    'role': 'tabpanel',
                    'id': 'slick-slide' + _.instanceUid + i,
                    'tabindex': -1
                });

                if (slideControlIndex !== -1) {
                   var ariaButtonControl = 'slick-slide-control' + _.instanceUid + slideControlIndex
                   if ($('#' + ariaButtonControl).length) {
                     $(this).attr({
                         'aria-describedby': ariaButtonControl
                     });
                   }
                }
            });

            _.$dots.attr('role', 'tablist').find('li').each(function(i) {
                var mappedSlideIndex = tabControlIndexes[i];

                $(this).attr({
                    'role': 'presentation'
                });

                $(this).find('button').first().attr({
                    'role': 'tab',
                    'id': 'slick-slide-control' + _.instanceUid + i,
                    'aria-controls': 'slick-slide' + _.instanceUid + mappedSlideIndex,
                    'aria-label': (i + 1) + ' of ' + numDotGroups,
                    'aria-selected': null,
                    'tabindex': '-1'
                });

            }).eq(_.currentSlide).find('button').attr({
                'aria-selected': 'true',
                'tabindex': '0'
            }).end();
        }

        for (var i=_.currentSlide, max=i+_.options.slidesToShow; i < max; i++) {
          if (_.options.focusOnChange) {
            _.$slides.eq(i).attr({'tabindex': '0'});
          } else {
            _.$slides.eq(i).removeAttr('tabindex');
          }
        }

        _.activateADA();

    };

    Slick.prototype.initArrowEvents = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
            _.$prevArrow
               .off('click.slick')
               .on('click.slick', {
                    message: 'previous'
               }, _.changeSlide);
            _.$nextArrow
               .off('click.slick')
               .on('click.slick', {
                    message: 'next'
               }, _.changeSlide);

            if (_.options.accessibility === true) {
                _.$prevArrow.on('keydown.slick', _.keyHandler);
                _.$nextArrow.on('keydown.slick', _.keyHandler);
            }
        }

    };

    Slick.prototype.initDotEvents = function() {

        var _ = this;

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
            $('li', _.$dots).on('click.slick', {
                message: 'index'
            }, _.changeSlide);

            if (_.options.accessibility === true) {
                _.$dots.on('keydown.slick', _.keyHandler);
            }
        }

        if (_.options.dots === true && _.options.pauseOnDotsHover === true && _.slideCount > _.options.slidesToShow) {

            $('li', _.$dots)
                .on('mouseenter.slick', $.proxy(_.interrupt, _, true))
                .on('mouseleave.slick', $.proxy(_.interrupt, _, false));

        }

    };

    Slick.prototype.initSlideEvents = function() {

        var _ = this;

        if ( _.options.pauseOnHover ) {

            _.$list.on('mouseenter.slick', $.proxy(_.interrupt, _, true));
            _.$list.on('mouseleave.slick', $.proxy(_.interrupt, _, false));

        }

    };

    Slick.prototype.initializeEvents = function() {

        var _ = this;

        _.initArrowEvents();

        _.initDotEvents();
        _.initSlideEvents();

        _.$list.on('touchstart.slick mousedown.slick', {
            action: 'start'
        }, _.swipeHandler);
        _.$list.on('touchmove.slick mousemove.slick', {
            action: 'move'
        }, _.swipeHandler);
        _.$list.on('touchend.slick mouseup.slick', {
            action: 'end'
        }, _.swipeHandler);
        _.$list.on('touchcancel.slick mouseleave.slick', {
            action: 'end'
        }, _.swipeHandler);

        _.$list.on('click.slick', _.clickHandler);

        $(document).on(_.visibilityChange, $.proxy(_.visibility, _));

        if (_.options.accessibility === true) {
            _.$list.on('keydown.slick', _.keyHandler);
        }

        if (_.options.focusOnSelect === true) {
            $(_.$slideTrack).children().on('click.slick', _.selectHandler);
        }

        $(window).on('orientationchange.slick.slick-' + _.instanceUid, $.proxy(_.orientationChange, _));

        $(window).on('resize.slick.slick-' + _.instanceUid, $.proxy(_.resize, _));

        $('[draggable!=true]', _.$slideTrack).on('dragstart', _.preventDefault);

        $(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);
        $(_.setPosition);

    };

    Slick.prototype.initUI = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow.show();
            _.$nextArrow.show();

        }

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$dots.show();

        }

    };

    Slick.prototype.keyHandler = function(event) {

        var _ = this;
         //Dont slide if the cursor is inside the form fields and arrow keys are pressed
        if(!event.target.tagName.match('TEXTAREA|INPUT|SELECT')) {
            if (event.keyCode === 37 && _.options.accessibility === true) {
                _.changeSlide({
                    data: {
                        message: _.options.rtl === true ? 'next' :  'previous'
                    }
                });
            } else if (event.keyCode === 39 && _.options.accessibility === true) {
                _.changeSlide({
                    data: {
                        message: _.options.rtl === true ? 'previous' : 'next'
                    }
                });
            }
        }

    };

    Slick.prototype.lazyLoad = function() {

        var _ = this,
            loadRange, cloneRange, rangeStart, rangeEnd;

        function loadImages(imagesScope) {

            $('img[data-lazy]', imagesScope).each(function() {

                var image = $(this),
                    imageSource = $(this).attr('data-lazy'),
                    imageSrcSet = $(this).attr('data-srcset'),
                    imageSizes  = $(this).attr('data-sizes') || _.$slider.attr('data-sizes'),
                    imageToLoad = document.createElement('img');

                imageToLoad.onload = function() {

                    image
                        .animate({ opacity: 0 }, 100, function() {

                            if (imageSrcSet) {
                                image
                                    .attr('srcset', imageSrcSet );

                                if (imageSizes) {
                                    image
                                        .attr('sizes', imageSizes );
                                }
                            }

                            image
                                .attr('src', imageSource)
                                .animate({ opacity: 1 }, 200, function() {
                                    image
                                        .removeAttr('data-lazy data-srcset data-sizes')
                                        .removeClass('slick-loading');
                                });
                            _.$slider.trigger('lazyLoaded', [_, image, imageSource]);
                        });

                };

                imageToLoad.onerror = function() {

                    image
                        .removeAttr( 'data-lazy' )
                        .removeClass( 'slick-loading' )
                        .addClass( 'slick-lazyload-error' );

                    _.$slider.trigger('lazyLoadError', [ _, image, imageSource ]);

                };

                imageToLoad.src = imageSource;

            });

        }

        if (_.options.centerMode === true) {
            if (_.options.infinite === true) {
                rangeStart = _.currentSlide + (_.options.slidesToShow / 2 + 1);
                rangeEnd = rangeStart + _.options.slidesToShow + 2;
            } else {
                rangeStart = Math.max(0, _.currentSlide - (_.options.slidesToShow / 2 + 1));
                rangeEnd = 2 + (_.options.slidesToShow / 2 + 1) + _.currentSlide;
            }
        } else {
            rangeStart = _.options.infinite ? _.options.slidesToShow + _.currentSlide : _.currentSlide;
            rangeEnd = Math.ceil(rangeStart + _.options.slidesToShow);
            if (_.options.fade === true) {
                if (rangeStart > 0) rangeStart--;
                if (rangeEnd <= _.slideCount) rangeEnd++;
            }
        }

        loadRange = _.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);

        if (_.options.lazyLoad === 'anticipated') {
            var prevSlide = rangeStart - 1,
                nextSlide = rangeEnd,
                $slides = _.$slider.find('.slick-slide');

            for (var i = 0; i < _.options.slidesToScroll; i++) {
                if (prevSlide < 0) prevSlide = _.slideCount - 1;
                loadRange = loadRange.add($slides.eq(prevSlide));
                loadRange = loadRange.add($slides.eq(nextSlide));
                prevSlide--;
                nextSlide++;
            }
        }

        loadImages(loadRange);

        if (_.slideCount <= _.options.slidesToShow) {
            cloneRange = _.$slider.find('.slick-slide');
            loadImages(cloneRange);
        } else
        if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
            cloneRange = _.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
            loadImages(cloneRange);
        } else if (_.currentSlide === 0) {
            cloneRange = _.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
            loadImages(cloneRange);
        }

    };

    Slick.prototype.loadSlider = function() {

        var _ = this;

        _.setPosition();

        _.$slideTrack.css({
            opacity: 1
        });

        _.$slider.removeClass('slick-loading');

        _.initUI();

        if (_.options.lazyLoad === 'progressive') {
            _.progressiveLazyLoad();
        }

    };

    Slick.prototype.next = Slick.prototype.slickNext = function() {

        var _ = this;

        _.changeSlide({
            data: {
                message: 'next'
            }
        });

    };

    Slick.prototype.orientationChange = function() {

        var _ = this;

        _.checkResponsive();
        _.setPosition();

    };

    Slick.prototype.pause = Slick.prototype.slickPause = function() {

        var _ = this;

        _.autoPlayClear();
        _.paused = true;

    };

    Slick.prototype.play = Slick.prototype.slickPlay = function() {

        var _ = this;

        _.autoPlay();
        _.options.autoplay = true;
        _.paused = false;
        _.focussed = false;
        _.interrupted = false;

    };

    Slick.prototype.postSlide = function(index) {

        var _ = this;

        if( !_.unslicked ) {

            _.$slider.trigger('afterChange', [_, index]);

            _.animating = false;

            if (_.slideCount > _.options.slidesToShow) {
                _.setPosition();
            }

            _.swipeLeft = null;

            if ( _.options.autoplay ) {
                _.autoPlay();
            }

            if (_.options.accessibility === true) {
                _.initADA();

                if (_.options.focusOnChange) {
                    var $currentSlide = $(_.$slides.get(_.currentSlide));
                    $currentSlide.attr('tabindex', 0).focus();
                }
            }

        }

    };

    Slick.prototype.prev = Slick.prototype.slickPrev = function() {

        var _ = this;

        _.changeSlide({
            data: {
                message: 'previous'
            }
        });

    };

    Slick.prototype.preventDefault = function(event) {

        event.preventDefault();

    };

    Slick.prototype.progressiveLazyLoad = function( tryCount ) {

        tryCount = tryCount || 1;

        var _ = this,
            $imgsToLoad = $( 'img[data-lazy]', _.$slider ),
            image,
            imageSource,
            imageSrcSet,
            imageSizes,
            imageToLoad;

        if ( $imgsToLoad.length ) {

            image = $imgsToLoad.first();
            imageSource = image.attr('data-lazy');
            imageSrcSet = image.attr('data-srcset');
            imageSizes  = image.attr('data-sizes') || _.$slider.attr('data-sizes');
            imageToLoad = document.createElement('img');

            imageToLoad.onload = function() {

                if (imageSrcSet) {
                    image
                        .attr('srcset', imageSrcSet );

                    if (imageSizes) {
                        image
                            .attr('sizes', imageSizes );
                    }
                }

                image
                    .attr( 'src', imageSource )
                    .removeAttr('data-lazy data-srcset data-sizes')
                    .removeClass('slick-loading');

                if ( _.options.adaptiveHeight === true ) {
                    _.setPosition();
                }

                _.$slider.trigger('lazyLoaded', [ _, image, imageSource ]);
                _.progressiveLazyLoad();

            };

            imageToLoad.onerror = function() {

                if ( tryCount < 3 ) {

                    /**
                     * try to load the image 3 times,
                     * leave a slight delay so we don't get
                     * servers blocking the request.
                     */
                    setTimeout( function() {
                        _.progressiveLazyLoad( tryCount + 1 );
                    }, 500 );

                } else {

                    image
                        .removeAttr( 'data-lazy' )
                        .removeClass( 'slick-loading' )
                        .addClass( 'slick-lazyload-error' );

                    _.$slider.trigger('lazyLoadError', [ _, image, imageSource ]);

                    _.progressiveLazyLoad();

                }

            };

            imageToLoad.src = imageSource;

        } else {

            _.$slider.trigger('allImagesLoaded', [ _ ]);

        }

    };

    Slick.prototype.refresh = function( initializing ) {

        var _ = this, currentSlide, lastVisibleIndex;

        lastVisibleIndex = _.slideCount - _.options.slidesToShow;

        // in non-infinite sliders, we don't want to go past the
        // last visible index.
        if( !_.options.infinite && ( _.currentSlide > lastVisibleIndex )) {
            _.currentSlide = lastVisibleIndex;
        }

        // if less slides than to show, go to start.
        if ( _.slideCount <= _.options.slidesToShow ) {
            _.currentSlide = 0;

        }

        currentSlide = _.currentSlide;

        _.destroy(true);

        $.extend(_, _.initials, { currentSlide: currentSlide });

        _.init();

        if( !initializing ) {

            _.changeSlide({
                data: {
                    message: 'index',
                    index: currentSlide
                }
            }, false);

        }

    };

    Slick.prototype.registerBreakpoints = function() {

        var _ = this, breakpoint, currentBreakpoint, l,
            responsiveSettings = _.options.responsive || null;

        if ( $.type(responsiveSettings) === 'array' && responsiveSettings.length ) {

            _.respondTo = _.options.respondTo || 'window';

            for ( breakpoint in responsiveSettings ) {

                l = _.breakpoints.length-1;

                if (responsiveSettings.hasOwnProperty(breakpoint)) {
                    currentBreakpoint = responsiveSettings[breakpoint].breakpoint;

                    // loop through the breakpoints and cut out any existing
                    // ones with the same breakpoint number, we don't want dupes.
                    while( l >= 0 ) {
                        if( _.breakpoints[l] && _.breakpoints[l] === currentBreakpoint ) {
                            _.breakpoints.splice(l,1);
                        }
                        l--;
                    }

                    _.breakpoints.push(currentBreakpoint);
                    _.breakpointSettings[currentBreakpoint] = responsiveSettings[breakpoint].settings;

                }

            }

            _.breakpoints.sort(function(a, b) {
                return ( _.options.mobileFirst ) ? a-b : b-a;
            });

        }

    };

    Slick.prototype.reinit = function() {

        var _ = this;

        _.$slides =
            _.$slideTrack
                .children(_.options.slide)
                .addClass('slick-slide');

        _.slideCount = _.$slides.length;

        if (_.currentSlide >= _.slideCount && _.currentSlide !== 0) {
            _.currentSlide = _.currentSlide - _.options.slidesToScroll;
        }

        if (_.slideCount <= _.options.slidesToShow) {
            _.currentSlide = 0;
        }

        _.registerBreakpoints();

        _.setProps();
        _.setupInfinite();
        _.buildArrows();
        _.updateArrows();
        _.initArrowEvents();
        _.buildDots();
        _.updateDots();
        _.initDotEvents();
        _.cleanUpSlideEvents();
        _.initSlideEvents();

        _.checkResponsive(false, true);

        if (_.options.focusOnSelect === true) {
            $(_.$slideTrack).children().on('click.slick', _.selectHandler);
        }

        _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

        _.setPosition();
        _.focusHandler();

        _.paused = !_.options.autoplay;
        _.autoPlay();

        _.$slider.trigger('reInit', [_]);

    };

    Slick.prototype.resize = function() {

        var _ = this;

        if ($(window).width() !== _.windowWidth) {
            clearTimeout(_.windowDelay);
            _.windowDelay = window.setTimeout(function() {
                _.windowWidth = $(window).width();
                _.checkResponsive();
                if( !_.unslicked ) { _.setPosition(); }
            }, 50);
        }
    };

    Slick.prototype.removeSlide = Slick.prototype.slickRemove = function(index, removeBefore, removeAll) {

        var _ = this;

        if (typeof(index) === 'boolean') {
            removeBefore = index;
            index = removeBefore === true ? 0 : _.slideCount - 1;
        } else {
            index = removeBefore === true ? --index : index;
        }

        if (_.slideCount < 1 || index < 0 || index > _.slideCount - 1) {
            return false;
        }

        _.unload();

        if (removeAll === true) {
            _.$slideTrack.children().remove();
        } else {
            _.$slideTrack.children(this.options.slide).eq(index).remove();
        }

        _.$slides = _.$slideTrack.children(this.options.slide);

        _.$slideTrack.children(this.options.slide).detach();

        _.$slideTrack.append(_.$slides);

        _.$slidesCache = _.$slides;

        _.reinit();

    };

    Slick.prototype.setCSS = function(position) {

        var _ = this,
            positionProps = {},
            x, y;

        if (_.options.rtl === true) {
            position = -position;
        }
        x = _.positionProp == 'left' ? Math.ceil(position) + 'px' : '0px';
        y = _.positionProp == 'top' ? Math.ceil(position) + 'px' : '0px';

        positionProps[_.positionProp] = position;

        if (_.transformsEnabled === false) {
            _.$slideTrack.css(positionProps);
        } else {
            positionProps = {};
            if (_.cssTransitions === false) {
                positionProps[_.animType] = 'translate(' + x + ', ' + y + ')';
                _.$slideTrack.css(positionProps);
            } else {
                positionProps[_.animType] = 'translate3d(' + x + ', ' + y + ', 0px)';
                _.$slideTrack.css(positionProps);
            }
        }

    };

    Slick.prototype.setDimensions = function() {

        var _ = this;

        if (_.options.vertical === false) {
            if (_.options.centerMode === true) {
                _.$list.css({
                    padding: ('0px ' + _.options.centerPadding)
                });
            }
        } else {
            _.$list.height(_.$slides.first().outerHeight(true) * _.options.slidesToShow);
            if (_.options.centerMode === true) {
                _.$list.css({
                    padding: (_.options.centerPadding + ' 0px')
                });
            }
        }

        _.listWidth = _.$list.width();
        _.listHeight = _.$list.height();


        if (_.options.vertical === false && _.options.variableWidth === false) {
            _.slideWidth = Math.ceil(_.listWidth / _.options.slidesToShow);
            _.$slideTrack.width(Math.ceil((_.slideWidth * _.$slideTrack.children('.slick-slide').length)));

        } else if (_.options.variableWidth === true) {
            _.$slideTrack.width(5000 * _.slideCount);
        } else {
            _.slideWidth = Math.ceil(_.listWidth);
            _.$slideTrack.height(Math.ceil((_.$slides.first().outerHeight(true) * _.$slideTrack.children('.slick-slide').length)));
        }

        var offset = _.$slides.first().outerWidth(true) - _.$slides.first().width();
        if (_.options.variableWidth === false) _.$slideTrack.children('.slick-slide').width(_.slideWidth - offset);

    };

    Slick.prototype.setFade = function() {

        var _ = this,
            targetLeft;

        _.$slides.each(function(index, element) {
            targetLeft = (_.slideWidth * index) * -1;
            if (_.options.rtl === true) {
                $(element).css({
                    position: 'relative',
                    right: targetLeft,
                    top: 0,
                    zIndex: _.options.zIndex - 2,
                    opacity: 0
                });
            } else {
                $(element).css({
                    position: 'relative',
                    left: targetLeft,
                    top: 0,
                    zIndex: _.options.zIndex - 2,
                    opacity: 0
                });
            }
        });

        _.$slides.eq(_.currentSlide).css({
            zIndex: _.options.zIndex - 1,
            opacity: 1
        });

    };

    Slick.prototype.setHeight = function() {

        var _ = this;

        if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
            var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
            _.$list.css('height', targetHeight);
        }

    };

    Slick.prototype.setOption =
    Slick.prototype.slickSetOption = function() {

        /**
         * accepts arguments in format of:
         *
         *  - for changing a single option's value:
         *     .slick("setOption", option, value, refresh )
         *
         *  - for changing a set of responsive options:
         *     .slick("setOption", 'responsive', [{}, ...], refresh )
         *
         *  - for updating multiple values at once (not responsive)
         *     .slick("setOption", { 'option': value, ... }, refresh )
         */

        var _ = this, l, item, option, value, refresh = false, type;

        if( $.type( arguments[0] ) === 'object' ) {

            option =  arguments[0];
            refresh = arguments[1];
            type = 'multiple';

        } else if ( $.type( arguments[0] ) === 'string' ) {

            option =  arguments[0];
            value = arguments[1];
            refresh = arguments[2];

            if ( arguments[0] === 'responsive' && $.type( arguments[1] ) === 'array' ) {

                type = 'responsive';

            } else if ( typeof arguments[1] !== 'undefined' ) {

                type = 'single';

            }

        }

        if ( type === 'single' ) {

            _.options[option] = value;


        } else if ( type === 'multiple' ) {

            $.each( option , function( opt, val ) {

                _.options[opt] = val;

            });


        } else if ( type === 'responsive' ) {

            for ( item in value ) {

                if( $.type( _.options.responsive ) !== 'array' ) {

                    _.options.responsive = [ value[item] ];

                } else {

                    l = _.options.responsive.length-1;

                    // loop through the responsive object and splice out duplicates.
                    while( l >= 0 ) {

                        if( _.options.responsive[l].breakpoint === value[item].breakpoint ) {

                            _.options.responsive.splice(l,1);

                        }

                        l--;

                    }

                    _.options.responsive.push( value[item] );

                }

            }

        }

        if ( refresh ) {

            _.unload();
            _.reinit();

        }

    };

    Slick.prototype.setPosition = function() {

        var _ = this;

        _.setDimensions();

        _.setHeight();

        if (_.options.fade === false) {
            _.setCSS(_.getLeft(_.currentSlide));
        } else {
            _.setFade();
        }

        _.$slider.trigger('setPosition', [_]);

    };

    Slick.prototype.setProps = function() {

        var _ = this,
            bodyStyle = document.body.style;

        _.positionProp = _.options.vertical === true ? 'top' : 'left';

        if (_.positionProp === 'top') {
            _.$slider.addClass('slick-vertical');
        } else {
            _.$slider.removeClass('slick-vertical');
        }

        if (bodyStyle.WebkitTransition !== undefined ||
            bodyStyle.MozTransition !== undefined ||
            bodyStyle.msTransition !== undefined) {
            if (_.options.useCSS === true) {
                _.cssTransitions = true;
            }
        }

        if ( _.options.fade ) {
            if ( typeof _.options.zIndex === 'number' ) {
                if( _.options.zIndex < 3 ) {
                    _.options.zIndex = 3;
                }
            } else {
                _.options.zIndex = _.defaults.zIndex;
            }
        }

        if (bodyStyle.OTransform !== undefined) {
            _.animType = 'OTransform';
            _.transformType = '-o-transform';
            _.transitionType = 'OTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.MozTransform !== undefined) {
            _.animType = 'MozTransform';
            _.transformType = '-moz-transform';
            _.transitionType = 'MozTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.MozPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.webkitTransform !== undefined) {
            _.animType = 'webkitTransform';
            _.transformType = '-webkit-transform';
            _.transitionType = 'webkitTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.msTransform !== undefined) {
            _.animType = 'msTransform';
            _.transformType = '-ms-transform';
            _.transitionType = 'msTransition';
            if (bodyStyle.msTransform === undefined) _.animType = false;
        }
        if (bodyStyle.transform !== undefined && _.animType !== false) {
            _.animType = 'transform';
            _.transformType = 'transform';
            _.transitionType = 'transition';
        }
        _.transformsEnabled = _.options.useTransform && (_.animType !== null && _.animType !== false);
    };


    Slick.prototype.setSlideClasses = function(index) {

        var _ = this,
            centerOffset, allSlides, indexOffset, remainder;

        allSlides = _.$slider
            .find('.slick-slide')
            .removeClass('slick-active slick-center slick-current')
            .attr('aria-hidden', 'true');

        _.$slides
            .eq(index)
            .addClass('slick-current');

        if (_.options.centerMode === true) {

            var evenCoef = _.options.slidesToShow % 2 === 0 ? 1 : 0;

            centerOffset = Math.floor(_.options.slidesToShow / 2);

            if (_.options.infinite === true) {

                if (index >= centerOffset && index <= (_.slideCount - 1) - centerOffset) {
                    _.$slides
                        .slice(index - centerOffset + evenCoef, index + centerOffset + 1)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                } else {

                    indexOffset = _.options.slidesToShow + index;
                    allSlides
                        .slice(indexOffset - centerOffset + 1 + evenCoef, indexOffset + centerOffset + 2)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                }

                if (index === 0) {

                    allSlides
                        .eq(allSlides.length - 1 - _.options.slidesToShow)
                        .addClass('slick-center');

                } else if (index === _.slideCount - 1) {

                    allSlides
                        .eq(_.options.slidesToShow)
                        .addClass('slick-center');

                }

            }

            _.$slides
                .eq(index)
                .addClass('slick-center');

        } else {

            if (index >= 0 && index <= (_.slideCount - _.options.slidesToShow)) {

                _.$slides
                    .slice(index, index + _.options.slidesToShow)
                    .addClass('slick-active')
                    .attr('aria-hidden', 'false');

            } else if (allSlides.length <= _.options.slidesToShow) {

                allSlides
                    .addClass('slick-active')
                    .attr('aria-hidden', 'false');

            } else {

                remainder = _.slideCount % _.options.slidesToShow;
                indexOffset = _.options.infinite === true ? _.options.slidesToShow + index : index;

                if (_.options.slidesToShow == _.options.slidesToScroll && (_.slideCount - index) < _.options.slidesToShow) {

                    allSlides
                        .slice(indexOffset - (_.options.slidesToShow - remainder), indexOffset + remainder)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                } else {

                    allSlides
                        .slice(indexOffset, indexOffset + _.options.slidesToShow)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                }

            }

        }

        if (_.options.lazyLoad === 'ondemand' || _.options.lazyLoad === 'anticipated') {
            _.lazyLoad();
        }
    };

    Slick.prototype.setupInfinite = function() {

        var _ = this,
            i, slideIndex, infiniteCount;

        if (_.options.fade === true) {
            _.options.centerMode = false;
        }

        if (_.options.infinite === true && _.options.fade === false) {

            slideIndex = null;

            if (_.slideCount > _.options.slidesToShow) {

                if (_.options.centerMode === true) {
                    infiniteCount = _.options.slidesToShow + 1;
                } else {
                    infiniteCount = _.options.slidesToShow;
                }

                for (i = _.slideCount; i > (_.slideCount -
                        infiniteCount); i -= 1) {
                    slideIndex = i - 1;
                    $(_.$slides[slideIndex]).clone(true).attr('id', '')
                        .attr('data-slick-index', slideIndex - _.slideCount)
                        .prependTo(_.$slideTrack).addClass('slick-cloned');
                }
                for (i = 0; i < infiniteCount  + _.slideCount; i += 1) {
                    slideIndex = i;
                    $(_.$slides[slideIndex]).clone(true).attr('id', '')
                        .attr('data-slick-index', slideIndex + _.slideCount)
                        .appendTo(_.$slideTrack).addClass('slick-cloned');
                }
                _.$slideTrack.find('.slick-cloned').find('[id]').each(function() {
                    $(this).attr('id', '');
                });

            }

        }

    };

    Slick.prototype.interrupt = function( toggle ) {

        var _ = this;

        if( !toggle ) {
            _.autoPlay();
        }
        _.interrupted = toggle;

    };

    Slick.prototype.selectHandler = function(event) {

        var _ = this;

        var targetElement =
            $(event.target).is('.slick-slide') ?
                $(event.target) :
                $(event.target).parents('.slick-slide');

        var index = parseInt(targetElement.attr('data-slick-index'));

        if (!index) index = 0;

        if (_.slideCount <= _.options.slidesToShow) {

            _.slideHandler(index, false, true);
            return;

        }

        _.slideHandler(index);

    };

    Slick.prototype.slideHandler = function(index, sync, dontAnimate) {

        var targetSlide, animSlide, oldSlide, slideLeft, targetLeft = null,
            _ = this, navTarget;

        sync = sync || false;

        if (_.animating === true && _.options.waitForAnimate === true) {
            return;
        }

        if (_.options.fade === true && _.currentSlide === index) {
            return;
        }

        if (sync === false) {
            _.asNavFor(index);
        }

        targetSlide = index;
        targetLeft = _.getLeft(targetSlide);
        slideLeft = _.getLeft(_.currentSlide);

        _.currentLeft = _.swipeLeft === null ? slideLeft : _.swipeLeft;

        if (_.options.infinite === false && _.options.centerMode === false && (index < 0 || index > _.getDotCount() * _.options.slidesToScroll)) {
            if (_.options.fade === false) {
                targetSlide = _.currentSlide;
                if (dontAnimate !== true && _.slideCount > _.options.slidesToShow) {
                    _.animateSlide(slideLeft, function() {
                        _.postSlide(targetSlide);
                    });
                } else {
                    _.postSlide(targetSlide);
                }
            }
            return;
        } else if (_.options.infinite === false && _.options.centerMode === true && (index < 0 || index > (_.slideCount - _.options.slidesToScroll))) {
            if (_.options.fade === false) {
                targetSlide = _.currentSlide;
                if (dontAnimate !== true && _.slideCount > _.options.slidesToShow) {
                    _.animateSlide(slideLeft, function() {
                        _.postSlide(targetSlide);
                    });
                } else {
                    _.postSlide(targetSlide);
                }
            }
            return;
        }

        if ( _.options.autoplay ) {
            clearInterval(_.autoPlayTimer);
        }

        if (targetSlide < 0) {
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = _.slideCount - (_.slideCount % _.options.slidesToScroll);
            } else {
                animSlide = _.slideCount + targetSlide;
            }
        } else if (targetSlide >= _.slideCount) {
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = 0;
            } else {
                animSlide = targetSlide - _.slideCount;
            }
        } else {
            animSlide = targetSlide;
        }

        _.animating = true;

        _.$slider.trigger('beforeChange', [_, _.currentSlide, animSlide]);

        oldSlide = _.currentSlide;
        _.currentSlide = animSlide;

        _.setSlideClasses(_.currentSlide);

        if ( _.options.asNavFor ) {

            navTarget = _.getNavTarget();
            navTarget = navTarget.slick('getSlick');

            if ( navTarget.slideCount <= navTarget.options.slidesToShow ) {
                navTarget.setSlideClasses(_.currentSlide);
            }

        }

        _.updateDots();
        _.updateArrows();

        if (_.options.fade === true) {
            if (dontAnimate !== true) {

                _.fadeSlideOut(oldSlide);

                _.fadeSlide(animSlide, function() {
                    _.postSlide(animSlide);
                });

            } else {
                _.postSlide(animSlide);
            }
            _.animateHeight();
            return;
        }

        if (dontAnimate !== true && _.slideCount > _.options.slidesToShow) {
            _.animateSlide(targetLeft, function() {
                _.postSlide(animSlide);
            });
        } else {
            _.postSlide(animSlide);
        }

    };

    Slick.prototype.startLoad = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow.hide();
            _.$nextArrow.hide();

        }

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$dots.hide();

        }

        _.$slider.addClass('slick-loading');

    };

    Slick.prototype.swipeDirection = function() {

        var xDist, yDist, r, swipeAngle, _ = this;

        xDist = _.touchObject.startX - _.touchObject.curX;
        yDist = _.touchObject.startY - _.touchObject.curY;
        r = Math.atan2(yDist, xDist);

        swipeAngle = Math.round(r * 180 / Math.PI);
        if (swipeAngle < 0) {
            swipeAngle = 360 - Math.abs(swipeAngle);
        }

        if ((swipeAngle <= 45) && (swipeAngle >= 0)) {
            return (_.options.rtl === false ? 'left' : 'right');
        }
        if ((swipeAngle <= 360) && (swipeAngle >= 315)) {
            return (_.options.rtl === false ? 'left' : 'right');
        }
        if ((swipeAngle >= 135) && (swipeAngle <= 225)) {
            return (_.options.rtl === false ? 'right' : 'left');
        }
        if (_.options.verticalSwiping === true) {
            if ((swipeAngle >= 35) && (swipeAngle <= 135)) {
                return 'down';
            } else {
                return 'up';
            }
        }

        return 'vertical';

    };

    Slick.prototype.swipeEnd = function(event) {

        var _ = this,
            slideCount,
            direction;

        _.dragging = false;
        _.swiping = false;

        if (_.scrolling) {
            _.scrolling = false;
            return false;
        }

        _.interrupted = false;
        _.shouldClick = ( _.touchObject.swipeLength > 10 ) ? false : true;

        if ( _.touchObject.curX === undefined ) {
            return false;
        }

        if ( _.touchObject.edgeHit === true ) {
            _.$slider.trigger('edge', [_, _.swipeDirection() ]);
        }

        if ( _.touchObject.swipeLength >= _.touchObject.minSwipe ) {

            direction = _.swipeDirection();

            switch ( direction ) {

                case 'left':
                case 'down':

                    slideCount =
                        _.options.swipeToSlide ?
                            _.checkNavigable( _.currentSlide + _.getSlideCount() ) :
                            _.currentSlide + _.getSlideCount();

                    _.currentDirection = 0;

                    break;

                case 'right':
                case 'up':

                    slideCount =
                        _.options.swipeToSlide ?
                            _.checkNavigable( _.currentSlide - _.getSlideCount() ) :
                            _.currentSlide - _.getSlideCount();

                    _.currentDirection = 1;

                    break;

                default:


            }

            if( direction != 'vertical' ) {

                _.slideHandler( slideCount );
                _.touchObject = {};
                _.$slider.trigger('swipe', [_, direction ]);

            }

        } else {

            if ( _.touchObject.startX !== _.touchObject.curX ) {

                _.slideHandler( _.currentSlide );
                _.touchObject = {};

            }

        }

    };

    Slick.prototype.swipeHandler = function(event) {

        var _ = this;

        if ((_.options.swipe === false) || ('ontouchend' in document && _.options.swipe === false)) {
            return;
        } else if (_.options.draggable === false && event.type.indexOf('mouse') !== -1) {
            return;
        }

        _.touchObject.fingerCount = event.originalEvent && event.originalEvent.touches !== undefined ?
            event.originalEvent.touches.length : 1;

        _.touchObject.minSwipe = _.listWidth / _.options
            .touchThreshold;

        if (_.options.verticalSwiping === true) {
            _.touchObject.minSwipe = _.listHeight / _.options
                .touchThreshold;
        }

        switch (event.data.action) {

            case 'start':
                _.swipeStart(event);
                break;

            case 'move':
                _.swipeMove(event);
                break;

            case 'end':
                _.swipeEnd(event);
                break;

        }

    };

    Slick.prototype.swipeMove = function(event) {

        var _ = this,
            edgeWasHit = false,
            curLeft, swipeDirection, swipeLength, positionOffset, touches, verticalSwipeLength;

        touches = event.originalEvent !== undefined ? event.originalEvent.touches : null;

        if (!_.dragging || _.scrolling || touches && touches.length !== 1) {
            return false;
        }

        curLeft = _.getLeft(_.currentSlide);

        _.touchObject.curX = touches !== undefined ? touches[0].pageX : event.clientX;
        _.touchObject.curY = touches !== undefined ? touches[0].pageY : event.clientY;

        _.touchObject.swipeLength = Math.round(Math.sqrt(
            Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));

        verticalSwipeLength = Math.round(Math.sqrt(
            Math.pow(_.touchObject.curY - _.touchObject.startY, 2)));

        if (!_.options.verticalSwiping && !_.swiping && verticalSwipeLength > 4) {
            _.scrolling = true;
            return false;
        }

        if (_.options.verticalSwiping === true) {
            _.touchObject.swipeLength = verticalSwipeLength;
        }

        swipeDirection = _.swipeDirection();

        if (event.originalEvent !== undefined && _.touchObject.swipeLength > 4) {
            _.swiping = true;
            event.preventDefault();
        }

        positionOffset = (_.options.rtl === false ? 1 : -1) * (_.touchObject.curX > _.touchObject.startX ? 1 : -1);
        if (_.options.verticalSwiping === true) {
            positionOffset = _.touchObject.curY > _.touchObject.startY ? 1 : -1;
        }


        swipeLength = _.touchObject.swipeLength;

        _.touchObject.edgeHit = false;

        if (_.options.infinite === false) {
            if ((_.currentSlide === 0 && swipeDirection === 'right') || (_.currentSlide >= _.getDotCount() && swipeDirection === 'left')) {
                swipeLength = _.touchObject.swipeLength * _.options.edgeFriction;
                _.touchObject.edgeHit = true;
            }
        }

        if (_.options.vertical === false) {
            _.swipeLeft = curLeft + swipeLength * positionOffset;
        } else {
            _.swipeLeft = curLeft + (swipeLength * (_.$list.height() / _.listWidth)) * positionOffset;
        }
        if (_.options.verticalSwiping === true) {
            _.swipeLeft = curLeft + swipeLength * positionOffset;
        }

        if (_.options.fade === true || _.options.touchMove === false) {
            return false;
        }

        if (_.animating === true) {
            _.swipeLeft = null;
            return false;
        }

        _.setCSS(_.swipeLeft);

    };

    Slick.prototype.swipeStart = function(event) {

        var _ = this,
            touches;

        _.interrupted = true;

        if (_.touchObject.fingerCount !== 1 || _.slideCount <= _.options.slidesToShow) {
            _.touchObject = {};
            return false;
        }

        if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
            touches = event.originalEvent.touches[0];
        }

        _.touchObject.startX = _.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
        _.touchObject.startY = _.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;

        _.dragging = true;

    };

    Slick.prototype.unfilterSlides = Slick.prototype.slickUnfilter = function() {

        var _ = this;

        if (_.$slidesCache !== null) {

            _.unload();

            _.$slideTrack.children(this.options.slide).detach();

            _.$slidesCache.appendTo(_.$slideTrack);

            _.reinit();

        }

    };

    Slick.prototype.unload = function() {

        var _ = this;

        $('.slick-cloned', _.$slider).remove();

        if (_.$dots) {
            _.$dots.remove();
        }

        if (_.$prevArrow && _.htmlExpr.test(_.options.prevArrow)) {
            _.$prevArrow.remove();
        }

        if (_.$nextArrow && _.htmlExpr.test(_.options.nextArrow)) {
            _.$nextArrow.remove();
        }

        _.$slides
            .removeClass('slick-slide slick-active slick-visible slick-current')
            .attr('aria-hidden', 'true')
            .css('width', '');

    };

    Slick.prototype.unslick = function(fromBreakpoint) {

        var _ = this;
        _.$slider.trigger('unslick', [_, fromBreakpoint]);
        _.destroy();

    };

    Slick.prototype.updateArrows = function() {

        var _ = this,
            centerOffset;

        centerOffset = Math.floor(_.options.slidesToShow / 2);

        if ( _.options.arrows === true &&
            _.slideCount > _.options.slidesToShow &&
            !_.options.infinite ) {

            _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
            _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            if (_.currentSlide === 0) {

                _.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow && _.options.centerMode === false) {

                _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            } else if (_.currentSlide >= _.slideCount - 1 && _.options.centerMode === true) {

                _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            }

        }

    };

    Slick.prototype.updateDots = function() {

        var _ = this;

        if (_.$dots !== null) {

            _.$dots
                .find('li')
                    .removeClass('slick-active')
                    .end();

            _.$dots
                .find('li')
                .eq(Math.floor(_.currentSlide / _.options.slidesToScroll))
                .addClass('slick-active');

        }

    };

    Slick.prototype.visibility = function() {

        var _ = this;

        if ( _.options.autoplay ) {

            if ( document[_.hidden] ) {

                _.interrupted = true;

            } else {

                _.interrupted = false;

            }

        }

    };

    $.fn.slick = function() {
        var _ = this,
            opt = arguments[0],
            args = Array.prototype.slice.call(arguments, 1),
            l = _.length,
            i,
            ret;
        for (i = 0; i < l; i++) {
            if (typeof opt == 'object' || typeof opt == 'undefined')
                _[i].slick = new Slick(_[i], opt);
            else
                ret = _[i].slick[opt].apply(_[i].slick, args);
            if (typeof ret != 'undefined') return ret;
        }
        return _;
    };

}));
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _equalHeights = __webpack_require__(1);

	var _equalHeights2 = _interopRequireDefault(_equalHeights);

	var _touchHelper = __webpack_require__(2);

	var _touchHelper2 = _interopRequireDefault(_touchHelper);

	var _header = __webpack_require__(3);

	var _header2 = _interopRequireDefault(_header);

	var _slider = __webpack_require__(5);

	var _slider2 = _interopRequireDefault(_slider);

	var _accordion = __webpack_require__(6);

	var _accordion2 = _interopRequireDefault(_accordion);

	var _rteStyles = __webpack_require__(7);

	var _rteStyles2 = _interopRequireDefault(_rteStyles);

	var _peoplePopup = __webpack_require__(8);

	var _peoplePopup2 = _interopRequireDefault(_peoplePopup);

	var _footer = __webpack_require__(9);

	var _footer2 = _interopRequireDefault(_footer);

	var _peoplePage = __webpack_require__(10);

	var _peoplePage2 = _interopRequireDefault(_peoplePage);

	var _navigation = __webpack_require__(11);

	var _navigation2 = _interopRequireDefault(_navigation);

	var _animations = __webpack_require__(12);

	var _animations2 = _interopRequireDefault(_animations);

	var _video = __webpack_require__(13);

	var _video2 = _interopRequireDefault(_video);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	$(function () {
		_touchHelper2.default.init();
		_equalHeights2.default.init();
		_header2.default.init();
		_slider2.default.init();
		_accordion2.default.init();
		_rteStyles2.default.init();
		_peoplePopup2.default.init();
		_footer2.default.init();
		_peoplePage2.default.init();
		_navigation2.default.init();
		_video2.default.init();
	});

	$(window).on('load', function () {
		$('html').addClass('loaded');
		_equalHeights2.default.init();
	});

	$(window).on('load scroll', function () {
		$('html').addClass('loaded');
		_animations2.default.init();
	});

	$(window).on('resize', function () {
		_equalHeights2.default.init();
	});

/***/ }),
/* 1 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var equalHeights = {

		init: function init() {

			this.setModuleTitles();
		},

		setModuleTitles: function setModuleTitles() {
			if ($(window).width() >= 970 && $('.js-equal-titles').length) {
				this.setHeights($('.js-equal-titles'), 3);
			} else {
				$('.js-equal-titles').removeAttr('style');
			}
		},

		setHeights: function setHeights(arrayItems, count) {
			if (arrayItems !== undefined && arrayItems.length > 0) {
				arrayItems.height('');

				var maxH = 0;

				if (count) {
					var arrays = [];
					while (arrayItems.length > 0) {
						arrays.push(arrayItems.splice(0, count));
					}

					for (var i = 0; i < arrays.length; i += 1) {
						var data = arrays[i];
						maxH = 0;
						for (var j = 0; j < data.length; j += 1) {
							var currentH = $(data[j]).outerHeight();
							if (currentH > maxH) {
								maxH = currentH;
							}
						}

						for (var k = 0; k < data.length; k += 1) {
							$(data[k]).css('height', maxH);
						}
					}
				} else {
					arrayItems.each(function () {
						var currentH2 = $(this).outerHeight();
						if (currentH2 > maxH) {
							maxH = currentH2;
						}
					});

					arrayItems.css('height', maxH);
				}
			}
		}
	};

	exports.default = equalHeights;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var touchHelper = {
		init: function init() {
			if (('ontouchstart' in window || navigator.msMaxTouchPoints > 0) && window.matchMedia('screen and (max-width: 1199px)').matches) {
				$('html').addClass('touch');
			} else {
				$('html').addClass('no-touch');
			}
		}
	};

	exports.default = touchHelper;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _scrollDisable = __webpack_require__(4);

	var _scrollDisable2 = _interopRequireDefault(_scrollDisable);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var header = {
		header: $('.header'),
		headerScrolledClass: 'header--scrolled',
		headerTransparentClass: 'header--transparent',
		headerHomepageClass: 'header--homepage',
		headerDarkClass: 'header--dark',
		headerReversedClass: 'header--reversed',
		navToggle: $('.js-nav-toggle'),
		primaryNavLink: $('.main-nav__link'),
		secondaryNavLink: $('.main-nav__secondary-link'),
		secondaryBackButton: $('.js-secondary-back-button'),
		navBackButton: $('.js-nav-back-button'),
		navClose: $('.js-close-navigation'),
		$navFull: $('.navigation-full .navigation'),
		isScrolledReversed: false,
		headerSimple: $('.header--simple'),

		init: function init() {
			var _this = this;

			// this.megamenuNavigation();
			this.mobileNavigation();
			this.navToggleFunction();

			this.navClose.on('click', function () {
				_this.header.removeClass('navigation-opened');
				_this.navToggle.removeClass('nav-toggle--opened');
				_scrollDisable2.default.enableScroll();
			});

			$(window).on('load resize', function () {
				_this.customNavigationScrollbar();
			});

			$(window).on('scroll', function () {
				_this.checkHeaderPosition();
				var scroll = $(window).scrollTop();
				if (scroll > 0) {
					_this.header.addClass(_this.headerScrolledClass);
				} else {
					_this.header.removeClass(_this.headerScrolledClass);
				}
			});
		},
		checkHeaderPosition: function checkHeaderPosition() {
			if (this.header.hasClass(this.headerTransparentClass) && this.header.hasClass(this.headerSimple) && this.header.hasClass(this.headerHomepageClass) && $(window).scrollTop() > 0) {
				console.log('test');
				this.header.addClass(this.headerScrolledClass);
				if (!this.isScrolledReversed && this.header.hasClass(this.headerReversedClass)) {
					if (this.header.hasClass(this.headerDarkClass)) {
						this.header.removeClass(this.headerDarkClass);
					} else {
						this.header.addClass(this.headerDarkClass);
					}
					this.isScrolledReversed = true;
				}
			} else {
				this.header.removeClass(this.headerScrolledClass);
				if (this.isScrolledReversed && this.header.hasClass(this.headerReversedClass)) {
					if (!this.header.hasClass(this.headerDarkClass)) {
						this.header.addClass(this.headerDarkClass);
					} else {
						this.header.removeClass(this.headerDarkClass);
					}
					this.isScrolledReversed = false;
				}
			}
		},

		navToggleFunction: function navToggleFunction() {
			this.navToggle.on('click', function (event) {
				var $eventCurrentTarget = $(event.currentTarget);
				var $header = $('.header');
				if ($eventCurrentTarget.hasClass('nav-toggle--opened')) {
					$eventCurrentTarget.removeClass('nav-toggle--opened');
					$header.removeClass('navigation-opened primary-navigation-opened secondary-navigation-opened');
					$('.header .main-nav__list').find('.main-nav__item--opened').removeClass('main-nav__item--opened');
					_scrollDisable2.default.enableScroll();
				} else {
					$eventCurrentTarget.addClass('nav-toggle--opened');
					$header.addClass('navigation-opened');
					_scrollDisable2.default.disableScroll();
				}
			});
		},

		megamenuNavigation: function megamenuNavigation() {
			this.secondaryNavLink.on('click', function (e) {
				var $linkParent = $(this).parent();
				var $linkParentWrap = $linkParent.parent();

				if ($(window).width() >= 1366 && $linkParent.hasClass('main-nav__has-children') && !$linkParent.hasClass('main-nav__item--opened')) {
					e.preventDefault();
					// Toggle secondary nav dropdown - on desktop devices
					$linkParent.toggleClass('main-nav__item--opened');
					$linkParentWrap.toggleClass('main-nav__list-opened');
				}
			});

			this.secondaryBackButton.on('click', function () {
				$(this).parent().removeClass('main-nav__item--opened');
				$(this).closest('.main-nav__list-opened').removeClass('main-nav__list-opened');
			});
		},

		mobileNavigation: function mobileNavigation() {
			var _this2 = this;

			this.primaryNavLink.on('click', function (e) {
				var $linkParent = $(this).parent();
				if ($(window).width() < 1366 && $linkParent.hasClass('main-nav__has-children') && !$linkParent.hasClass('main-nav__item--opened')) {
					e.preventDefault();
					if (!$linkParent.hasClass('main-nav__item--opened')) {
						$('.header').addClass('primary-navigation-opened');
						$linkParent.addClass('main-nav__item--opened');
					}
				}
				if ($(window).width() < 1366 && $('.header').hasClass('secondary-navigation-opened')) {
					e.preventDefault();
					$('.header').removeClass('secondary-navigation-opened');
					$(this).next().find('.main-nav__item--opened').removeClass('main-nav__item--opened');
				}
			});

			this.secondaryNavLink.on('click', function (e) {
				var $linkParent = $(this).parent();
				if ($(window).width() < 1366 && !$('.header').hasClass('secondary-navigation-opened') && $linkParent.hasClass('main-nav__has-children') && !$linkParent.hasClass('main-nav__item--opened')) {
					e.preventDefault();
					if (!$linkParent.hasClass('main-nav__item--opened')) {
						$('.header').addClass('secondary-navigation-opened');
						$linkParent.addClass('main-nav__item--opened');
					}
				}
			});

			this.navBackButton.on('click', function () {
				if (_this2.header.hasClass('secondary-navigation-opened')) {
					_this2.header.removeClass('secondary-navigation-opened');
					$('.header .main-nav__list').find('.main-nav__secondary-item.main-nav__item--opened').removeClass('main-nav__item--opened');
				} else {
					_this2.header.removeClass('primary-navigation-opened');
					$('.header .main-nav__list').find('.main-nav__item--opened').removeClass('main-nav__item--opened');
				}
			});
		},

		customNavigationScrollbar: function customNavigationScrollbar() {
			if ($(window).width() > 1365) {
				this.$navFull.mCustomScrollbar({
					scrollInertia: 500
				});
			} else {
				this.$navFull.mCustomScrollbar('destroy');
			}
		}
	};

	exports.default = header;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var scrollDisable = {
		topScroll: 0,
		isScrollDisabled: false,

		disableScroll: function disableScroll() {
			if (!this.isScrollDisabled) {
				this.topScroll = $(window).scrollTop();
				$('body').css('top', -this.topScroll + 'px').addClass('scroll-disabled');
				this.isScrollDisabled = true;
			}
		},

		enableScroll: function enableScroll() {
			$('body').removeAttr('style').removeClass('scroll-disabled');
			$(window).scrollTop(this.topScroll);
			this.isScrollDisabled = false;
		}
	};

	exports.default = scrollDisable;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var slider = {
		sliderInvest: $('.js-invest-slider'),
		sliderNews: $('.js-news-slider'),
		sliderBanner: $('.js-banner-slider'),

		init: function init() {
			this.newsSlider();
			this.investSlider();
			this.bannerSlider();
		},
		investSlider: function investSlider() {
			this.sliderInvest.slick({
				dots: true,
				arrows: false,
				infinite: false,
				slidesToShow: 1.3,
				centerMode: true,
				draggable: false,
				variableWidth: true,
				centerPadding: '5px',
				mobileFirst: true,
				responsive: [{
					breakpoint: 1025,
					settings: 'unslick'
				}]
			});
		},
		newsSlider: function newsSlider() {
			this.sliderNews.slick({
				slideToShow: 4,
				adaptiveHeight: true,
				infinite: false,
				arrows: false,
				variableWidth: true
			});
		},
		bannerSlider: function bannerSlider() {
			this.sliderBanner.slick({
				arrows: false,
				infinite: false,
				slidesToShow: 1,
				draggable: false,
				variableWidth: true,
				centerPadding: '5px',
				mobileFirst: true,
				responsive: [{
					breakpoint: 650,
					settings: 'unslick'
				}]
			});
		}
	};

	exports.default = slider;

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var accordion = {
		$accordion: $('.accordion'),
		$accBtn: $('.js-acc-btn'),
		$accContent: $('.js-acc-content'),
		accBtnActive: 'accordion__btn--active',
		accContentActive: 'accordion__content--active',

		init: function init() {
			this.bindEvents();
		},

		bindEvents: function bindEvents() {
			var pageLoad = true;
			var _this = this;
			this.$accBtn.each(function () {
				$(this).on('click', function () {
					var _this2 = this;

					if ($(this.nextElementSibling).hasClass(_this.accContentActive) || $(this).hasClass(_this.accBtnActive)) {
						$(this).removeClass(_this.accBtnActive);
						$(this.nextElementSibling).stop().removeClass(_this.accContentActive).slideUp('fast');
					} else {
						_this.$accContent.removeClass(_this.accContentActive);
						_this.$accBtn.next().stop().slideUp('fast');
						$(this.nextElementSibling).stop().slideDown('fast', function () {
							if (!pageLoad) {
								$('html, body').animate({ scrollTop: $(_this2).offset().top - $('.header').height() - 20 }, 400);
							}
						}).addClass(_this.accContentActive);
						_this.$accBtn.removeClass(_this.accBtnActive);
						$(this).addClass(_this.accBtnActive);
					}
				});
			});
			$(window).on('load', function () {
				_this.$accordion.each(function () {
					var $this = $(this);
					if ($this.data('close-first') === 'False') {
						$this.find('.js-acc-btn').first().click();
					}
					setTimeout(function () {
						pageLoad = false;
					}, 1000);
				});
			});
		}
	};

	exports.default = accordion;

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var rteStyles = {

		init: function init() {
			this.cleanRteTables();
			this.rteIframe();
		},

		cleanRteTables: function cleanRteTables() {
			var table = $('.rte table');
			var rteTable = table.wrap('<div class="table"/>');
			table.wrap('<div class="table__wrap"/>');

			rteTable.removeAttr('style');
			rteTable.removeAttr('border');

			$('.table__wrap').on('scroll', function () {
				if ($(this).scrollLeft() + $(this).width() >= $(this).find('table').width()) {
					$(this).parent().addClass('table--scrolled');
				} else {
					$(this).parent().removeClass('table--scrolled');
				}
			});
		},

		rteIframe: function rteIframe() {
			$('.rte').find('iframe').each(function () {
				var $iframe = $(this);
				if ($(window).width() < 768) {
					if ($iframe.data('mobile-height')) {
						$iframe.css('height', $iframe.data('mobile-height'));
					}
				}
				if ($iframe.attr('width') !== '100%') {
					$iframe.css('width', $iframe.attr('width') + 'px');
				}
			});
		}
	};

	exports.default = rteStyles;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _scrollDisable = __webpack_require__(4);

	var _scrollDisable2 = _interopRequireDefault(_scrollDisable);

	var _footer = __webpack_require__(9);

	var _footer2 = _interopRequireDefault(_footer);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var peoplePopup = {
		popupOverlay: $('.js-popup-overlay'),
		popup: $('.js-popup'),
		popupOpenedClass: 'popup-overlay--opened',
		popupCloseBtn: $('.js-close-popup'),
		popupContent: '.popup__content',
		popupArrow: $('.popup__arrow'),
		popupArrowPrev: $('.popup__arrow--previous'),
		popupArrowNext: $('.popup__arrow--next'),
		arrowDisabledClass: 'popup__arrow--disabled',
		popupDots: $('.popup__dots'),
		popupDot: '.popup__dot',
		popupDotActiveClass: 'popup__dot--active',
		$peopleList: $('.people'),
		peopleItem: '.js-people-group',
		peopleDetails: '.people-2__details',
		peopleDetailsImage: '.people-2__details-image',
		peopleDetailsInfo: '.people-2__details-info',
		popupScrollArrow: '.popup__scroll-arrow',
		currentItemActive: 0,

		init: function init() {
			if (!$(this.peopleItem).length) {
				return;
			}
			this.preparePopupLayout();
			this.openPeoplePopup();
			this.bindPopupSwitches();
			this.bindClosePopupEvents();
		},

		preparePopupLayout: function preparePopupLayout() {
			var _this = this;
			var $item = _this.$peopleList.find(_this.peopleItem);
			if ($item.length === 1) {
				_this.popupArrow.remove();
				_this.popupDots.remove();
			} else {
				$item.each(function (i) {
					var itemDotButton = '<button class="popup__dot" type="button">' + (i + 1) + '</button>';
					_this.popupDots.append(itemDotButton);
				});
			}
			$item.each(function (i) {
				$(this).attr('data-person-index', i);
			});
		},

		checkArrowsAvailability: function checkArrowsAvailability(itemIndex) {
			if (itemIndex === 0) {
				this.popupArrowPrev.addClass(this.arrowDisabledClass);
				this.popupArrowNext.removeClass(this.arrowDisabledClass);
			} else if (itemIndex === this.$peopleList.find(this.peopleItem).length - 1) {
				this.popupArrowPrev.removeClass(this.arrowDisabledClass);
				this.popupArrowNext.addClass(this.arrowDisabledClass);
			} else {
				this.popupArrow.removeClass(this.arrowDisabledClass);
			}
		},

		openPeoplePopup: function openPeoplePopup() {
			var _this = this;
			$(_this.peopleItem).find('a').on('click', function (e) {
				var personIdx = $(this).closest(_this.peopleItem).data('person-index');

				_this.checkArrowsAvailability(personIdx);
				_this.replacePopupContent(personIdx);

				_scrollDisable2.default.disableScroll();
				e.preventDefault();
			});
		},

		replacePopupContent: function replacePopupContent(personIndex) {
			var _this2 = this;

			var $popup = this.popupOverlay;
			var $person = $(this.peopleItem).eq(personIndex);
			var personDetails = $person.find(this.peopleDetails).html();
			this.currentItemActive = personIndex;

			$popup.find(this.popupContent).html('');

			if (!$popup.hasClass(this.popupOpenedClass)) {
				$popup.stop().fadeIn(500).addClass(this.popupOpenedClass).removeAttr('aria-hidden');
			}
			this.popupDots.find('.' + this.popupDotActiveClass).removeClass(this.popupDotActiveClass);
			$(this.popupDot).eq(personIndex).addClass(this.popupDotActiveClass);
			$popup.find(this.popupContent).html(personDetails);
			$popup.find(this.popupContent).animate({ scrollTop: 0 }, 10);

			setTimeout(function () {
				_this2.checkBouncingArrow();
			}, 100);
		},

		bindClosePopupEvents: function bindClosePopupEvents() {
			var _this = this;

			// Close popup on click on X, overlay and Escape key
			_this.popupCloseBtn.on('click', function () {
				_this.closePopup();
			});

			_this.popupOverlay.on('click', function (e) {
				if (e.target.parentElement.classList.contains('js-close-popup') || e.target !== this) return;
				_this.closePopup();
			});

			$(document).off('keydown').on('keydown', function (e) {
				if (e.which !== 27) {
					return;
				}
				_this.closePopup();
			});
		},

		closePopup: function closePopup() {
			var _this3 = this;

			this.popupOverlay.stop().fadeOut(500, function () {
				_this3.popupOverlay.find(_this3.popupContent).html('');
			}).removeClass(this.popupOpenedClass).attr('aria-hidden', 'true');
			_scrollDisable2.default.enableScroll();
			_footer2.default.checkFooter();
		},

		bindPopupSwitches: function bindPopupSwitches() {
			var _this = this;
			_this.popupArrow.off('click').on('click', function () {
				var $arrow = $(this);
				if (!$arrow.hasClass(_this.arrowDisabledClass)) {
					if ($arrow.is(_this.popupArrowPrev)) {
						_this.currentItemActive--;
					} else {
						_this.currentItemActive++;
					}
					_this.checkArrowsAvailability(_this.currentItemActive);
					_this.replacePopupContent(_this.currentItemActive);
					setTimeout(function () {
						_this.checkBouncingArrow();
					}, 100);
				}
			});

			$(_this.popupDot).off('click').on('click', function () {
				var $dot = $(this);
				_this.currentItemActive = $dot.index();
				_this.checkArrowsAvailability(_this.currentItemActive);
				_this.replacePopupContent(_this.currentItemActive);
			});
		},

		checkBouncingArrow: function checkBouncingArrow() {
			var $popup = this.popupOverlay;
			var popupContentHeight = Math.floor($popup.find(this.popupContent).height());
			var popupImageHeight = Math.floor($popup.find(this.peopleDetailsImage).outerHeight());
			var popupInfoHeight = Math.floor($popup.find(this.peopleDetailsInfo)[0].scrollHeight);

			var _this = this;

			if ($(window).width() < 768) {
				if (popupImageHeight + popupInfoHeight > popupContentHeight) {
					$popup.find(this.popupScrollArrow).stop().show();
				} else {
					$popup.find(this.popupScrollArrow).stop().hide();
				}
			} else {
				if (popupInfoHeight > popupContentHeight) {
					$popup.find(this.popupScrollArrow).stop().show();
				} else {
					$popup.find(this.popupScrollArrow).stop().hide();
				}
			}

			$popup.find(_this.popupContent).on('scroll', function () {
				var scr = $(this).scrollTop();

				if ($(window).width() < 768) {
					if (scr + popupContentHeight >= popupImageHeight + popupInfoHeight) {
						$popup.find(_this.popupScrollArrow).stop().fadeOut();
					} else {
						$popup.find(_this.popupScrollArrow).stop().fadeIn();
					}
				} else {
					if (scr + popupContentHeight >= popupInfoHeight) {
						$popup.find(_this.popupScrollArrow).stop().fadeOut();
					} else {
						$popup.find(_this.popupScrollArrow).stop().fadeIn();
					}
				}
			});
		}
	};

	exports.default = peoplePopup;

/***/ }),
/* 9 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var footer = {

		init: function init() {
			var _this = this;

			this.autoFillEmailInput();
			this.checkFooter();
			$(window).on('resize', function () {
				_this.checkFooter();
			});
		},

		autoFillEmailInput: function autoFillEmailInput() {
			var $inputEmail = $('form #EmailAddress.form__input');
			if ($inputEmail.length !== 0) {
				var email = $('.main-content').data('email-input');
				$inputEmail.val(email);
			}
		},

		checkFooter: function checkFooter() {
			if ($('.popup-overlay--opened').length !== 0) {
				return;
			}
			if ($('body').height() < $(window).height() + 1) {
				var footerHeight = $('.footer').height() + 'px';
				console.log(footerHeight);
				$('.main').css({
					'height': 'calc(100vh - ' + footerHeight + ')'
				});
			} else {
				$('.main').removeAttr('style');
			}
		}
	};

	exports.default = footer;

/***/ }),
/* 10 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var peoplePage = {

		init: function init() {
			this.personPopupAutoOpen();
		},

		personPopupAutoOpen: function personPopupAutoOpen() {
			if (!String.prototype.endsWith) {
				String.prototype.endsWith = function (search, this_len) {
					if (this_len === undefined || this_len > this.length) {
						this_len = this.length;
					}
					return this.substring(this_len - search.length, this_len) === search;
				};
			}
			var locPath = location.pathname;
			if (!locPath.endsWith('/')) {
				locPath += '/';
			}

			var $person = $('.people-2__group[data-url="' + locPath + '"]');

			if ($person !== undefined && $person !== null) {
				setTimeout(function () {
					$person.find('a').click();
				}, 500);
			}
		}
	};

	exports.default = peoplePage;

/***/ }),
/* 11 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var navigation = {
		$navLink: $('.navigation-hover .main-nav__item.main-nav__has-children'),
		$sideNavLink: $('.sidenav__has-children'),
		$sideNavSecondaryLink: $('.sidenav-secondary__has-children'),

		init: function init() {
			this.mainNavHover();
			this.sideNav();
		},

		mainNavHover: function mainNavHover() {
			if ($(window).width() > 1200) {
				this.$navLink.on('mouseenter', function () {
					$('.main-nav__item.main-nav__has-children').not($(this)).removeClass('main-nav__item--hover');
					$(this).addClass('main-nav__item--hover');
				});
				this.$navLink.on('mouseleave', function () {
					var $this = $(this);
					setTimeout(function () {
						$this.removeClass('main-nav__item--hover');
					}, 600);
				});
			}
		},
		sideNav: function sideNav() {
			this.$sideNavLink.on('click', function () {
				$('.sidenav__secondary-list').toggle();
				$('.sidenav__tertiary-list').toggle();
				$(this).toggleClass('sidenav__link--active');
			});
		}
	};

	exports.default = navigation;

/***/ }),
/* 12 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	var animations = {

		$fadeElement: $('.js-fade-element'),
		$drawLine: $('.js-draw-line'),
		$newsCards: $('.js-news-cards'),
		$riseCards: $('.js-rise-cards'),

		elementFadeClass: 'fade--finish',
		lineDrawClass: 'draw--finish',
		cardsSlideClass: 'news-cards--slide',
		riseCardsVisibleClass: 'rise-card--visible',

		init: function init() {
			this.fadeElement();
			this.lineDraw();
			this.newsCardsAnimation();
			this.riseCards();
		},

		fadeElement: function fadeElement() {
			var _this = this;

			if (this.$fadeElement.length) {
				var src = $(window).scrollTop();
				var windowHeight = $(window).height();
				this.$fadeElement.each(function (index, element) {
					var $element = $(element);
					var offTop = $element.offset().top;
					var animationDelay = $element.data('animation-delay');

					if (src + windowHeight * 0.7 > offTop || src + windowHeight > offTop + $element.height()) {
						if (animationDelay) {
							$element.css('transition-delay', animationDelay + 'ms');
						}
						$element.addClass(_this.elementFadeClass);
					}
				});
			}
		},

		lineDraw: function lineDraw() {
			var _this2 = this;

			if (this.$drawLine.length) {
				var src = $(window).scrollTop();
				var windowHeight = $(window).height();
				this.$drawLine.each(function (index, element) {
					var $element = $(element);
					var offTop = $element.offset().top;

					if (src + windowHeight * 0.7 > offTop || src + windowHeight > offTop + $element.height()) {
						$('.logo-intro__line1').addClass(_this2.lineDrawClass);
						$('.logo-intro__line2').addClass(_this2.lineDrawClass);
						$('.logo-intro__line3').addClass(_this2.lineDrawClass);
						$('.logo-intro__line4').addClass(_this2.lineDrawClass);
					}
				});
			}
		},

		newsCardsAnimation: function newsCardsAnimation() {
			var _this3 = this;

			if (this.$newsCards.length) {
				var src = $(window).scrollTop();
				var windowHeight = $(window).height();
				this.$newsCards.each(function (index, element) {
					var $element = $(element);
					var offTop = $element.offset().top;

					if (src + windowHeight * 0.7 > offTop || src + windowHeight > offTop + $element.height()) {
						$element.addClass(_this3.cardsSlideClass);
					}
				});
			}
		},

		riseCards: function riseCards() {
			var _this4 = this;

			if (this.$riseCards.length) {
				var src = $(window).scrollTop();
				var windowHeight = $(window).height();
				this.$riseCards.each(function (index, element) {
					var $element = $(element);
					var offTop = $element.offset().top;
					var animationDelay = $element.data('animation-delay');

					if (src + windowHeight * 0.7 > offTop || src + windowHeight > offTop + $element.height()) {
						if (animationDelay) {
							$element.css('transition-delay', animationDelay + 'ms');
						}
						$element.addClass(_this4.riseCardsVisibleClass);
					}
				});
			}
		}
	};

	exports.default = animations;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _plyr = __webpack_require__(14);

	var _plyr2 = _interopRequireDefault(_plyr);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var video = {

		$bannerCardVideoSelector: $('.js-cta-video'),
		$exploreCardVideoSelector: $('.js-explore-video'),
		$bannerCardEmbededVideoSelector: $('.cta-card__embeded-video'),

		init: function init() {
			this.ambientVideo();
			this.bannerCardVideoPlayOnHover();
			this.bannerCardEmbededVideoPlayOnHover();
			this.cardEmbededVideoPlayOnHover();
			this.cardVideoPlayOnHover();
		},

		ambientVideo: function ambientVideo() {
			var player = new _plyr2.default('#banner__embeded-video', {
				controls: ['play-large'],
				vimeo: {
					byline: false,
					portrait: false,
					title: false,
					speed: true,
					transparent: false
				},
				muted: true,
				hideControls: true,
				resetOnEnd: true,
				autopause: false,
				autoplay: true
			});
		},

		// function for video HTML
		bannerCardVideoPlayOnHover: function bannerCardVideoPlayOnHover() {
			this.$bannerCardVideoSelector.each(function (index, element) {
				var $element = $(element);

				$element.on('mouseover', function () {
					$(this).parent().find('video')[0].play();
				});
			});
		},
		// function for video HTML
		cardVideoPlayOnHover: function cardVideoPlayOnHover() {
			this.$exploreCardVideoSelector.each(function (index, element) {
				var $element = $(element);

				$element.on('mouseover', function () {
					$(this).find('video')[0].play();
				});
			});
		},

		// function for card embeded video
		bannerCardEmbededVideoPlayOnHover: function bannerCardEmbededVideoPlayOnHover() {
			var player = {};
			var playerNodes = $('.js-card-video');

			playerNodes.each(function (_, element) {
				var id = $(element).find('iframe').attr('src').split('?')[0];
				var encodedId = btoa(id);
				console.log('1', id, encodedId);

				player[encodedId] = new _plyr2.default(element, {
					enabled: true,
					volume: 0,
					autopause: false,
					controls: false,
					captions: { active: true },
					muted: true,
					loop: {
						active: true
					}
				});
			});

			$('.js-cta-embeded-video').hover(function () {
				var id = $(this).parents('.js-card-parent').find('iframe').attr('src').split('?')[0];
				var encodedId = btoa(id);
				console.log('2', player[encodedId], encodedId, id);
				player[encodedId].play();
			});
		},

		// function for card embeded video
		cardEmbededVideoPlayOnHover: function cardEmbededVideoPlayOnHover() {
			var player = {};
			var playerNodes = $('.js-explore-card-video');

			playerNodes.each(function (_, element) {
				var id = $(element).find('iframe').attr('src').split('?')[0];
				var encodedId = btoa(id);
				console.log('1', id, encodedId);

				player[encodedId] = new _plyr2.default(element, {
					enabled: true,
					volume: 0,
					autopause: false,
					controls: false,
					captions: { active: true },
					muted: true,
					loop: {
						active: true
					}
				});
			});

			$('.js-card-parent-video').hover(function () {
				var id = $(this).find('iframe').attr('src').split('?')[0];
				var encodedId = btoa(id);
				console.log('2', player[encodedId], encodedId, id);
				player[encodedId].play();
			});
		}

	};

	exports.default = video;

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(global) {"use strict";

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

	"object" == (typeof navigator === "undefined" ? "undefined" : _typeof(navigator)) && function (e, t) {
	  "object" == ( false ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = t() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (t), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : (e = e || self).Plyr = t();
	}(undefined, function () {
	  "use strict";
	  !function () {
	    if ("undefined" != typeof window) try {
	      var e = new window.CustomEvent("test", { cancelable: !0 });if (e.preventDefault(), !0 !== e.defaultPrevented) throw new Error("Could not prevent default");
	    } catch (e) {
	      var t = function t(e, _t2) {
	        var n, i;return (_t2 = _t2 || {}).bubbles = !!_t2.bubbles, _t2.cancelable = !!_t2.cancelable, (n = document.createEvent("CustomEvent")).initCustomEvent(e, _t2.bubbles, _t2.cancelable, _t2.detail), i = n.preventDefault, n.preventDefault = function () {
	          i.call(this);try {
	            Object.defineProperty(this, "defaultPrevented", { get: function get() {
	                return !0;
	              } });
	          } catch (e) {
	            this.defaultPrevented = !0;
	          }
	        }, n;
	      };t.prototype = window.Event.prototype, window.CustomEvent = t;
	    }
	  }();var e = "undefined" != typeof globalThis ? globalThis : "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : {};function t(e, t) {
	    return e(t = { exports: {} }, t.exports), t.exports;
	  }var n = function n(e) {
	    return e && e.Math == Math && e;
	  },
	      i = n("object" == (typeof globalThis === "undefined" ? "undefined" : _typeof(globalThis)) && globalThis) || n("object" == (typeof window === "undefined" ? "undefined" : _typeof(window)) && window) || n("object" == (typeof self === "undefined" ? "undefined" : _typeof(self)) && self) || n("object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e) || Function("return this")(),
	      r = function r(e) {
	    try {
	      return !!e();
	    } catch (e) {
	      return !0;
	    }
	  },
	      a = !r(function () {
	    return 7 != Object.defineProperty({}, 1, { get: function get() {
	        return 7;
	      } })[1];
	  }),
	      o = {}.propertyIsEnumerable,
	      s = Object.getOwnPropertyDescriptor,
	      l = { f: s && !o.call({ 1: 2 }, 1) ? function (e) {
	      var t = s(this, e);return !!t && t.enumerable;
	    } : o },
	      c = function c(e, t) {
	    return { enumerable: !(1 & e), configurable: !(2 & e), writable: !(4 & e), value: t };
	  },
	      u = {}.toString,
	      h = function h(e) {
	    return u.call(e).slice(8, -1);
	  },
	      f = "".split,
	      d = r(function () {
	    return !Object("z").propertyIsEnumerable(0);
	  }) ? function (e) {
	    return "String" == h(e) ? f.call(e, "") : Object(e);
	  } : Object,
	      p = function p(e) {
	    if (null == e) throw TypeError("Can't call method on " + e);return e;
	  },
	      m = function m(e) {
	    return d(p(e));
	  },
	      g = function g(e) {
	    return "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) ? null !== e : "function" == typeof e;
	  },
	      v = function v(e, t) {
	    if (!g(e)) return e;var n, i;if (t && "function" == typeof (n = e.toString) && !g(i = n.call(e))) return i;if ("function" == typeof (n = e.valueOf) && !g(i = n.call(e))) return i;if (!t && "function" == typeof (n = e.toString) && !g(i = n.call(e))) return i;throw TypeError("Can't convert object to primitive value");
	  },
	      y = {}.hasOwnProperty,
	      b = function b(e, t) {
	    return y.call(e, t);
	  },
	      w = i.document,
	      k = g(w) && g(w.createElement),
	      T = function T(e) {
	    return k ? w.createElement(e) : {};
	  },
	      S = !a && !r(function () {
	    return 7 != Object.defineProperty(T("div"), "a", { get: function get() {
	        return 7;
	      } }).a;
	  }),
	      E = Object.getOwnPropertyDescriptor,
	      A = { f: a ? E : function (e, t) {
	      if (e = m(e), t = v(t, !0), S) try {
	        return E(e, t);
	      } catch (e) {}if (b(e, t)) return c(!l.f.call(e, t), e[t]);
	    } },
	      P = function P(e) {
	    if (!g(e)) throw TypeError(String(e) + " is not an object");return e;
	  },
	      x = Object.defineProperty,
	      C = { f: a ? x : function (e, t, n) {
	      if (P(e), t = v(t, !0), P(n), S) try {
	        return x(e, t, n);
	      } catch (e) {}if ("get" in n || "set" in n) throw TypeError("Accessors not supported");return "value" in n && (e[t] = n.value), e;
	    } },
	      O = a ? function (e, t, n) {
	    return C.f(e, t, c(1, n));
	  } : function (e, t, n) {
	    return e[t] = n, e;
	  },
	      I = function I(e, t) {
	    try {
	      O(i, e, t);
	    } catch (n) {
	      i[e] = t;
	    }return t;
	  },
	      L = i["__core-js_shared__"] || I("__core-js_shared__", {}),
	      j = Function.toString;"function" != typeof L.inspectSource && (L.inspectSource = function (e) {
	    return j.call(e);
	  });var N,
	      R,
	      M,
	      _ = L.inspectSource,
	      U = i.WeakMap,
	      D = "function" == typeof U && /native code/.test(_(U)),
	      F = t(function (e) {
	    (e.exports = function (e, t) {
	      return L[e] || (L[e] = void 0 !== t ? t : {});
	    })("versions", []).push({ version: "3.6.5", mode: "global", copyright: "© 2020 Denis Pushkarev (zloirock.ru)" });
	  }),
	      q = 0,
	      H = Math.random(),
	      B = function B(e) {
	    return "Symbol(" + String(void 0 === e ? "" : e) + ")_" + (++q + H).toString(36);
	  },
	      V = F("keys"),
	      z = function z(e) {
	    return V[e] || (V[e] = B(e));
	  },
	      W = {},
	      K = i.WeakMap;if (D) {
	    var $ = new K(),
	        Y = $.get,
	        G = $.has,
	        X = $.set;N = function N(e, t) {
	      return X.call($, e, t), t;
	    }, R = function R(e) {
	      return Y.call($, e) || {};
	    }, M = function M(e) {
	      return G.call($, e);
	    };
	  } else {
	    var Q = z("state");W[Q] = !0, N = function N(e, t) {
	      return O(e, Q, t), t;
	    }, R = function R(e) {
	      return b(e, Q) ? e[Q] : {};
	    }, M = function M(e) {
	      return b(e, Q);
	    };
	  }var J,
	      Z = { set: N, get: R, has: M, enforce: function enforce(e) {
	      return M(e) ? R(e) : N(e, {});
	    }, getterFor: function getterFor(e) {
	      return function (t) {
	        var n;if (!g(t) || (n = R(t)).type !== e) throw TypeError("Incompatible receiver, " + e + " required");return n;
	      };
	    } },
	      ee = t(function (e) {
	    var t = Z.get,
	        n = Z.enforce,
	        r = String(String).split("String");(e.exports = function (e, t, a, o) {
	      var s = !!o && !!o.unsafe,
	          l = !!o && !!o.enumerable,
	          c = !!o && !!o.noTargetGet;"function" == typeof a && ("string" != typeof t || b(a, "name") || O(a, "name", t), n(a).source = r.join("string" == typeof t ? t : "")), e !== i ? (s ? !c && e[t] && (l = !0) : delete e[t], l ? e[t] = a : O(e, t, a)) : l ? e[t] = a : I(t, a);
	    })(Function.prototype, "toString", function () {
	      return "function" == typeof this && t(this).source || _(this);
	    });
	  }),
	      te = i,
	      ne = function ne(e) {
	    return "function" == typeof e ? e : void 0;
	  },
	      ie = function ie(e, t) {
	    return arguments.length < 2 ? ne(te[e]) || ne(i[e]) : te[e] && te[e][t] || i[e] && i[e][t];
	  },
	      re = Math.ceil,
	      ae = Math.floor,
	      oe = function oe(e) {
	    return isNaN(e = +e) ? 0 : (e > 0 ? ae : re)(e);
	  },
	      se = Math.min,
	      le = function le(e) {
	    return e > 0 ? se(oe(e), 9007199254740991) : 0;
	  },
	      ce = Math.max,
	      ue = Math.min,
	      he = function he(e, t) {
	    var n = oe(e);return n < 0 ? ce(n + t, 0) : ue(n, t);
	  },
	      fe = function fe(e) {
	    return function (t, n, i) {
	      var r,
	          a = m(t),
	          o = le(a.length),
	          s = he(i, o);if (e && n != n) {
	        for (; o > s;) {
	          if ((r = a[s++]) != r) return !0;
	        }
	      } else for (; o > s; s++) {
	        if ((e || s in a) && a[s] === n) return e || s || 0;
	      }return !e && -1;
	    };
	  },
	      de = { includes: fe(!0), indexOf: fe(!1) },
	      pe = de.indexOf,
	      me = function me(e, t) {
	    var n,
	        i = m(e),
	        r = 0,
	        a = [];for (n in i) {
	      !b(W, n) && b(i, n) && a.push(n);
	    }for (; t.length > r;) {
	      b(i, n = t[r++]) && (~pe(a, n) || a.push(n));
	    }return a;
	  },
	      ge = ["constructor", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "toLocaleString", "toString", "valueOf"],
	      ve = ge.concat("length", "prototype"),
	      ye = { f: Object.getOwnPropertyNames || function (e) {
	      return me(e, ve);
	    } },
	      be = { f: Object.getOwnPropertySymbols },
	      we = ie("Reflect", "ownKeys") || function (e) {
	    var t = ye.f(P(e)),
	        n = be.f;return n ? t.concat(n(e)) : t;
	  },
	      ke = function ke(e, t) {
	    for (var n = we(t), i = C.f, r = A.f, a = 0; a < n.length; a++) {
	      var o = n[a];b(e, o) || i(e, o, r(t, o));
	    }
	  },
	      Te = /#|\.prototype\./,
	      Se = function Se(e, t) {
	    var n = Ae[Ee(e)];return n == xe || n != Pe && ("function" == typeof t ? r(t) : !!t);
	  },
	      Ee = Se.normalize = function (e) {
	    return String(e).replace(Te, ".").toLowerCase();
	  },
	      Ae = Se.data = {},
	      Pe = Se.NATIVE = "N",
	      xe = Se.POLYFILL = "P",
	      Ce = Se,
	      Oe = A.f,
	      Ie = function Ie(e, t) {
	    var n,
	        r,
	        a,
	        o,
	        s,
	        l = e.target,
	        c = e.global,
	        u = e.stat;if (n = c ? i : u ? i[l] || I(l, {}) : (i[l] || {}).prototype) for (r in t) {
	      if (o = t[r], a = e.noTargetGet ? (s = Oe(n, r)) && s.value : n[r], !Ce(c ? r : l + (u ? "." : "#") + r, e.forced) && void 0 !== a) {
	        if ((typeof o === "undefined" ? "undefined" : _typeof(o)) == (typeof a === "undefined" ? "undefined" : _typeof(a))) continue;ke(o, a);
	      }(e.sham || a && a.sham) && O(o, "sham", !0), ee(n, r, o, e);
	    }
	  },
	      Le = !!Object.getOwnPropertySymbols && !r(function () {
	    return !String(Symbol());
	  }),
	      je = Le && !Symbol.sham && "symbol" == _typeof(Symbol.iterator),
	      Ne = Array.isArray || function (e) {
	    return "Array" == h(e);
	  },
	      Re = function Re(e) {
	    return Object(p(e));
	  },
	      Me = Object.keys || function (e) {
	    return me(e, ge);
	  },
	      _e = a ? Object.defineProperties : function (e, t) {
	    P(e);for (var n, i = Me(t), r = i.length, a = 0; r > a;) {
	      C.f(e, n = i[a++], t[n]);
	    }return e;
	  },
	      Ue = ie("document", "documentElement"),
	      De = z("IE_PROTO"),
	      Fe = function Fe() {},
	      qe = function qe(e) {
	    return "<script>" + e + "<\/script>";
	  },
	      _He = function He() {
	    try {
	      J = document.domain && new ActiveXObject("htmlfile");
	    } catch (e) {}var e, t;_He = J ? function (e) {
	      e.write(qe("")), e.close();var t = e.parentWindow.Object;return e = null, t;
	    }(J) : ((t = T("iframe")).style.display = "none", Ue.appendChild(t), t.src = String("javascript:"), (e = t.contentWindow.document).open(), e.write(qe("document.F=Object")), e.close(), e.F);for (var n = ge.length; n--;) {
	      delete _He.prototype[ge[n]];
	    }return _He();
	  };W[De] = !0;var Be = Object.create || function (e, t) {
	    var n;return null !== e ? (Fe.prototype = P(e), n = new Fe(), Fe.prototype = null, n[De] = e) : n = _He(), void 0 === t ? n : _e(n, t);
	  },
	      Ve = ye.f,
	      ze = {}.toString,
	      We = "object" == (typeof window === "undefined" ? "undefined" : _typeof(window)) && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [],
	      Ke = { f: function f(e) {
	      return We && "[object Window]" == ze.call(e) ? function (e) {
	        try {
	          return Ve(e);
	        } catch (e) {
	          return We.slice();
	        }
	      }(e) : Ve(m(e));
	    } },
	      $e = F("wks"),
	      Ye = i.Symbol,
	      Ge = je ? Ye : Ye && Ye.withoutSetter || B,
	      Xe = function Xe(e) {
	    return b($e, e) || (Le && b(Ye, e) ? $e[e] = Ye[e] : $e[e] = Ge("Symbol." + e)), $e[e];
	  },
	      Qe = { f: Xe },
	      Je = C.f,
	      Ze = function Ze(e) {
	    var t = te.Symbol || (te.Symbol = {});b(t, e) || Je(t, e, { value: Qe.f(e) });
	  },
	      et = C.f,
	      tt = Xe("toStringTag"),
	      nt = function nt(e, t, n) {
	    e && !b(e = n ? e : e.prototype, tt) && et(e, tt, { configurable: !0, value: t });
	  },
	      it = function it(e) {
	    if ("function" != typeof e) throw TypeError(String(e) + " is not a function");return e;
	  },
	      rt = function rt(e, t, n) {
	    if (it(e), void 0 === t) return e;switch (n) {case 0:
	        return function () {
	          return e.call(t);
	        };case 1:
	        return function (n) {
	          return e.call(t, n);
	        };case 2:
	        return function (n, i) {
	          return e.call(t, n, i);
	        };case 3:
	        return function (n, i, r) {
	          return e.call(t, n, i, r);
	        };}return function () {
	      return e.apply(t, arguments);
	    };
	  },
	      at = Xe("species"),
	      ot = function ot(e, t) {
	    var n;return Ne(e) && ("function" != typeof (n = e.constructor) || n !== Array && !Ne(n.prototype) ? g(n) && null === (n = n[at]) && (n = void 0) : n = void 0), new (void 0 === n ? Array : n)(0 === t ? 0 : t);
	  },
	      st = [].push,
	      lt = function lt(e) {
	    var t = 1 == e,
	        n = 2 == e,
	        i = 3 == e,
	        r = 4 == e,
	        a = 6 == e,
	        o = 5 == e || a;return function (s, l, c, u) {
	      for (var h, f, p = Re(s), m = d(p), g = rt(l, c, 3), v = le(m.length), y = 0, b = u || ot, w = t ? b(s, v) : n ? b(s, 0) : void 0; v > y; y++) {
	        if ((o || y in m) && (f = g(h = m[y], y, p), e)) if (t) w[y] = f;else if (f) switch (e) {case 3:
	            return !0;case 5:
	            return h;case 6:
	            return y;case 2:
	            st.call(w, h);} else if (r) return !1;
	      }return a ? -1 : i || r ? r : w;
	    };
	  },
	      ct = { forEach: lt(0), map: lt(1), filter: lt(2), some: lt(3), every: lt(4), find: lt(5), findIndex: lt(6) },
	      ut = ct.forEach,
	      ht = z("hidden"),
	      ft = Xe("toPrimitive"),
	      dt = Z.set,
	      pt = Z.getterFor("Symbol"),
	      mt = Object.prototype,
	      _gt = i.Symbol,
	      vt = ie("JSON", "stringify"),
	      yt = A.f,
	      bt = C.f,
	      wt = Ke.f,
	      kt = l.f,
	      Tt = F("symbols"),
	      St = F("op-symbols"),
	      Et = F("string-to-symbol-registry"),
	      At = F("symbol-to-string-registry"),
	      Pt = F("wks"),
	      xt = i.QObject,
	      Ct = !xt || !xt.prototype || !xt.prototype.findChild,
	      Ot = a && r(function () {
	    return 7 != Be(bt({}, "a", { get: function get() {
	        return bt(this, "a", { value: 7 }).a;
	      } })).a;
	  }) ? function (e, t, n) {
	    var i = yt(mt, t);i && delete mt[t], bt(e, t, n), i && e !== mt && bt(mt, t, i);
	  } : bt,
	      It = function It(e, t) {
	    var n = Tt[e] = Be(_gt.prototype);return dt(n, { type: "Symbol", tag: e, description: t }), a || (n.description = t), n;
	  },
	      Lt = je ? function (e) {
	    return "symbol" == (typeof e === "undefined" ? "undefined" : _typeof(e));
	  } : function (e) {
	    return Object(e) instanceof _gt;
	  },
	      jt = function jt(e, t, n) {
	    e === mt && jt(St, t, n), P(e);var i = v(t, !0);return P(n), b(Tt, i) ? (n.enumerable ? (b(e, ht) && e[ht][i] && (e[ht][i] = !1), n = Be(n, { enumerable: c(0, !1) })) : (b(e, ht) || bt(e, ht, c(1, {})), e[ht][i] = !0), Ot(e, i, n)) : bt(e, i, n);
	  },
	      Nt = function Nt(e, t) {
	    P(e);var n = m(t),
	        i = Me(n).concat(Ut(n));return ut(i, function (t) {
	      a && !Rt.call(n, t) || jt(e, t, n[t]);
	    }), e;
	  },
	      Rt = function Rt(e) {
	    var t = v(e, !0),
	        n = kt.call(this, t);return !(this === mt && b(Tt, t) && !b(St, t)) && (!(n || !b(this, t) || !b(Tt, t) || b(this, ht) && this[ht][t]) || n);
	  },
	      Mt = function Mt(e, t) {
	    var n = m(e),
	        i = v(t, !0);if (n !== mt || !b(Tt, i) || b(St, i)) {
	      var r = yt(n, i);return !r || !b(Tt, i) || b(n, ht) && n[ht][i] || (r.enumerable = !0), r;
	    }
	  },
	      _t = function _t(e) {
	    var t = wt(m(e)),
	        n = [];return ut(t, function (e) {
	      b(Tt, e) || b(W, e) || n.push(e);
	    }), n;
	  },
	      Ut = function Ut(e) {
	    var t = e === mt,
	        n = wt(t ? St : m(e)),
	        i = [];return ut(n, function (e) {
	      !b(Tt, e) || t && !b(mt, e) || i.push(Tt[e]);
	    }), i;
	  };if (Le || (ee((_gt = function gt() {
	    if (this instanceof _gt) throw TypeError("Symbol is not a constructor");var e = arguments.length && void 0 !== arguments[0] ? String(arguments[0]) : void 0,
	        t = B(e),
	        n = function n(e) {
	      this === mt && n.call(St, e), b(this, ht) && b(this[ht], t) && (this[ht][t] = !1), Ot(this, t, c(1, e));
	    };return a && Ct && Ot(mt, t, { configurable: !0, set: n }), It(t, e);
	  }).prototype, "toString", function () {
	    return pt(this).tag;
	  }), ee(_gt, "withoutSetter", function (e) {
	    return It(B(e), e);
	  }), l.f = Rt, C.f = jt, A.f = Mt, ye.f = Ke.f = _t, be.f = Ut, Qe.f = function (e) {
	    return It(Xe(e), e);
	  }, a && (bt(_gt.prototype, "description", { configurable: !0, get: function get() {
	      return pt(this).description;
	    } }), ee(mt, "propertyIsEnumerable", Rt, { unsafe: !0 }))), Ie({ global: !0, wrap: !0, forced: !Le, sham: !Le }, { Symbol: _gt }), ut(Me(Pt), function (e) {
	    Ze(e);
	  }), Ie({ target: "Symbol", stat: !0, forced: !Le }, { for: function _for(e) {
	      var t = String(e);if (b(Et, t)) return Et[t];var n = _gt(t);return Et[t] = n, At[n] = t, n;
	    }, keyFor: function keyFor(e) {
	      if (!Lt(e)) throw TypeError(e + " is not a symbol");if (b(At, e)) return At[e];
	    }, useSetter: function useSetter() {
	      Ct = !0;
	    }, useSimple: function useSimple() {
	      Ct = !1;
	    } }), Ie({ target: "Object", stat: !0, forced: !Le, sham: !a }, { create: function create(e, t) {
	      return void 0 === t ? Be(e) : Nt(Be(e), t);
	    }, defineProperty: jt, defineProperties: Nt, getOwnPropertyDescriptor: Mt }), Ie({ target: "Object", stat: !0, forced: !Le }, { getOwnPropertyNames: _t, getOwnPropertySymbols: Ut }), Ie({ target: "Object", stat: !0, forced: r(function () {
	      be.f(1);
	    }) }, { getOwnPropertySymbols: function getOwnPropertySymbols(e) {
	      return be.f(Re(e));
	    } }), vt) {
	    var Dt = !Le || r(function () {
	      var e = _gt();return "[null]" != vt([e]) || "{}" != vt({ a: e }) || "{}" != vt(Object(e));
	    });Ie({ target: "JSON", stat: !0, forced: Dt }, { stringify: function stringify(e, t, n) {
	        for (var i, r = [e], a = 1; arguments.length > a;) {
	          r.push(arguments[a++]);
	        }if (i = t, (g(t) || void 0 !== e) && !Lt(e)) return Ne(t) || (t = function t(e, _t3) {
	          if ("function" == typeof i && (_t3 = i.call(this, e, _t3)), !Lt(_t3)) return _t3;
	        }), r[1] = t, vt.apply(null, r);
	      } });
	  }_gt.prototype[ft] || O(_gt.prototype, ft, _gt.prototype.valueOf), nt(_gt, "Symbol"), W[ht] = !0;var Ft = C.f,
	      qt = i.Symbol;if (a && "function" == typeof qt && (!("description" in qt.prototype) || void 0 !== qt().description)) {
	    var Ht = {},
	        Bt = function Bt() {
	      var e = arguments.length < 1 || void 0 === arguments[0] ? void 0 : String(arguments[0]),
	          t = this instanceof Bt ? new qt(e) : void 0 === e ? qt() : qt(e);return "" === e && (Ht[t] = !0), t;
	    };ke(Bt, qt);var Vt = Bt.prototype = qt.prototype;Vt.constructor = Bt;var zt = Vt.toString,
	        Wt = "Symbol(test)" == String(qt("test")),
	        Kt = /^Symbol\((.*)\)[^)]+$/;Ft(Vt, "description", { configurable: !0, get: function get() {
	        var e = g(this) ? this.valueOf() : this,
	            t = zt.call(e);if (b(Ht, e)) return "";var n = Wt ? t.slice(7, -1) : t.replace(Kt, "$1");return "" === n ? void 0 : n;
	      } }), Ie({ global: !0, forced: !0 }, { Symbol: Bt });
	  }Ze("iterator");var $t = function $t(e, t) {
	    var n = [][e];return !!n && r(function () {
	      n.call(null, t || function () {
	        throw 1;
	      }, 1);
	    });
	  },
	      Yt = Object.defineProperty,
	      Gt = {},
	      Xt = function Xt(e) {
	    throw e;
	  },
	      Qt = function Qt(e, t) {
	    if (b(Gt, e)) return Gt[e];t || (t = {});var n = [][e],
	        i = !!b(t, "ACCESSORS") && t.ACCESSORS,
	        o = b(t, 0) ? t[0] : Xt,
	        s = b(t, 1) ? t[1] : void 0;return Gt[e] = !!n && !r(function () {
	      if (i && !a) return !0;var e = { length: -1 };i ? Yt(e, 1, { enumerable: !0, get: Xt }) : e[1] = 1, n.call(e, o, s);
	    });
	  },
	      Jt = ct.forEach,
	      Zt = $t("forEach"),
	      en = Qt("forEach"),
	      tn = Zt && en ? [].forEach : function (e) {
	    return Jt(this, e, arguments.length > 1 ? arguments[1] : void 0);
	  };Ie({ target: "Array", proto: !0, forced: [].forEach != tn }, { forEach: tn });var nn = de.indexOf,
	      rn = [].indexOf,
	      an = !!rn && 1 / [1].indexOf(1, -0) < 0,
	      on = $t("indexOf"),
	      sn = Qt("indexOf", { ACCESSORS: !0, 1: 0 });Ie({ target: "Array", proto: !0, forced: an || !on || !sn }, { indexOf: function indexOf(e) {
	      return an ? rn.apply(this, arguments) || 0 : nn(this, e, arguments.length > 1 ? arguments[1] : void 0);
	    } });var ln = Xe("unscopables"),
	      cn = Array.prototype;null == cn[ln] && C.f(cn, ln, { configurable: !0, value: Be(null) });var un,
	      hn,
	      fn,
	      dn = function dn(e) {
	    cn[ln][e] = !0;
	  },
	      pn = {},
	      mn = !r(function () {
	    function e() {}return e.prototype.constructor = null, Object.getPrototypeOf(new e()) !== e.prototype;
	  }),
	      gn = z("IE_PROTO"),
	      vn = Object.prototype,
	      yn = mn ? Object.getPrototypeOf : function (e) {
	    return e = Re(e), b(e, gn) ? e[gn] : "function" == typeof e.constructor && e instanceof e.constructor ? e.constructor.prototype : e instanceof Object ? vn : null;
	  },
	      bn = Xe("iterator"),
	      wn = !1;[].keys && ("next" in (fn = [].keys()) ? (hn = yn(yn(fn))) !== Object.prototype && (un = hn) : wn = !0), null == un && (un = {}), b(un, bn) || O(un, bn, function () {
	    return this;
	  });var kn = { IteratorPrototype: un, BUGGY_SAFARI_ITERATORS: wn },
	      Tn = kn.IteratorPrototype,
	      Sn = function Sn() {
	    return this;
	  },
	      En = function En(e, t, n) {
	    var i = t + " Iterator";return e.prototype = Be(Tn, { next: c(1, n) }), nt(e, i, !1), pn[i] = Sn, e;
	  },
	      An = Object.setPrototypeOf || ("__proto__" in {} ? function () {
	    var e,
	        t = !1,
	        n = {};try {
	      (e = Object.getOwnPropertyDescriptor(Object.prototype, "__proto__").set).call(n, []), t = n instanceof Array;
	    } catch (e) {}return function (n, i) {
	      return P(n), function (e) {
	        if (!g(e) && null !== e) throw TypeError("Can't set " + String(e) + " as a prototype");
	      }(i), t ? e.call(n, i) : n.__proto__ = i, n;
	    };
	  }() : void 0),
	      Pn = kn.IteratorPrototype,
	      xn = kn.BUGGY_SAFARI_ITERATORS,
	      Cn = Xe("iterator"),
	      On = function On() {
	    return this;
	  },
	      In = function In(e, t, n, i, r, a, o) {
	    En(n, t, i);var s,
	        l,
	        c,
	        u = function u(e) {
	      if (e === r && m) return m;if (!xn && e in d) return d[e];switch (e) {case "keys":case "values":case "entries":
	          return function () {
	            return new n(this, e);
	          };}return function () {
	        return new n(this);
	      };
	    },
	        h = t + " Iterator",
	        f = !1,
	        d = e.prototype,
	        p = d[Cn] || d["@@iterator"] || r && d[r],
	        m = !xn && p || u(r),
	        g = "Array" == t && d.entries || p;if (g && (s = yn(g.call(new e())), Pn !== Object.prototype && s.next && (yn(s) !== Pn && (An ? An(s, Pn) : "function" != typeof s[Cn] && O(s, Cn, On)), nt(s, h, !0))), "values" == r && p && "values" !== p.name && (f = !0, m = function m() {
	      return p.call(this);
	    }), d[Cn] !== m && O(d, Cn, m), pn[t] = m, r) if (l = { values: u("values"), keys: a ? m : u("keys"), entries: u("entries") }, o) for (c in l) {
	      (xn || f || !(c in d)) && ee(d, c, l[c]);
	    } else Ie({ target: t, proto: !0, forced: xn || f }, l);return l;
	  },
	      Ln = Z.set,
	      jn = Z.getterFor("Array Iterator"),
	      Nn = In(Array, "Array", function (e, t) {
	    Ln(this, { type: "Array Iterator", target: m(e), index: 0, kind: t });
	  }, function () {
	    var e = jn(this),
	        t = e.target,
	        n = e.kind,
	        i = e.index++;return !t || i >= t.length ? (e.target = void 0, { value: void 0, done: !0 }) : "keys" == n ? { value: i, done: !1 } : "values" == n ? { value: t[i], done: !1 } : { value: [i, t[i]], done: !1 };
	  }, "values");pn.Arguments = pn.Array, dn("keys"), dn("values"), dn("entries");var Rn = [].join,
	      Mn = d != Object,
	      _n = $t("join", ",");Ie({ target: "Array", proto: !0, forced: Mn || !_n }, { join: function join(e) {
	      return Rn.call(m(this), void 0 === e ? "," : e);
	    } });var Un,
	      Dn,
	      Fn = function Fn(e, t, n) {
	    var i = v(t);i in e ? C.f(e, i, c(0, n)) : e[i] = n;
	  },
	      qn = ie("navigator", "userAgent") || "",
	      Hn = i.process,
	      Bn = Hn && Hn.versions,
	      Vn = Bn && Bn.v8;Vn ? Dn = (Un = Vn.split("."))[0] + Un[1] : qn && (!(Un = qn.match(/Edge\/(\d+)/)) || Un[1] >= 74) && (Un = qn.match(/Chrome\/(\d+)/)) && (Dn = Un[1]);var zn = Dn && +Dn,
	      Wn = Xe("species"),
	      Kn = function Kn(e) {
	    return zn >= 51 || !r(function () {
	      var t = [];return (t.constructor = {})[Wn] = function () {
	        return { foo: 1 };
	      }, 1 !== t[e](Boolean).foo;
	    });
	  },
	      $n = Kn("slice"),
	      Yn = Qt("slice", { ACCESSORS: !0, 0: 0, 1: 2 }),
	      Gn = Xe("species"),
	      Xn = [].slice,
	      Qn = Math.max;Ie({ target: "Array", proto: !0, forced: !$n || !Yn }, { slice: function slice(e, t) {
	      var n,
	          i,
	          r,
	          a = m(this),
	          o = le(a.length),
	          s = he(e, o),
	          l = he(void 0 === t ? o : t, o);if (Ne(a) && ("function" != typeof (n = a.constructor) || n !== Array && !Ne(n.prototype) ? g(n) && null === (n = n[Gn]) && (n = void 0) : n = void 0, n === Array || void 0 === n)) return Xn.call(a, s, l);for (i = new (void 0 === n ? Array : n)(Qn(l - s, 0)), r = 0; s < l; s++, r++) {
	        s in a && Fn(i, r, a[s]);
	      }return i.length = r, i;
	    } });var Jn = {};Jn[Xe("toStringTag")] = "z";var Zn = "[object z]" === String(Jn),
	      ei = Xe("toStringTag"),
	      ti = "Arguments" == h(function () {
	    return arguments;
	  }()),
	      ni = Zn ? h : function (e) {
	    var t, n, i;return void 0 === e ? "Undefined" : null === e ? "Null" : "string" == typeof (n = function (e, t) {
	      try {
	        return e[t];
	      } catch (e) {}
	    }(t = Object(e), ei)) ? n : ti ? h(t) : "Object" == (i = h(t)) && "function" == typeof t.callee ? "Arguments" : i;
	  },
	      ii = Zn ? {}.toString : function () {
	    return "[object " + ni(this) + "]";
	  };Zn || ee(Object.prototype, "toString", ii, { unsafe: !0 });var ri = function ri() {
	    var e = P(this),
	        t = "";return e.global && (t += "g"), e.ignoreCase && (t += "i"), e.multiline && (t += "m"), e.dotAll && (t += "s"), e.unicode && (t += "u"), e.sticky && (t += "y"), t;
	  };function ai(e, t) {
	    return RegExp(e, t);
	  }var oi = { UNSUPPORTED_Y: r(function () {
	      var e = ai("a", "y");return e.lastIndex = 2, null != e.exec("abcd");
	    }), BROKEN_CARET: r(function () {
	      var e = ai("^r", "gy");return e.lastIndex = 2, null != e.exec("str");
	    }) },
	      si = RegExp.prototype.exec,
	      li = String.prototype.replace,
	      ci = si,
	      ui = function () {
	    var e = /a/,
	        t = /b*/g;return si.call(e, "a"), si.call(t, "a"), 0 !== e.lastIndex || 0 !== t.lastIndex;
	  }(),
	      hi = oi.UNSUPPORTED_Y || oi.BROKEN_CARET,
	      fi = void 0 !== /()??/.exec("")[1];(ui || fi || hi) && (ci = function ci(e) {
	    var t,
	        n,
	        i,
	        r,
	        a = this,
	        o = hi && a.sticky,
	        s = ri.call(a),
	        l = a.source,
	        c = 0,
	        u = e;return o && (-1 === (s = s.replace("y", "")).indexOf("g") && (s += "g"), u = String(e).slice(a.lastIndex), a.lastIndex > 0 && (!a.multiline || a.multiline && "\n" !== e[a.lastIndex - 1]) && (l = "(?: " + l + ")", u = " " + u, c++), n = new RegExp("^(?:" + l + ")", s)), fi && (n = new RegExp("^" + l + "$(?!\\s)", s)), ui && (t = a.lastIndex), i = si.call(o ? n : a, u), o ? i ? (i.input = i.input.slice(c), i[0] = i[0].slice(c), i.index = a.lastIndex, a.lastIndex += i[0].length) : a.lastIndex = 0 : ui && i && (a.lastIndex = a.global ? i.index + i[0].length : t), fi && i && i.length > 1 && li.call(i[0], n, function () {
	      for (r = 1; r < arguments.length - 2; r++) {
	        void 0 === arguments[r] && (i[r] = void 0);
	      }
	    }), i;
	  });var di = ci;Ie({ target: "RegExp", proto: !0, forced: /./.exec !== di }, { exec: di });var pi = RegExp.prototype,
	      mi = pi.toString,
	      gi = r(function () {
	    return "/a/b" != mi.call({ source: "a", flags: "b" });
	  }),
	      vi = "toString" != mi.name;(gi || vi) && ee(RegExp.prototype, "toString", function () {
	    var e = P(this),
	        t = String(e.source),
	        n = e.flags;return "/" + t + "/" + String(void 0 === n && e instanceof RegExp && !("flags" in pi) ? ri.call(e) : n);
	  }, { unsafe: !0 });var yi = function yi(e) {
	    return function (t, n) {
	      var i,
	          r,
	          a = String(p(t)),
	          o = oe(n),
	          s = a.length;return o < 0 || o >= s ? e ? "" : void 0 : (i = a.charCodeAt(o)) < 55296 || i > 56319 || o + 1 === s || (r = a.charCodeAt(o + 1)) < 56320 || r > 57343 ? e ? a.charAt(o) : i : e ? a.slice(o, o + 2) : r - 56320 + (i - 55296 << 10) + 65536;
	    };
	  },
	      bi = { codeAt: yi(!1), charAt: yi(!0) },
	      wi = bi.charAt,
	      ki = Z.set,
	      Ti = Z.getterFor("String Iterator");In(String, "String", function (e) {
	    ki(this, { type: "String Iterator", string: String(e), index: 0 });
	  }, function () {
	    var e,
	        t = Ti(this),
	        n = t.string,
	        i = t.index;return i >= n.length ? { value: void 0, done: !0 } : (e = wi(n, i), t.index += e.length, { value: e, done: !1 });
	  });var Si = Xe("species"),
	      Ei = !r(function () {
	    var e = /./;return e.exec = function () {
	      var e = [];return e.groups = { a: "7" }, e;
	    }, "7" !== "".replace(e, "$<a>");
	  }),
	      Ai = "$0" === "a".replace(/./, "$0"),
	      Pi = Xe("replace"),
	      xi = !!/./[Pi] && "" === /./[Pi]("a", "$0"),
	      Ci = !r(function () {
	    var e = /(?:)/,
	        t = e.exec;e.exec = function () {
	      return t.apply(this, arguments);
	    };var n = "ab".split(e);return 2 !== n.length || "a" !== n[0] || "b" !== n[1];
	  }),
	      Oi = function Oi(e, t, n, i) {
	    var a = Xe(e),
	        o = !r(function () {
	      var t = {};return t[a] = function () {
	        return 7;
	      }, 7 != ""[e](t);
	    }),
	        s = o && !r(function () {
	      var t = !1,
	          n = /a/;return "split" === e && ((n = {}).constructor = {}, n.constructor[Si] = function () {
	        return n;
	      }, n.flags = "", n[a] = /./[a]), n.exec = function () {
	        return t = !0, null;
	      }, n[a](""), !t;
	    });if (!o || !s || "replace" === e && (!Ei || !Ai || xi) || "split" === e && !Ci) {
	      var l = /./[a],
	          c = n(a, ""[e], function (e, t, n, i, r) {
	        return t.exec === di ? o && !r ? { done: !0, value: l.call(t, n, i) } : { done: !0, value: e.call(n, t, i) } : { done: !1 };
	      }, { REPLACE_KEEPS_$0: Ai, REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE: xi }),
	          u = c[0],
	          h = c[1];ee(String.prototype, e, u), ee(RegExp.prototype, a, 2 == t ? function (e, t) {
	        return h.call(e, this, t);
	      } : function (e) {
	        return h.call(e, this);
	      });
	    }i && O(RegExp.prototype[a], "sham", !0);
	  },
	      Ii = bi.charAt,
	      Li = function Li(e, t, n) {
	    return t + (n ? Ii(e, t).length : 1);
	  },
	      ji = function ji(e, t) {
	    var n = e.exec;if ("function" == typeof n) {
	      var i = n.call(e, t);if ("object" != (typeof i === "undefined" ? "undefined" : _typeof(i))) throw TypeError("RegExp exec method returned something other than an Object or null");return i;
	    }if ("RegExp" !== h(e)) throw TypeError("RegExp#exec called on incompatible receiver");return di.call(e, t);
	  },
	      Ni = Math.max,
	      Ri = Math.min,
	      Mi = Math.floor,
	      _i = /\$([$&'`]|\d\d?|<[^>]*>)/g,
	      Ui = /\$([$&'`]|\d\d?)/g;Oi("replace", 2, function (e, t, n, i) {
	    var r = i.REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE,
	        a = i.REPLACE_KEEPS_$0,
	        o = r ? "$" : "$0";return [function (n, i) {
	      var r = p(this),
	          a = null == n ? void 0 : n[e];return void 0 !== a ? a.call(n, r, i) : t.call(String(r), n, i);
	    }, function (e, i) {
	      if (!r && a || "string" == typeof i && -1 === i.indexOf(o)) {
	        var l = n(t, e, this, i);if (l.done) return l.value;
	      }var c = P(e),
	          u = String(this),
	          h = "function" == typeof i;h || (i = String(i));var f = c.global;if (f) {
	        var d = c.unicode;c.lastIndex = 0;
	      }for (var p = [];;) {
	        var m = ji(c, u);if (null === m) break;if (p.push(m), !f) break;"" === String(m[0]) && (c.lastIndex = Li(u, le(c.lastIndex), d));
	      }for (var g, v = "", y = 0, b = 0; b < p.length; b++) {
	        m = p[b];for (var w = String(m[0]), k = Ni(Ri(oe(m.index), u.length), 0), T = [], S = 1; S < m.length; S++) {
	          T.push(void 0 === (g = m[S]) ? g : String(g));
	        }var E = m.groups;if (h) {
	          var A = [w].concat(T, k, u);void 0 !== E && A.push(E);var x = String(i.apply(void 0, A));
	        } else x = s(w, u, k, T, E, i);k >= y && (v += u.slice(y, k) + x, y = k + w.length);
	      }return v + u.slice(y);
	    }];function s(e, n, i, r, a, o) {
	      var s = i + e.length,
	          l = r.length,
	          c = Ui;return void 0 !== a && (a = Re(a), c = _i), t.call(o, c, function (t, o) {
	        var c;switch (o.charAt(0)) {case "$":
	            return "$";case "&":
	            return e;case "`":
	            return n.slice(0, i);case "'":
	            return n.slice(s);case "<":
	            c = a[o.slice(1, -1)];break;default:
	            var u = +o;if (0 === u) return t;if (u > l) {
	              var h = Mi(u / 10);return 0 === h ? t : h <= l ? void 0 === r[h - 1] ? o.charAt(1) : r[h - 1] + o.charAt(1) : t;
	            }c = r[u - 1];}return void 0 === c ? "" : c;
	      });
	    }
	  });var Di = Object.is || function (e, t) {
	    return e === t ? 0 !== e || 1 / e == 1 / t : e != e && t != t;
	  };Oi("search", 1, function (e, t, n) {
	    return [function (t) {
	      var n = p(this),
	          i = null == t ? void 0 : t[e];return void 0 !== i ? i.call(t, n) : new RegExp(t)[e](String(n));
	    }, function (e) {
	      var i = n(t, e, this);if (i.done) return i.value;var r = P(e),
	          a = String(this),
	          o = r.lastIndex;Di(o, 0) || (r.lastIndex = 0);var s = ji(r, a);return Di(r.lastIndex, o) || (r.lastIndex = o), null === s ? -1 : s.index;
	    }];
	  });var Fi = Xe("match"),
	      qi = function qi(e) {
	    var t;return g(e) && (void 0 !== (t = e[Fi]) ? !!t : "RegExp" == h(e));
	  },
	      Hi = Xe("species"),
	      Bi = function Bi(e, t) {
	    var n,
	        i = P(e).constructor;return void 0 === i || null == (n = P(i)[Hi]) ? t : it(n);
	  },
	      Vi = [].push,
	      zi = Math.min,
	      Wi = !r(function () {
	    return !RegExp(4294967295, "y");
	  });Oi("split", 2, function (e, t, n) {
	    var i;return i = "c" == "abbc".split(/(b)*/)[1] || 4 != "test".split(/(?:)/, -1).length || 2 != "ab".split(/(?:ab)*/).length || 4 != ".".split(/(.?)(.?)/).length || ".".split(/()()/).length > 1 || "".split(/.?/).length ? function (e, n) {
	      var i = String(p(this)),
	          r = void 0 === n ? 4294967295 : n >>> 0;if (0 === r) return [];if (void 0 === e) return [i];if (!qi(e)) return t.call(i, e, r);for (var a, o, s, l = [], c = (e.ignoreCase ? "i" : "") + (e.multiline ? "m" : "") + (e.unicode ? "u" : "") + (e.sticky ? "y" : ""), u = 0, h = new RegExp(e.source, c + "g"); (a = di.call(h, i)) && !((o = h.lastIndex) > u && (l.push(i.slice(u, a.index)), a.length > 1 && a.index < i.length && Vi.apply(l, a.slice(1)), s = a[0].length, u = o, l.length >= r));) {
	        h.lastIndex === a.index && h.lastIndex++;
	      }return u === i.length ? !s && h.test("") || l.push("") : l.push(i.slice(u)), l.length > r ? l.slice(0, r) : l;
	    } : "0".split(void 0, 0).length ? function (e, n) {
	      return void 0 === e && 0 === n ? [] : t.call(this, e, n);
	    } : t, [function (t, n) {
	      var r = p(this),
	          a = null == t ? void 0 : t[e];return void 0 !== a ? a.call(t, r, n) : i.call(String(r), t, n);
	    }, function (e, r) {
	      var a = n(i, e, this, r, i !== t);if (a.done) return a.value;var o = P(e),
	          s = String(this),
	          l = Bi(o, RegExp),
	          c = o.unicode,
	          u = (o.ignoreCase ? "i" : "") + (o.multiline ? "m" : "") + (o.unicode ? "u" : "") + (Wi ? "y" : "g"),
	          h = new l(Wi ? o : "^(?:" + o.source + ")", u),
	          f = void 0 === r ? 4294967295 : r >>> 0;if (0 === f) return [];if (0 === s.length) return null === ji(h, s) ? [s] : [];for (var d = 0, p = 0, m = []; p < s.length;) {
	        h.lastIndex = Wi ? p : 0;var g,
	            v = ji(h, Wi ? s : s.slice(p));if (null === v || (g = zi(le(h.lastIndex + (Wi ? 0 : p)), s.length)) === d) p = Li(s, p, c);else {
	          if (m.push(s.slice(d, p)), m.length === f) return m;for (var y = 1; y <= v.length - 1; y++) {
	            if (m.push(v[y]), m.length === f) return m;
	          }p = d = g;
	        }
	      }return m.push(s.slice(d)), m;
	    }];
	  }, !Wi);var Ki = { CSSRuleList: 0, CSSStyleDeclaration: 0, CSSValueList: 0, ClientRectList: 0, DOMRectList: 0, DOMStringList: 0, DOMTokenList: 1, DataTransferItemList: 0, FileList: 0, HTMLAllCollection: 0, HTMLCollection: 0, HTMLFormElement: 0, HTMLSelectElement: 0, MediaList: 0, MimeTypeArray: 0, NamedNodeMap: 0, NodeList: 1, PaintRequestList: 0, Plugin: 0, PluginArray: 0, SVGLengthList: 0, SVGNumberList: 0, SVGPathSegList: 0, SVGPointList: 0, SVGStringList: 0, SVGTransformList: 0, SourceBufferList: 0, StyleSheetList: 0, TextTrackCueList: 0, TextTrackList: 0, TouchList: 0 };for (var $i in Ki) {
	    var Yi = i[$i],
	        Gi = Yi && Yi.prototype;if (Gi && Gi.forEach !== tn) try {
	      O(Gi, "forEach", tn);
	    } catch (e) {
	      Gi.forEach = tn;
	    }
	  }var Xi = Xe("iterator"),
	      Qi = Xe("toStringTag"),
	      Ji = Nn.values;for (var Zi in Ki) {
	    var er = i[Zi],
	        tr = er && er.prototype;if (tr) {
	      if (tr[Xi] !== Ji) try {
	        O(tr, Xi, Ji);
	      } catch (e) {
	        tr[Xi] = Ji;
	      }if (tr[Qi] || O(tr, Qi, Zi), Ki[Zi]) for (var nr in Nn) {
	        if (tr[nr] !== Nn[nr]) try {
	          O(tr, nr, Nn[nr]);
	        } catch (e) {
	          tr[nr] = Nn[nr];
	        }
	      }
	    }
	  }var ir = Xe("iterator"),
	      rr = !r(function () {
	    var e = new URL("b?a=1&b=2&c=3", "http://a"),
	        t = e.searchParams,
	        n = "";return e.pathname = "c%20d", t.forEach(function (e, i) {
	      t.delete("b"), n += i + e;
	    }), !t.sort || "http://a/c%20d?a=1&c=3" !== e.href || "3" !== t.get("c") || "a=1" !== String(new URLSearchParams("?a=1")) || !t[ir] || "a" !== new URL("https://a@b").username || "b" !== new URLSearchParams(new URLSearchParams("a=b")).get("a") || "xn--e1aybc" !== new URL("http://тест").host || "#%D0%B1" !== new URL("http://a#б").hash || "a1c3" !== n || "x" !== new URL("http://x", void 0).host;
	  }),
	      ar = function ar(e, t, n) {
	    if (!(e instanceof t)) throw TypeError("Incorrect " + (n ? n + " " : "") + "invocation");return e;
	  },
	      or = Object.assign,
	      sr = Object.defineProperty,
	      lr = !or || r(function () {
	    if (a && 1 !== or({ b: 1 }, or(sr({}, "a", { enumerable: !0, get: function get() {
	        sr(this, "b", { value: 3, enumerable: !1 });
	      } }), { b: 2 })).b) return !0;var e = {},
	        t = {},
	        n = Symbol();return e[n] = 7, "abcdefghijklmnopqrst".split("").forEach(function (e) {
	      t[e] = e;
	    }), 7 != or({}, e)[n] || "abcdefghijklmnopqrst" != Me(or({}, t)).join("");
	  }) ? function (e, t) {
	    for (var n = Re(e), i = arguments.length, r = 1, o = be.f, s = l.f; i > r;) {
	      for (var c, u = d(arguments[r++]), h = o ? Me(u).concat(o(u)) : Me(u), f = h.length, p = 0; f > p;) {
	        c = h[p++], a && !s.call(u, c) || (n[c] = u[c]);
	      }
	    }return n;
	  } : or,
	      cr = function cr(e, t, n, i) {
	    try {
	      return i ? t(P(n)[0], n[1]) : t(n);
	    } catch (t) {
	      var r = e.return;throw void 0 !== r && P(r.call(e)), t;
	    }
	  },
	      ur = Xe("iterator"),
	      hr = Array.prototype,
	      fr = function fr(e) {
	    return void 0 !== e && (pn.Array === e || hr[ur] === e);
	  },
	      dr = Xe("iterator"),
	      pr = function pr(e) {
	    if (null != e) return e[dr] || e["@@iterator"] || pn[ni(e)];
	  },
	      mr = function mr(e) {
	    var t,
	        n,
	        i,
	        r,
	        a,
	        o,
	        s = Re(e),
	        l = "function" == typeof this ? this : Array,
	        c = arguments.length,
	        u = c > 1 ? arguments[1] : void 0,
	        h = void 0 !== u,
	        f = pr(s),
	        d = 0;if (h && (u = rt(u, c > 2 ? arguments[2] : void 0, 2)), null == f || l == Array && fr(f)) for (n = new l(t = le(s.length)); t > d; d++) {
	      o = h ? u(s[d], d) : s[d], Fn(n, d, o);
	    } else for (a = (r = f.call(s)).next, n = new l(); !(i = a.call(r)).done; d++) {
	      o = h ? cr(r, u, [i.value, d], !0) : i.value, Fn(n, d, o);
	    }return n.length = d, n;
	  },
	      gr = /[^\0-\u007E]/,
	      vr = /[.\u3002\uFF0E\uFF61]/g,
	      yr = "Overflow: input needs wider integers to process",
	      br = Math.floor,
	      wr = String.fromCharCode,
	      kr = function kr(e) {
	    return e + 22 + 75 * (e < 26);
	  },
	      Tr = function Tr(e, t, n) {
	    var i = 0;for (e = n ? br(e / 700) : e >> 1, e += br(e / t); e > 455; i += 36) {
	      e = br(e / 35);
	    }return br(i + 36 * e / (e + 38));
	  },
	      Sr = function Sr(e) {
	    var t,
	        n,
	        i = [],
	        r = (e = function (e) {
	      for (var t = [], n = 0, i = e.length; n < i;) {
	        var r = e.charCodeAt(n++);if (r >= 55296 && r <= 56319 && n < i) {
	          var a = e.charCodeAt(n++);56320 == (64512 & a) ? t.push(((1023 & r) << 10) + (1023 & a) + 65536) : (t.push(r), n--);
	        } else t.push(r);
	      }return t;
	    }(e)).length,
	        a = 128,
	        o = 0,
	        s = 72;for (t = 0; t < e.length; t++) {
	      (n = e[t]) < 128 && i.push(wr(n));
	    }var l = i.length,
	        c = l;for (l && i.push("-"); c < r;) {
	      var u = 2147483647;for (t = 0; t < e.length; t++) {
	        (n = e[t]) >= a && n < u && (u = n);
	      }var h = c + 1;if (u - a > br((2147483647 - o) / h)) throw RangeError(yr);for (o += (u - a) * h, a = u, t = 0; t < e.length; t++) {
	        if ((n = e[t]) < a && ++o > 2147483647) throw RangeError(yr);if (n == a) {
	          for (var f = o, d = 36;; d += 36) {
	            var p = d <= s ? 1 : d >= s + 26 ? 26 : d - s;if (f < p) break;var m = f - p,
	                g = 36 - p;i.push(wr(kr(p + m % g))), f = br(m / g);
	          }i.push(wr(kr(f))), s = Tr(o, h, c == l), o = 0, ++c;
	        }
	      }++o, ++a;
	    }return i.join("");
	  },
	      Er = function Er(e, t, n) {
	    for (var i in t) {
	      ee(e, i, t[i], n);
	    }return e;
	  },
	      Ar = function Ar(e) {
	    var t = pr(e);if ("function" != typeof t) throw TypeError(String(e) + " is not iterable");return P(t.call(e));
	  },
	      Pr = ie("fetch"),
	      xr = ie("Headers"),
	      Cr = Xe("iterator"),
	      Or = Z.set,
	      Ir = Z.getterFor("URLSearchParams"),
	      Lr = Z.getterFor("URLSearchParamsIterator"),
	      jr = /\+/g,
	      Nr = Array(4),
	      Rr = function Rr(e) {
	    return Nr[e - 1] || (Nr[e - 1] = RegExp("((?:%[\\da-f]{2}){" + e + "})", "gi"));
	  },
	      Mr = function Mr(e) {
	    try {
	      return decodeURIComponent(e);
	    } catch (t) {
	      return e;
	    }
	  },
	      _r = function _r(e) {
	    var t = e.replace(jr, " "),
	        n = 4;try {
	      return decodeURIComponent(t);
	    } catch (e) {
	      for (; n;) {
	        t = t.replace(Rr(n--), Mr);
	      }return t;
	    }
	  },
	      Ur = /[!'()~]|%20/g,
	      Dr = { "!": "%21", "'": "%27", "(": "%28", ")": "%29", "~": "%7E", "%20": "+" },
	      Fr = function Fr(e) {
	    return Dr[e];
	  },
	      qr = function qr(e) {
	    return encodeURIComponent(e).replace(Ur, Fr);
	  },
	      Hr = function Hr(e, t) {
	    if (t) for (var n, i, r = t.split("&"), a = 0; a < r.length;) {
	      (n = r[a++]).length && (i = n.split("="), e.push({ key: _r(i.shift()), value: _r(i.join("=")) }));
	    }
	  },
	      Br = function Br(e) {
	    this.entries.length = 0, Hr(this.entries, e);
	  },
	      Vr = function Vr(e, t) {
	    if (e < t) throw TypeError("Not enough arguments");
	  },
	      zr = En(function (e, t) {
	    Or(this, { type: "URLSearchParamsIterator", iterator: Ar(Ir(e).entries), kind: t });
	  }, "Iterator", function () {
	    var e = Lr(this),
	        t = e.kind,
	        n = e.iterator.next(),
	        i = n.value;return n.done || (n.value = "keys" === t ? i.key : "values" === t ? i.value : [i.key, i.value]), n;
	  }),
	      Wr = function Wr() {
	    ar(this, Wr, "URLSearchParams");var e,
	        t,
	        n,
	        i,
	        r,
	        a,
	        o,
	        s,
	        l,
	        c = arguments.length > 0 ? arguments[0] : void 0,
	        u = this,
	        h = [];if (Or(u, { type: "URLSearchParams", entries: h, updateURL: function updateURL() {}, updateSearchParams: Br }), void 0 !== c) if (g(c)) {
	      if ("function" == typeof (e = pr(c))) for (n = (t = e.call(c)).next; !(i = n.call(t)).done;) {
	        if ((o = (a = (r = Ar(P(i.value))).next).call(r)).done || (s = a.call(r)).done || !a.call(r).done) throw TypeError("Expected sequence with length 2");h.push({ key: o.value + "", value: s.value + "" });
	      } else for (l in c) {
	        b(c, l) && h.push({ key: l, value: c[l] + "" });
	      }
	    } else Hr(h, "string" == typeof c ? "?" === c.charAt(0) ? c.slice(1) : c : c + "");
	  },
	      Kr = Wr.prototype;Er(Kr, { append: function append(e, t) {
	      Vr(arguments.length, 2);var n = Ir(this);n.entries.push({ key: e + "", value: t + "" }), n.updateURL();
	    }, delete: function _delete(e) {
	      Vr(arguments.length, 1);for (var t = Ir(this), n = t.entries, i = e + "", r = 0; r < n.length;) {
	        n[r].key === i ? n.splice(r, 1) : r++;
	      }t.updateURL();
	    }, get: function get(e) {
	      Vr(arguments.length, 1);for (var t = Ir(this).entries, n = e + "", i = 0; i < t.length; i++) {
	        if (t[i].key === n) return t[i].value;
	      }return null;
	    }, getAll: function getAll(e) {
	      Vr(arguments.length, 1);for (var t = Ir(this).entries, n = e + "", i = [], r = 0; r < t.length; r++) {
	        t[r].key === n && i.push(t[r].value);
	      }return i;
	    }, has: function has(e) {
	      Vr(arguments.length, 1);for (var t = Ir(this).entries, n = e + "", i = 0; i < t.length;) {
	        if (t[i++].key === n) return !0;
	      }return !1;
	    }, set: function set(e, t) {
	      Vr(arguments.length, 1);for (var n, i = Ir(this), r = i.entries, a = !1, o = e + "", s = t + "", l = 0; l < r.length; l++) {
	        (n = r[l]).key === o && (a ? r.splice(l--, 1) : (a = !0, n.value = s));
	      }a || r.push({ key: o, value: s }), i.updateURL();
	    }, sort: function sort() {
	      var e,
	          t,
	          n,
	          i = Ir(this),
	          r = i.entries,
	          a = r.slice();for (r.length = 0, n = 0; n < a.length; n++) {
	        for (e = a[n], t = 0; t < n; t++) {
	          if (r[t].key > e.key) {
	            r.splice(t, 0, e);break;
	          }
	        }t === n && r.push(e);
	      }i.updateURL();
	    }, forEach: function forEach(e) {
	      for (var t, n = Ir(this).entries, i = rt(e, arguments.length > 1 ? arguments[1] : void 0, 3), r = 0; r < n.length;) {
	        i((t = n[r++]).value, t.key, this);
	      }
	    }, keys: function keys() {
	      return new zr(this, "keys");
	    }, values: function values() {
	      return new zr(this, "values");
	    }, entries: function entries() {
	      return new zr(this, "entries");
	    } }, { enumerable: !0 }), ee(Kr, Cr, Kr.entries), ee(Kr, "toString", function () {
	    for (var e, t = Ir(this).entries, n = [], i = 0; i < t.length;) {
	      e = t[i++], n.push(qr(e.key) + "=" + qr(e.value));
	    }return n.join("&");
	  }, { enumerable: !0 }), nt(Wr, "URLSearchParams"), Ie({ global: !0, forced: !rr }, { URLSearchParams: Wr }), rr || "function" != typeof Pr || "function" != typeof xr || Ie({ global: !0, enumerable: !0, forced: !0 }, { fetch: function fetch(e) {
	      var t,
	          n,
	          i,
	          r = [e];return arguments.length > 1 && (t = arguments[1], g(t) && (n = t.body, "URLSearchParams" === ni(n) && ((i = t.headers ? new xr(t.headers) : new xr()).has("content-type") || i.set("content-type", "application/x-www-form-urlencoded;charset=UTF-8"), t = Be(t, { body: c(0, String(n)), headers: c(0, i) }))), r.push(t)), Pr.apply(this, r);
	    } });var $r,
	      Yr = { URLSearchParams: Wr, getState: Ir },
	      Gr = bi.codeAt,
	      Xr = i.URL,
	      Qr = Yr.URLSearchParams,
	      Jr = Yr.getState,
	      Zr = Z.set,
	      ea = Z.getterFor("URL"),
	      ta = Math.floor,
	      na = Math.pow,
	      ia = /[A-Za-z]/,
	      ra = /[\d+-.A-Za-z]/,
	      aa = /\d/,
	      oa = /^(0x|0X)/,
	      sa = /^[0-7]+$/,
	      la = /^\d+$/,
	      ca = /^[\dA-Fa-f]+$/,
	      ua = /[\u0000\u0009\u000A\u000D #%/:?@[\\]]/,
	      ha = /[\u0000\u0009\u000A\u000D #/:?@[\\]]/,
	      fa = /^[\u0000-\u001F ]+|[\u0000-\u001F ]+$/g,
	      da = /[\u0009\u000A\u000D]/g,
	      pa = function pa(e, t) {
	    var n, i, r;if ("[" == t.charAt(0)) {
	      if ("]" != t.charAt(t.length - 1)) return "Invalid host";if (!(n = ga(t.slice(1, -1)))) return "Invalid host";e.host = n;
	    } else if (Ea(e)) {
	      if (t = function (e) {
	        var t,
	            n,
	            i = [],
	            r = e.toLowerCase().replace(vr, ".").split(".");for (t = 0; t < r.length; t++) {
	          n = r[t], i.push(gr.test(n) ? "xn--" + Sr(n) : n);
	        }return i.join(".");
	      }(t), ua.test(t)) return "Invalid host";if (null === (n = ma(t))) return "Invalid host";e.host = n;
	    } else {
	      if (ha.test(t)) return "Invalid host";for (n = "", i = mr(t), r = 0; r < i.length; r++) {
	        n += Ta(i[r], ya);
	      }e.host = n;
	    }
	  },
	      ma = function ma(e) {
	    var t,
	        n,
	        i,
	        r,
	        a,
	        o,
	        s,
	        l = e.split(".");if (l.length && "" == l[l.length - 1] && l.pop(), (t = l.length) > 4) return e;for (n = [], i = 0; i < t; i++) {
	      if ("" == (r = l[i])) return e;if (a = 10, r.length > 1 && "0" == r.charAt(0) && (a = oa.test(r) ? 16 : 8, r = r.slice(8 == a ? 1 : 2)), "" === r) o = 0;else {
	        if (!(10 == a ? la : 8 == a ? sa : ca).test(r)) return e;o = parseInt(r, a);
	      }n.push(o);
	    }for (i = 0; i < t; i++) {
	      if (o = n[i], i == t - 1) {
	        if (o >= na(256, 5 - t)) return null;
	      } else if (o > 255) return null;
	    }for (s = n.pop(), i = 0; i < n.length; i++) {
	      s += n[i] * na(256, 3 - i);
	    }return s;
	  },
	      ga = function ga(e) {
	    var t,
	        n,
	        i,
	        r,
	        a,
	        o,
	        s,
	        l = [0, 0, 0, 0, 0, 0, 0, 0],
	        c = 0,
	        u = null,
	        h = 0,
	        f = function f() {
	      return e.charAt(h);
	    };if (":" == f()) {
	      if (":" != e.charAt(1)) return;h += 2, u = ++c;
	    }for (; f();) {
	      if (8 == c) return;if (":" != f()) {
	        for (t = n = 0; n < 4 && ca.test(f());) {
	          t = 16 * t + parseInt(f(), 16), h++, n++;
	        }if ("." == f()) {
	          if (0 == n) return;if (h -= n, c > 6) return;for (i = 0; f();) {
	            if (r = null, i > 0) {
	              if (!("." == f() && i < 4)) return;h++;
	            }if (!aa.test(f())) return;for (; aa.test(f());) {
	              if (a = parseInt(f(), 10), null === r) r = a;else {
	                if (0 == r) return;r = 10 * r + a;
	              }if (r > 255) return;h++;
	            }l[c] = 256 * l[c] + r, 2 != ++i && 4 != i || c++;
	          }if (4 != i) return;break;
	        }if (":" == f()) {
	          if (h++, !f()) return;
	        } else if (f()) return;l[c++] = t;
	      } else {
	        if (null !== u) return;h++, u = ++c;
	      }
	    }if (null !== u) for (o = c - u, c = 7; 0 != c && o > 0;) {
	      s = l[c], l[c--] = l[u + o - 1], l[u + --o] = s;
	    } else if (8 != c) return;return l;
	  },
	      va = function va(e) {
	    var t, n, i, r;if ("number" == typeof e) {
	      for (t = [], n = 0; n < 4; n++) {
	        t.unshift(e % 256), e = ta(e / 256);
	      }return t.join(".");
	    }if ("object" == (typeof e === "undefined" ? "undefined" : _typeof(e))) {
	      for (t = "", i = function (e) {
	        for (var t = null, n = 1, i = null, r = 0, a = 0; a < 8; a++) {
	          0 !== e[a] ? (r > n && (t = i, n = r), i = null, r = 0) : (null === i && (i = a), ++r);
	        }return r > n && (t = i, n = r), t;
	      }(e), n = 0; n < 8; n++) {
	        r && 0 === e[n] || (r && (r = !1), i === n ? (t += n ? ":" : "::", r = !0) : (t += e[n].toString(16), n < 7 && (t += ":")));
	      }return "[" + t + "]";
	    }return e;
	  },
	      ya = {},
	      ba = lr({}, ya, { " ": 1, '"': 1, "<": 1, ">": 1, "`": 1 }),
	      wa = lr({}, ba, { "#": 1, "?": 1, "{": 1, "}": 1 }),
	      ka = lr({}, wa, { "/": 1, ":": 1, ";": 1, "=": 1, "@": 1, "[": 1, "\\": 1, "]": 1, "^": 1, "|": 1 }),
	      Ta = function Ta(e, t) {
	    var n = Gr(e, 0);return n > 32 && n < 127 && !b(t, e) ? e : encodeURIComponent(e);
	  },
	      Sa = { ftp: 21, file: null, http: 80, https: 443, ws: 80, wss: 443 },
	      Ea = function Ea(e) {
	    return b(Sa, e.scheme);
	  },
	      Aa = function Aa(e) {
	    return "" != e.username || "" != e.password;
	  },
	      Pa = function Pa(e) {
	    return !e.host || e.cannotBeABaseURL || "file" == e.scheme;
	  },
	      xa = function xa(e, t) {
	    var n;return 2 == e.length && ia.test(e.charAt(0)) && (":" == (n = e.charAt(1)) || !t && "|" == n);
	  },
	      Ca = function Ca(e) {
	    var t;return e.length > 1 && xa(e.slice(0, 2)) && (2 == e.length || "/" === (t = e.charAt(2)) || "\\" === t || "?" === t || "#" === t);
	  },
	      Oa = function Oa(e) {
	    var t = e.path,
	        n = t.length;!n || "file" == e.scheme && 1 == n && xa(t[0], !0) || t.pop();
	  },
	      Ia = function Ia(e) {
	    return "." === e || "%2e" === e.toLowerCase();
	  },
	      La = {},
	      ja = {},
	      Na = {},
	      Ra = {},
	      Ma = {},
	      _a = {},
	      Ua = {},
	      Da = {},
	      Fa = {},
	      qa = {},
	      Ha = {},
	      Ba = {},
	      Va = {},
	      za = {},
	      Wa = {},
	      Ka = {},
	      $a = {},
	      Ya = {},
	      Ga = {},
	      Xa = {},
	      Qa = {},
	      Ja = function Ja(e, t, n, i) {
	    var r,
	        a,
	        o,
	        s,
	        l,
	        c = n || La,
	        u = 0,
	        h = "",
	        f = !1,
	        d = !1,
	        p = !1;for (n || (e.scheme = "", e.username = "", e.password = "", e.host = null, e.port = null, e.path = [], e.query = null, e.fragment = null, e.cannotBeABaseURL = !1, t = t.replace(fa, "")), t = t.replace(da, ""), r = mr(t); u <= r.length;) {
	      switch (a = r[u], c) {case La:
	          if (!a || !ia.test(a)) {
	            if (n) return "Invalid scheme";c = Na;continue;
	          }h += a.toLowerCase(), c = ja;break;case ja:
	          if (a && (ra.test(a) || "+" == a || "-" == a || "." == a)) h += a.toLowerCase();else {
	            if (":" != a) {
	              if (n) return "Invalid scheme";h = "", c = Na, u = 0;continue;
	            }if (n && (Ea(e) != b(Sa, h) || "file" == h && (Aa(e) || null !== e.port) || "file" == e.scheme && !e.host)) return;if (e.scheme = h, n) return void (Ea(e) && Sa[e.scheme] == e.port && (e.port = null));h = "", "file" == e.scheme ? c = za : Ea(e) && i && i.scheme == e.scheme ? c = Ra : Ea(e) ? c = Da : "/" == r[u + 1] ? (c = Ma, u++) : (e.cannotBeABaseURL = !0, e.path.push(""), c = Ga);
	          }break;case Na:
	          if (!i || i.cannotBeABaseURL && "#" != a) return "Invalid scheme";if (i.cannotBeABaseURL && "#" == a) {
	            e.scheme = i.scheme, e.path = i.path.slice(), e.query = i.query, e.fragment = "", e.cannotBeABaseURL = !0, c = Qa;break;
	          }c = "file" == i.scheme ? za : _a;continue;case Ra:
	          if ("/" != a || "/" != r[u + 1]) {
	            c = _a;continue;
	          }c = Fa, u++;break;case Ma:
	          if ("/" == a) {
	            c = qa;break;
	          }c = Ya;continue;case _a:
	          if (e.scheme = i.scheme, a == $r) e.username = i.username, e.password = i.password, e.host = i.host, e.port = i.port, e.path = i.path.slice(), e.query = i.query;else if ("/" == a || "\\" == a && Ea(e)) c = Ua;else if ("?" == a) e.username = i.username, e.password = i.password, e.host = i.host, e.port = i.port, e.path = i.path.slice(), e.query = "", c = Xa;else {
	            if ("#" != a) {
	              e.username = i.username, e.password = i.password, e.host = i.host, e.port = i.port, e.path = i.path.slice(), e.path.pop(), c = Ya;continue;
	            }e.username = i.username, e.password = i.password, e.host = i.host, e.port = i.port, e.path = i.path.slice(), e.query = i.query, e.fragment = "", c = Qa;
	          }break;case Ua:
	          if (!Ea(e) || "/" != a && "\\" != a) {
	            if ("/" != a) {
	              e.username = i.username, e.password = i.password, e.host = i.host, e.port = i.port, c = Ya;continue;
	            }c = qa;
	          } else c = Fa;break;case Da:
	          if (c = Fa, "/" != a || "/" != h.charAt(u + 1)) continue;u++;break;case Fa:
	          if ("/" != a && "\\" != a) {
	            c = qa;continue;
	          }break;case qa:
	          if ("@" == a) {
	            f && (h = "%40" + h), f = !0, o = mr(h);for (var m = 0; m < o.length; m++) {
	              var g = o[m];if (":" != g || p) {
	                var v = Ta(g, ka);p ? e.password += v : e.username += v;
	              } else p = !0;
	            }h = "";
	          } else if (a == $r || "/" == a || "?" == a || "#" == a || "\\" == a && Ea(e)) {
	            if (f && "" == h) return "Invalid authority";u -= mr(h).length + 1, h = "", c = Ha;
	          } else h += a;break;case Ha:case Ba:
	          if (n && "file" == e.scheme) {
	            c = Ka;continue;
	          }if (":" != a || d) {
	            if (a == $r || "/" == a || "?" == a || "#" == a || "\\" == a && Ea(e)) {
	              if (Ea(e) && "" == h) return "Invalid host";if (n && "" == h && (Aa(e) || null !== e.port)) return;if (s = pa(e, h)) return s;if (h = "", c = $a, n) return;continue;
	            }"[" == a ? d = !0 : "]" == a && (d = !1), h += a;
	          } else {
	            if ("" == h) return "Invalid host";if (s = pa(e, h)) return s;if (h = "", c = Va, n == Ba) return;
	          }break;case Va:
	          if (!aa.test(a)) {
	            if (a == $r || "/" == a || "?" == a || "#" == a || "\\" == a && Ea(e) || n) {
	              if ("" != h) {
	                var y = parseInt(h, 10);if (y > 65535) return "Invalid port";e.port = Ea(e) && y === Sa[e.scheme] ? null : y, h = "";
	              }if (n) return;c = $a;continue;
	            }return "Invalid port";
	          }h += a;break;case za:
	          if (e.scheme = "file", "/" == a || "\\" == a) c = Wa;else {
	            if (!i || "file" != i.scheme) {
	              c = Ya;continue;
	            }if (a == $r) e.host = i.host, e.path = i.path.slice(), e.query = i.query;else if ("?" == a) e.host = i.host, e.path = i.path.slice(), e.query = "", c = Xa;else {
	              if ("#" != a) {
	                Ca(r.slice(u).join("")) || (e.host = i.host, e.path = i.path.slice(), Oa(e)), c = Ya;continue;
	              }e.host = i.host, e.path = i.path.slice(), e.query = i.query, e.fragment = "", c = Qa;
	            }
	          }break;case Wa:
	          if ("/" == a || "\\" == a) {
	            c = Ka;break;
	          }i && "file" == i.scheme && !Ca(r.slice(u).join("")) && (xa(i.path[0], !0) ? e.path.push(i.path[0]) : e.host = i.host), c = Ya;continue;case Ka:
	          if (a == $r || "/" == a || "\\" == a || "?" == a || "#" == a) {
	            if (!n && xa(h)) c = Ya;else if ("" == h) {
	              if (e.host = "", n) return;c = $a;
	            } else {
	              if (s = pa(e, h)) return s;if ("localhost" == e.host && (e.host = ""), n) return;h = "", c = $a;
	            }continue;
	          }h += a;break;case $a:
	          if (Ea(e)) {
	            if (c = Ya, "/" != a && "\\" != a) continue;
	          } else if (n || "?" != a) {
	            if (n || "#" != a) {
	              if (a != $r && (c = Ya, "/" != a)) continue;
	            } else e.fragment = "", c = Qa;
	          } else e.query = "", c = Xa;break;case Ya:
	          if (a == $r || "/" == a || "\\" == a && Ea(e) || !n && ("?" == a || "#" == a)) {
	            if (".." === (l = (l = h).toLowerCase()) || "%2e." === l || ".%2e" === l || "%2e%2e" === l ? (Oa(e), "/" == a || "\\" == a && Ea(e) || e.path.push("")) : Ia(h) ? "/" == a || "\\" == a && Ea(e) || e.path.push("") : ("file" == e.scheme && !e.path.length && xa(h) && (e.host && (e.host = ""), h = h.charAt(0) + ":"), e.path.push(h)), h = "", "file" == e.scheme && (a == $r || "?" == a || "#" == a)) for (; e.path.length > 1 && "" === e.path[0];) {
	              e.path.shift();
	            }"?" == a ? (e.query = "", c = Xa) : "#" == a && (e.fragment = "", c = Qa);
	          } else h += Ta(a, wa);break;case Ga:
	          "?" == a ? (e.query = "", c = Xa) : "#" == a ? (e.fragment = "", c = Qa) : a != $r && (e.path[0] += Ta(a, ya));break;case Xa:
	          n || "#" != a ? a != $r && ("'" == a && Ea(e) ? e.query += "%27" : e.query += "#" == a ? "%23" : Ta(a, ya)) : (e.fragment = "", c = Qa);break;case Qa:
	          a != $r && (e.fragment += Ta(a, ba));}u++;
	    }
	  },
	      Za = function Za(e) {
	    var t,
	        n,
	        i = ar(this, Za, "URL"),
	        r = arguments.length > 1 ? arguments[1] : void 0,
	        o = String(e),
	        s = Zr(i, { type: "URL" });if (void 0 !== r) if (r instanceof Za) t = ea(r);else if (n = Ja(t = {}, String(r))) throw TypeError(n);if (n = Ja(s, o, null, t)) throw TypeError(n);var l = s.searchParams = new Qr(),
	        c = Jr(l);c.updateSearchParams(s.query), c.updateURL = function () {
	      s.query = String(l) || null;
	    }, a || (i.href = to.call(i), i.origin = no.call(i), i.protocol = io.call(i), i.username = ro.call(i), i.password = ao.call(i), i.host = oo.call(i), i.hostname = so.call(i), i.port = lo.call(i), i.pathname = co.call(i), i.search = uo.call(i), i.searchParams = ho.call(i), i.hash = fo.call(i));
	  },
	      eo = Za.prototype,
	      to = function to() {
	    var e = ea(this),
	        t = e.scheme,
	        n = e.username,
	        i = e.password,
	        r = e.host,
	        a = e.port,
	        o = e.path,
	        s = e.query,
	        l = e.fragment,
	        c = t + ":";return null !== r ? (c += "//", Aa(e) && (c += n + (i ? ":" + i : "") + "@"), c += va(r), null !== a && (c += ":" + a)) : "file" == t && (c += "//"), c += e.cannotBeABaseURL ? o[0] : o.length ? "/" + o.join("/") : "", null !== s && (c += "?" + s), null !== l && (c += "#" + l), c;
	  },
	      no = function no() {
	    var e = ea(this),
	        t = e.scheme,
	        n = e.port;if ("blob" == t) try {
	      return new URL(t.path[0]).origin;
	    } catch (e) {
	      return "null";
	    }return "file" != t && Ea(e) ? t + "://" + va(e.host) + (null !== n ? ":" + n : "") : "null";
	  },
	      io = function io() {
	    return ea(this).scheme + ":";
	  },
	      ro = function ro() {
	    return ea(this).username;
	  },
	      ao = function ao() {
	    return ea(this).password;
	  },
	      oo = function oo() {
	    var e = ea(this),
	        t = e.host,
	        n = e.port;return null === t ? "" : null === n ? va(t) : va(t) + ":" + n;
	  },
	      so = function so() {
	    var e = ea(this).host;return null === e ? "" : va(e);
	  },
	      lo = function lo() {
	    var e = ea(this).port;return null === e ? "" : String(e);
	  },
	      co = function co() {
	    var e = ea(this),
	        t = e.path;return e.cannotBeABaseURL ? t[0] : t.length ? "/" + t.join("/") : "";
	  },
	      uo = function uo() {
	    var e = ea(this).query;return e ? "?" + e : "";
	  },
	      ho = function ho() {
	    return ea(this).searchParams;
	  },
	      fo = function fo() {
	    var e = ea(this).fragment;return e ? "#" + e : "";
	  },
	      po = function po(e, t) {
	    return { get: e, set: t, configurable: !0, enumerable: !0 };
	  };if (a && _e(eo, { href: po(to, function (e) {
	      var t = ea(this),
	          n = String(e),
	          i = Ja(t, n);if (i) throw TypeError(i);Jr(t.searchParams).updateSearchParams(t.query);
	    }), origin: po(no), protocol: po(io, function (e) {
	      var t = ea(this);Ja(t, String(e) + ":", La);
	    }), username: po(ro, function (e) {
	      var t = ea(this),
	          n = mr(String(e));if (!Pa(t)) {
	        t.username = "";for (var i = 0; i < n.length; i++) {
	          t.username += Ta(n[i], ka);
	        }
	      }
	    }), password: po(ao, function (e) {
	      var t = ea(this),
	          n = mr(String(e));if (!Pa(t)) {
	        t.password = "";for (var i = 0; i < n.length; i++) {
	          t.password += Ta(n[i], ka);
	        }
	      }
	    }), host: po(oo, function (e) {
	      var t = ea(this);t.cannotBeABaseURL || Ja(t, String(e), Ha);
	    }), hostname: po(so, function (e) {
	      var t = ea(this);t.cannotBeABaseURL || Ja(t, String(e), Ba);
	    }), port: po(lo, function (e) {
	      var t = ea(this);Pa(t) || ("" == (e = String(e)) ? t.port = null : Ja(t, e, Va));
	    }), pathname: po(co, function (e) {
	      var t = ea(this);t.cannotBeABaseURL || (t.path = [], Ja(t, e + "", $a));
	    }), search: po(uo, function (e) {
	      var t = ea(this);"" == (e = String(e)) ? t.query = null : ("?" == e.charAt(0) && (e = e.slice(1)), t.query = "", Ja(t, e, Xa)), Jr(t.searchParams).updateSearchParams(t.query);
	    }), searchParams: po(ho), hash: po(fo, function (e) {
	      var t = ea(this);"" != (e = String(e)) ? ("#" == e.charAt(0) && (e = e.slice(1)), t.fragment = "", Ja(t, e, Qa)) : t.fragment = null;
	    }) }), ee(eo, "toJSON", function () {
	    return to.call(this);
	  }, { enumerable: !0 }), ee(eo, "toString", function () {
	    return to.call(this);
	  }, { enumerable: !0 }), Xr) {
	    var mo = Xr.createObjectURL,
	        go = Xr.revokeObjectURL;mo && ee(Za, "createObjectURL", function (e) {
	      return mo.apply(Xr, arguments);
	    }), go && ee(Za, "revokeObjectURL", function (e) {
	      return go.apply(Xr, arguments);
	    });
	  }function vo(e) {
	    return (vo = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (e) {
	      return typeof e === "undefined" ? "undefined" : _typeof(e);
	    } : function (e) {
	      return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e === "undefined" ? "undefined" : _typeof(e);
	    })(e);
	  }function yo(e, t) {
	    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
	  }function bo(e, t) {
	    for (var n = 0; n < t.length; n++) {
	      var i = t[n];i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i);
	    }
	  }function wo(e, t, n) {
	    return t && bo(e.prototype, t), n && bo(e, n), e;
	  }function ko(e, t, n) {
	    return t in e ? Object.defineProperty(e, t, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : e[t] = n, e;
	  }function To(e, t) {
	    var n = Object.keys(e);if (Object.getOwnPropertySymbols) {
	      var i = Object.getOwnPropertySymbols(e);t && (i = i.filter(function (t) {
	        return Object.getOwnPropertyDescriptor(e, t).enumerable;
	      })), n.push.apply(n, i);
	    }return n;
	  }function So(e) {
	    for (var t = 1; t < arguments.length; t++) {
	      var n = null != arguments[t] ? arguments[t] : {};t % 2 ? To(Object(n), !0).forEach(function (t) {
	        ko(e, t, n[t]);
	      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : To(Object(n)).forEach(function (t) {
	        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
	      });
	    }return e;
	  }function Eo(e, t) {
	    if (null == e) return {};var n,
	        i,
	        r = function (e, t) {
	      if (null == e) return {};var n,
	          i,
	          r = {},
	          a = Object.keys(e);for (i = 0; i < a.length; i++) {
	        n = a[i], t.indexOf(n) >= 0 || (r[n] = e[n]);
	      }return r;
	    }(e, t);if (Object.getOwnPropertySymbols) {
	      var a = Object.getOwnPropertySymbols(e);for (i = 0; i < a.length; i++) {
	        n = a[i], t.indexOf(n) >= 0 || Object.prototype.propertyIsEnumerable.call(e, n) && (r[n] = e[n]);
	      }
	    }return r;
	  }function Ao(e, t) {
	    return function (e) {
	      if (Array.isArray(e)) return e;
	    }(e) || function (e, t) {
	      if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(e))) return;var n = [],
	          i = !0,
	          r = !1,
	          a = void 0;try {
	        for (var o, s = e[Symbol.iterator](); !(i = (o = s.next()).done) && (n.push(o.value), !t || n.length !== t); i = !0) {}
	      } catch (e) {
	        r = !0, a = e;
	      } finally {
	        try {
	          i || null == s.return || s.return();
	        } finally {
	          if (r) throw a;
	        }
	      }return n;
	    }(e, t) || xo(e, t) || function () {
	      throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
	    }();
	  }function Po(e) {
	    return function (e) {
	      if (Array.isArray(e)) return Co(e);
	    }(e) || function (e) {
	      if ("undefined" != typeof Symbol && Symbol.iterator in Object(e)) return Array.from(e);
	    }(e) || xo(e) || function () {
	      throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
	    }();
	  }function xo(e, t) {
	    if (e) {
	      if ("string" == typeof e) return Co(e, t);var n = Object.prototype.toString.call(e).slice(8, -1);return "Object" === n && e.constructor && (n = e.constructor.name), "Map" === n || "Set" === n ? Array.from(e) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? Co(e, t) : void 0;
	    }
	  }function Co(e, t) {
	    (null == t || t > e.length) && (t = e.length);for (var n = 0, i = new Array(t); n < t; n++) {
	      i[n] = e[n];
	    }return i;
	  }nt(Za, "URL"), Ie({ global: !0, forced: !rr, sham: !a }, { URL: Za }), function (e) {
	    var t = function () {
	      try {
	        return !!Symbol.iterator;
	      } catch (e) {
	        return !1;
	      }
	    }(),
	        n = function n(e) {
	      var n = { next: function next() {
	          var t = e.shift();return { done: void 0 === t, value: t };
	        } };return t && (n[Symbol.iterator] = function () {
	        return n;
	      }), n;
	    },
	        i = function i(e) {
	      return encodeURIComponent(e).replace(/%20/g, "+");
	    },
	        r = function r(e) {
	      return decodeURIComponent(String(e).replace(/\+/g, " "));
	    };(function () {
	      try {
	        var t = e.URLSearchParams;return "a=1" === new t("?a=1").toString() && "function" == typeof t.prototype.set;
	      } catch (e) {
	        return !1;
	      }
	    })() || function () {
	      var r = function e(t) {
	        Object.defineProperty(this, "_entries", { writable: !0, value: {} });var n = vo(t);if ("undefined" === n) ;else if ("string" === n) "" !== t && this._fromString(t);else if (t instanceof e) {
	          var i = this;t.forEach(function (e, t) {
	            i.append(t, e);
	          });
	        } else {
	          if (null === t || "object" !== n) throw new TypeError("Unsupported input's type for URLSearchParams");if ("[object Array]" === Object.prototype.toString.call(t)) for (var r = 0; r < t.length; r++) {
	            var a = t[r];if ("[object Array]" !== Object.prototype.toString.call(a) && 2 === a.length) throw new TypeError("Expected [string, any] as entry at index " + r + " of URLSearchParams's input");this.append(a[0], a[1]);
	          } else for (var o in t) {
	            t.hasOwnProperty(o) && this.append(o, t[o]);
	          }
	        }
	      },
	          a = r.prototype;a.append = function (e, t) {
	        e in this._entries ? this._entries[e].push(String(t)) : this._entries[e] = [String(t)];
	      }, a.delete = function (e) {
	        delete this._entries[e];
	      }, a.get = function (e) {
	        return e in this._entries ? this._entries[e][0] : null;
	      }, a.getAll = function (e) {
	        return e in this._entries ? this._entries[e].slice(0) : [];
	      }, a.has = function (e) {
	        return e in this._entries;
	      }, a.set = function (e, t) {
	        this._entries[e] = [String(t)];
	      }, a.forEach = function (e, t) {
	        var n;for (var i in this._entries) {
	          if (this._entries.hasOwnProperty(i)) {
	            n = this._entries[i];for (var r = 0; r < n.length; r++) {
	              e.call(t, n[r], i, this);
	            }
	          }
	        }
	      }, a.keys = function () {
	        var e = [];return this.forEach(function (t, n) {
	          e.push(n);
	        }), n(e);
	      }, a.values = function () {
	        var e = [];return this.forEach(function (t) {
	          e.push(t);
	        }), n(e);
	      }, a.entries = function () {
	        var e = [];return this.forEach(function (t, n) {
	          e.push([n, t]);
	        }), n(e);
	      }, t && (a[Symbol.iterator] = a.entries), a.toString = function () {
	        var e = [];return this.forEach(function (t, n) {
	          e.push(i(n) + "=" + i(t));
	        }), e.join("&");
	      }, e.URLSearchParams = r;
	    }();var a = e.URLSearchParams.prototype;"function" != typeof a.sort && (a.sort = function () {
	      var e = this,
	          t = [];this.forEach(function (n, i) {
	        t.push([i, n]), e._entries || e.delete(i);
	      }), t.sort(function (e, t) {
	        return e[0] < t[0] ? -1 : e[0] > t[0] ? 1 : 0;
	      }), e._entries && (e._entries = {});for (var n = 0; n < t.length; n++) {
	        this.append(t[n][0], t[n][1]);
	      }
	    }), "function" != typeof a._fromString && Object.defineProperty(a, "_fromString", { enumerable: !1, configurable: !1, writable: !1, value: function value(e) {
	        if (this._entries) this._entries = {};else {
	          var t = [];this.forEach(function (e, n) {
	            t.push(n);
	          });for (var n = 0; n < t.length; n++) {
	            this.delete(t[n]);
	          }
	        }var i,
	            a = (e = e.replace(/^\?/, "")).split("&");for (n = 0; n < a.length; n++) {
	          i = a[n].split("="), this.append(r(i[0]), i.length > 1 ? r(i[1]) : "");
	        }
	      } });
	  }(void 0 !== e ? e : "undefined" != typeof window ? window : "undefined" != typeof self ? self : e), function (e) {
	    if (function () {
	      try {
	        var t = new e.URL("b", "http://a");return t.pathname = "c d", "http://a/c%20d" === t.href && t.searchParams;
	      } catch (e) {
	        return !1;
	      }
	    }() || function () {
	      var t = e.URL,
	          n = function n(t, _n2) {
	        "string" != typeof t && (t = String(t));var i,
	            r = document;if (_n2 && (void 0 === e.location || _n2 !== e.location.href)) {
	          (i = (r = document.implementation.createHTMLDocument("")).createElement("base")).href = _n2, r.head.appendChild(i);try {
	            if (0 !== i.href.indexOf(_n2)) throw new Error(i.href);
	          } catch (e) {
	            throw new Error("URL unable to set base " + _n2 + " due to " + e);
	          }
	        }var a = r.createElement("a");if (a.href = t, i && (r.body.appendChild(a), a.href = a.href), ":" === a.protocol || !/:/.test(a.href)) throw new TypeError("Invalid URL");Object.defineProperty(this, "_anchorElement", { value: a });var o = new e.URLSearchParams(this.search),
	            s = !0,
	            l = !0,
	            c = this;["append", "delete", "set"].forEach(function (e) {
	          var t = o[e];o[e] = function () {
	            t.apply(o, arguments), s && (l = !1, c.search = o.toString(), l = !0);
	          };
	        }), Object.defineProperty(this, "searchParams", { value: o, enumerable: !0 });var u = void 0;Object.defineProperty(this, "_updateSearchParams", { enumerable: !1, configurable: !1, writable: !1, value: function value() {
	            this.search !== u && (u = this.search, l && (s = !1, this.searchParams._fromString(this.search), s = !0));
	          } });
	      },
	          i = n.prototype;["hash", "host", "hostname", "port", "protocol"].forEach(function (e) {
	        !function (e) {
	          Object.defineProperty(i, e, { get: function get() {
	              return this._anchorElement[e];
	            }, set: function set(t) {
	              this._anchorElement[e] = t;
	            }, enumerable: !0 });
	        }(e);
	      }), Object.defineProperty(i, "search", { get: function get() {
	          return this._anchorElement.search;
	        }, set: function set(e) {
	          this._anchorElement.search = e, this._updateSearchParams();
	        }, enumerable: !0 }), Object.defineProperties(i, { toString: { get: function get() {
	            var e = this;return function () {
	              return e.href;
	            };
	          } }, href: { get: function get() {
	            return this._anchorElement.href.replace(/\?$/, "");
	          }, set: function set(e) {
	            this._anchorElement.href = e, this._updateSearchParams();
	          }, enumerable: !0 }, pathname: { get: function get() {
	            return this._anchorElement.pathname.replace(/(^\/?)/, "/");
	          }, set: function set(e) {
	            this._anchorElement.pathname = e;
	          }, enumerable: !0 }, origin: { get: function get() {
	            var e = { "http:": 80, "https:": 443, "ftp:": 21 }[this._anchorElement.protocol],
	                t = this._anchorElement.port != e && "" !== this._anchorElement.port;return this._anchorElement.protocol + "//" + this._anchorElement.hostname + (t ? ":" + this._anchorElement.port : "");
	          }, enumerable: !0 }, password: { get: function get() {
	            return "";
	          }, set: function set(e) {}, enumerable: !0 }, username: { get: function get() {
	            return "";
	          }, set: function set(e) {}, enumerable: !0 } }), n.createObjectURL = function (e) {
	        return t.createObjectURL.apply(t, arguments);
	      }, n.revokeObjectURL = function (e) {
	        return t.revokeObjectURL.apply(t, arguments);
	      }, e.URL = n;
	    }(), void 0 !== e.location && !("origin" in e.location)) {
	      var t = function t() {
	        return e.location.protocol + "//" + e.location.hostname + (e.location.port ? ":" + e.location.port : "");
	      };try {
	        Object.defineProperty(e.location, "origin", { get: t, enumerable: !0 });
	      } catch (n) {
	        setInterval(function () {
	          e.location.origin = t();
	        }, 100);
	      }
	    }
	  }(void 0 !== e ? e : "undefined" != typeof window ? window : "undefined" != typeof self ? self : e);var Oo = Xe("isConcatSpreadable"),
	      Io = zn >= 51 || !r(function () {
	    var e = [];return e[Oo] = !1, e.concat()[0] !== e;
	  }),
	      Lo = Kn("concat"),
	      jo = function jo(e) {
	    if (!g(e)) return !1;var t = e[Oo];return void 0 !== t ? !!t : Ne(e);
	  };Ie({ target: "Array", proto: !0, forced: !Io || !Lo }, { concat: function concat(e) {
	      var t,
	          n,
	          i,
	          r,
	          a,
	          o = Re(this),
	          s = ot(o, 0),
	          l = 0;for (t = -1, i = arguments.length; t < i; t++) {
	        if (a = -1 === t ? o : arguments[t], jo(a)) {
	          if (l + (r = le(a.length)) > 9007199254740991) throw TypeError("Maximum allowed index exceeded");for (n = 0; n < r; n++, l++) {
	            n in a && Fn(s, l, a[n]);
	          }
	        } else {
	          if (l >= 9007199254740991) throw TypeError("Maximum allowed index exceeded");Fn(s, l++, a);
	        }
	      }return s.length = l, s;
	    } });var No = ct.filter,
	      Ro = Kn("filter"),
	      Mo = Qt("filter");Ie({ target: "Array", proto: !0, forced: !Ro || !Mo }, { filter: function filter(e) {
	      return No(this, e, arguments.length > 1 ? arguments[1] : void 0);
	    } });var _o = ct.find,
	      Uo = !0,
	      Do = Qt("find");"find" in [] && Array(1).find(function () {
	    Uo = !1;
	  }), Ie({ target: "Array", proto: !0, forced: Uo || !Do }, { find: function find(e) {
	      return _o(this, e, arguments.length > 1 ? arguments[1] : void 0);
	    } }), dn("find");var Fo = Xe("iterator"),
	      qo = !1;try {
	    var Ho = 0,
	        Bo = { next: function next() {
	        return { done: !!Ho++ };
	      }, return: function _return() {
	        qo = !0;
	      } };Bo[Fo] = function () {
	      return this;
	    }, Array.from(Bo, function () {
	      throw 2;
	    });
	  } catch (e) {}var Vo = function Vo(e, t) {
	    if (!t && !qo) return !1;var n = !1;try {
	      var i = {};i[Fo] = function () {
	        return { next: function next() {
	            return { done: n = !0 };
	          } };
	      }, e(i);
	    } catch (e) {}return n;
	  },
	      zo = !Vo(function (e) {
	    Array.from(e);
	  });Ie({ target: "Array", stat: !0, forced: zo }, { from: mr });var Wo = de.includes,
	      Ko = Qt("indexOf", { ACCESSORS: !0, 1: 0 });Ie({ target: "Array", proto: !0, forced: !Ko }, { includes: function includes(e) {
	      return Wo(this, e, arguments.length > 1 ? arguments[1] : void 0);
	    } }), dn("includes");var $o = ct.map,
	      Yo = Kn("map"),
	      Go = Qt("map");Ie({ target: "Array", proto: !0, forced: !Yo || !Go }, { map: function map(e) {
	      return $o(this, e, arguments.length > 1 ? arguments[1] : void 0);
	    } });var Xo = function Xo(e, t, n) {
	    var i, r;return An && "function" == typeof (i = t.constructor) && i !== n && g(r = i.prototype) && r !== n.prototype && An(e, r), e;
	  },
	      Qo = "\t\n\x0B\f\r  \u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF",
	      Jo = "[" + Qo + "]",
	      Zo = RegExp("^" + Jo + Jo + "*"),
	      es = RegExp(Jo + Jo + "*$"),
	      ts = function ts(e) {
	    return function (t) {
	      var n = String(p(t));return 1 & e && (n = n.replace(Zo, "")), 2 & e && (n = n.replace(es, "")), n;
	    };
	  },
	      ns = { start: ts(1), end: ts(2), trim: ts(3) },
	      is = ye.f,
	      rs = A.f,
	      as = C.f,
	      os = ns.trim,
	      ss = i.Number,
	      ls = ss.prototype,
	      cs = "Number" == h(Be(ls)),
	      us = function us(e) {
	    var t,
	        n,
	        i,
	        r,
	        a,
	        o,
	        s,
	        l,
	        c = v(e, !1);if ("string" == typeof c && c.length > 2) if (43 === (t = (c = os(c)).charCodeAt(0)) || 45 === t) {
	      if (88 === (n = c.charCodeAt(2)) || 120 === n) return NaN;
	    } else if (48 === t) {
	      switch (c.charCodeAt(1)) {case 66:case 98:
	          i = 2, r = 49;break;case 79:case 111:
	          i = 8, r = 55;break;default:
	          return +c;}for (o = (a = c.slice(2)).length, s = 0; s < o; s++) {
	        if ((l = a.charCodeAt(s)) < 48 || l > r) return NaN;
	      }return parseInt(a, i);
	    }return +c;
	  };if (Ce("Number", !ss(" 0o1") || !ss("0b1") || ss("+0x1"))) {
	    for (var hs, fs = function fs(e) {
	      var t = arguments.length < 1 ? 0 : e,
	          n = this;return n instanceof fs && (cs ? r(function () {
	        ls.valueOf.call(n);
	      }) : "Number" != h(n)) ? Xo(new ss(us(t)), n, fs) : us(t);
	    }, ds = a ? is(ss) : "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","), ps = 0; ds.length > ps; ps++) {
	      b(ss, hs = ds[ps]) && !b(fs, hs) && as(fs, hs, rs(ss, hs));
	    }fs.prototype = ls, ls.constructor = fs, ee(i, "Number", fs);
	  }var ms = r(function () {
	    Me(1);
	  });Ie({ target: "Object", stat: !0, forced: ms }, { keys: function keys(e) {
	      return Me(Re(e));
	    } });var gs = function gs(e) {
	    if (qi(e)) throw TypeError("The method doesn't accept regular expressions");return e;
	  },
	      vs = Xe("match"),
	      ys = function ys(e) {
	    var t = /./;try {
	      "/./"[e](t);
	    } catch (n) {
	      try {
	        return t[vs] = !1, "/./"[e](t);
	      } catch (e) {}
	    }return !1;
	  };Ie({ target: "String", proto: !0, forced: !ys("includes") }, { includes: function includes(e) {
	      return !!~String(p(this)).indexOf(gs(e), arguments.length > 1 ? arguments[1] : void 0);
	    } });var bs = !r(function () {
	    return Object.isExtensible(Object.preventExtensions({}));
	  }),
	      ws = t(function (e) {
	    var t = C.f,
	        n = B("meta"),
	        i = 0,
	        r = Object.isExtensible || function () {
	      return !0;
	    },
	        a = function a(e) {
	      t(e, n, { value: { objectID: "O" + ++i, weakData: {} } });
	    },
	        o = e.exports = { REQUIRED: !1, fastKey: function fastKey(e, t) {
	        if (!g(e)) return "symbol" == (typeof e === "undefined" ? "undefined" : _typeof(e)) ? e : ("string" == typeof e ? "S" : "P") + e;if (!b(e, n)) {
	          if (!r(e)) return "F";if (!t) return "E";a(e);
	        }return e[n].objectID;
	      }, getWeakData: function getWeakData(e, t) {
	        if (!b(e, n)) {
	          if (!r(e)) return !0;if (!t) return !1;a(e);
	        }return e[n].weakData;
	      }, onFreeze: function onFreeze(e) {
	        return bs && o.REQUIRED && r(e) && !b(e, n) && a(e), e;
	      } };W[n] = !0;
	  }),
	      ks = (ws.REQUIRED, ws.fastKey, ws.getWeakData, ws.onFreeze, t(function (e) {
	    var t = function t(e, _t4) {
	      this.stopped = e, this.result = _t4;
	    };(e.exports = function (e, n, i, r, a) {
	      var o,
	          s,
	          l,
	          c,
	          u,
	          h,
	          f,
	          d = rt(n, i, r ? 2 : 1);if (a) o = e;else {
	        if ("function" != typeof (s = pr(e))) throw TypeError("Target is not iterable");if (fr(s)) {
	          for (l = 0, c = le(e.length); c > l; l++) {
	            if ((u = r ? d(P(f = e[l])[0], f[1]) : d(e[l])) && u instanceof t) return u;
	          }return new t(!1);
	        }o = s.call(e);
	      }for (h = o.next; !(f = h.call(o)).done;) {
	        if ("object" == _typeof(u = cr(o, d, f.value, r)) && u && u instanceof t) return u;
	      }return new t(!1);
	    }).stop = function (e) {
	      return new t(!0, e);
	    };
	  })),
	      Ts = ws.getWeakData,
	      Ss = Z.set,
	      Es = Z.getterFor,
	      As = ct.find,
	      Ps = ct.findIndex,
	      xs = 0,
	      Cs = function Cs(e) {
	    return e.frozen || (e.frozen = new Os());
	  },
	      Os = function Os() {
	    this.entries = [];
	  },
	      Is = function Is(e, t) {
	    return As(e.entries, function (e) {
	      return e[0] === t;
	    });
	  };Os.prototype = { get: function get(e) {
	      var t = Is(this, e);if (t) return t[1];
	    }, has: function has(e) {
	      return !!Is(this, e);
	    }, set: function set(e, t) {
	      var n = Is(this, e);n ? n[1] = t : this.entries.push([e, t]);
	    }, delete: function _delete(e) {
	      var t = Ps(this.entries, function (t) {
	        return t[0] === e;
	      });return ~t && this.entries.splice(t, 1), !!~t;
	    } };var Ls = { getConstructor: function getConstructor(e, t, n, i) {
	      var r = e(function (e, a) {
	        ar(e, r, t), Ss(e, { type: t, id: xs++, frozen: void 0 }), null != a && ks(a, e[i], e, n);
	      }),
	          a = Es(t),
	          o = function o(e, t, n) {
	        var i = a(e),
	            r = Ts(P(t), !0);return !0 === r ? Cs(i).set(t, n) : r[i.id] = n, e;
	      };return Er(r.prototype, { delete: function _delete(e) {
	          var t = a(this);if (!g(e)) return !1;var n = Ts(e);return !0 === n ? Cs(t).delete(e) : n && b(n, t.id) && delete n[t.id];
	        }, has: function has(e) {
	          var t = a(this);if (!g(e)) return !1;var n = Ts(e);return !0 === n ? Cs(t).has(e) : n && b(n, t.id);
	        } }), Er(r.prototype, n ? { get: function get(e) {
	          var t = a(this);if (g(e)) {
	            var n = Ts(e);return !0 === n ? Cs(t).get(e) : n ? n[t.id] : void 0;
	          }
	        }, set: function set(e, t) {
	          return o(this, e, t);
	        } } : { add: function add(e) {
	          return o(this, e, !0);
	        } }), r;
	    } },
	      js = (t(function (e) {
	    var t,
	        n = Z.enforce,
	        a = !i.ActiveXObject && "ActiveXObject" in i,
	        o = Object.isExtensible,
	        s = function s(e) {
	      return function () {
	        return e(this, arguments.length ? arguments[0] : void 0);
	      };
	    },
	        l = e.exports = function (e, t, n) {
	      var a = -1 !== e.indexOf("Map"),
	          o = -1 !== e.indexOf("Weak"),
	          s = a ? "set" : "add",
	          l = i[e],
	          c = l && l.prototype,
	          u = l,
	          h = {},
	          f = function f(e) {
	        var t = c[e];ee(c, e, "add" == e ? function (e) {
	          return t.call(this, 0 === e ? 0 : e), this;
	        } : "delete" == e ? function (e) {
	          return !(o && !g(e)) && t.call(this, 0 === e ? 0 : e);
	        } : "get" == e ? function (e) {
	          return o && !g(e) ? void 0 : t.call(this, 0 === e ? 0 : e);
	        } : "has" == e ? function (e) {
	          return !(o && !g(e)) && t.call(this, 0 === e ? 0 : e);
	        } : function (e, n) {
	          return t.call(this, 0 === e ? 0 : e, n), this;
	        });
	      };if (Ce(e, "function" != typeof l || !(o || c.forEach && !r(function () {
	        new l().entries().next();
	      })))) u = n.getConstructor(t, e, a, s), ws.REQUIRED = !0;else if (Ce(e, !0)) {
	        var d = new u(),
	            p = d[s](o ? {} : -0, 1) != d,
	            m = r(function () {
	          d.has(1);
	        }),
	            v = Vo(function (e) {
	          new l(e);
	        }),
	            y = !o && r(function () {
	          for (var e = new l(), t = 5; t--;) {
	            e[s](t, t);
	          }return !e.has(-0);
	        });v || ((u = t(function (t, n) {
	          ar(t, u, e);var i = Xo(new l(), t, u);return null != n && ks(n, i[s], i, a), i;
	        })).prototype = c, c.constructor = u), (m || y) && (f("delete"), f("has"), a && f("get")), (y || p) && f(s), o && c.clear && delete c.clear;
	      }return h[e] = u, Ie({ global: !0, forced: u != l }, h), nt(u, e), o || n.setStrong(u, e, a), u;
	    }("WeakMap", s, Ls);if (D && a) {
	      t = Ls.getConstructor(s, "WeakMap", !0), ws.REQUIRED = !0;var c = l.prototype,
	          u = c.delete,
	          h = c.has,
	          f = c.get,
	          d = c.set;Er(c, { delete: function _delete(e) {
	          if (g(e) && !o(e)) {
	            var i = n(this);return i.frozen || (i.frozen = new t()), u.call(this, e) || i.frozen.delete(e);
	          }return u.call(this, e);
	        }, has: function has(e) {
	          if (g(e) && !o(e)) {
	            var i = n(this);return i.frozen || (i.frozen = new t()), h.call(this, e) || i.frozen.has(e);
	          }return h.call(this, e);
	        }, get: function get(e) {
	          if (g(e) && !o(e)) {
	            var i = n(this);return i.frozen || (i.frozen = new t()), h.call(this, e) ? f.call(this, e) : i.frozen.get(e);
	          }return f.call(this, e);
	        }, set: function set(e, i) {
	          if (g(e) && !o(e)) {
	            var r = n(this);r.frozen || (r.frozen = new t()), h.call(this, e) ? d.call(this, e, i) : r.frozen.set(e, i);
	          } else d.call(this, e, i);return this;
	        } });
	    }
	  }), ct.every),
	      Ns = $t("every"),
	      Rs = Qt("every");Ie({ target: "Array", proto: !0, forced: !Ns || !Rs }, { every: function every(e) {
	      return js(this, e, arguments.length > 1 ? arguments[1] : void 0);
	    } }), Ie({ target: "Object", stat: !0, forced: Object.assign !== lr }, { assign: lr });var Ms = ns.trim;Ie({ target: "String", proto: !0, forced: function (e) {
	      return r(function () {
	        return !!Qo[e]() || "​᠎" != "​᠎"[e]() || Qo[e].name !== e;
	      });
	    }("trim") }, { trim: function trim() {
	      return Ms(this);
	    } });var _s = ct.some,
	      Us = $t("some"),
	      Ds = Qt("some");Ie({ target: "Array", proto: !0, forced: !Us || !Ds }, { some: function some(e) {
	      return _s(this, e, arguments.length > 1 ? arguments[1] : void 0);
	    } });var Fs = "".repeat || function (e) {
	    var t = String(p(this)),
	        n = "",
	        i = oe(e);if (i < 0 || i == 1 / 0) throw RangeError("Wrong number of repetitions");for (; i > 0; (i >>>= 1) && (t += t)) {
	      1 & i && (n += t);
	    }return n;
	  },
	      qs = 1..toFixed,
	      Hs = Math.floor,
	      Bs = function Bs(e, t, n) {
	    return 0 === t ? n : t % 2 == 1 ? Bs(e, t - 1, n * e) : Bs(e * e, t / 2, n);
	  },
	      Vs = qs && ("0.000" !== 8e-5.toFixed(3) || "1" !== .9.toFixed(0) || "1.25" !== 1.255.toFixed(2) || "1000000000000000128" !== 0xde0b6b3a7640080.toFixed(0)) || !r(function () {
	    qs.call({});
	  });Ie({ target: "Number", proto: !0, forced: Vs }, { toFixed: function toFixed(e) {
	      var t,
	          n,
	          i,
	          r,
	          a = function (e) {
	        if ("number" != typeof e && "Number" != h(e)) throw TypeError("Incorrect invocation");return +e;
	      }(this),
	          o = oe(e),
	          s = [0, 0, 0, 0, 0, 0],
	          l = "",
	          c = "0",
	          u = function u(e, t) {
	        for (var n = -1, i = t; ++n < 6;) {
	          i += e * s[n], s[n] = i % 1e7, i = Hs(i / 1e7);
	        }
	      },
	          f = function f(e) {
	        for (var t = 6, n = 0; --t >= 0;) {
	          n += s[t], s[t] = Hs(n / e), n = n % e * 1e7;
	        }
	      },
	          d = function d() {
	        for (var e = 6, t = ""; --e >= 0;) {
	          if ("" !== t || 0 === e || 0 !== s[e]) {
	            var n = String(s[e]);t = "" === t ? n : t + Fs.call("0", 7 - n.length) + n;
	          }
	        }return t;
	      };if (o < 0 || o > 20) throw RangeError("Incorrect fraction digits");if (a != a) return "NaN";if (a <= -1e21 || a >= 1e21) return String(a);if (a < 0 && (l = "-", a = -a), a > 1e-21) if (n = (t = function (e) {
	        for (var t = 0, n = e; n >= 4096;) {
	          t += 12, n /= 4096;
	        }for (; n >= 2;) {
	          t += 1, n /= 2;
	        }return t;
	      }(a * Bs(2, 69, 1)) - 69) < 0 ? a * Bs(2, -t, 1) : a / Bs(2, t, 1), n *= 4503599627370496, (t = 52 - t) > 0) {
	        for (u(0, n), i = o; i >= 7;) {
	          u(1e7, 0), i -= 7;
	        }for (u(Bs(10, i, 1), 0), i = t - 1; i >= 23;) {
	          f(1 << 23), i -= 23;
	        }f(1 << i), u(1, 1), f(2), c = d();
	      } else u(0, n), u(1 << -t, 0), c = d() + Fs.call("0", o);return c = o > 0 ? l + ((r = c.length) <= o ? "0." + Fs.call("0", o - r) + c : c.slice(0, r - o) + "." + c.slice(r - o)) : l + c;
	    } });var zs = l.f,
	      Ws = function Ws(e) {
	    return function (t) {
	      for (var n, i = m(t), r = Me(i), o = r.length, s = 0, l = []; o > s;) {
	        n = r[s++], a && !zs.call(i, n) || l.push(e ? [n, i[n]] : i[n]);
	      }return l;
	    };
	  },
	      Ks = { entries: Ws(!0), values: Ws(!1) },
	      $s = Ks.entries;Ie({ target: "Object", stat: !0 }, { entries: function entries(e) {
	      return $s(e);
	    } });var Ys = Ks.values;Ie({ target: "Object", stat: !0 }, { values: function values(e) {
	      return Ys(e);
	    } }), Ie({ target: "Number", stat: !0 }, { isNaN: function isNaN(e) {
	      return e != e;
	    } });var Gs = A.f,
	      Xs = r(function () {
	    Gs(1);
	  });function Qs(e, t) {
	    for (var n = 0; n < t.length; n++) {
	      var i = t[n];i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i);
	    }
	  }function Js(e, t, n) {
	    return t in e ? Object.defineProperty(e, t, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : e[t] = n, e;
	  }function Zs(e, t) {
	    var n = Object.keys(e);if (Object.getOwnPropertySymbols) {
	      var i = Object.getOwnPropertySymbols(e);t && (i = i.filter(function (t) {
	        return Object.getOwnPropertyDescriptor(e, t).enumerable;
	      })), n.push.apply(n, i);
	    }return n;
	  }function el(e) {
	    for (var t = 1; t < arguments.length; t++) {
	      var n = null != arguments[t] ? arguments[t] : {};t % 2 ? Zs(Object(n), !0).forEach(function (t) {
	        Js(e, t, n[t]);
	      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : Zs(Object(n)).forEach(function (t) {
	        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
	      });
	    }return e;
	  }Ie({ target: "Object", stat: !0, forced: !a || Xs, sham: !a }, { getOwnPropertyDescriptor: function getOwnPropertyDescriptor(e, t) {
	      return Gs(m(e), t);
	    } }), Ie({ target: "Object", stat: !0, sham: !a }, { getOwnPropertyDescriptors: function getOwnPropertyDescriptors(e) {
	      for (var t, n, i = m(e), r = A.f, a = we(i), o = {}, s = 0; a.length > s;) {
	        void 0 !== (n = r(i, t = a[s++])) && Fn(o, t, n);
	      }return o;
	    } }), Oi("match", 1, function (e, t, n) {
	    return [function (t) {
	      var n = p(this),
	          i = null == t ? void 0 : t[e];return void 0 !== i ? i.call(t, n) : new RegExp(t)[e](String(n));
	    }, function (e) {
	      var i = n(t, e, this);if (i.done) return i.value;var r = P(e),
	          a = String(this);if (!r.global) return ji(r, a);var o = r.unicode;r.lastIndex = 0;for (var s, l = [], c = 0; null !== (s = ji(r, a));) {
	        var u = String(s[0]);l[c] = u, "" === u && (r.lastIndex = Li(a, le(r.lastIndex), o)), c++;
	      }return 0 === c ? null : l;
	    }];
	  });var tl = { addCSS: !0, thumbWidth: 15, watch: !0 };function nl(e, t) {
	    return function () {
	      return Array.from(document.querySelectorAll(t)).includes(this);
	    }.call(e, t);
	  }var il = function il(e) {
	    return null != e ? e.constructor : null;
	  },
	      rl = function rl(e, t) {
	    return !!(e && t && e instanceof t);
	  },
	      al = function al(e) {
	    return null == e;
	  },
	      ol = function ol(e) {
	    return il(e) === Object;
	  },
	      sl = function sl(e) {
	    return il(e) === String;
	  },
	      ll = function ll(e) {
	    return Array.isArray(e);
	  },
	      cl = function cl(e) {
	    return rl(e, NodeList);
	  },
	      ul = sl,
	      hl = ll,
	      fl = cl,
	      dl = function dl(e) {
	    return rl(e, Element);
	  },
	      pl = function pl(e) {
	    return rl(e, Event);
	  },
	      ml = function ml(e) {
	    return al(e) || (sl(e) || ll(e) || cl(e)) && !e.length || ol(e) && !Object.keys(e).length;
	  };function gl(e, t) {
	    if (1 > t) {
	      var n = function (e) {
	        var t = "".concat(e).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);return t ? Math.max(0, (t[1] ? t[1].length : 0) - (t[2] ? +t[2] : 0)) : 0;
	      }(t);return parseFloat(e.toFixed(n));
	    }return Math.round(e / t) * t;
	  }var vl,
	      yl,
	      bl,
	      wl = function () {
	    function e(t, n) {
	      (function (e, t) {
	        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
	      })(this, e), dl(t) ? this.element = t : ul(t) && (this.element = document.querySelector(t)), dl(this.element) && ml(this.element.rangeTouch) && (this.config = el({}, tl, {}, n), this.init());
	    }return function (e, t, n) {
	      t && Qs(e.prototype, t), n && Qs(e, n);
	    }(e, [{ key: "init", value: function value() {
	        e.enabled && (this.config.addCSS && (this.element.style.userSelect = "none", this.element.style.webKitUserSelect = "none", this.element.style.touchAction = "manipulation"), this.listeners(!0), this.element.rangeTouch = this);
	      } }, { key: "destroy", value: function value() {
	        e.enabled && (this.config.addCSS && (this.element.style.userSelect = "", this.element.style.webKitUserSelect = "", this.element.style.touchAction = ""), this.listeners(!1), this.element.rangeTouch = null);
	      } }, { key: "listeners", value: function value(e) {
	        var t = this,
	            n = e ? "addEventListener" : "removeEventListener";["touchstart", "touchmove", "touchend"].forEach(function (e) {
	          t.element[n](e, function (e) {
	            return t.set(e);
	          }, !1);
	        });
	      } }, { key: "get", value: function value(t) {
	        if (!e.enabled || !pl(t)) return null;var n,
	            i = t.target,
	            r = t.changedTouches[0],
	            a = parseFloat(i.getAttribute("min")) || 0,
	            o = parseFloat(i.getAttribute("max")) || 100,
	            s = parseFloat(i.getAttribute("step")) || 1,
	            l = i.getBoundingClientRect(),
	            c = 100 / l.width * (this.config.thumbWidth / 2) / 100;return 0 > (n = 100 / l.width * (r.clientX - l.left)) ? n = 0 : 100 < n && (n = 100), 50 > n ? n -= (100 - 2 * n) * c : 50 < n && (n += 2 * (n - 50) * c), a + gl(n / 100 * (o - a), s);
	      } }, { key: "set", value: function value(t) {
	        e.enabled && pl(t) && !t.target.disabled && (t.preventDefault(), t.target.value = this.get(t), function (e, t) {
	          if (e && t) {
	            var n = new Event(t, { bubbles: !0 });e.dispatchEvent(n);
	          }
	        }(t.target, "touchend" === t.type ? "change" : "input"));
	      } }], [{ key: "setup", value: function value(t) {
	        var n = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {},
	            i = null;if (ml(t) || ul(t) ? i = Array.from(document.querySelectorAll(ul(t) ? t : 'input[type="range"]')) : dl(t) ? i = [t] : fl(t) ? i = Array.from(t) : hl(t) && (i = t.filter(dl)), ml(i)) return null;var r = el({}, tl, {}, n);if (ul(t) && r.watch) {
	          var a = new MutationObserver(function (n) {
	            Array.from(n).forEach(function (n) {
	              Array.from(n.addedNodes).forEach(function (n) {
	                dl(n) && nl(n, t) && new e(n, r);
	              });
	            });
	          });a.observe(document.body, { childList: !0, subtree: !0 });
	        }return i.map(function (t) {
	          return new e(t, n);
	        });
	      } }, { key: "enabled", get: function get() {
	        return "ontouchstart" in document.documentElement;
	      } }]), e;
	  }(),
	      kl = i.Promise,
	      Tl = Xe("species"),
	      Sl = function Sl(e) {
	    var t = ie(e),
	        n = C.f;a && t && !t[Tl] && n(t, Tl, { configurable: !0, get: function get() {
	        return this;
	      } });
	  },
	      El = /(iphone|ipod|ipad).*applewebkit/i.test(qn),
	      Al = i.location,
	      Pl = i.setImmediate,
	      xl = i.clearImmediate,
	      Cl = i.process,
	      Ol = i.MessageChannel,
	      Il = i.Dispatch,
	      Ll = 0,
	      jl = {},
	      Nl = function Nl(e) {
	    if (jl.hasOwnProperty(e)) {
	      var t = jl[e];delete jl[e], t();
	    }
	  },
	      Rl = function Rl(e) {
	    return function () {
	      Nl(e);
	    };
	  },
	      Ml = function Ml(e) {
	    Nl(e.data);
	  },
	      _l = function _l(e) {
	    i.postMessage(e + "", Al.protocol + "//" + Al.host);
	  };Pl && xl || (Pl = function Pl(e) {
	    for (var t = [], n = 1; arguments.length > n;) {
	      t.push(arguments[n++]);
	    }return jl[++Ll] = function () {
	      ("function" == typeof e ? e : Function(e)).apply(void 0, t);
	    }, vl(Ll), Ll;
	  }, xl = function xl(e) {
	    delete jl[e];
	  }, "process" == h(Cl) ? vl = function vl(e) {
	    Cl.nextTick(Rl(e));
	  } : Il && Il.now ? vl = function vl(e) {
	    Il.now(Rl(e));
	  } : Ol && !El ? (bl = (yl = new Ol()).port2, yl.port1.onmessage = Ml, vl = rt(bl.postMessage, bl, 1)) : !i.addEventListener || "function" != typeof postMessage || i.importScripts || r(_l) || "file:" === Al.protocol ? vl = "onreadystatechange" in T("script") ? function (e) {
	    Ue.appendChild(T("script")).onreadystatechange = function () {
	      Ue.removeChild(this), Nl(e);
	    };
	  } : function (e) {
	    setTimeout(Rl(e), 0);
	  } : (vl = _l, i.addEventListener("message", Ml, !1)));var Ul,
	      Dl,
	      Fl,
	      ql,
	      Hl,
	      Bl,
	      Vl,
	      zl,
	      Wl = { set: Pl, clear: xl },
	      Kl = A.f,
	      $l = Wl.set,
	      Yl = i.MutationObserver || i.WebKitMutationObserver,
	      Gl = i.process,
	      Xl = i.Promise,
	      Ql = "process" == h(Gl),
	      Jl = Kl(i, "queueMicrotask"),
	      Zl = Jl && Jl.value;Zl || (Ul = function Ul() {
	    var e, t;for (Ql && (e = Gl.domain) && e.exit(); Dl;) {
	      t = Dl.fn, Dl = Dl.next;try {
	        t();
	      } catch (e) {
	        throw Dl ? ql() : Fl = void 0, e;
	      }
	    }Fl = void 0, e && e.enter();
	  }, Ql ? ql = function ql() {
	    Gl.nextTick(Ul);
	  } : Yl && !El ? (Hl = !0, Bl = document.createTextNode(""), new Yl(Ul).observe(Bl, { characterData: !0 }), ql = function ql() {
	    Bl.data = Hl = !Hl;
	  }) : Xl && Xl.resolve ? (Vl = Xl.resolve(void 0), zl = Vl.then, ql = function ql() {
	    zl.call(Vl, Ul);
	  }) : ql = function ql() {
	    $l.call(i, Ul);
	  });var ec,
	      tc,
	      nc,
	      ic,
	      rc = Zl || function (e) {
	    var t = { fn: e, next: void 0 };Fl && (Fl.next = t), Dl || (Dl = t, ql()), Fl = t;
	  },
	      ac = function ac(e) {
	    var t, n;this.promise = new e(function (e, i) {
	      if (void 0 !== t || void 0 !== n) throw TypeError("Bad Promise constructor");t = e, n = i;
	    }), this.resolve = it(t), this.reject = it(n);
	  },
	      oc = { f: function f(e) {
	      return new ac(e);
	    } },
	      sc = function sc(e, t) {
	    if (P(e), g(t) && t.constructor === e) return t;var n = oc.f(e);return (0, n.resolve)(t), n.promise;
	  },
	      lc = function lc(e) {
	    try {
	      return { error: !1, value: e() };
	    } catch (e) {
	      return { error: !0, value: e };
	    }
	  },
	      cc = Wl.set,
	      uc = Xe("species"),
	      hc = "Promise",
	      fc = Z.get,
	      dc = Z.set,
	      pc = Z.getterFor(hc),
	      _mc = kl,
	      gc = i.TypeError,
	      vc = i.document,
	      yc = i.process,
	      bc = ie("fetch"),
	      wc = oc.f,
	      kc = wc,
	      Tc = "process" == h(yc),
	      Sc = !!(vc && vc.createEvent && i.dispatchEvent),
	      Ec = Ce(hc, function () {
	    if (!(_(_mc) !== String(_mc))) {
	      if (66 === zn) return !0;if (!Tc && "function" != typeof PromiseRejectionEvent) return !0;
	    }if (zn >= 51 && /native code/.test(_mc)) return !1;var e = _mc.resolve(1),
	        t = function t(e) {
	      e(function () {}, function () {});
	    };return (e.constructor = {})[uc] = t, !(e.then(function () {}) instanceof t);
	  }),
	      Ac = Ec || !Vo(function (e) {
	    _mc.all(e).catch(function () {});
	  }),
	      Pc = function Pc(e) {
	    var t;return !(!g(e) || "function" != typeof (t = e.then)) && t;
	  },
	      xc = function xc(e, t, n) {
	    if (!t.notified) {
	      t.notified = !0;var i = t.reactions;rc(function () {
	        for (var r = t.value, a = 1 == t.state, o = 0; i.length > o;) {
	          var s,
	              l,
	              c,
	              u = i[o++],
	              h = a ? u.ok : u.fail,
	              f = u.resolve,
	              d = u.reject,
	              p = u.domain;try {
	            h ? (a || (2 === t.rejection && Lc(e, t), t.rejection = 1), !0 === h ? s = r : (p && p.enter(), s = h(r), p && (p.exit(), c = !0)), s === u.promise ? d(gc("Promise-chain cycle")) : (l = Pc(s)) ? l.call(s, f, d) : f(s)) : d(r);
	          } catch (e) {
	            p && !c && p.exit(), d(e);
	          }
	        }t.reactions = [], t.notified = !1, n && !t.rejection && Oc(e, t);
	      });
	    }
	  },
	      Cc = function Cc(e, t, n) {
	    var r, a;Sc ? ((r = vc.createEvent("Event")).promise = t, r.reason = n, r.initEvent(e, !1, !0), i.dispatchEvent(r)) : r = { promise: t, reason: n }, (a = i["on" + e]) ? a(r) : "unhandledrejection" === e && function (e, t) {
	      var n = i.console;n && n.error && (1 === arguments.length ? n.error(e) : n.error(e, t));
	    }("Unhandled promise rejection", n);
	  },
	      Oc = function Oc(e, t) {
	    cc.call(i, function () {
	      var n,
	          i = t.value;if (Ic(t) && (n = lc(function () {
	        Tc ? yc.emit("unhandledRejection", i, e) : Cc("unhandledrejection", e, i);
	      }), t.rejection = Tc || Ic(t) ? 2 : 1, n.error)) throw n.value;
	    });
	  },
	      Ic = function Ic(e) {
	    return 1 !== e.rejection && !e.parent;
	  },
	      Lc = function Lc(e, t) {
	    cc.call(i, function () {
	      Tc ? yc.emit("rejectionHandled", e) : Cc("rejectionhandled", e, t.value);
	    });
	  },
	      jc = function jc(e, t, n, i) {
	    return function (r) {
	      e(t, n, r, i);
	    };
	  },
	      Nc = function Nc(e, t, n, i) {
	    t.done || (t.done = !0, i && (t = i), t.value = n, t.state = 2, xc(e, t, !0));
	  },
	      Rc = function Rc(e, t, n, i) {
	    if (!t.done) {
	      t.done = !0, i && (t = i);try {
	        if (e === n) throw gc("Promise can't be resolved itself");var r = Pc(n);r ? rc(function () {
	          var i = { done: !1 };try {
	            r.call(n, jc(Rc, e, i, t), jc(Nc, e, i, t));
	          } catch (n) {
	            Nc(e, i, n, t);
	          }
	        }) : (t.value = n, t.state = 1, xc(e, t, !1));
	      } catch (n) {
	        Nc(e, { done: !1 }, n, t);
	      }
	    }
	  };Ec && (_mc = function mc(e) {
	    ar(this, _mc, hc), it(e), ec.call(this);var t = fc(this);try {
	      e(jc(Rc, this, t), jc(Nc, this, t));
	    } catch (e) {
	      Nc(this, t, e);
	    }
	  }, (ec = function ec(e) {
	    dc(this, { type: hc, done: !1, notified: !1, parent: !1, reactions: [], rejection: !1, state: 0, value: void 0 });
	  }).prototype = Er(_mc.prototype, { then: function then(e, t) {
	      var n = pc(this),
	          i = wc(Bi(this, _mc));return i.ok = "function" != typeof e || e, i.fail = "function" == typeof t && t, i.domain = Tc ? yc.domain : void 0, n.parent = !0, n.reactions.push(i), 0 != n.state && xc(this, n, !1), i.promise;
	    }, catch: function _catch(e) {
	      return this.then(void 0, e);
	    } }), tc = function tc() {
	    var e = new ec(),
	        t = fc(e);this.promise = e, this.resolve = jc(Rc, e, t), this.reject = jc(Nc, e, t);
	  }, oc.f = wc = function wc(e) {
	    return e === _mc || e === nc ? new tc(e) : kc(e);
	  }, "function" == typeof kl && (ic = kl.prototype.then, ee(kl.prototype, "then", function (e, t) {
	    var n = this;return new _mc(function (e, t) {
	      ic.call(n, e, t);
	    }).then(e, t);
	  }, { unsafe: !0 }), "function" == typeof bc && Ie({ global: !0, enumerable: !0, forced: !0 }, { fetch: function fetch(e) {
	      return sc(_mc, bc.apply(i, arguments));
	    } }))), Ie({ global: !0, wrap: !0, forced: Ec }, { Promise: _mc }), nt(_mc, hc, !1), Sl(hc), nc = ie(hc), Ie({ target: hc, stat: !0, forced: Ec }, { reject: function reject(e) {
	      var t = wc(this);return t.reject.call(void 0, e), t.promise;
	    } }), Ie({ target: hc, stat: !0, forced: Ec }, { resolve: function resolve(e) {
	      return sc(this, e);
	    } }), Ie({ target: hc, stat: !0, forced: Ac }, { all: function all(e) {
	      var t = this,
	          n = wc(t),
	          i = n.resolve,
	          r = n.reject,
	          a = lc(function () {
	        var n = it(t.resolve),
	            a = [],
	            o = 0,
	            s = 1;ks(e, function (e) {
	          var l = o++,
	              c = !1;a.push(void 0), s++, n.call(t, e).then(function (e) {
	            c || (c = !0, a[l] = e, --s || i(a));
	          }, r);
	        }), --s || i(a);
	      });return a.error && r(a.value), n.promise;
	    }, race: function race(e) {
	      var t = this,
	          n = wc(t),
	          i = n.reject,
	          r = lc(function () {
	        var r = it(t.resolve);ks(e, function (e) {
	          r.call(t, e).then(n.resolve, i);
	        });
	      });return r.error && i(r.value), n.promise;
	    } });var Mc,
	      _c = A.f,
	      Uc = "".startsWith,
	      Dc = Math.min,
	      Fc = ys("startsWith"),
	      qc = !(Fc || (Mc = _c(String.prototype, "startsWith"), !Mc || Mc.writable));Ie({ target: "String", proto: !0, forced: !qc && !Fc }, { startsWith: function startsWith(e) {
	      var t = String(p(this));gs(e);var n = le(Dc(arguments.length > 1 ? arguments[1] : void 0, t.length)),
	          i = String(e);return Uc ? Uc.call(t, i, n) : t.slice(n, n + i.length) === i;
	    } });var Hc,
	      Bc,
	      Vc,
	      zc = function zc(e) {
	    return null != e ? e.constructor : null;
	  },
	      Wc = function Wc(e, t) {
	    return Boolean(e && t && e instanceof t);
	  },
	      Kc = function Kc(e) {
	    return null == e;
	  },
	      $c = function $c(e) {
	    return zc(e) === Object;
	  },
	      Yc = function Yc(e) {
	    return zc(e) === String;
	  },
	      Gc = function Gc(e) {
	    return zc(e) === Function;
	  },
	      Xc = function Xc(e) {
	    return Array.isArray(e);
	  },
	      Qc = function Qc(e) {
	    return Wc(e, NodeList);
	  },
	      Jc = function Jc(e) {
	    return Kc(e) || (Yc(e) || Xc(e) || Qc(e)) && !e.length || $c(e) && !Object.keys(e).length;
	  },
	      Zc = Kc,
	      eu = $c,
	      tu = function tu(e) {
	    return zc(e) === Number && !Number.isNaN(e);
	  },
	      nu = Yc,
	      iu = function iu(e) {
	    return zc(e) === Boolean;
	  },
	      ru = Gc,
	      au = Xc,
	      ou = Qc,
	      su = function su(e) {
	    return Wc(e, Element);
	  },
	      lu = function lu(e) {
	    return Wc(e, Event);
	  },
	      cu = function cu(e) {
	    return Wc(e, KeyboardEvent);
	  },
	      uu = function uu(e) {
	    return Wc(e, TextTrack) || !Kc(e) && Yc(e.kind);
	  },
	      hu = function hu(e) {
	    return Wc(e, Promise) && Gc(e.then);
	  },
	      fu = function fu(e) {
	    if (Wc(e, window.URL)) return !0;if (!Yc(e)) return !1;var t = e;e.startsWith("http://") && e.startsWith("https://") || (t = "http://".concat(e));try {
	      return !Jc(new URL(t).hostname);
	    } catch (e) {
	      return !1;
	    }
	  },
	      du = Jc,
	      pu = (Hc = document.createElement("span"), Bc = { WebkitTransition: "webkitTransitionEnd", MozTransition: "transitionend", OTransition: "oTransitionEnd otransitionend", transition: "transitionend" }, Vc = Object.keys(Bc).find(function (e) {
	    return void 0 !== Hc.style[e];
	  }), !!nu(Vc) && Bc[Vc]);function mu(e, t) {
	    setTimeout(function () {
	      try {
	        e.hidden = !0, e.offsetHeight, e.hidden = !1;
	      } catch (e) {}
	    }, t);
	  }var gu = { isIE:
	    /* @cc_on!@ */
	    !!document.documentMode, isEdge: window.navigator.userAgent.includes("Edge"), isWebkit: "WebkitAppearance" in document.documentElement.style && !/Edge/.test(navigator.userAgent), isIPhone: /(iPhone|iPod)/gi.test(navigator.platform), isIos: /(iPad|iPhone|iPod)/gi.test(navigator.platform) },
	      vu = function vu(e) {
	    return function (t, n, i, r) {
	      it(n);var a = Re(t),
	          o = d(a),
	          s = le(a.length),
	          l = e ? s - 1 : 0,
	          c = e ? -1 : 1;if (i < 2) for (;;) {
	        if (l in o) {
	          r = o[l], l += c;break;
	        }if (l += c, e ? l < 0 : s <= l) throw TypeError("Reduce of empty array with no initial value");
	      }for (; e ? l >= 0 : s > l; l += c) {
	        l in o && (r = n(r, o[l], l, a));
	      }return r;
	    };
	  },
	      yu = { left: vu(!1), right: vu(!0) }.left,
	      bu = $t("reduce"),
	      wu = Qt("reduce", { 1: 0 });function ku(e, t) {
	    return t.split(".").reduce(function (e, t) {
	      return e && e[t];
	    }, e);
	  }function Tu() {
	    for (var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, t = arguments.length, n = new Array(t > 1 ? t - 1 : 0), i = 1; i < t; i++) {
	      n[i - 1] = arguments[i];
	    }if (!n.length) return e;var r = n.shift();return eu(r) ? (Object.keys(r).forEach(function (t) {
	      eu(r[t]) ? (Object.keys(e).includes(t) || Object.assign(e, ko({}, t, {})), Tu(e[t], r[t])) : Object.assign(e, ko({}, t, r[t]));
	    }), Tu.apply(void 0, [e].concat(n))) : e;
	  }function Su(e, t) {
	    var n = e.length ? e : [e];Array.from(n).reverse().forEach(function (e, n) {
	      var i = n > 0 ? t.cloneNode(!0) : t,
	          r = e.parentNode,
	          a = e.nextSibling;i.appendChild(e), a ? r.insertBefore(i, a) : r.appendChild(i);
	    });
	  }function Eu(e, t) {
	    su(e) && !du(t) && Object.entries(t).filter(function (e) {
	      var t = Ao(e, 2)[1];return !Zc(t);
	    }).forEach(function (t) {
	      var n = Ao(t, 2),
	          i = n[0],
	          r = n[1];return e.setAttribute(i, r);
	    });
	  }function Au(e, t, n) {
	    var i = document.createElement(e);return eu(t) && Eu(i, t), nu(n) && (i.innerText = n), i;
	  }function Pu(e, t, n, i) {
	    su(t) && t.appendChild(Au(e, n, i));
	  }function xu(e) {
	    ou(e) || au(e) ? Array.from(e).forEach(xu) : su(e) && su(e.parentNode) && e.parentNode.removeChild(e);
	  }function Cu(e) {
	    if (su(e)) for (var t = e.childNodes.length; t > 0;) {
	      e.removeChild(e.lastChild), t -= 1;
	    }
	  }function Ou(e, t) {
	    return su(t) && su(t.parentNode) && su(e) ? (t.parentNode.replaceChild(e, t), e) : null;
	  }function Iu(e, t) {
	    if (!nu(e) || du(e)) return {};var n = {},
	        i = Tu({}, t);return e.split(",").forEach(function (e) {
	      var t = e.trim(),
	          r = t.replace(".", ""),
	          a = t.replace(/[[\]]/g, "").split("="),
	          o = Ao(a, 1)[0],
	          s = a.length > 1 ? a[1].replace(/["']/g, "") : "";switch (t.charAt(0)) {case ".":
	          nu(i.class) ? n.class = "".concat(i.class, " ").concat(r) : n.class = r;break;case "#":
	          n.id = t.replace("#", "");break;case "[":
	          n[o] = s;}
	    }), Tu(i, n);
	  }function Lu(e, t) {
	    if (su(e)) {
	      var n = t;iu(n) || (n = !e.hidden), e.hidden = n;
	    }
	  }function ju(e, t, n) {
	    if (ou(e)) return Array.from(e).map(function (e) {
	      return ju(e, t, n);
	    });if (su(e)) {
	      var i = "toggle";return void 0 !== n && (i = n ? "add" : "remove"), e.classList[i](t), e.classList.contains(t);
	    }return !1;
	  }function Nu(e, t) {
	    return su(e) && e.classList.contains(t);
	  }function Ru(e, t) {
	    var n = Element.prototype;return (n.matches || n.webkitMatchesSelector || n.mozMatchesSelector || n.msMatchesSelector || function () {
	      return Array.from(document.querySelectorAll(t)).includes(this);
	    }).call(e, t);
	  }function Mu(e) {
	    return this.elements.container.querySelectorAll(e);
	  }function _u(e) {
	    return this.elements.container.querySelector(e);
	  }function Uu() {
	    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
	        t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];su(e) && (e.focus({ preventScroll: !0 }), t && ju(e, this.config.classNames.tabFocus));
	  }Ie({ target: "Array", proto: !0, forced: !bu || !wu }, { reduce: function reduce(e) {
	      return yu(this, e, arguments.length, arguments.length > 1 ? arguments[1] : void 0);
	    } });var Du,
	      Fu = { "audio/ogg": "vorbis", "audio/wav": "1", "video/webm": "vp8, vorbis", "video/mp4": "avc1.42E01E, mp4a.40.2", "video/ogg": "theora" },
	      qu = { audio: "canPlayType" in document.createElement("audio"), video: "canPlayType" in document.createElement("video"), check: function check(e, t, n) {
	      var i = gu.isIPhone && n && qu.playsinline,
	          r = qu[e] || "html5" !== t;return { api: r, ui: r && qu.rangeInput && ("video" !== e || !gu.isIPhone || i) };
	    }, pip: !(gu.isIPhone || !ru(Au("video").webkitSetPresentationMode) && (!document.pictureInPictureEnabled || Au("video").disablePictureInPicture)), airplay: ru(window.WebKitPlaybackTargetAvailabilityEvent), playsinline: "playsInline" in document.createElement("video"), mime: function mime(e) {
	      if (du(e)) return !1;var t = Ao(e.split("/"), 1)[0],
	          n = e;if (!this.isHTML5 || t !== this.type) return !1;Object.keys(Fu).includes(n) && (n += '; codecs="'.concat(Fu[e], '"'));try {
	        return Boolean(n && this.media.canPlayType(n).replace(/no/, ""));
	      } catch (e) {
	        return !1;
	      }
	    }, textTracks: "textTracks" in document.createElement("video"), rangeInput: (Du = document.createElement("input"), Du.type = "range", "range" === Du.type), touch: "ontouchstart" in document.documentElement, transitions: !1 !== pu, reducedMotion: "matchMedia" in window && window.matchMedia("(prefers-reduced-motion)").matches },
	      Hu = function () {
	    var e = !1;try {
	      var t = Object.defineProperty({}, "passive", { get: function get() {
	          return e = !0, null;
	        } });window.addEventListener("test", null, t), window.removeEventListener("test", null, t);
	    } catch (e) {}return e;
	  }();function Bu(e, t, n) {
	    var i = this,
	        r = arguments.length > 3 && void 0 !== arguments[3] && arguments[3],
	        a = !(arguments.length > 4 && void 0 !== arguments[4]) || arguments[4],
	        o = arguments.length > 5 && void 0 !== arguments[5] && arguments[5];if (e && "addEventListener" in e && !du(t) && ru(n)) {
	      var s = t.split(" "),
	          l = o;Hu && (l = { passive: a, capture: o }), s.forEach(function (t) {
	        i && i.eventListeners && r && i.eventListeners.push({ element: e, type: t, callback: n, options: l }), e[r ? "addEventListener" : "removeEventListener"](t, n, l);
	      });
	    }
	  }function Vu(e) {
	    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
	        n = arguments.length > 2 ? arguments[2] : void 0,
	        i = !(arguments.length > 3 && void 0 !== arguments[3]) || arguments[3],
	        r = arguments.length > 4 && void 0 !== arguments[4] && arguments[4];Bu.call(this, e, t, n, !0, i, r);
	  }function zu(e) {
	    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
	        n = arguments.length > 2 ? arguments[2] : void 0,
	        i = !(arguments.length > 3 && void 0 !== arguments[3]) || arguments[3],
	        r = arguments.length > 4 && void 0 !== arguments[4] && arguments[4];Bu.call(this, e, t, n, !1, i, r);
	  }function Wu(e) {
	    var t = this,
	        n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
	        i = arguments.length > 2 ? arguments[2] : void 0,
	        r = !(arguments.length > 3 && void 0 !== arguments[3]) || arguments[3],
	        a = arguments.length > 4 && void 0 !== arguments[4] && arguments[4],
	        o = function o() {
	      zu(e, n, o, r, a);for (var s = arguments.length, l = new Array(s), c = 0; c < s; c++) {
	        l[c] = arguments[c];
	      }i.apply(t, l);
	    };Bu.call(this, e, n, o, !0, r, a);
	  }function Ku(e) {
	    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
	        n = arguments.length > 2 && void 0 !== arguments[2] && arguments[2],
	        i = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : {};if (su(e) && !du(t)) {
	      var r = new CustomEvent(t, { bubbles: n, detail: So(So({}, i), {}, { plyr: this }) });e.dispatchEvent(r);
	    }
	  }function $u() {
	    this && this.eventListeners && (this.eventListeners.forEach(function (e) {
	      var t = e.element,
	          n = e.type,
	          i = e.callback,
	          r = e.options;t.removeEventListener(n, i, r);
	    }), this.eventListeners = []);
	  }function Yu() {
	    var e = this;return new Promise(function (t) {
	      return e.ready ? setTimeout(t, 0) : Vu.call(e, e.elements.container, "ready", t);
	    }).then(function () {});
	  }function Gu(e) {
	    hu(e) && e.then(null, function () {});
	  }function Xu(e) {
	    return !!(au(e) || nu(e) && e.includes(":")) && (au(e) ? e : e.split(":")).map(Number).every(tu);
	  }function Qu(e) {
	    if (!au(e) || !e.every(tu)) return null;var t = Ao(e, 2),
	        n = t[0],
	        i = t[1],
	        r = function e(t, n) {
	      return 0 === n ? t : e(n, t % n);
	    }(n, i);return [n / r, i / r];
	  }function Ju(e) {
	    var t = function t(e) {
	      return Xu(e) ? e.split(":").map(Number) : null;
	    },
	        n = t(e);if (null === n && (n = t(this.config.ratio)), null === n && !du(this.embed) && au(this.embed.ratio) && (n = this.embed.ratio), null === n && this.isHTML5) {
	      var i = this.media;n = Qu([i.videoWidth, i.videoHeight]);
	    }return n;
	  }function Zu(e) {
	    if (!this.isVideo) return {};var t = this.elements.wrapper,
	        n = Ju.call(this, e),
	        i = Ao(au(n) ? n : [0, 0], 2),
	        r = 100 / i[0] * i[1];if (t.style.paddingBottom = "".concat(r, "%"), this.isVimeo && !this.config.vimeo.premium && this.supported.ui) {
	      var a = 100 / this.media.offsetWidth * parseInt(window.getComputedStyle(this.media).paddingBottom, 10),
	          o = (a - r) / (a / 50);this.media.style.transform = "translateY(-".concat(o, "%)");
	    } else this.isHTML5 && t.classList.toggle(this.config.classNames.videoFixedRatio, null !== n);return { padding: r, ratio: n };
	  }var eh = { getSources: function getSources() {
	      var e = this;return this.isHTML5 ? Array.from(this.media.querySelectorAll("source")).filter(function (t) {
	        var n = t.getAttribute("type");return !!du(n) || qu.mime.call(e, n);
	      }) : [];
	    }, getQualityOptions: function getQualityOptions() {
	      return this.config.quality.forced ? this.config.quality.options : eh.getSources.call(this).map(function (e) {
	        return Number(e.getAttribute("size"));
	      }).filter(Boolean);
	    }, setup: function setup() {
	      if (this.isHTML5) {
	        var e = this;e.options.speed = e.config.speed.options, du(this.config.ratio) || Zu.call(e), Object.defineProperty(e.media, "quality", { get: function get() {
	            var t = eh.getSources.call(e).find(function (t) {
	              return t.getAttribute("src") === e.source;
	            });return t && Number(t.getAttribute("size"));
	          }, set: function set(t) {
	            if (e.quality !== t) {
	              if (e.config.quality.forced && ru(e.config.quality.onChange)) e.config.quality.onChange(t);else {
	                var n = eh.getSources.call(e).find(function (e) {
	                  return Number(e.getAttribute("size")) === t;
	                });if (!n) return;var i = e.media,
	                    r = i.currentTime,
	                    a = i.paused,
	                    o = i.preload,
	                    s = i.readyState,
	                    l = i.playbackRate;e.media.src = n.getAttribute("src"), ("none" !== o || s) && (e.once("loadedmetadata", function () {
	                  e.speed = l, e.currentTime = r, a || Gu(e.play());
	                }), e.media.load());
	              }Ku.call(e, e.media, "qualitychange", !1, { quality: t });
	            }
	          } });
	      }
	    }, cancelRequests: function cancelRequests() {
	      this.isHTML5 && (xu(eh.getSources.call(this)), this.media.setAttribute("src", this.config.blankVideo), this.media.load(), this.debug.log("Cancelled network requests"));
	    } };function th(e) {
	    return au(e) ? e.filter(function (t, n) {
	      return e.indexOf(t) === n;
	    }) : e;
	  }var nh = C.f,
	      ih = ye.f,
	      rh = Z.set,
	      ah = Xe("match"),
	      oh = i.RegExp,
	      sh = oh.prototype,
	      lh = /a/g,
	      ch = /a/g,
	      uh = new oh(lh) !== lh,
	      hh = oi.UNSUPPORTED_Y;if (a && Ce("RegExp", !uh || hh || r(function () {
	    return ch[ah] = !1, oh(lh) != lh || oh(ch) == ch || "/a/i" != oh(lh, "i");
	  }))) {
	    for (var fh = function fh(e, t) {
	      var n,
	          i = this instanceof fh,
	          r = qi(e),
	          a = void 0 === t;if (!i && r && e.constructor === fh && a) return e;uh ? r && !a && (e = e.source) : e instanceof fh && (a && (t = ri.call(e)), e = e.source), hh && (n = !!t && t.indexOf("y") > -1) && (t = t.replace(/y/g, ""));var o = Xo(uh ? new oh(e, t) : oh(e, t), i ? this : sh, fh);return hh && n && rh(o, { sticky: n }), o;
	    }, dh = function dh(e) {
	      (e in fh) || nh(fh, e, { configurable: !0, get: function get() {
	          return oh[e];
	        }, set: function set(t) {
	          oh[e] = t;
	        } });
	    }, ph = ih(oh), mh = 0; ph.length > mh;) {
	      dh(ph[mh++]);
	    }sh.constructor = fh, fh.prototype = sh, ee(i, "RegExp", fh);
	  }function gh(e) {
	    for (var t = arguments.length, n = new Array(t > 1 ? t - 1 : 0), i = 1; i < t; i++) {
	      n[i - 1] = arguments[i];
	    }return du(e) ? e : e.toString().replace(/{(\d+)}/g, function (e, t) {
	      return n[t].toString();
	    });
	  }Sl("RegExp");var vh = function vh() {
	    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
	        t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
	        n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "";return e.replace(new RegExp(t.toString().replace(/([.*+?^=!:${}()|[\]/\\])/g, "\\$1"), "g"), n.toString());
	  },
	      yh = function yh() {
	    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";return e.toString().replace(/\w\S*/g, function (e) {
	      return e.charAt(0).toUpperCase() + e.substr(1).toLowerCase();
	    });
	  };function bh() {
	    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
	        t = e.toString();return t = vh(t, "-", " "), t = vh(t, "_", " "), t = yh(t), vh(t, " ", "");
	  }function wh(e) {
	    var t = document.createElement("div");return t.appendChild(e), t.innerHTML;
	  }var kh = { pip: "PIP", airplay: "AirPlay", html5: "HTML5", vimeo: "Vimeo", youtube: "YouTube" },
	      Th = function Th() {
	    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
	        t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};if (du(e) || du(t)) return "";var n = ku(t.i18n, e);if (du(n)) return Object.keys(kh).includes(e) ? kh[e] : "";var i = { "{seektime}": t.seekTime, "{title}": t.title };return Object.entries(i).forEach(function (e) {
	      var t = Ao(e, 2),
	          i = t[0],
	          r = t[1];n = vh(n, i, r);
	    }), n;
	  },
	      Sh = function () {
	    function e(t) {
	      yo(this, e), this.enabled = t.config.storage.enabled, this.key = t.config.storage.key;
	    }return wo(e, [{ key: "get", value: function value(t) {
	        if (!e.supported || !this.enabled) return null;var n = window.localStorage.getItem(this.key);if (du(n)) return null;var i = JSON.parse(n);return nu(t) && t.length ? i[t] : i;
	      } }, { key: "set", value: function value(t) {
	        if (e.supported && this.enabled && eu(t)) {
	          var n = this.get();du(n) && (n = {}), Tu(n, t), window.localStorage.setItem(this.key, JSON.stringify(n));
	        }
	      } }], [{ key: "supported", get: function get() {
	        try {
	          if (!("localStorage" in window)) return !1;return window.localStorage.setItem("___test", "___test"), window.localStorage.removeItem("___test"), !0;
	        } catch (e) {
	          return !1;
	        }
	      } }]), e;
	  }();function Eh(e) {
	    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "text";return new Promise(function (n, i) {
	      try {
	        var r = new XMLHttpRequest();if (!("withCredentials" in r)) return;r.addEventListener("load", function () {
	          if ("text" === t) try {
	            n(JSON.parse(r.responseText));
	          } catch (e) {
	            n(r.responseText);
	          } else n(r.response);
	        }), r.addEventListener("error", function () {
	          throw new Error(r.status);
	        }), r.open("GET", e, !0), r.responseType = t, r.send();
	      } catch (e) {
	        i(e);
	      }
	    });
	  }function Ah(e, t) {
	    if (nu(e)) {
	      var n = nu(t),
	          i = function i() {
	        return null !== document.getElementById(t);
	      },
	          r = function r(e, t) {
	        e.innerHTML = t, n && i() || document.body.insertAdjacentElement("afterbegin", e);
	      };if (!n || !i()) {
	        var a = Sh.supported,
	            o = document.createElement("div");if (o.setAttribute("hidden", ""), n && o.setAttribute("id", t), a) {
	          var s = window.localStorage.getItem("".concat("cache", "-").concat(t));if (null !== s) {
	            var l = JSON.parse(s);r(o, l.content);
	          }
	        }Eh(e).then(function (e) {
	          du(e) || (a && window.localStorage.setItem("".concat("cache", "-").concat(t), JSON.stringify({ content: e })), r(o, e));
	        }).catch(function () {});
	      }
	    }
	  }var Ph = Math.ceil,
	      xh = Math.floor;Ie({ target: "Math", stat: !0 }, { trunc: function trunc(e) {
	      return (e > 0 ? xh : Ph)(e);
	    } });var Ch = function Ch(e) {
	    return Math.trunc(e / 60 / 60 % 60, 10);
	  },
	      Oh = function Oh(e) {
	    return Math.trunc(e / 60 % 60, 10);
	  },
	      Ih = function Ih(e) {
	    return Math.trunc(e % 60, 10);
	  };function Lh() {
	    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
	        t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
	        n = arguments.length > 2 && void 0 !== arguments[2] && arguments[2];if (!tu(e)) return Lh(void 0, t, n);var i = function i(e) {
	      return "0".concat(e).slice(-2);
	    },
	        r = Ch(e),
	        a = Oh(e),
	        o = Ih(e);return r = t || r > 0 ? "".concat(r, ":") : "", "".concat(n && e > 0 ? "-" : "").concat(r).concat(i(a), ":").concat(i(o));
	  }var jh = { getIconUrl: function getIconUrl() {
	      var e = new URL(this.config.iconUrl, window.location).host !== window.location.host || gu.isIE && !window.svg4everybody;return { url: this.config.iconUrl, cors: e };
	    }, findElements: function findElements() {
	      try {
	        return this.elements.controls = _u.call(this, this.config.selectors.controls.wrapper), this.elements.buttons = { play: Mu.call(this, this.config.selectors.buttons.play), pause: _u.call(this, this.config.selectors.buttons.pause), restart: _u.call(this, this.config.selectors.buttons.restart), rewind: _u.call(this, this.config.selectors.buttons.rewind), fastForward: _u.call(this, this.config.selectors.buttons.fastForward), mute: _u.call(this, this.config.selectors.buttons.mute), pip: _u.call(this, this.config.selectors.buttons.pip), airplay: _u.call(this, this.config.selectors.buttons.airplay), settings: _u.call(this, this.config.selectors.buttons.settings), captions: _u.call(this, this.config.selectors.buttons.captions), fullscreen: _u.call(this, this.config.selectors.buttons.fullscreen) }, this.elements.progress = _u.call(this, this.config.selectors.progress), this.elements.inputs = { seek: _u.call(this, this.config.selectors.inputs.seek), volume: _u.call(this, this.config.selectors.inputs.volume) }, this.elements.display = { buffer: _u.call(this, this.config.selectors.display.buffer), currentTime: _u.call(this, this.config.selectors.display.currentTime), duration: _u.call(this, this.config.selectors.display.duration) }, su(this.elements.progress) && (this.elements.display.seekTooltip = this.elements.progress.querySelector(".".concat(this.config.classNames.tooltip))), !0;
	      } catch (e) {
	        return this.debug.warn("It looks like there is a problem with your custom controls HTML", e), this.toggleNativeControls(!0), !1;
	      }
	    }, createIcon: function createIcon(e, t) {
	      var n = jh.getIconUrl.call(this),
	          i = "".concat(n.cors ? "" : n.url, "#").concat(this.config.iconPrefix),
	          r = document.createElementNS("http://www.w3.org/2000/svg", "svg");Eu(r, Tu(t, { "aria-hidden": "true", focusable: "false" }));var a = document.createElementNS("http://www.w3.org/2000/svg", "use"),
	          o = "".concat(i, "-").concat(e);return "href" in a && a.setAttributeNS("http://www.w3.org/1999/xlink", "href", o), a.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", o), r.appendChild(a), r;
	    }, createLabel: function createLabel(e) {
	      var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
	          n = Th(e, this.config),
	          i = So(So({}, t), {}, { class: [t.class, this.config.classNames.hidden].filter(Boolean).join(" ") });return Au("span", i, n);
	    }, createBadge: function createBadge(e) {
	      if (du(e)) return null;var t = Au("span", { class: this.config.classNames.menu.value });return t.appendChild(Au("span", { class: this.config.classNames.menu.badge }, e)), t;
	    }, createButton: function createButton(e, t) {
	      var n = this,
	          i = Tu({}, t),
	          r = function () {
	        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
	            t = e.toString();return (t = bh(t)).charAt(0).toLowerCase() + t.slice(1);
	      }(e),
	          a = { element: "button", toggle: !1, label: null, icon: null, labelPressed: null, iconPressed: null };switch (["element", "icon", "label"].forEach(function (e) {
	        Object.keys(i).includes(e) && (a[e] = i[e], delete i[e]);
	      }), "button" !== a.element || Object.keys(i).includes("type") || (i.type = "button"), Object.keys(i).includes("class") ? i.class.split(" ").some(function (e) {
	        return e === n.config.classNames.control;
	      }) || Tu(i, { class: "".concat(i.class, " ").concat(this.config.classNames.control) }) : i.class = this.config.classNames.control, e) {case "play":
	          a.toggle = !0, a.label = "play", a.labelPressed = "pause", a.icon = "play", a.iconPressed = "pause";break;case "mute":
	          a.toggle = !0, a.label = "mute", a.labelPressed = "unmute", a.icon = "volume", a.iconPressed = "muted";break;case "captions":
	          a.toggle = !0, a.label = "enableCaptions", a.labelPressed = "disableCaptions", a.icon = "captions-off", a.iconPressed = "captions-on";break;case "fullscreen":
	          a.toggle = !0, a.label = "enterFullscreen", a.labelPressed = "exitFullscreen", a.icon = "enter-fullscreen", a.iconPressed = "exit-fullscreen";break;case "play-large":
	          i.class += " ".concat(this.config.classNames.control, "--overlaid"), r = "play", a.label = "play", a.icon = "play";break;default:
	          du(a.label) && (a.label = r), du(a.icon) && (a.icon = e);}var o = Au(a.element);return a.toggle ? (o.appendChild(jh.createIcon.call(this, a.iconPressed, { class: "icon--pressed" })), o.appendChild(jh.createIcon.call(this, a.icon, { class: "icon--not-pressed" })), o.appendChild(jh.createLabel.call(this, a.labelPressed, { class: "label--pressed" })), o.appendChild(jh.createLabel.call(this, a.label, { class: "label--not-pressed" }))) : (o.appendChild(jh.createIcon.call(this, a.icon)), o.appendChild(jh.createLabel.call(this, a.label))), Tu(i, Iu(this.config.selectors.buttons[r], i)), Eu(o, i), "play" === r ? (au(this.elements.buttons[r]) || (this.elements.buttons[r] = []), this.elements.buttons[r].push(o)) : this.elements.buttons[r] = o, o;
	    }, createRange: function createRange(e, t) {
	      var n = Au("input", Tu(Iu(this.config.selectors.inputs[e]), { type: "range", min: 0, max: 100, step: .01, value: 0, autocomplete: "off", role: "slider", "aria-label": Th(e, this.config), "aria-valuemin": 0, "aria-valuemax": 100, "aria-valuenow": 0 }, t));return this.elements.inputs[e] = n, jh.updateRangeFill.call(this, n), wl.setup(n), n;
	    }, createProgress: function createProgress(e, t) {
	      var n = Au("progress", Tu(Iu(this.config.selectors.display[e]), { min: 0, max: 100, value: 0, role: "progressbar", "aria-hidden": !0 }, t));if ("volume" !== e) {
	        n.appendChild(Au("span", null, "0"));var i = { played: "played", buffer: "buffered" }[e],
	            r = i ? Th(i, this.config) : "";n.innerText = "% ".concat(r.toLowerCase());
	      }return this.elements.display[e] = n, n;
	    }, createTime: function createTime(e, t) {
	      var n = Iu(this.config.selectors.display[e], t),
	          i = Au("div", Tu(n, { class: "".concat(n.class ? n.class : "", " ").concat(this.config.classNames.display.time, " ").trim(), "aria-label": Th(e, this.config) }), "00:00");return this.elements.display[e] = i, i;
	    }, bindMenuItemShortcuts: function bindMenuItemShortcuts(e, t) {
	      var n = this;Vu.call(this, e, "keydown keyup", function (i) {
	        if ([32, 38, 39, 40].includes(i.which) && (i.preventDefault(), i.stopPropagation(), "keydown" !== i.type)) {
	          var r,
	              a = Ru(e, '[role="menuitemradio"]');if (!a && [32, 39].includes(i.which)) jh.showMenuPanel.call(n, t, !0);else 32 !== i.which && (40 === i.which || a && 39 === i.which ? (r = e.nextElementSibling, su(r) || (r = e.parentNode.firstElementChild)) : (r = e.previousElementSibling, su(r) || (r = e.parentNode.lastElementChild)), Uu.call(n, r, !0));
	        }
	      }, !1), Vu.call(this, e, "keyup", function (e) {
	        13 === e.which && jh.focusFirstMenuItem.call(n, null, !0);
	      });
	    }, createMenuItem: function createMenuItem(e) {
	      var t = this,
	          n = e.value,
	          i = e.list,
	          r = e.type,
	          a = e.title,
	          o = e.badge,
	          s = void 0 === o ? null : o,
	          l = e.checked,
	          c = void 0 !== l && l,
	          u = Iu(this.config.selectors.inputs[r]),
	          h = Au("button", Tu(u, { type: "button", role: "menuitemradio", class: "".concat(this.config.classNames.control, " ").concat(u.class ? u.class : "").trim(), "aria-checked": c, value: n })),
	          f = Au("span");f.innerHTML = a, su(s) && f.appendChild(s), h.appendChild(f), Object.defineProperty(h, "checked", { enumerable: !0, get: function get() {
	          return "true" === h.getAttribute("aria-checked");
	        }, set: function set(e) {
	          e && Array.from(h.parentNode.children).filter(function (e) {
	            return Ru(e, '[role="menuitemradio"]');
	          }).forEach(function (e) {
	            return e.setAttribute("aria-checked", "false");
	          }), h.setAttribute("aria-checked", e ? "true" : "false");
	        } }), this.listeners.bind(h, "click keyup", function (e) {
	        if (!cu(e) || 32 === e.which) {
	          switch (e.preventDefault(), e.stopPropagation(), h.checked = !0, r) {case "language":
	              t.currentTrack = Number(n);break;case "quality":
	              t.quality = n;break;case "speed":
	              t.speed = parseFloat(n);}jh.showMenuPanel.call(t, "home", cu(e));
	        }
	      }, r, !1), jh.bindMenuItemShortcuts.call(this, h, r), i.appendChild(h);
	    }, formatTime: function formatTime() {
	      var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
	          t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];if (!tu(e)) return e;var n = Ch(this.duration) > 0;return Lh(e, n, t);
	    }, updateTimeDisplay: function updateTimeDisplay() {
	      var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
	          t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0,
	          n = arguments.length > 2 && void 0 !== arguments[2] && arguments[2];su(e) && tu(t) && (e.innerText = jh.formatTime(t, n));
	    }, updateVolume: function updateVolume() {
	      this.supported.ui && (su(this.elements.inputs.volume) && jh.setRange.call(this, this.elements.inputs.volume, this.muted ? 0 : this.volume), su(this.elements.buttons.mute) && (this.elements.buttons.mute.pressed = this.muted || 0 === this.volume));
	    }, setRange: function setRange(e) {
	      var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;su(e) && (e.value = t, jh.updateRangeFill.call(this, e));
	    }, updateProgress: function updateProgress(e) {
	      var t = this;if (this.supported.ui && lu(e)) {
	        var n = 0;if (e) switch (e.type) {case "timeupdate":case "seeking":case "seeked":
	            n = function (e, t) {
	              return 0 === e || 0 === t || Number.isNaN(e) || Number.isNaN(t) ? 0 : (e / t * 100).toFixed(2);
	            }(this.currentTime, this.duration), "timeupdate" === e.type && jh.setRange.call(this, this.elements.inputs.seek, n);break;case "playing":case "progress":
	            !function (e, n) {
	              var i = tu(n) ? n : 0,
	                  r = su(e) ? e : t.elements.display.buffer;if (su(r)) {
	                r.value = i;var a = r.getElementsByTagName("span")[0];su(a) && (a.childNodes[0].nodeValue = i);
	              }
	            }(this.elements.display.buffer, 100 * this.buffered);}
	      }
	    }, updateRangeFill: function updateRangeFill(e) {
	      var t = lu(e) ? e.target : e;if (su(t) && "range" === t.getAttribute("type")) {
	        if (Ru(t, this.config.selectors.inputs.seek)) {
	          t.setAttribute("aria-valuenow", this.currentTime);var n = jh.formatTime(this.currentTime),
	              i = jh.formatTime(this.duration),
	              r = Th("seekLabel", this.config);t.setAttribute("aria-valuetext", r.replace("{currentTime}", n).replace("{duration}", i));
	        } else if (Ru(t, this.config.selectors.inputs.volume)) {
	          var a = 100 * t.value;t.setAttribute("aria-valuenow", a), t.setAttribute("aria-valuetext", "".concat(a.toFixed(1), "%"));
	        } else t.setAttribute("aria-valuenow", t.value);gu.isWebkit && t.style.setProperty("--value", "".concat(t.value / t.max * 100, "%"));
	      }
	    }, updateSeekTooltip: function updateSeekTooltip(e) {
	      var t = this;if (this.config.tooltips.seek && su(this.elements.inputs.seek) && su(this.elements.display.seekTooltip) && 0 !== this.duration) {
	        var n = "".concat(this.config.classNames.tooltip, "--visible"),
	            i = function i(e) {
	          return ju(t.elements.display.seekTooltip, n, e);
	        };if (this.touch) i(!1);else {
	          var r = 0,
	              a = this.elements.progress.getBoundingClientRect();if (lu(e)) r = 100 / a.width * (e.pageX - a.left);else {
	            if (!Nu(this.elements.display.seekTooltip, n)) return;r = parseFloat(this.elements.display.seekTooltip.style.left, 10);
	          }r < 0 ? r = 0 : r > 100 && (r = 100), jh.updateTimeDisplay.call(this, this.elements.display.seekTooltip, this.duration / 100 * r), this.elements.display.seekTooltip.style.left = "".concat(r, "%"), lu(e) && ["mouseenter", "mouseleave"].includes(e.type) && i("mouseenter" === e.type);
	        }
	      }
	    }, timeUpdate: function timeUpdate(e) {
	      var t = !su(this.elements.display.duration) && this.config.invertTime;jh.updateTimeDisplay.call(this, this.elements.display.currentTime, t ? this.duration - this.currentTime : this.currentTime, t), e && "timeupdate" === e.type && this.media.seeking || jh.updateProgress.call(this, e);
	    }, durationUpdate: function durationUpdate() {
	      if (this.supported.ui && (this.config.invertTime || !this.currentTime)) {
	        if (this.duration >= Math.pow(2, 32)) return Lu(this.elements.display.currentTime, !0), void Lu(this.elements.progress, !0);su(this.elements.inputs.seek) && this.elements.inputs.seek.setAttribute("aria-valuemax", this.duration);var e = su(this.elements.display.duration);!e && this.config.displayDuration && this.paused && jh.updateTimeDisplay.call(this, this.elements.display.currentTime, this.duration), e && jh.updateTimeDisplay.call(this, this.elements.display.duration, this.duration), jh.updateSeekTooltip.call(this);
	      }
	    }, toggleMenuButton: function toggleMenuButton(e, t) {
	      Lu(this.elements.settings.buttons[e], !t);
	    }, updateSetting: function updateSetting(e, t, n) {
	      var i = this.elements.settings.panels[e],
	          r = null,
	          a = t;if ("captions" === e) r = this.currentTrack;else {
	        if (r = du(n) ? this[e] : n, du(r) && (r = this.config[e].default), !du(this.options[e]) && !this.options[e].includes(r)) return void this.debug.warn("Unsupported value of '".concat(r, "' for ").concat(e));if (!this.config[e].options.includes(r)) return void this.debug.warn("Disabled value of '".concat(r, "' for ").concat(e));
	      }if (su(a) || (a = i && i.querySelector('[role="menu"]')), su(a)) {
	        this.elements.settings.buttons[e].querySelector(".".concat(this.config.classNames.menu.value)).innerHTML = jh.getLabel.call(this, e, r);var o = a && a.querySelector('[value="'.concat(r, '"]'));su(o) && (o.checked = !0);
	      }
	    }, getLabel: function getLabel(e, t) {
	      switch (e) {case "speed":
	          return 1 === t ? Th("normal", this.config) : "".concat(t, "&times;");case "quality":
	          if (tu(t)) {
	            var n = Th("qualityLabel.".concat(t), this.config);return n.length ? n : "".concat(t, "p");
	          }return yh(t);case "captions":
	          return Mh.getLabel.call(this);default:
	          return null;}
	    }, setQualityMenu: function setQualityMenu(e) {
	      var t = this;if (su(this.elements.settings.panels.quality)) {
	        var n = this.elements.settings.panels.quality.querySelector('[role="menu"]');au(e) && (this.options.quality = th(e).filter(function (e) {
	          return t.config.quality.options.includes(e);
	        }));var i = !du(this.options.quality) && this.options.quality.length > 1;if (jh.toggleMenuButton.call(this, "quality", i), Cu(n), jh.checkMenu.call(this), i) {
	          var r = function r(e) {
	            var n = Th("qualityBadge.".concat(e), t.config);return n.length ? jh.createBadge.call(t, n) : null;
	          };this.options.quality.sort(function (e, n) {
	            var i = t.config.quality.options;return i.indexOf(e) > i.indexOf(n) ? 1 : -1;
	          }).forEach(function (e) {
	            jh.createMenuItem.call(t, { value: e, list: n, type: "quality", title: jh.getLabel.call(t, "quality", e), badge: r(e) });
	          }), jh.updateSetting.call(this, "quality", n);
	        }
	      }
	    }, setCaptionsMenu: function setCaptionsMenu() {
	      var e = this;if (su(this.elements.settings.panels.captions)) {
	        var t = this.elements.settings.panels.captions.querySelector('[role="menu"]'),
	            n = Mh.getTracks.call(this),
	            i = Boolean(n.length);if (jh.toggleMenuButton.call(this, "captions", i), Cu(t), jh.checkMenu.call(this), i) {
	          var r = n.map(function (n, i) {
	            return { value: i, checked: e.captions.toggled && e.currentTrack === i, title: Mh.getLabel.call(e, n), badge: n.language && jh.createBadge.call(e, n.language.toUpperCase()), list: t, type: "language" };
	          });r.unshift({ value: -1, checked: !this.captions.toggled, title: Th("disabled", this.config), list: t, type: "language" }), r.forEach(jh.createMenuItem.bind(this)), jh.updateSetting.call(this, "captions", t);
	        }
	      }
	    }, setSpeedMenu: function setSpeedMenu() {
	      var e = this;if (su(this.elements.settings.panels.speed)) {
	        var t = this.elements.settings.panels.speed.querySelector('[role="menu"]');this.options.speed = this.options.speed.filter(function (t) {
	          return t >= e.minimumSpeed && t <= e.maximumSpeed;
	        });var n = !du(this.options.speed) && this.options.speed.length > 1;jh.toggleMenuButton.call(this, "speed", n), Cu(t), jh.checkMenu.call(this), n && (this.options.speed.forEach(function (n) {
	          jh.createMenuItem.call(e, { value: n, list: t, type: "speed", title: jh.getLabel.call(e, "speed", n) });
	        }), jh.updateSetting.call(this, "speed", t));
	      }
	    }, checkMenu: function checkMenu() {
	      var e = this.elements.settings.buttons,
	          t = !du(e) && Object.values(e).some(function (e) {
	        return !e.hidden;
	      });Lu(this.elements.settings.menu, !t);
	    }, focusFirstMenuItem: function focusFirstMenuItem(e) {
	      var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];if (!this.elements.settings.popup.hidden) {
	        var n = e;su(n) || (n = Object.values(this.elements.settings.panels).find(function (e) {
	          return !e.hidden;
	        }));var i = n.querySelector('[role^="menuitem"]');Uu.call(this, i, t);
	      }
	    }, toggleMenu: function toggleMenu(e) {
	      var t = this.elements.settings.popup,
	          n = this.elements.buttons.settings;if (su(t) && su(n)) {
	        var i = t.hidden,
	            r = i;if (iu(e)) r = e;else if (cu(e) && 27 === e.which) r = !1;else if (lu(e)) {
	          var a = ru(e.composedPath) ? e.composedPath()[0] : e.target,
	              o = t.contains(a);if (o || !o && e.target !== n && r) return;
	        }n.setAttribute("aria-expanded", r), Lu(t, !r), ju(this.elements.container, this.config.classNames.menu.open, r), r && cu(e) ? jh.focusFirstMenuItem.call(this, null, !0) : r || i || Uu.call(this, n, cu(e));
	      }
	    }, getMenuSize: function getMenuSize(e) {
	      var t = e.cloneNode(!0);t.style.position = "absolute", t.style.opacity = 0, t.removeAttribute("hidden"), e.parentNode.appendChild(t);var n = t.scrollWidth,
	          i = t.scrollHeight;return xu(t), { width: n, height: i };
	    }, showMenuPanel: function showMenuPanel() {
	      var e = this,
	          t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
	          n = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
	          i = this.elements.container.querySelector("#plyr-settings-".concat(this.id, "-").concat(t));if (su(i)) {
	        var r = i.parentNode,
	            a = Array.from(r.children).find(function (e) {
	          return !e.hidden;
	        });if (qu.transitions && !qu.reducedMotion) {
	          r.style.width = "".concat(a.scrollWidth, "px"), r.style.height = "".concat(a.scrollHeight, "px");var o = jh.getMenuSize.call(this, i),
	              s = function t(n) {
	            n.target === r && ["width", "height"].includes(n.propertyName) && (r.style.width = "", r.style.height = "", zu.call(e, r, pu, t));
	          };Vu.call(this, r, pu, s), r.style.width = "".concat(o.width, "px"), r.style.height = "".concat(o.height, "px");
	        }Lu(a, !0), Lu(i, !1), jh.focusFirstMenuItem.call(this, i, n);
	      }
	    }, setDownloadUrl: function setDownloadUrl() {
	      var e = this.elements.buttons.download;su(e) && e.setAttribute("href", this.download);
	    }, create: function create(e) {
	      var t = this,
	          n = jh.bindMenuItemShortcuts,
	          i = jh.createButton,
	          r = jh.createProgress,
	          a = jh.createRange,
	          o = jh.createTime,
	          s = jh.setQualityMenu,
	          l = jh.setSpeedMenu,
	          c = jh.showMenuPanel;this.elements.controls = null, au(this.config.controls) && this.config.controls.includes("play-large") && this.elements.container.appendChild(i.call(this, "play-large"));var u = Au("div", Iu(this.config.selectors.controls.wrapper));this.elements.controls = u;var h = { class: "plyr__controls__item" };return th(au(this.config.controls) ? this.config.controls : []).forEach(function (s) {
	        if ("restart" === s && u.appendChild(i.call(t, "restart", h)), "rewind" === s && u.appendChild(i.call(t, "rewind", h)), "play" === s && u.appendChild(i.call(t, "play", h)), "fast-forward" === s && u.appendChild(i.call(t, "fast-forward", h)), "progress" === s) {
	          var l = Au("div", { class: "".concat(h.class, " plyr__progress__container") }),
	              f = Au("div", Iu(t.config.selectors.progress));if (f.appendChild(a.call(t, "seek", { id: "plyr-seek-".concat(e.id) })), f.appendChild(r.call(t, "buffer")), t.config.tooltips.seek) {
	            var d = Au("span", { class: t.config.classNames.tooltip }, "00:00");f.appendChild(d), t.elements.display.seekTooltip = d;
	          }t.elements.progress = f, l.appendChild(t.elements.progress), u.appendChild(l);
	        }if ("current-time" === s && u.appendChild(o.call(t, "currentTime", h)), "duration" === s && u.appendChild(o.call(t, "duration", h)), "mute" === s || "volume" === s) {
	          var p = t.elements.volume;if (su(p) && u.contains(p) || (p = Au("div", Tu({}, h, { class: "".concat(h.class, " plyr__volume").trim() })), t.elements.volume = p, u.appendChild(p)), "mute" === s && p.appendChild(i.call(t, "mute")), "volume" === s && !gu.isIos) {
	            var m = { max: 1, step: .05, value: t.config.volume };p.appendChild(a.call(t, "volume", Tu(m, { id: "plyr-volume-".concat(e.id) })));
	          }
	        }if ("captions" === s && u.appendChild(i.call(t, "captions", h)), "settings" === s && !du(t.config.settings)) {
	          var g = Au("div", Tu({}, h, { class: "".concat(h.class, " plyr__menu").trim(), hidden: "" }));g.appendChild(i.call(t, "settings", { "aria-haspopup": !0, "aria-controls": "plyr-settings-".concat(e.id), "aria-expanded": !1 }));var v = Au("div", { class: "plyr__menu__container", id: "plyr-settings-".concat(e.id), hidden: "" }),
	              y = Au("div"),
	              b = Au("div", { id: "plyr-settings-".concat(e.id, "-home") }),
	              w = Au("div", { role: "menu" });b.appendChild(w), y.appendChild(b), t.elements.settings.panels.home = b, t.config.settings.forEach(function (i) {
	            var r = Au("button", Tu(Iu(t.config.selectors.buttons.settings), { type: "button", class: "".concat(t.config.classNames.control, " ").concat(t.config.classNames.control, "--forward"), role: "menuitem", "aria-haspopup": !0, hidden: "" }));n.call(t, r, i), Vu.call(t, r, "click", function () {
	              c.call(t, i, !1);
	            });var a = Au("span", null, Th(i, t.config)),
	                o = Au("span", { class: t.config.classNames.menu.value });o.innerHTML = e[i], a.appendChild(o), r.appendChild(a), w.appendChild(r);var s = Au("div", { id: "plyr-settings-".concat(e.id, "-").concat(i), hidden: "" }),
	                l = Au("button", { type: "button", class: "".concat(t.config.classNames.control, " ").concat(t.config.classNames.control, "--back") });l.appendChild(Au("span", { "aria-hidden": !0 }, Th(i, t.config))), l.appendChild(Au("span", { class: t.config.classNames.hidden }, Th("menuBack", t.config))), Vu.call(t, s, "keydown", function (e) {
	              37 === e.which && (e.preventDefault(), e.stopPropagation(), c.call(t, "home", !0));
	            }, !1), Vu.call(t, l, "click", function () {
	              c.call(t, "home", !1);
	            }), s.appendChild(l), s.appendChild(Au("div", { role: "menu" })), y.appendChild(s), t.elements.settings.buttons[i] = r, t.elements.settings.panels[i] = s;
	          }), v.appendChild(y), g.appendChild(v), u.appendChild(g), t.elements.settings.popup = v, t.elements.settings.menu = g;
	        }if ("pip" === s && qu.pip && u.appendChild(i.call(t, "pip", h)), "airplay" === s && qu.airplay && u.appendChild(i.call(t, "airplay", h)), "download" === s) {
	          var k = Tu({}, h, { element: "a", href: t.download, target: "_blank" });t.isHTML5 && (k.download = "");var T = t.config.urls.download;!fu(T) && t.isEmbed && Tu(k, { icon: "logo-".concat(t.provider), label: t.provider }), u.appendChild(i.call(t, "download", k));
	        }"fullscreen" === s && u.appendChild(i.call(t, "fullscreen", h));
	      }), this.isHTML5 && s.call(this, eh.getQualityOptions.call(this)), l.call(this), u;
	    }, inject: function inject() {
	      var e = this;if (this.config.loadSprite) {
	        var t = jh.getIconUrl.call(this);t.cors && Ah(t.url, "sprite-plyr");
	      }this.id = Math.floor(1e4 * Math.random());var n = null;this.elements.controls = null;var i = { id: this.id, seektime: this.config.seekTime, title: this.config.title },
	          r = !0;ru(this.config.controls) && (this.config.controls = this.config.controls.call(this, i)), this.config.controls || (this.config.controls = []), su(this.config.controls) || nu(this.config.controls) ? n = this.config.controls : (n = jh.create.call(this, { id: this.id, seektime: this.config.seekTime, speed: this.speed, quality: this.quality, captions: Mh.getLabel.call(this) }), r = !1);var a, o;if (r && nu(this.config.controls) && (a = n, Object.entries(i).forEach(function (e) {
	        var t = Ao(e, 2),
	            n = t[0],
	            i = t[1];a = vh(a, "{".concat(n, "}"), i);
	      }), n = a), nu(this.config.selectors.controls.container) && (o = document.querySelector(this.config.selectors.controls.container)), su(o) || (o = this.elements.container), o[su(n) ? "insertAdjacentElement" : "insertAdjacentHTML"]("afterbegin", n), su(this.elements.controls) || jh.findElements.call(this), !du(this.elements.buttons)) {
	        var s = function s(t) {
	          var n = e.config.classNames.controlPressed;Object.defineProperty(t, "pressed", { enumerable: !0, get: function get() {
	              return Nu(t, n);
	            }, set: function set() {
	              var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];ju(t, n, e);
	            } });
	        };Object.values(this.elements.buttons).filter(Boolean).forEach(function (e) {
	          au(e) || ou(e) ? Array.from(e).filter(Boolean).forEach(s) : s(e);
	        });
	      }if (gu.isEdge && mu(o), this.config.tooltips.controls) {
	        var l = this.config,
	            c = l.classNames,
	            u = l.selectors,
	            h = "".concat(u.controls.wrapper, " ").concat(u.labels, " .").concat(c.hidden),
	            f = Mu.call(this, h);Array.from(f).forEach(function (t) {
	          ju(t, e.config.classNames.hidden, !1), ju(t, e.config.classNames.tooltip, !0);
	        });
	      }
	    } };function Nh(e) {
	    var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
	        n = e;if (t) {
	      var i = document.createElement("a");i.href = n, n = i.href;
	    }try {
	      return new URL(n);
	    } catch (e) {
	      return null;
	    }
	  }function Rh(e) {
	    var t = new URLSearchParams();return eu(e) && Object.entries(e).forEach(function (e) {
	      var n = Ao(e, 2),
	          i = n[0],
	          r = n[1];t.set(i, r);
	    }), t;
	  }var Mh = { setup: function setup() {
	      if (this.supported.ui) if (!this.isVideo || this.isYouTube || this.isHTML5 && !qu.textTracks) au(this.config.controls) && this.config.controls.includes("settings") && this.config.settings.includes("captions") && jh.setCaptionsMenu.call(this);else {
	        if (su(this.elements.captions) || (this.elements.captions = Au("div", Iu(this.config.selectors.captions)), function (e, t) {
	          su(e) && su(t) && t.parentNode.insertBefore(e, t.nextSibling);
	        }(this.elements.captions, this.elements.wrapper)), gu.isIE && window.URL) {
	          var e = this.media.querySelectorAll("track");Array.from(e).forEach(function (e) {
	            var t = e.getAttribute("src"),
	                n = Nh(t);null !== n && n.hostname !== window.location.href.hostname && ["http:", "https:"].includes(n.protocol) && Eh(t, "blob").then(function (t) {
	              e.setAttribute("src", window.URL.createObjectURL(t));
	            }).catch(function () {
	              xu(e);
	            });
	          });
	        }var t = th((navigator.languages || [navigator.language || navigator.userLanguage || "en"]).map(function (e) {
	          return e.split("-")[0];
	        })),
	            n = (this.storage.get("language") || this.config.captions.language || "auto").toLowerCase();if ("auto" === n) n = Ao(t, 1)[0];var i = this.storage.get("captions");if (iu(i) || (i = this.config.captions.active), Object.assign(this.captions, { toggled: !1, active: i, language: n, languages: t }), this.isHTML5) {
	          var r = this.config.captions.update ? "addtrack removetrack" : "removetrack";Vu.call(this, this.media.textTracks, r, Mh.update.bind(this));
	        }setTimeout(Mh.update.bind(this), 0);
	      }
	    }, update: function update() {
	      var e = this,
	          t = Mh.getTracks.call(this, !0),
	          n = this.captions,
	          i = n.active,
	          r = n.language,
	          a = n.meta,
	          o = n.currentTrackNode,
	          s = Boolean(t.find(function (e) {
	        return e.language === r;
	      }));this.isHTML5 && this.isVideo && t.filter(function (e) {
	        return !a.get(e);
	      }).forEach(function (t) {
	        e.debug.log("Track added", t), a.set(t, { default: "showing" === t.mode }), "showing" === t.mode && (t.mode = "hidden"), Vu.call(e, t, "cuechange", function () {
	          return Mh.updateCues.call(e);
	        });
	      }), (s && this.language !== r || !t.includes(o)) && (Mh.setLanguage.call(this, r), Mh.toggle.call(this, i && s)), ju(this.elements.container, this.config.classNames.captions.enabled, !du(t)), au(this.config.controls) && this.config.controls.includes("settings") && this.config.settings.includes("captions") && jh.setCaptionsMenu.call(this);
	    }, toggle: function toggle(e) {
	      var t = this,
	          n = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];if (this.supported.ui) {
	        var i = this.captions.toggled,
	            r = this.config.classNames.captions.active,
	            a = Zc(e) ? !i : e;if (a !== i) {
	          if (n || (this.captions.active = a, this.storage.set({ captions: a })), !this.language && a && !n) {
	            var o = Mh.getTracks.call(this),
	                s = Mh.findTrack.call(this, [this.captions.language].concat(Po(this.captions.languages)), !0);return this.captions.language = s.language, void Mh.set.call(this, o.indexOf(s));
	          }this.elements.buttons.captions && (this.elements.buttons.captions.pressed = a), ju(this.elements.container, r, a), this.captions.toggled = a, jh.updateSetting.call(this, "captions"), Ku.call(this, this.media, a ? "captionsenabled" : "captionsdisabled");
	        }setTimeout(function () {
	          a && t.captions.toggled && (t.captions.currentTrackNode.mode = "hidden");
	        });
	      }
	    }, set: function set(e) {
	      var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
	          n = Mh.getTracks.call(this);if (-1 !== e) {
	        if (tu(e)) {
	          if (e in n) {
	            if (this.captions.currentTrack !== e) {
	              this.captions.currentTrack = e;var i = n[e],
	                  r = i || {},
	                  a = r.language;this.captions.currentTrackNode = i, jh.updateSetting.call(this, "captions"), t || (this.captions.language = a, this.storage.set({ language: a })), this.isVimeo && this.embed.enableTextTrack(a), Ku.call(this, this.media, "languagechange");
	            }Mh.toggle.call(this, !0, t), this.isHTML5 && this.isVideo && Mh.updateCues.call(this);
	          } else this.debug.warn("Track not found", e);
	        } else this.debug.warn("Invalid caption argument", e);
	      } else Mh.toggle.call(this, !1, t);
	    }, setLanguage: function setLanguage(e) {
	      var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];if (nu(e)) {
	        var n = e.toLowerCase();this.captions.language = n;var i = Mh.getTracks.call(this),
	            r = Mh.findTrack.call(this, [n]);Mh.set.call(this, i.indexOf(r), t);
	      } else this.debug.warn("Invalid language argument", e);
	    }, getTracks: function getTracks() {
	      var e = this,
	          t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
	          n = Array.from((this.media || {}).textTracks || []);return n.filter(function (n) {
	        return !e.isHTML5 || t || e.captions.meta.has(n);
	      }).filter(function (e) {
	        return ["captions", "subtitles"].includes(e.kind);
	      });
	    }, findTrack: function findTrack(e) {
	      var t,
	          n = this,
	          i = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
	          r = Mh.getTracks.call(this),
	          a = function a(e) {
	        return Number((n.captions.meta.get(e) || {}).default);
	      },
	          o = Array.from(r).sort(function (e, t) {
	        return a(t) - a(e);
	      });return e.every(function (e) {
	        return !(t = o.find(function (t) {
	          return t.language === e;
	        }));
	      }), t || (i ? o[0] : void 0);
	    }, getCurrentTrack: function getCurrentTrack() {
	      return Mh.getTracks.call(this)[this.currentTrack];
	    }, getLabel: function getLabel(e) {
	      var t = e;return !uu(t) && qu.textTracks && this.captions.toggled && (t = Mh.getCurrentTrack.call(this)), uu(t) ? du(t.label) ? du(t.language) ? Th("enabled", this.config) : e.language.toUpperCase() : t.label : Th("disabled", this.config);
	    }, updateCues: function updateCues(e) {
	      if (this.supported.ui) if (su(this.elements.captions)) {
	        if (Zc(e) || Array.isArray(e)) {
	          var t = e;if (!t) {
	            var n = Mh.getCurrentTrack.call(this);t = Array.from((n || {}).activeCues || []).map(function (e) {
	              return e.getCueAsHTML();
	            }).map(wh);
	          }var i = t.map(function (e) {
	            return e.trim();
	          }).join("\n");if (i !== this.elements.captions.innerHTML) {
	            Cu(this.elements.captions);var r = Au("span", Iu(this.config.selectors.caption));r.innerHTML = i, this.elements.captions.appendChild(r), Ku.call(this, this.media, "cuechange");
	          }
	        } else this.debug.warn("updateCues: Invalid input", e);
	      } else this.debug.warn("No captions element to render to");
	    } },
	      _h = { enabled: !0, title: "", debug: !1, autoplay: !1, autopause: !0, playsinline: !0, seekTime: 10, volume: 1, muted: !1, duration: null, displayDuration: !0, invertTime: !0, toggleInvert: !0, ratio: null, clickToPlay: !0, hideControls: !0, resetOnEnd: !1, disableContextMenu: !0, loadSprite: !0, iconPrefix: "plyr", iconUrl: "https://cdn.plyr.io/3.6.2/plyr.svg", blankVideo: "https://cdn.plyr.io/static/blank.mp4", quality: { default: 576, options: [4320, 2880, 2160, 1440, 1080, 720, 576, 480, 360, 240], forced: !1, onChange: null }, loop: { active: !1 }, speed: { selected: 1, options: [.5, .75, 1, 1.25, 1.5, 1.75, 2, 4] }, keyboard: { focused: !0, global: !1 }, tooltips: { controls: !1, seek: !0 }, captions: { active: !1, language: "auto", update: !1 }, fullscreen: { enabled: !0, fallback: !0, iosNative: !1 }, storage: { enabled: !0, key: "plyr" }, controls: ["play-large", "play", "progress", "current-time", "mute", "volume", "captions", "settings", "pip", "airplay", "fullscreen"], settings: ["captions", "quality", "speed"], i18n: { restart: "Restart", rewind: "Rewind {seektime}s", play: "Play", pause: "Pause", fastForward: "Forward {seektime}s", seek: "Seek", seekLabel: "{currentTime} of {duration}", played: "Played", buffered: "Buffered", currentTime: "Current time", duration: "Duration", volume: "Volume", mute: "Mute", unmute: "Unmute", enableCaptions: "Enable captions", disableCaptions: "Disable captions", download: "Download", enterFullscreen: "Enter fullscreen", exitFullscreen: "Exit fullscreen", frameTitle: "Player for {title}", captions: "Captions", settings: "Settings", pip: "PIP", menuBack: "Go back to previous menu", speed: "Speed", normal: "Normal", quality: "Quality", loop: "Loop", start: "Start", end: "End", all: "All", reset: "Reset", disabled: "Disabled", enabled: "Enabled", advertisement: "Ad", qualityBadge: { 2160: "4K", 1440: "HD", 1080: "HD", 720: "HD", 576: "SD", 480: "SD" } }, urls: { download: null, vimeo: { sdk: "https://player.vimeo.com/api/player.js", iframe: "https://player.vimeo.com/video/{0}?{1}", api: "https://vimeo.com/api/v2/video/{0}.json" }, youtube: { sdk: "https://www.youtube.com/iframe_api", api: "https://noembed.com/embed?url=https://www.youtube.com/watch?v={0}" }, googleIMA: { sdk: "https://imasdk.googleapis.com/js/sdkloader/ima3.js" } }, listeners: { seek: null, play: null, pause: null, restart: null, rewind: null, fastForward: null, mute: null, volume: null, captions: null, download: null, fullscreen: null, pip: null, airplay: null, speed: null, quality: null, loop: null, language: null }, events: ["ended", "progress", "stalled", "playing", "waiting", "canplay", "canplaythrough", "loadstart", "loadeddata", "loadedmetadata", "timeupdate", "volumechange", "play", "pause", "error", "seeking", "seeked", "emptied", "ratechange", "cuechange", "download", "enterfullscreen", "exitfullscreen", "captionsenabled", "captionsdisabled", "languagechange", "controlshidden", "controlsshown", "ready", "statechange", "qualitychange", "adsloaded", "adscontentpause", "adscontentresume", "adstarted", "adsmidpoint", "adscomplete", "adsallcomplete", "adsimpression", "adsclick"], selectors: { editable: "input, textarea, select, [contenteditable]", container: ".plyr", controls: { container: null, wrapper: ".plyr__controls" }, labels: "[data-plyr]", buttons: { play: '[data-plyr="play"]', pause: '[data-plyr="pause"]', restart: '[data-plyr="restart"]', rewind: '[data-plyr="rewind"]', fastForward: '[data-plyr="fast-forward"]', mute: '[data-plyr="mute"]', captions: '[data-plyr="captions"]', download: '[data-plyr="download"]', fullscreen: '[data-plyr="fullscreen"]', pip: '[data-plyr="pip"]', airplay: '[data-plyr="airplay"]', settings: '[data-plyr="settings"]', loop: '[data-plyr="loop"]' }, inputs: { seek: '[data-plyr="seek"]', volume: '[data-plyr="volume"]', speed: '[data-plyr="speed"]', language: '[data-plyr="language"]', quality: '[data-plyr="quality"]' }, display: { currentTime: ".plyr__time--current", duration: ".plyr__time--duration", buffer: ".plyr__progress__buffer", loop: ".plyr__progress__loop", volume: ".plyr__volume--display" }, progress: ".plyr__progress", captions: ".plyr__captions", caption: ".plyr__caption" }, classNames: { type: "plyr--{0}", provider: "plyr--{0}", video: "plyr__video-wrapper", embed: "plyr__video-embed", videoFixedRatio: "plyr__video-wrapper--fixed-ratio", embedContainer: "plyr__video-embed__container", poster: "plyr__poster", posterEnabled: "plyr__poster-enabled", ads: "plyr__ads", control: "plyr__control", controlPressed: "plyr__control--pressed", playing: "plyr--playing", paused: "plyr--paused", stopped: "plyr--stopped", loading: "plyr--loading", hover: "plyr--hover", tooltip: "plyr__tooltip", cues: "plyr__cues", hidden: "plyr__sr-only", hideControls: "plyr--hide-controls", isIos: "plyr--is-ios", isTouch: "plyr--is-touch", uiSupported: "plyr--full-ui", noTransition: "plyr--no-transition", display: { time: "plyr__time" }, menu: { value: "plyr__menu__value", badge: "plyr__badge", open: "plyr--menu-open" }, captions: { enabled: "plyr--captions-enabled", active: "plyr--captions-active" }, fullscreen: { enabled: "plyr--fullscreen-enabled", fallback: "plyr--fullscreen-fallback" }, pip: { supported: "plyr--pip-supported", active: "plyr--pip-active" }, airplay: { supported: "plyr--airplay-supported", active: "plyr--airplay-active" }, tabFocus: "plyr__tab-focus", previewThumbnails: { thumbContainer: "plyr__preview-thumb", thumbContainerShown: "plyr__preview-thumb--is-shown", imageContainer: "plyr__preview-thumb__image-container", timeContainer: "plyr__preview-thumb__time-container", scrubbingContainer: "plyr__preview-scrubbing", scrubbingContainerShown: "plyr__preview-scrubbing--is-shown" } }, attributes: { embed: { provider: "data-plyr-provider", id: "data-plyr-embed-id" } }, ads: { enabled: !1, publisherId: "", tagUrl: "" }, previewThumbnails: { enabled: !1, src: "" }, vimeo: { byline: !1, portrait: !1, title: !1, speed: !0, transparent: !1, premium: !1, referrerPolicy: null }, youtube: { noCookie: !0, rel: 0, showinfo: 0, iv_load_policy: 3, modestbranding: 1 } },
	      Uh = "picture-in-picture",
	      Dh = "inline",
	      Fh = { html5: "html5", youtube: "youtube", vimeo: "vimeo" },
	      qh = "audio",
	      Hh = "video";var Bh = function Bh() {},
	      Vh = function () {
	    function e() {
	      var t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];yo(this, e), this.enabled = window.console && t, this.enabled && this.log("Debugging enabled");
	    }return wo(e, [{ key: "log", get: function get() {
	        return this.enabled ? Function.prototype.bind.call(console.log, console) : Bh;
	      } }, { key: "warn", get: function get() {
	        return this.enabled ? Function.prototype.bind.call(console.warn, console) : Bh;
	      } }, { key: "error", get: function get() {
	        return this.enabled ? Function.prototype.bind.call(console.error, console) : Bh;
	      } }]), e;
	  }(),
	      zh = function () {
	    function e(t) {
	      var n = this;yo(this, e), this.player = t, this.prefix = e.prefix, this.property = e.property, this.scrollPosition = { x: 0, y: 0 }, this.forceFallback = "force" === t.config.fullscreen.fallback, this.player.elements.fullscreen = t.config.fullscreen.container && function (e, t) {
	        return (Element.prototype.closest || function () {
	          var e = this;do {
	            if (Ru.matches(e, t)) return e;e = e.parentElement || e.parentNode;
	          } while (null !== e && 1 === e.nodeType);return null;
	        }).call(e, t);
	      }(this.player.elements.container, t.config.fullscreen.container), Vu.call(this.player, document, "ms" === this.prefix ? "MSFullscreenChange" : "".concat(this.prefix, "fullscreenchange"), function () {
	        n.onChange();
	      }), Vu.call(this.player, this.player.elements.container, "dblclick", function (e) {
	        su(n.player.elements.controls) && n.player.elements.controls.contains(e.target) || n.toggle();
	      }), Vu.call(this, this.player.elements.container, "keydown", function (e) {
	        return n.trapFocus(e);
	      }), this.update();
	    }return wo(e, [{ key: "onChange", value: function value() {
	        if (this.enabled) {
	          var e = this.player.elements.buttons.fullscreen;su(e) && (e.pressed = this.active), Ku.call(this.player, this.target, this.active ? "enterfullscreen" : "exitfullscreen", !0);
	        }
	      } }, { key: "toggleFallback", value: function value() {
	        var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];if (e ? this.scrollPosition = { x: window.scrollX || 0, y: window.scrollY || 0 } : window.scrollTo(this.scrollPosition.x, this.scrollPosition.y), document.body.style.overflow = e ? "hidden" : "", ju(this.target, this.player.config.classNames.fullscreen.fallback, e), gu.isIos) {
	          var t = document.head.querySelector('meta[name="viewport"]'),
	              n = "viewport-fit=cover";t || (t = document.createElement("meta")).setAttribute("name", "viewport");var i = nu(t.content) && t.content.includes(n);e ? (this.cleanupViewport = !i, i || (t.content += ",".concat(n))) : this.cleanupViewport && (t.content = t.content.split(",").filter(function (e) {
	            return e.trim() !== n;
	          }).join(","));
	        }this.onChange();
	      } }, { key: "trapFocus", value: function value(e) {
	        if (!gu.isIos && this.active && "Tab" === e.key && 9 === e.keyCode) {
	          var t = document.activeElement,
	              n = Mu.call(this.player, "a[href], button:not(:disabled), input:not(:disabled), [tabindex]"),
	              i = Ao(n, 1)[0],
	              r = n[n.length - 1];t !== r || e.shiftKey ? t === i && e.shiftKey && (r.focus(), e.preventDefault()) : (i.focus(), e.preventDefault());
	        }
	      } }, { key: "update", value: function value() {
	        var t;this.enabled ? (t = this.forceFallback ? "Fallback (forced)" : e.native ? "Native" : "Fallback", this.player.debug.log("".concat(t, " fullscreen enabled"))) : this.player.debug.log("Fullscreen not supported and fallback disabled");ju(this.player.elements.container, this.player.config.classNames.fullscreen.enabled, this.enabled);
	      } }, { key: "enter", value: function value() {
	        this.enabled && (gu.isIos && this.player.config.fullscreen.iosNative ? this.target.webkitEnterFullscreen() : !e.native || this.forceFallback ? this.toggleFallback(!0) : this.prefix ? du(this.prefix) || this.target["".concat(this.prefix, "Request").concat(this.property)]() : this.target.requestFullscreen({ navigationUI: "hide" }));
	      } }, { key: "exit", value: function value() {
	        if (this.enabled) if (gu.isIos && this.player.config.fullscreen.iosNative) this.target.webkitExitFullscreen(), Gu(this.player.play());else if (!e.native || this.forceFallback) this.toggleFallback(!1);else if (this.prefix) {
	          if (!du(this.prefix)) {
	            var t = "moz" === this.prefix ? "Cancel" : "Exit";document["".concat(this.prefix).concat(t).concat(this.property)]();
	          }
	        } else (document.cancelFullScreen || document.exitFullscreen).call(document);
	      } }, { key: "toggle", value: function value() {
	        this.active ? this.exit() : this.enter();
	      } }, { key: "usingNative", get: function get() {
	        return e.native && !this.forceFallback;
	      } }, { key: "enabled", get: function get() {
	        return (e.native || this.player.config.fullscreen.fallback) && this.player.config.fullscreen.enabled && this.player.supported.ui && this.player.isVideo;
	      } }, { key: "active", get: function get() {
	        if (!this.enabled) return !1;if (!e.native || this.forceFallback) return Nu(this.target, this.player.config.classNames.fullscreen.fallback);var t = this.prefix ? document["".concat(this.prefix).concat(this.property, "Element")] : document.fullscreenElement;return t && t.shadowRoot ? t === this.target.getRootNode().host : t === this.target;
	      } }, { key: "target", get: function get() {
	        return gu.isIos && this.player.config.fullscreen.iosNative ? this.player.media : this.player.elements.fullscreen || this.player.elements.container;
	      } }], [{ key: "native", get: function get() {
	        return !!(document.fullscreenEnabled || document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled);
	      } }, { key: "prefix", get: function get() {
	        if (ru(document.exitFullscreen)) return "";var e = "";return ["webkit", "moz", "ms"].some(function (t) {
	          return !(!ru(document["".concat(t, "ExitFullscreen")]) && !ru(document["".concat(t, "CancelFullScreen")])) && (e = t, !0);
	        }), e;
	      } }, { key: "property", get: function get() {
	        return "moz" === this.prefix ? "FullScreen" : "Fullscreen";
	      } }]), e;
	  }(),
	      Wh = Math.sign || function (e) {
	    return 0 == (e = +e) || e != e ? e : e < 0 ? -1 : 1;
	  };function Kh(e) {
	    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 1;return new Promise(function (n, i) {
	      var r = new Image(),
	          a = function a() {
	        delete r.onload, delete r.onerror, (r.naturalWidth >= t ? n : i)(r);
	      };Object.assign(r, { onload: a, onerror: a, src: e });
	    });
	  }Ie({ target: "Math", stat: !0 }, { sign: Wh });var $h = { addStyleHook: function addStyleHook() {
	      ju(this.elements.container, this.config.selectors.container.replace(".", ""), !0), ju(this.elements.container, this.config.classNames.uiSupported, this.supported.ui);
	    }, toggleNativeControls: function toggleNativeControls() {
	      var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];e && this.isHTML5 ? this.media.setAttribute("controls", "") : this.media.removeAttribute("controls");
	    }, build: function build() {
	      var e = this;if (this.listeners.media(), !this.supported.ui) return this.debug.warn("Basic support only for ".concat(this.provider, " ").concat(this.type)), void $h.toggleNativeControls.call(this, !0);su(this.elements.controls) || (jh.inject.call(this), this.listeners.controls()), $h.toggleNativeControls.call(this), this.isHTML5 && Mh.setup.call(this), this.volume = null, this.muted = null, this.loop = null, this.quality = null, this.speed = null, jh.updateVolume.call(this), jh.timeUpdate.call(this), $h.checkPlaying.call(this), ju(this.elements.container, this.config.classNames.pip.supported, qu.pip && this.isHTML5 && this.isVideo), ju(this.elements.container, this.config.classNames.airplay.supported, qu.airplay && this.isHTML5), ju(this.elements.container, this.config.classNames.isIos, gu.isIos), ju(this.elements.container, this.config.classNames.isTouch, this.touch), this.ready = !0, setTimeout(function () {
	        Ku.call(e, e.media, "ready");
	      }, 0), $h.setTitle.call(this), this.poster && $h.setPoster.call(this, this.poster, !1).catch(function () {}), this.config.duration && jh.durationUpdate.call(this);
	    }, setTitle: function setTitle() {
	      var e = Th("play", this.config);if (nu(this.config.title) && !du(this.config.title) && (e += ", ".concat(this.config.title)), Array.from(this.elements.buttons.play || []).forEach(function (t) {
	        t.setAttribute("aria-label", e);
	      }), this.isEmbed) {
	        var t = _u.call(this, "iframe");if (!su(t)) return;var n = du(this.config.title) ? "video" : this.config.title,
	            i = Th("frameTitle", this.config);t.setAttribute("title", i.replace("{title}", n));
	      }
	    }, togglePoster: function togglePoster(e) {
	      ju(this.elements.container, this.config.classNames.posterEnabled, e);
	    }, setPoster: function setPoster(e) {
	      var t = this,
	          n = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];return n && this.poster ? Promise.reject(new Error("Poster already set")) : (this.media.setAttribute("data-poster", e), Yu.call(this).then(function () {
	        return Kh(e);
	      }).catch(function (n) {
	        throw e === t.poster && $h.togglePoster.call(t, !1), n;
	      }).then(function () {
	        if (e !== t.poster) throw new Error("setPoster cancelled by later call to setPoster");
	      }).then(function () {
	        return Object.assign(t.elements.poster.style, { backgroundImage: "url('".concat(e, "')"), backgroundSize: "" }), $h.togglePoster.call(t, !0), e;
	      }));
	    }, checkPlaying: function checkPlaying(e) {
	      var t = this;ju(this.elements.container, this.config.classNames.playing, this.playing), ju(this.elements.container, this.config.classNames.paused, this.paused), ju(this.elements.container, this.config.classNames.stopped, this.stopped), Array.from(this.elements.buttons.play || []).forEach(function (e) {
	        Object.assign(e, { pressed: t.playing }), e.setAttribute("aria-label", Th(t.playing ? "pause" : "play", t.config));
	      }), lu(e) && "timeupdate" === e.type || $h.toggleControls.call(this);
	    }, checkLoading: function checkLoading(e) {
	      var t = this;this.loading = ["stalled", "waiting"].includes(e.type), clearTimeout(this.timers.loading), this.timers.loading = setTimeout(function () {
	        ju(t.elements.container, t.config.classNames.loading, t.loading), $h.toggleControls.call(t);
	      }, this.loading ? 250 : 0);
	    }, toggleControls: function toggleControls(e) {
	      var t = this.elements.controls;if (t && this.config.hideControls) {
	        var n = this.touch && this.lastSeekTime + 2e3 > Date.now();this.toggleControls(Boolean(e || this.loading || this.paused || t.pressed || t.hover || n));
	      }
	    }, migrateStyles: function migrateStyles() {
	      var e = this;Object.values(So({}, this.media.style)).filter(function (e) {
	        return !du(e) && e.startsWith("--plyr");
	      }).forEach(function (t) {
	        e.elements.container.style.setProperty(t, e.media.style.getPropertyValue(t)), e.media.style.removeProperty(t);
	      }), du(this.media.style) && this.media.removeAttribute("style");
	    } },
	      Yh = function () {
	    function e(t) {
	      yo(this, e), this.player = t, this.lastKey = null, this.focusTimer = null, this.lastKeyDown = null, this.handleKey = this.handleKey.bind(this), this.toggleMenu = this.toggleMenu.bind(this), this.setTabFocus = this.setTabFocus.bind(this), this.firstTouch = this.firstTouch.bind(this);
	    }return wo(e, [{ key: "handleKey", value: function value(e) {
	        var t = this.player,
	            n = t.elements,
	            i = e.keyCode ? e.keyCode : e.which,
	            r = "keydown" === e.type,
	            a = r && i === this.lastKey;if (!(e.altKey || e.ctrlKey || e.metaKey || e.shiftKey) && tu(i)) {
	          if (r) {
	            var o = document.activeElement;if (su(o)) {
	              var s = t.config.selectors.editable;if (o !== n.inputs.seek && Ru(o, s)) return;if (32 === e.which && Ru(o, 'button, [role^="menuitem"]')) return;
	            }switch ([32, 37, 38, 39, 40, 48, 49, 50, 51, 52, 53, 54, 56, 57, 67, 70, 73, 75, 76, 77, 79].includes(i) && (e.preventDefault(), e.stopPropagation()), i) {case 48:case 49:case 50:case 51:case 52:case 53:case 54:case 55:case 56:case 57:
	                a || (t.currentTime = t.duration / 10 * (i - 48));break;case 32:case 75:
	                a || Gu(t.togglePlay());break;case 38:
	                t.increaseVolume(.1);break;case 40:
	                t.decreaseVolume(.1);break;case 77:
	                a || (t.muted = !t.muted);break;case 39:
	                t.forward();break;case 37:
	                t.rewind();break;case 70:
	                t.fullscreen.toggle();break;case 67:
	                a || t.toggleCaptions();break;case 76:
	                t.loop = !t.loop;}27 === i && !t.fullscreen.usingNative && t.fullscreen.active && t.fullscreen.toggle(), this.lastKey = i;
	          } else this.lastKey = null;
	        }
	      } }, { key: "toggleMenu", value: function value(e) {
	        jh.toggleMenu.call(this.player, e);
	      } }, { key: "firstTouch", value: function value() {
	        var e = this.player,
	            t = e.elements;e.touch = !0, ju(t.container, e.config.classNames.isTouch, !0);
	      } }, { key: "setTabFocus", value: function value(e) {
	        var t = this.player,
	            n = t.elements;if (clearTimeout(this.focusTimer), "keydown" !== e.type || 9 === e.which) {
	          "keydown" === e.type && (this.lastKeyDown = e.timeStamp);var i,
	              r = e.timeStamp - this.lastKeyDown <= 20;if ("focus" !== e.type || r) i = t.config.classNames.tabFocus, ju(Mu.call(t, ".".concat(i)), i, !1), "focusout" !== e.type && (this.focusTimer = setTimeout(function () {
	            var e = document.activeElement;n.container.contains(e) && ju(document.activeElement, t.config.classNames.tabFocus, !0);
	          }, 10));
	        }
	      } }, { key: "global", value: function value() {
	        var e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0],
	            t = this.player;t.config.keyboard.global && Bu.call(t, window, "keydown keyup", this.handleKey, e, !1), Bu.call(t, document.body, "click", this.toggleMenu, e), Wu.call(t, document.body, "touchstart", this.firstTouch), Bu.call(t, document.body, "keydown focus blur focusout", this.setTabFocus, e, !1, !0);
	      } }, { key: "container", value: function value() {
	        var e = this.player,
	            t = e.config,
	            n = e.elements,
	            i = e.timers;!t.keyboard.global && t.keyboard.focused && Vu.call(e, n.container, "keydown keyup", this.handleKey, !1), Vu.call(e, n.container, "mousemove mouseleave touchstart touchmove enterfullscreen exitfullscreen", function (t) {
	          var r = n.controls;r && "enterfullscreen" === t.type && (r.pressed = !1, r.hover = !1);var a = 0;["touchstart", "touchmove", "mousemove"].includes(t.type) && ($h.toggleControls.call(e, !0), a = e.touch ? 3e3 : 2e3), clearTimeout(i.controls), i.controls = setTimeout(function () {
	            return $h.toggleControls.call(e, !1);
	          }, a);
	        });var r = function r(t) {
	          if (!t) return Zu.call(e);var i = n.container.getBoundingClientRect(),
	              r = i.width,
	              a = i.height;return Zu.call(e, "".concat(r, ":").concat(a));
	        },
	            a = function a() {
	          clearTimeout(i.resized), i.resized = setTimeout(r, 50);
	        };Vu.call(e, n.container, "enterfullscreen exitfullscreen", function (t) {
	          var i = e.fullscreen,
	              o = i.target,
	              s = i.usingNative;if (o === n.container && (e.isEmbed || !du(e.config.ratio))) {
	            var l = "enterfullscreen" === t.type,
	                c = r(l);c.padding;!function (t, n, i) {
	              if (e.isVimeo && !e.config.vimeo.premium) {
	                var r = e.elements.wrapper.firstChild,
	                    a = Ao(t, 2)[1],
	                    o = Ao(Ju.call(e), 2),
	                    s = o[0],
	                    l = o[1];r.style.maxWidth = i ? "".concat(a / l * s, "px") : null, r.style.margin = i ? "0 auto" : null;
	              }
	            }(c.ratio, 0, l), s || (l ? Vu.call(e, window, "resize", a) : zu.call(e, window, "resize", a));
	          }
	        });
	      } }, { key: "media", value: function value() {
	        var e = this,
	            t = this.player,
	            n = t.elements;if (Vu.call(t, t.media, "timeupdate seeking seeked", function (e) {
	          return jh.timeUpdate.call(t, e);
	        }), Vu.call(t, t.media, "durationchange loadeddata loadedmetadata", function (e) {
	          return jh.durationUpdate.call(t, e);
	        }), Vu.call(t, t.media, "ended", function () {
	          t.isHTML5 && t.isVideo && t.config.resetOnEnd && (t.restart(), t.pause());
	        }), Vu.call(t, t.media, "progress playing seeking seeked", function (e) {
	          return jh.updateProgress.call(t, e);
	        }), Vu.call(t, t.media, "volumechange", function (e) {
	          return jh.updateVolume.call(t, e);
	        }), Vu.call(t, t.media, "playing play pause ended emptied timeupdate", function (e) {
	          return $h.checkPlaying.call(t, e);
	        }), Vu.call(t, t.media, "waiting canplay seeked playing", function (e) {
	          return $h.checkLoading.call(t, e);
	        }), t.supported.ui && t.config.clickToPlay && !t.isAudio) {
	          var i = _u.call(t, ".".concat(t.config.classNames.video));if (!su(i)) return;Vu.call(t, n.container, "click", function (r) {
	            ([n.container, i].includes(r.target) || i.contains(r.target)) && (t.touch && t.config.hideControls || (t.ended ? (e.proxy(r, t.restart, "restart"), e.proxy(r, function () {
	              Gu(t.play());
	            }, "play")) : e.proxy(r, function () {
	              Gu(t.togglePlay());
	            }, "play")));
	          });
	        }t.supported.ui && t.config.disableContextMenu && Vu.call(t, n.wrapper, "contextmenu", function (e) {
	          e.preventDefault();
	        }, !1), Vu.call(t, t.media, "volumechange", function () {
	          t.storage.set({ volume: t.volume, muted: t.muted });
	        }), Vu.call(t, t.media, "ratechange", function () {
	          jh.updateSetting.call(t, "speed"), t.storage.set({ speed: t.speed });
	        }), Vu.call(t, t.media, "qualitychange", function (e) {
	          jh.updateSetting.call(t, "quality", null, e.detail.quality);
	        }), Vu.call(t, t.media, "ready qualitychange", function () {
	          jh.setDownloadUrl.call(t);
	        });var r = t.config.events.concat(["keyup", "keydown"]).join(" ");Vu.call(t, t.media, r, function (e) {
	          var i = e.detail,
	              r = void 0 === i ? {} : i;"error" === e.type && (r = t.media.error), Ku.call(t, n.container, e.type, !0, r);
	        });
	      } }, { key: "proxy", value: function value(e, t, n) {
	        var i = this.player,
	            r = i.config.listeners[n],
	            a = !0;ru(r) && (a = r.call(i, e)), !1 !== a && ru(t) && t.call(i, e);
	      } }, { key: "bind", value: function value(e, t, n, i) {
	        var r = this,
	            a = !(arguments.length > 4 && void 0 !== arguments[4]) || arguments[4],
	            o = this.player,
	            s = o.config.listeners[i],
	            l = ru(s);Vu.call(o, e, t, function (e) {
	          return r.proxy(e, n, i);
	        }, a && !l);
	      } }, { key: "controls", value: function value() {
	        var e = this,
	            t = this.player,
	            n = t.elements,
	            i = gu.isIE ? "change" : "input";if (n.buttons.play && Array.from(n.buttons.play).forEach(function (n) {
	          e.bind(n, "click", function () {
	            Gu(t.togglePlay());
	          }, "play");
	        }), this.bind(n.buttons.restart, "click", t.restart, "restart"), this.bind(n.buttons.rewind, "click", t.rewind, "rewind"), this.bind(n.buttons.fastForward, "click", t.forward, "fastForward"), this.bind(n.buttons.mute, "click", function () {
	          t.muted = !t.muted;
	        }, "mute"), this.bind(n.buttons.captions, "click", function () {
	          return t.toggleCaptions();
	        }), this.bind(n.buttons.download, "click", function () {
	          Ku.call(t, t.media, "download");
	        }, "download"), this.bind(n.buttons.fullscreen, "click", function () {
	          t.fullscreen.toggle();
	        }, "fullscreen"), this.bind(n.buttons.pip, "click", function () {
	          t.pip = "toggle";
	        }, "pip"), this.bind(n.buttons.airplay, "click", t.airplay, "airplay"), this.bind(n.buttons.settings, "click", function (e) {
	          e.stopPropagation(), e.preventDefault(), jh.toggleMenu.call(t, e);
	        }, null, !1), this.bind(n.buttons.settings, "keyup", function (e) {
	          var n = e.which;[13, 32].includes(n) && (13 !== n ? (e.preventDefault(), e.stopPropagation(), jh.toggleMenu.call(t, e)) : jh.focusFirstMenuItem.call(t, null, !0));
	        }, null, !1), this.bind(n.settings.menu, "keydown", function (e) {
	          27 === e.which && jh.toggleMenu.call(t, e);
	        }), this.bind(n.inputs.seek, "mousedown mousemove", function (e) {
	          var t = n.progress.getBoundingClientRect(),
	              i = 100 / t.width * (e.pageX - t.left);e.currentTarget.setAttribute("seek-value", i);
	        }), this.bind(n.inputs.seek, "mousedown mouseup keydown keyup touchstart touchend", function (e) {
	          var n = e.currentTarget,
	              i = e.keyCode ? e.keyCode : e.which;if (!cu(e) || 39 === i || 37 === i) {
	            t.lastSeekTime = Date.now();var r = n.hasAttribute("play-on-seeked"),
	                a = ["mouseup", "touchend", "keyup"].includes(e.type);r && a ? (n.removeAttribute("play-on-seeked"), Gu(t.play())) : !a && t.playing && (n.setAttribute("play-on-seeked", ""), t.pause());
	          }
	        }), gu.isIos) {
	          var r = Mu.call(t, 'input[type="range"]');Array.from(r).forEach(function (t) {
	            return e.bind(t, i, function (e) {
	              return mu(e.target);
	            });
	          });
	        }this.bind(n.inputs.seek, i, function (e) {
	          var n = e.currentTarget,
	              i = n.getAttribute("seek-value");du(i) && (i = n.value), n.removeAttribute("seek-value"), t.currentTime = i / n.max * t.duration;
	        }, "seek"), this.bind(n.progress, "mouseenter mouseleave mousemove", function (e) {
	          return jh.updateSeekTooltip.call(t, e);
	        }), this.bind(n.progress, "mousemove touchmove", function (e) {
	          var n = t.previewThumbnails;n && n.loaded && n.startMove(e);
	        }), this.bind(n.progress, "mouseleave touchend click", function () {
	          var e = t.previewThumbnails;e && e.loaded && e.endMove(!1, !0);
	        }), this.bind(n.progress, "mousedown touchstart", function (e) {
	          var n = t.previewThumbnails;n && n.loaded && n.startScrubbing(e);
	        }), this.bind(n.progress, "mouseup touchend", function (e) {
	          var n = t.previewThumbnails;n && n.loaded && n.endScrubbing(e);
	        }), gu.isWebkit && Array.from(Mu.call(t, 'input[type="range"]')).forEach(function (n) {
	          e.bind(n, "input", function (e) {
	            return jh.updateRangeFill.call(t, e.target);
	          });
	        }), t.config.toggleInvert && !su(n.display.duration) && this.bind(n.display.currentTime, "click", function () {
	          0 !== t.currentTime && (t.config.invertTime = !t.config.invertTime, jh.timeUpdate.call(t));
	        }), this.bind(n.inputs.volume, i, function (e) {
	          t.volume = e.target.value;
	        }, "volume"), this.bind(n.controls, "mouseenter mouseleave", function (e) {
	          n.controls.hover = !t.touch && "mouseenter" === e.type;
	        }), n.fullscreen && Array.from(n.fullscreen.children).filter(function (e) {
	          return !e.contains(n.container);
	        }).forEach(function (i) {
	          e.bind(i, "mouseenter mouseleave", function (e) {
	            n.controls.hover = !t.touch && "mouseenter" === e.type;
	          });
	        }), this.bind(n.controls, "mousedown mouseup touchstart touchend touchcancel", function (e) {
	          n.controls.pressed = ["mousedown", "touchstart"].includes(e.type);
	        }), this.bind(n.controls, "focusin", function () {
	          var i = t.config,
	              r = t.timers;ju(n.controls, i.classNames.noTransition, !0), $h.toggleControls.call(t, !0), setTimeout(function () {
	            ju(n.controls, i.classNames.noTransition, !1);
	          }, 0);var a = e.touch ? 3e3 : 4e3;clearTimeout(r.controls), r.controls = setTimeout(function () {
	            return $h.toggleControls.call(t, !1);
	          }, a);
	        }), this.bind(n.inputs.volume, "wheel", function (e) {
	          var n = e.webkitDirectionInvertedFromDevice,
	              i = Ao([e.deltaX, -e.deltaY].map(function (e) {
	            return n ? -e : e;
	          }), 2),
	              r = i[0],
	              a = i[1],
	              o = Math.sign(Math.abs(r) > Math.abs(a) ? r : a);t.increaseVolume(o / 50);var s = t.media.volume;(1 === o && s < 1 || -1 === o && s > 0) && e.preventDefault();
	        }, "volume", !1);
	      } }]), e;
	  }(),
	      Gh = Kn("splice"),
	      Xh = Qt("splice", { ACCESSORS: !0, 0: 0, 1: 2 }),
	      Qh = Math.max,
	      Jh = Math.min;Ie({ target: "Array", proto: !0, forced: !Gh || !Xh }, { splice: function splice(e, t) {
	      var n,
	          i,
	          r,
	          a,
	          o,
	          s,
	          l = Re(this),
	          c = le(l.length),
	          u = he(e, c),
	          h = arguments.length;if (0 === h ? n = i = 0 : 1 === h ? (n = 0, i = c - u) : (n = h - 2, i = Jh(Qh(oe(t), 0), c - u)), c + n - i > 9007199254740991) throw TypeError("Maximum allowed length exceeded");for (r = ot(l, i), a = 0; a < i; a++) {
	        (o = u + a) in l && Fn(r, a, l[o]);
	      }if (r.length = i, n < i) {
	        for (a = u; a < c - i; a++) {
	          s = a + n, (o = a + i) in l ? l[s] = l[o] : delete l[s];
	        }for (a = c; a > c - i + n; a--) {
	          delete l[a - 1];
	        }
	      } else if (n > i) for (a = c - i; a > u; a--) {
	        s = a + n - 1, (o = a + i - 1) in l ? l[s] = l[o] : delete l[s];
	      }for (a = 0; a < n; a++) {
	        l[a + u] = arguments[a + 2];
	      }return l.length = c - i + n, r;
	    } });var Zh = t(function (e, t) {
	    e.exports = function () {
	      var e = function e() {},
	          t = {},
	          n = {},
	          i = {};function r(e, t) {
	        if (e) {
	          var r = i[e];if (n[e] = t, r) for (; r.length;) {
	            r[0](e, t), r.splice(0, 1);
	          }
	        }
	      }function a(t, n) {
	        t.call && (t = { success: t }), n.length ? (t.error || e)(n) : (t.success || e)(t);
	      }function o(t, n, i, r) {
	        var a,
	            s,
	            l = document,
	            c = i.async,
	            u = (i.numRetries || 0) + 1,
	            h = i.before || e,
	            f = t.replace(/[\?|#].*$/, ""),
	            d = t.replace(/^(css|img)!/, "");r = r || 0, /(^css!|\.css$)/.test(f) ? ((s = l.createElement("link")).rel = "stylesheet", s.href = d, (a = "hideFocus" in s) && s.relList && (a = 0, s.rel = "preload", s.as = "style")) : /(^img!|\.(png|gif|jpg|svg|webp)$)/.test(f) ? (s = l.createElement("img")).src = d : ((s = l.createElement("script")).src = t, s.async = void 0 === c || c), s.onload = s.onerror = s.onbeforeload = function (e) {
	          var l = e.type[0];if (a) try {
	            s.sheet.cssText.length || (l = "e");
	          } catch (e) {
	            18 != e.code && (l = "e");
	          }if ("e" == l) {
	            if ((r += 1) < u) return o(t, n, i, r);
	          } else if ("preload" == s.rel && "style" == s.as) return s.rel = "stylesheet";n(t, l, e.defaultPrevented);
	        }, !1 !== h(t, s) && l.head.appendChild(s);
	      }function s(e, n, i) {
	        var s, l;if (n && n.trim && (s = n), l = (s ? i : n) || {}, s) {
	          if (s in t) throw "LoadJS";t[s] = !0;
	        }function c(t, n) {
	          !function (e, t, n) {
	            var i,
	                r,
	                a = (e = e.push ? e : [e]).length,
	                s = a,
	                l = [];for (i = function i(e, n, _i2) {
	              if ("e" == n && l.push(e), "b" == n) {
	                if (!_i2) return;l.push(e);
	              }--a || t(l);
	            }, r = 0; r < s; r++) {
	              o(e[r], i, n);
	            }
	          }(e, function (e) {
	            a(l, e), t && a({ success: t, error: n }, e), r(s, e);
	          }, l);
	        }if (l.returnPromise) return new Promise(c);c();
	      }return s.ready = function (e, t) {
	        return function (e, t) {
	          e = e.push ? e : [e];var r,
	              a,
	              o,
	              s = [],
	              l = e.length,
	              c = l;for (r = function r(e, n) {
	            n.length && s.push(e), --c || t(s);
	          }; l--;) {
	            a = e[l], (o = n[a]) ? r(a, o) : (i[a] = i[a] || []).push(r);
	          }
	        }(e, function (e) {
	          a(t, e);
	        }), s;
	      }, s.done = function (e) {
	        r(e, []);
	      }, s.reset = function () {
	        t = {}, n = {}, i = {};
	      }, s.isDefined = function (e) {
	        return e in t;
	      }, s;
	    }();
	  });function ef(e) {
	    return new Promise(function (t, n) {
	      Zh(e, { success: t, error: n });
	    });
	  }function tf(e) {
	    e && !this.embed.hasPlayed && (this.embed.hasPlayed = !0), this.media.paused === e && (this.media.paused = !e, Ku.call(this, this.media, e ? "play" : "pause"));
	  }var nf = { setup: function setup() {
	      var e = this;ju(e.elements.wrapper, e.config.classNames.embed, !0), e.options.speed = e.config.speed.options, Zu.call(e), eu(window.Vimeo) ? nf.ready.call(e) : ef(e.config.urls.vimeo.sdk).then(function () {
	        nf.ready.call(e);
	      }).catch(function (t) {
	        e.debug.warn("Vimeo SDK (player.js) failed to load", t);
	      });
	    }, ready: function ready() {
	      var e = this,
	          t = this,
	          n = t.config.vimeo,
	          i = n.premium,
	          r = n.referrerPolicy,
	          a = Eo(n, ["premium", "referrerPolicy"]);i && Object.assign(a, { controls: !1, sidedock: !1 });var o = Rh(So({ loop: t.config.loop.active, autoplay: t.autoplay, muted: t.muted, gesture: "media", playsinline: !this.config.fullscreen.iosNative }, a)),
	          s = t.media.getAttribute("src");du(s) && (s = t.media.getAttribute(t.config.attributes.embed.id));var l,
	          c = du(l = s) ? null : tu(Number(l)) ? l : l.match(/^.*(vimeo.com\/|video\/)(\d+).*/) ? RegExp.$2 : l,
	          u = Au("iframe"),
	          h = gh(t.config.urls.vimeo.iframe, c, o);u.setAttribute("src", h), u.setAttribute("allowfullscreen", ""), u.setAttribute("allow", "autoplay,fullscreen,picture-in-picture"), du(r) || u.setAttribute("referrerPolicy", r);var f = t.poster;if (i) u.setAttribute("data-poster", f), t.media = Ou(u, t.media);else {
	        var d = Au("div", { class: t.config.classNames.embedContainer, "data-poster": f });d.appendChild(u), t.media = Ou(d, t.media);
	      }Eh(gh(t.config.urls.vimeo.api, c), "json").then(function (e) {
	        if (!du(e)) {
	          var n = new URL(e[0].thumbnail_large);n.pathname = "".concat(n.pathname.split("_")[0], ".jpg"), $h.setPoster.call(t, n.href).catch(function () {});
	        }
	      }), t.embed = new window.Vimeo.Player(u, { autopause: t.config.autopause, muted: t.muted }), t.media.paused = !0, t.media.currentTime = 0, t.supported.ui && t.embed.disableTextTrack(), t.media.play = function () {
	        return tf.call(t, !0), t.embed.play();
	      }, t.media.pause = function () {
	        return tf.call(t, !1), t.embed.pause();
	      }, t.media.stop = function () {
	        t.pause(), t.currentTime = 0;
	      };var p = t.media.currentTime;Object.defineProperty(t.media, "currentTime", { get: function get() {
	          return p;
	        }, set: function set(e) {
	          var n = t.embed,
	              i = t.media,
	              r = t.paused,
	              a = t.volume,
	              o = r && !n.hasPlayed;i.seeking = !0, Ku.call(t, i, "seeking"), Promise.resolve(o && n.setVolume(0)).then(function () {
	            return n.setCurrentTime(e);
	          }).then(function () {
	            return o && n.pause();
	          }).then(function () {
	            return o && n.setVolume(a);
	          }).catch(function () {});
	        } });var m = t.config.speed.selected;Object.defineProperty(t.media, "playbackRate", { get: function get() {
	          return m;
	        }, set: function set(e) {
	          t.embed.setPlaybackRate(e).then(function () {
	            m = e, Ku.call(t, t.media, "ratechange");
	          }).catch(function () {
	            t.options.speed = [1];
	          });
	        } });var g = t.config.volume;Object.defineProperty(t.media, "volume", { get: function get() {
	          return g;
	        }, set: function set(e) {
	          t.embed.setVolume(e).then(function () {
	            g = e, Ku.call(t, t.media, "volumechange");
	          });
	        } });var v = t.config.muted;Object.defineProperty(t.media, "muted", { get: function get() {
	          return v;
	        }, set: function set(e) {
	          var n = !!iu(e) && e;t.embed.setVolume(n ? 0 : t.config.volume).then(function () {
	            v = n, Ku.call(t, t.media, "volumechange");
	          });
	        } });var y,
	          b = t.config.loop;Object.defineProperty(t.media, "loop", { get: function get() {
	          return b;
	        }, set: function set(e) {
	          var n = iu(e) ? e : t.config.loop.active;t.embed.setLoop(n).then(function () {
	            b = n;
	          });
	        } }), t.embed.getVideoUrl().then(function (e) {
	        y = e, jh.setDownloadUrl.call(t);
	      }).catch(function (t) {
	        e.debug.warn(t);
	      }), Object.defineProperty(t.media, "currentSrc", { get: function get() {
	          return y;
	        } }), Object.defineProperty(t.media, "ended", { get: function get() {
	          return t.currentTime === t.duration;
	        } }), Promise.all([t.embed.getVideoWidth(), t.embed.getVideoHeight()]).then(function (n) {
	        var i = Ao(n, 2),
	            r = i[0],
	            a = i[1];t.embed.ratio = [r, a], Zu.call(e);
	      }), t.embed.setAutopause(t.config.autopause).then(function (e) {
	        t.config.autopause = e;
	      }), t.embed.getVideoTitle().then(function (n) {
	        t.config.title = n, $h.setTitle.call(e);
	      }), t.embed.getCurrentTime().then(function (e) {
	        p = e, Ku.call(t, t.media, "timeupdate");
	      }), t.embed.getDuration().then(function (e) {
	        t.media.duration = e, Ku.call(t, t.media, "durationchange");
	      }), t.embed.getTextTracks().then(function (e) {
	        t.media.textTracks = e, Mh.setup.call(t);
	      }), t.embed.on("cuechange", function (e) {
	        var n = e.cues,
	            i = (void 0 === n ? [] : n).map(function (e) {
	          return function (e) {
	            var t = document.createDocumentFragment(),
	                n = document.createElement("div");return t.appendChild(n), n.innerHTML = e, t.firstChild.innerText;
	          }(e.text);
	        });Mh.updateCues.call(t, i);
	      }), t.embed.on("loaded", function () {
	        (t.embed.getPaused().then(function (e) {
	          tf.call(t, !e), e || Ku.call(t, t.media, "playing");
	        }), su(t.embed.element) && t.supported.ui) && t.embed.element.setAttribute("tabindex", -1);
	      }), t.embed.on("bufferstart", function () {
	        Ku.call(t, t.media, "waiting");
	      }), t.embed.on("bufferend", function () {
	        Ku.call(t, t.media, "playing");
	      }), t.embed.on("play", function () {
	        tf.call(t, !0), Ku.call(t, t.media, "playing");
	      }), t.embed.on("pause", function () {
	        tf.call(t, !1);
	      }), t.embed.on("timeupdate", function (e) {
	        t.media.seeking = !1, p = e.seconds, Ku.call(t, t.media, "timeupdate");
	      }), t.embed.on("progress", function (e) {
	        t.media.buffered = e.percent, Ku.call(t, t.media, "progress"), 1 === parseInt(e.percent, 10) && Ku.call(t, t.media, "canplaythrough"), t.embed.getDuration().then(function (e) {
	          e !== t.media.duration && (t.media.duration = e, Ku.call(t, t.media, "durationchange"));
	        });
	      }), t.embed.on("seeked", function () {
	        t.media.seeking = !1, Ku.call(t, t.media, "seeked");
	      }), t.embed.on("ended", function () {
	        t.media.paused = !0, Ku.call(t, t.media, "ended");
	      }), t.embed.on("error", function (e) {
	        t.media.error = e, Ku.call(t, t.media, "error");
	      }), setTimeout(function () {
	        return $h.build.call(t);
	      }, 0);
	    } };function rf(e) {
	    e && !this.embed.hasPlayed && (this.embed.hasPlayed = !0), this.media.paused === e && (this.media.paused = !e, Ku.call(this, this.media, e ? "play" : "pause"));
	  }function af(e) {
	    return e.noCookie ? "https://www.youtube-nocookie.com" : "http:" === window.location.protocol ? "http://www.youtube.com" : void 0;
	  }var of = { setup: function setup() {
	      var e = this;if (ju(this.elements.wrapper, this.config.classNames.embed, !0), eu(window.YT) && ru(window.YT.Player)) of.ready.call(this);else {
	        var t = window.onYouTubeIframeAPIReady;window.onYouTubeIframeAPIReady = function () {
	          ru(t) && t(), of.ready.call(e);
	        }, ef(this.config.urls.youtube.sdk).catch(function (t) {
	          e.debug.warn("YouTube API failed to load", t);
	        });
	      }
	    }, getTitle: function getTitle(e) {
	      var t = this;Eh(gh(this.config.urls.youtube.api, e)).then(function (e) {
	        if (eu(e)) {
	          var n = e.title,
	              i = e.height,
	              r = e.width;t.config.title = n, $h.setTitle.call(t), t.embed.ratio = [r, i];
	        }Zu.call(t);
	      }).catch(function () {
	        Zu.call(t);
	      });
	    }, ready: function ready() {
	      var e = this,
	          t = e.media && e.media.getAttribute("id");if (du(t) || !t.startsWith("youtube-")) {
	        var n = e.media.getAttribute("src");du(n) && (n = e.media.getAttribute(this.config.attributes.embed.id));var i,
	            r,
	            a = du(i = n) ? null : i.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/) ? RegExp.$2 : i,
	            o = (r = e.provider, "".concat(r, "-").concat(Math.floor(1e4 * Math.random()))),
	            s = Au("div", { id: o, "data-poster": e.poster });e.media = Ou(s, e.media);var l = function l(e) {
	          return "https://i.ytimg.com/vi/".concat(a, "/").concat(e, "default.jpg");
	        };Kh(l("maxres"), 121).catch(function () {
	          return Kh(l("sd"), 121);
	        }).catch(function () {
	          return Kh(l("hq"));
	        }).then(function (t) {
	          return $h.setPoster.call(e, t.src);
	        }).then(function (t) {
	          t.includes("maxres") || (e.elements.poster.style.backgroundSize = "cover");
	        }).catch(function () {});var c = e.config.youtube;e.embed = new window.YT.Player(o, { videoId: a, host: af(c), playerVars: Tu({}, { autoplay: e.config.autoplay ? 1 : 0, hl: e.config.hl, controls: e.supported.ui ? 0 : 1, disablekb: 1, playsinline: e.config.fullscreen.iosNative ? 0 : 1, cc_load_policy: e.captions.active ? 1 : 0, cc_lang_pref: e.config.captions.language, widget_referrer: window ? window.location.href : null }, c), events: { onError: function onError(t) {
	              if (!e.media.error) {
	                var n = t.data,
	                    i = { 2: "The request contains an invalid parameter value. For example, this error occurs if you specify a video ID that does not have 11 characters, or if the video ID contains invalid characters, such as exclamation points or asterisks.", 5: "The requested content cannot be played in an HTML5 player or another error related to the HTML5 player has occurred.", 100: "The video requested was not found. This error occurs when a video has been removed (for any reason) or has been marked as private.", 101: "The owner of the requested video does not allow it to be played in embedded players.", 150: "The owner of the requested video does not allow it to be played in embedded players." }[n] || "An unknown error occured";e.media.error = { code: n, message: i }, Ku.call(e, e.media, "error");
	              }
	            }, onPlaybackRateChange: function onPlaybackRateChange(t) {
	              var n = t.target;e.media.playbackRate = n.getPlaybackRate(), Ku.call(e, e.media, "ratechange");
	            }, onReady: function onReady(t) {
	              if (!ru(e.media.play)) {
	                var n = t.target;of.getTitle.call(e, a), e.media.play = function () {
	                  rf.call(e, !0), n.playVideo();
	                }, e.media.pause = function () {
	                  rf.call(e, !1), n.pauseVideo();
	                }, e.media.stop = function () {
	                  n.stopVideo();
	                }, e.media.duration = n.getDuration(), e.media.paused = !0, e.media.currentTime = 0, Object.defineProperty(e.media, "currentTime", { get: function get() {
	                    return Number(n.getCurrentTime());
	                  }, set: function set(t) {
	                    e.paused && !e.embed.hasPlayed && e.embed.mute(), e.media.seeking = !0, Ku.call(e, e.media, "seeking"), n.seekTo(t);
	                  } }), Object.defineProperty(e.media, "playbackRate", { get: function get() {
	                    return n.getPlaybackRate();
	                  }, set: function set(e) {
	                    n.setPlaybackRate(e);
	                  } });var i = e.config.volume;Object.defineProperty(e.media, "volume", { get: function get() {
	                    return i;
	                  }, set: function set(t) {
	                    i = t, n.setVolume(100 * i), Ku.call(e, e.media, "volumechange");
	                  } });var r = e.config.muted;Object.defineProperty(e.media, "muted", { get: function get() {
	                    return r;
	                  }, set: function set(t) {
	                    var i = iu(t) ? t : r;r = i, n[i ? "mute" : "unMute"](), Ku.call(e, e.media, "volumechange");
	                  } }), Object.defineProperty(e.media, "currentSrc", { get: function get() {
	                    return n.getVideoUrl();
	                  } }), Object.defineProperty(e.media, "ended", { get: function get() {
	                    return e.currentTime === e.duration;
	                  } });var o = n.getAvailablePlaybackRates();e.options.speed = o.filter(function (t) {
	                  return e.config.speed.options.includes(t);
	                }), e.supported.ui && e.media.setAttribute("tabindex", -1), Ku.call(e, e.media, "timeupdate"), Ku.call(e, e.media, "durationchange"), clearInterval(e.timers.buffering), e.timers.buffering = setInterval(function () {
	                  e.media.buffered = n.getVideoLoadedFraction(), (null === e.media.lastBuffered || e.media.lastBuffered < e.media.buffered) && Ku.call(e, e.media, "progress"), e.media.lastBuffered = e.media.buffered, 1 === e.media.buffered && (clearInterval(e.timers.buffering), Ku.call(e, e.media, "canplaythrough"));
	                }, 200), setTimeout(function () {
	                  return $h.build.call(e);
	                }, 50);
	              }
	            }, onStateChange: function onStateChange(t) {
	              var n = t.target;switch (clearInterval(e.timers.playing), e.media.seeking && [1, 2].includes(t.data) && (e.media.seeking = !1, Ku.call(e, e.media, "seeked")), t.data) {case -1:
	                  Ku.call(e, e.media, "timeupdate"), e.media.buffered = n.getVideoLoadedFraction(), Ku.call(e, e.media, "progress");break;case 0:
	                  rf.call(e, !1), e.media.loop ? (n.stopVideo(), n.playVideo()) : Ku.call(e, e.media, "ended");break;case 1:
	                  e.config.autoplay || !e.media.paused || e.embed.hasPlayed ? (rf.call(e, !0), Ku.call(e, e.media, "playing"), e.timers.playing = setInterval(function () {
	                    Ku.call(e, e.media, "timeupdate");
	                  }, 50), e.media.duration !== n.getDuration() && (e.media.duration = n.getDuration(), Ku.call(e, e.media, "durationchange"))) : e.media.pause();break;case 2:
	                  e.muted || e.embed.unMute(), rf.call(e, !1);break;case 3:
	                  Ku.call(e, e.media, "waiting");}Ku.call(e, e.elements.container, "statechange", !1, { code: t.data });
	            } } });
	      }
	    } },
	      sf = { setup: function setup() {
	      this.media ? (ju(this.elements.container, this.config.classNames.type.replace("{0}", this.type), !0), ju(this.elements.container, this.config.classNames.provider.replace("{0}", this.provider), !0), this.isEmbed && ju(this.elements.container, this.config.classNames.type.replace("{0}", "video"), !0), this.isVideo && (this.elements.wrapper = Au("div", { class: this.config.classNames.video }), Su(this.media, this.elements.wrapper), this.elements.poster = Au("div", { class: this.config.classNames.poster }), this.elements.wrapper.appendChild(this.elements.poster)), this.isHTML5 ? eh.setup.call(this) : this.isYouTube ? of.setup.call(this) : this.isVimeo && nf.setup.call(this)) : this.debug.warn("No media element found!");
	    } },
	      lf = function () {
	    function e(t) {
	      var n = this;yo(this, e), this.player = t, this.config = t.config.ads, this.playing = !1, this.initialized = !1, this.elements = { container: null, displayContainer: null }, this.manager = null, this.loader = null, this.cuePoints = null, this.events = {}, this.safetyTimer = null, this.countdownTimer = null, this.managerPromise = new Promise(function (e, t) {
	        n.on("loaded", e), n.on("error", t);
	      }), this.load();
	    }return wo(e, [{ key: "load", value: function value() {
	        var e = this;this.enabled && (eu(window.google) && eu(window.google.ima) ? this.ready() : ef(this.player.config.urls.googleIMA.sdk).then(function () {
	          e.ready();
	        }).catch(function () {
	          e.trigger("error", new Error("Google IMA SDK failed to load"));
	        }));
	      } }, { key: "ready", value: function value() {
	        var e,
	            t = this;this.enabled || ((e = this).manager && e.manager.destroy(), e.elements.displayContainer && e.elements.displayContainer.destroy(), e.elements.container.remove()), this.startSafetyTimer(12e3, "ready()"), this.managerPromise.then(function () {
	          t.clearSafetyTimer("onAdsManagerLoaded()");
	        }), this.listeners(), this.setupIMA();
	      } }, { key: "setupIMA", value: function value() {
	        var e = this;this.elements.container = Au("div", { class: this.player.config.classNames.ads }), this.player.elements.container.appendChild(this.elements.container), google.ima.settings.setVpaidMode(google.ima.ImaSdkSettings.VpaidMode.ENABLED), google.ima.settings.setLocale(this.player.config.ads.language), google.ima.settings.setDisableCustomPlaybackForIOS10Plus(this.player.config.playsinline), this.elements.displayContainer = new google.ima.AdDisplayContainer(this.elements.container, this.player.media), this.loader = new google.ima.AdsLoader(this.elements.displayContainer), this.loader.addEventListener(google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, function (t) {
	          return e.onAdsManagerLoaded(t);
	        }, !1), this.loader.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, function (t) {
	          return e.onAdError(t);
	        }, !1), this.requestAds();
	      } }, { key: "requestAds", value: function value() {
	        var e = this.player.elements.container;try {
	          var t = new google.ima.AdsRequest();t.adTagUrl = this.tagUrl, t.linearAdSlotWidth = e.offsetWidth, t.linearAdSlotHeight = e.offsetHeight, t.nonLinearAdSlotWidth = e.offsetWidth, t.nonLinearAdSlotHeight = e.offsetHeight, t.forceNonLinearFullSlot = !1, t.setAdWillPlayMuted(!this.player.muted), this.loader.requestAds(t);
	        } catch (e) {
	          this.onAdError(e);
	        }
	      } }, { key: "pollCountdown", value: function value() {
	        var e = this,
	            t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];if (!t) return clearInterval(this.countdownTimer), void this.elements.container.removeAttribute("data-badge-text");var n = function n() {
	          var t = Lh(Math.max(e.manager.getRemainingTime(), 0)),
	              n = "".concat(Th("advertisement", e.player.config), " - ").concat(t);e.elements.container.setAttribute("data-badge-text", n);
	        };this.countdownTimer = setInterval(n, 100);
	      } }, { key: "onAdsManagerLoaded", value: function value(e) {
	        var t = this;if (this.enabled) {
	          var n = new google.ima.AdsRenderingSettings();n.restoreCustomPlaybackStateOnAdBreakComplete = !0, n.enablePreloading = !0, this.manager = e.getAdsManager(this.player, n), this.cuePoints = this.manager.getCuePoints(), this.manager.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, function (e) {
	            return t.onAdError(e);
	          }), Object.keys(google.ima.AdEvent.Type).forEach(function (e) {
	            t.manager.addEventListener(google.ima.AdEvent.Type[e], function (e) {
	              return t.onAdEvent(e);
	            });
	          }), this.trigger("loaded");
	        }
	      } }, { key: "addCuePoints", value: function value() {
	        var e = this;du(this.cuePoints) || this.cuePoints.forEach(function (t) {
	          if (0 !== t && -1 !== t && t < e.player.duration) {
	            var n = e.player.elements.progress;if (su(n)) {
	              var i = 100 / e.player.duration * t,
	                  r = Au("span", { class: e.player.config.classNames.cues });r.style.left = "".concat(i.toString(), "%"), n.appendChild(r);
	            }
	          }
	        });
	      } }, { key: "onAdEvent", value: function value(e) {
	        var t = this,
	            n = this.player.elements.container,
	            i = e.getAd(),
	            r = e.getAdData();switch (function (e) {
	          Ku.call(t.player, t.player.media, "ads".concat(e.replace(/_/g, "").toLowerCase()));
	        }(e.type), e.type) {case google.ima.AdEvent.Type.LOADED:
	            this.trigger("loaded"), this.pollCountdown(!0), i.isLinear() || (i.width = n.offsetWidth, i.height = n.offsetHeight);break;case google.ima.AdEvent.Type.STARTED:
	            this.manager.setVolume(this.player.volume);break;case google.ima.AdEvent.Type.ALL_ADS_COMPLETED:
	            this.player.ended ? this.loadAds() : this.loader.contentComplete();break;case google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED:
	            this.pauseContent();break;case google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED:
	            this.pollCountdown(), this.resumeContent();break;case google.ima.AdEvent.Type.LOG:
	            r.adError && this.player.debug.warn("Non-fatal ad error: ".concat(r.adError.getMessage()));}
	      } }, { key: "onAdError", value: function value(e) {
	        this.cancel(), this.player.debug.warn("Ads error", e);
	      } }, { key: "listeners", value: function value() {
	        var e,
	            t = this,
	            n = this.player.elements.container;this.player.on("canplay", function () {
	          t.addCuePoints();
	        }), this.player.on("ended", function () {
	          t.loader.contentComplete();
	        }), this.player.on("timeupdate", function () {
	          e = t.player.currentTime;
	        }), this.player.on("seeked", function () {
	          var n = t.player.currentTime;du(t.cuePoints) || t.cuePoints.forEach(function (i, r) {
	            e < i && i < n && (t.manager.discardAdBreak(), t.cuePoints.splice(r, 1));
	          });
	        }), window.addEventListener("resize", function () {
	          t.manager && t.manager.resize(n.offsetWidth, n.offsetHeight, google.ima.ViewMode.NORMAL);
	        });
	      } }, { key: "play", value: function value() {
	        var e = this,
	            t = this.player.elements.container;this.managerPromise || this.resumeContent(), this.managerPromise.then(function () {
	          e.manager.setVolume(e.player.volume), e.elements.displayContainer.initialize();try {
	            e.initialized || (e.manager.init(t.offsetWidth, t.offsetHeight, google.ima.ViewMode.NORMAL), e.manager.start()), e.initialized = !0;
	          } catch (t) {
	            e.onAdError(t);
	          }
	        }).catch(function () {});
	      } }, { key: "resumeContent", value: function value() {
	        this.elements.container.style.zIndex = "", this.playing = !1, Gu(this.player.media.play());
	      } }, { key: "pauseContent", value: function value() {
	        this.elements.container.style.zIndex = 3, this.playing = !0, this.player.media.pause();
	      } }, { key: "cancel", value: function value() {
	        this.initialized && this.resumeContent(), this.trigger("error"), this.loadAds();
	      } }, { key: "loadAds", value: function value() {
	        var e = this;this.managerPromise.then(function () {
	          e.manager && e.manager.destroy(), e.managerPromise = new Promise(function (t) {
	            e.on("loaded", t), e.player.debug.log(e.manager);
	          }), e.initialized = !1, e.requestAds();
	        }).catch(function () {});
	      } }, { key: "trigger", value: function value(e) {
	        for (var t = this, n = arguments.length, i = new Array(n > 1 ? n - 1 : 0), r = 1; r < n; r++) {
	          i[r - 1] = arguments[r];
	        }var a = this.events[e];au(a) && a.forEach(function (e) {
	          ru(e) && e.apply(t, i);
	        });
	      } }, { key: "on", value: function value(e, t) {
	        return au(this.events[e]) || (this.events[e] = []), this.events[e].push(t), this;
	      } }, { key: "startSafetyTimer", value: function value(e, t) {
	        var n = this;this.player.debug.log("Safety timer invoked from: ".concat(t)), this.safetyTimer = setTimeout(function () {
	          n.cancel(), n.clearSafetyTimer("startSafetyTimer()");
	        }, e);
	      } }, { key: "clearSafetyTimer", value: function value(e) {
	        Zc(this.safetyTimer) || (this.player.debug.log("Safety timer cleared from: ".concat(e)), clearTimeout(this.safetyTimer), this.safetyTimer = null);
	      } }, { key: "enabled", get: function get() {
	        var e = this.config;return this.player.isHTML5 && this.player.isVideo && e.enabled && (!du(e.publisherId) || fu(e.tagUrl));
	      } }, { key: "tagUrl", get: function get() {
	        var e = this.config;if (fu(e.tagUrl)) return e.tagUrl;var t = { AV_PUBLISHERID: "58c25bb0073ef448b1087ad6", AV_CHANNELID: "5a0458dc28a06145e4519d21", AV_URL: window.location.hostname, cb: Date.now(), AV_WIDTH: 640, AV_HEIGHT: 480, AV_CDIM2: e.publisherId };return "".concat("https://go.aniview.com/api/adserver6/vast/", "?").concat(Rh(t));
	      } }]), e;
	  }(),
	      cf = ct.findIndex,
	      uf = !0,
	      hf = Qt("findIndex");"findIndex" in [] && Array(1).findIndex(function () {
	    uf = !1;
	  }), Ie({ target: "Array", proto: !0, forced: uf || !hf }, { findIndex: function findIndex(e) {
	      return cf(this, e, arguments.length > 1 ? arguments[1] : void 0);
	    } }), dn("findIndex");var ff = Math.min,
	      df = [].lastIndexOf,
	      pf = !!df && 1 / [1].lastIndexOf(1, -0) < 0,
	      mf = $t("lastIndexOf"),
	      gf = Qt("indexOf", { ACCESSORS: !0, 1: 0 }),
	      vf = pf || !mf || !gf ? function (e) {
	    if (pf) return df.apply(this, arguments) || 0;var t = m(this),
	        n = le(t.length),
	        i = n - 1;for (arguments.length > 1 && (i = ff(i, oe(arguments[1]))), i < 0 && (i = n + i); i >= 0; i--) {
	      if (i in t && t[i] === e) return i || 0;
	    }return -1;
	  } : df;Ie({ target: "Array", proto: !0, forced: vf !== [].lastIndexOf }, { lastIndexOf: vf });var yf = function yf(e, t) {
	    var n = {};return e > t.width / t.height ? (n.width = t.width, n.height = 1 / e * t.width) : (n.height = t.height, n.width = e * t.height), n;
	  },
	      bf = function () {
	    function e(t) {
	      yo(this, e), this.player = t, this.thumbnails = [], this.loaded = !1, this.lastMouseMoveTime = Date.now(), this.mouseDown = !1, this.loadedImages = [], this.elements = { thumb: {}, scrubbing: {} }, this.load();
	    }return wo(e, [{ key: "load", value: function value() {
	        var e = this;this.player.elements.display.seekTooltip && (this.player.elements.display.seekTooltip.hidden = this.enabled), this.enabled && this.getThumbnails().then(function () {
	          e.enabled && (e.render(), e.determineContainerAutoSizing(), e.loaded = !0);
	        });
	      } }, { key: "getThumbnails", value: function value() {
	        var e = this;return new Promise(function (t) {
	          var n = e.player.config.previewThumbnails.src;if (du(n)) throw new Error("Missing previewThumbnails.src config attribute");var i = function i() {
	            e.thumbnails.sort(function (e, t) {
	              return e.height - t.height;
	            }), e.player.debug.log("Preview thumbnails", e.thumbnails), t();
	          };if (ru(n)) n(function (t) {
	            e.thumbnails = t, i();
	          });else {
	            var r = (nu(n) ? [n] : n).map(function (t) {
	              return e.getThumbnail(t);
	            });Promise.all(r).then(i);
	          }
	        });
	      } }, { key: "getThumbnail", value: function value(e) {
	        var t = this;return new Promise(function (n) {
	          Eh(e).then(function (i) {
	            var r,
	                a,
	                o = { frames: (r = i, a = [], r.split(/\r\n\r\n|\n\n|\r\r/).forEach(function (e) {
	                var t = {};e.split(/\r\n|\n|\r/).forEach(function (e) {
	                  if (tu(t.startTime)) {
	                    if (!du(e.trim()) && du(t.text)) {
	                      var n = e.trim().split("#xywh="),
	                          i = Ao(n, 1);if (t.text = i[0], n[1]) {
	                        var r = Ao(n[1].split(","), 4);t.x = r[0], t.y = r[1], t.w = r[2], t.h = r[3];
	                      }
	                    }
	                  } else {
	                    var a = e.match(/([0-9]{2})?:?([0-9]{2}):([0-9]{2}).([0-9]{2,3})( ?--> ?)([0-9]{2})?:?([0-9]{2}):([0-9]{2}).([0-9]{2,3})/);a && (t.startTime = 60 * Number(a[1] || 0) * 60 + 60 * Number(a[2]) + Number(a[3]) + Number("0.".concat(a[4])), t.endTime = 60 * Number(a[6] || 0) * 60 + 60 * Number(a[7]) + Number(a[8]) + Number("0.".concat(a[9])));
	                  }
	                }), t.text && a.push(t);
	              }), a), height: null, urlPrefix: "" };o.frames[0].text.startsWith("/") || o.frames[0].text.startsWith("http://") || o.frames[0].text.startsWith("https://") || (o.urlPrefix = e.substring(0, e.lastIndexOf("/") + 1));var s = new Image();s.onload = function () {
	              o.height = s.naturalHeight, o.width = s.naturalWidth, t.thumbnails.push(o), n();
	            }, s.src = o.urlPrefix + o.frames[0].text;
	          });
	        });
	      } }, { key: "startMove", value: function value(e) {
	        if (this.loaded && lu(e) && ["touchmove", "mousemove"].includes(e.type) && this.player.media.duration) {
	          if ("touchmove" === e.type) this.seekTime = this.player.media.duration * (this.player.elements.inputs.seek.value / 100);else {
	            var t = this.player.elements.progress.getBoundingClientRect(),
	                n = 100 / t.width * (e.pageX - t.left);this.seekTime = this.player.media.duration * (n / 100), this.seekTime < 0 && (this.seekTime = 0), this.seekTime > this.player.media.duration - 1 && (this.seekTime = this.player.media.duration - 1), this.mousePosX = e.pageX, this.elements.thumb.time.innerText = Lh(this.seekTime);
	          }this.showImageAtCurrentTime();
	        }
	      } }, { key: "endMove", value: function value() {
	        this.toggleThumbContainer(!1, !0);
	      } }, { key: "startScrubbing", value: function value(e) {
	        (Zc(e.button) || !1 === e.button || 0 === e.button) && (this.mouseDown = !0, this.player.media.duration && (this.toggleScrubbingContainer(!0), this.toggleThumbContainer(!1, !0), this.showImageAtCurrentTime()));
	      } }, { key: "endScrubbing", value: function value() {
	        var e = this;this.mouseDown = !1, Math.ceil(this.lastTime) === Math.ceil(this.player.media.currentTime) ? this.toggleScrubbingContainer(!1) : Wu.call(this.player, this.player.media, "timeupdate", function () {
	          e.mouseDown || e.toggleScrubbingContainer(!1);
	        });
	      } }, { key: "listeners", value: function value() {
	        var e = this;this.player.on("play", function () {
	          e.toggleThumbContainer(!1, !0);
	        }), this.player.on("seeked", function () {
	          e.toggleThumbContainer(!1);
	        }), this.player.on("timeupdate", function () {
	          e.lastTime = e.player.media.currentTime;
	        });
	      } }, { key: "render", value: function value() {
	        this.elements.thumb.container = Au("div", { class: this.player.config.classNames.previewThumbnails.thumbContainer }), this.elements.thumb.imageContainer = Au("div", { class: this.player.config.classNames.previewThumbnails.imageContainer }), this.elements.thumb.container.appendChild(this.elements.thumb.imageContainer);var e = Au("div", { class: this.player.config.classNames.previewThumbnails.timeContainer });this.elements.thumb.time = Au("span", {}, "00:00"), e.appendChild(this.elements.thumb.time), this.elements.thumb.container.appendChild(e), su(this.player.elements.progress) && this.player.elements.progress.appendChild(this.elements.thumb.container), this.elements.scrubbing.container = Au("div", { class: this.player.config.classNames.previewThumbnails.scrubbingContainer }), this.player.elements.wrapper.appendChild(this.elements.scrubbing.container);
	      } }, { key: "destroy", value: function value() {
	        this.elements.thumb.container && this.elements.thumb.container.remove(), this.elements.scrubbing.container && this.elements.scrubbing.container.remove();
	      } }, { key: "showImageAtCurrentTime", value: function value() {
	        var e = this;this.mouseDown ? this.setScrubbingContainerSize() : this.setThumbContainerSizeAndPos();var t = this.thumbnails[0].frames.findIndex(function (t) {
	          return e.seekTime >= t.startTime && e.seekTime <= t.endTime;
	        }),
	            n = t >= 0,
	            i = 0;this.mouseDown || this.toggleThumbContainer(n), n && (this.thumbnails.forEach(function (n, r) {
	          e.loadedImages.includes(n.frames[t].text) && (i = r);
	        }), t !== this.showingThumb && (this.showingThumb = t, this.loadImage(i)));
	      } }, { key: "loadImage", value: function value() {
	        var e = this,
	            t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
	            n = this.showingThumb,
	            i = this.thumbnails[t],
	            r = i.urlPrefix,
	            a = i.frames[n],
	            o = i.frames[n].text,
	            s = r + o;if (this.currentImageElement && this.currentImageElement.dataset.filename === o) this.showImage(this.currentImageElement, a, t, n, o, !1), this.currentImageElement.dataset.index = n, this.removeOldImages(this.currentImageElement);else {
	          this.loadingImage && this.usingSprites && (this.loadingImage.onload = null);var l = new Image();l.src = s, l.dataset.index = n, l.dataset.filename = o, this.showingThumbFilename = o, this.player.debug.log("Loading image: ".concat(s)), l.onload = function () {
	            return e.showImage(l, a, t, n, o, !0);
	          }, this.loadingImage = l, this.removeOldImages(l);
	        }
	      } }, { key: "showImage", value: function value(e, t, n, i, r) {
	        var a = !(arguments.length > 5 && void 0 !== arguments[5]) || arguments[5];this.player.debug.log("Showing thumb: ".concat(r, ". num: ").concat(i, ". qual: ").concat(n, ". newimg: ").concat(a)), this.setImageSizeAndOffset(e, t), a && (this.currentImageContainer.appendChild(e), this.currentImageElement = e, this.loadedImages.includes(r) || this.loadedImages.push(r)), this.preloadNearby(i, !0).then(this.preloadNearby(i, !1)).then(this.getHigherQuality(n, e, t, r));
	      } }, { key: "removeOldImages", value: function value(e) {
	        var t = this;Array.from(this.currentImageContainer.children).forEach(function (n) {
	          if ("img" === n.tagName.toLowerCase()) {
	            var i = t.usingSprites ? 500 : 1e3;if (n.dataset.index !== e.dataset.index && !n.dataset.deleting) {
	              n.dataset.deleting = !0;var r = t.currentImageContainer;setTimeout(function () {
	                r.removeChild(n), t.player.debug.log("Removing thumb: ".concat(n.dataset.filename));
	              }, i);
	            }
	          }
	        });
	      } }, { key: "preloadNearby", value: function value(e) {
	        var t = this,
	            n = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];return new Promise(function (i) {
	          setTimeout(function () {
	            var r = t.thumbnails[0].frames[e].text;if (t.showingThumbFilename === r) {
	              var a;a = n ? t.thumbnails[0].frames.slice(e) : t.thumbnails[0].frames.slice(0, e).reverse();var o = !1;a.forEach(function (e) {
	                var n = e.text;if (n !== r && !t.loadedImages.includes(n)) {
	                  o = !0, t.player.debug.log("Preloading thumb filename: ".concat(n));var a = t.thumbnails[0].urlPrefix + n,
	                      s = new Image();s.src = a, s.onload = function () {
	                    t.player.debug.log("Preloaded thumb filename: ".concat(n)), t.loadedImages.includes(n) || t.loadedImages.push(n), i();
	                  };
	                }
	              }), o || i();
	            }
	          }, 300);
	        });
	      } }, { key: "getHigherQuality", value: function value(e, t, n, i) {
	        var r = this;if (e < this.thumbnails.length - 1) {
	          var a = t.naturalHeight;this.usingSprites && (a = n.h), a < this.thumbContainerHeight && setTimeout(function () {
	            r.showingThumbFilename === i && (r.player.debug.log("Showing higher quality thumb for: ".concat(i)), r.loadImage(e + 1));
	          }, 300);
	        }
	      } }, { key: "toggleThumbContainer", value: function value() {
	        var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
	            t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
	            n = this.player.config.classNames.previewThumbnails.thumbContainerShown;this.elements.thumb.container.classList.toggle(n, e), !e && t && (this.showingThumb = null, this.showingThumbFilename = null);
	      } }, { key: "toggleScrubbingContainer", value: function value() {
	        var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
	            t = this.player.config.classNames.previewThumbnails.scrubbingContainerShown;this.elements.scrubbing.container.classList.toggle(t, e), e || (this.showingThumb = null, this.showingThumbFilename = null);
	      } }, { key: "determineContainerAutoSizing", value: function value() {
	        (this.elements.thumb.imageContainer.clientHeight > 20 || this.elements.thumb.imageContainer.clientWidth > 20) && (this.sizeSpecifiedInCSS = !0);
	      } }, { key: "setThumbContainerSizeAndPos", value: function value() {
	        if (this.sizeSpecifiedInCSS) {
	          if (this.elements.thumb.imageContainer.clientHeight > 20 && this.elements.thumb.imageContainer.clientWidth < 20) {
	            var e = Math.floor(this.elements.thumb.imageContainer.clientHeight * this.thumbAspectRatio);this.elements.thumb.imageContainer.style.width = "".concat(e, "px");
	          } else if (this.elements.thumb.imageContainer.clientHeight < 20 && this.elements.thumb.imageContainer.clientWidth > 20) {
	            var t = Math.floor(this.elements.thumb.imageContainer.clientWidth / this.thumbAspectRatio);this.elements.thumb.imageContainer.style.height = "".concat(t, "px");
	          }
	        } else {
	          var n = Math.floor(this.thumbContainerHeight * this.thumbAspectRatio);this.elements.thumb.imageContainer.style.height = "".concat(this.thumbContainerHeight, "px"), this.elements.thumb.imageContainer.style.width = "".concat(n, "px");
	        }this.setThumbContainerPos();
	      } }, { key: "setThumbContainerPos", value: function value() {
	        var e = this.player.elements.progress.getBoundingClientRect(),
	            t = this.player.elements.container.getBoundingClientRect(),
	            n = this.elements.thumb.container,
	            i = t.left - e.left + 10,
	            r = t.right - e.left - n.clientWidth - 10,
	            a = this.mousePosX - e.left - n.clientWidth / 2;a < i && (a = i), a > r && (a = r), n.style.left = "".concat(a, "px");
	      } }, { key: "setScrubbingContainerSize", value: function value() {
	        var e = yf(this.thumbAspectRatio, { width: this.player.media.clientWidth, height: this.player.media.clientHeight }),
	            t = e.width,
	            n = e.height;this.elements.scrubbing.container.style.width = "".concat(t, "px"), this.elements.scrubbing.container.style.height = "".concat(n, "px");
	      } }, { key: "setImageSizeAndOffset", value: function value(e, t) {
	        if (this.usingSprites) {
	          var n = this.thumbContainerHeight / t.h;e.style.height = "".concat(e.naturalHeight * n, "px"), e.style.width = "".concat(e.naturalWidth * n, "px"), e.style.left = "-".concat(t.x * n, "px"), e.style.top = "-".concat(t.y * n, "px");
	        }
	      } }, { key: "enabled", get: function get() {
	        return this.player.isHTML5 && this.player.isVideo && this.player.config.previewThumbnails.enabled;
	      } }, { key: "currentImageContainer", get: function get() {
	        return this.mouseDown ? this.elements.scrubbing.container : this.elements.thumb.imageContainer;
	      } }, { key: "usingSprites", get: function get() {
	        return Object.keys(this.thumbnails[0].frames[0]).includes("w");
	      } }, { key: "thumbAspectRatio", get: function get() {
	        return this.usingSprites ? this.thumbnails[0].frames[0].w / this.thumbnails[0].frames[0].h : this.thumbnails[0].width / this.thumbnails[0].height;
	      } }, { key: "thumbContainerHeight", get: function get() {
	        return this.mouseDown ? yf(this.thumbAspectRatio, { width: this.player.media.clientWidth, height: this.player.media.clientHeight }).height : this.sizeSpecifiedInCSS ? this.elements.thumb.imageContainer.clientHeight : Math.floor(this.player.media.clientWidth / this.thumbAspectRatio / 4);
	      } }, { key: "currentImageElement", get: function get() {
	        return this.mouseDown ? this.currentScrubbingImageElement : this.currentThumbnailImageElement;
	      }, set: function set(e) {
	        this.mouseDown ? this.currentScrubbingImageElement = e : this.currentThumbnailImageElement = e;
	      } }]), e;
	  }(),
	      wf = { insertElements: function insertElements(e, t) {
	      var n = this;nu(t) ? Pu(e, this.media, { src: t }) : au(t) && t.forEach(function (t) {
	        Pu(e, n.media, t);
	      });
	    }, change: function change(e) {
	      var t = this;ku(e, "sources.length") ? (eh.cancelRequests.call(this), this.destroy.call(this, function () {
	        t.options.quality = [], xu(t.media), t.media = null, su(t.elements.container) && t.elements.container.removeAttribute("class");var n = e.sources,
	            i = e.type,
	            r = Ao(n, 1)[0],
	            a = r.provider,
	            o = void 0 === a ? Fh.html5 : a,
	            s = r.src,
	            l = "html5" === o ? i : "div",
	            c = "html5" === o ? {} : { src: s };Object.assign(t, { provider: o, type: i, supported: qu.check(i, o, t.config.playsinline), media: Au(l, c) }), t.elements.container.appendChild(t.media), iu(e.autoplay) && (t.config.autoplay = e.autoplay), t.isHTML5 && (t.config.crossorigin && t.media.setAttribute("crossorigin", ""), t.config.autoplay && t.media.setAttribute("autoplay", ""), du(e.poster) || (t.poster = e.poster), t.config.loop.active && t.media.setAttribute("loop", ""), t.config.muted && t.media.setAttribute("muted", ""), t.config.playsinline && t.media.setAttribute("playsinline", "")), $h.addStyleHook.call(t), t.isHTML5 && wf.insertElements.call(t, "source", n), t.config.title = e.title, sf.setup.call(t), t.isHTML5 && Object.keys(e).includes("tracks") && wf.insertElements.call(t, "track", e.tracks), (t.isHTML5 || t.isEmbed && !t.supported.ui) && $h.build.call(t), t.isHTML5 && t.media.load(), du(e.previewThumbnails) || (Object.assign(t.config.previewThumbnails, e.previewThumbnails), t.previewThumbnails && t.previewThumbnails.loaded && (t.previewThumbnails.destroy(), t.previewThumbnails = null), t.config.previewThumbnails.enabled && (t.previewThumbnails = new bf(t))), t.fullscreen.update();
	      }, !0)) : this.debug.warn("Invalid source format");
	    } };var kf,
	      Tf = function () {
	    function e(t, n) {
	      var i = this;if (yo(this, e), this.timers = {}, this.ready = !1, this.loading = !1, this.failed = !1, this.touch = qu.touch, this.media = t, nu(this.media) && (this.media = document.querySelectorAll(this.media)), (window.jQuery && this.media instanceof jQuery || ou(this.media) || au(this.media)) && (this.media = this.media[0]), this.config = Tu({}, _h, e.defaults, n || {}, function () {
	        try {
	          return JSON.parse(i.media.getAttribute("data-plyr-config"));
	        } catch (e) {
	          return {};
	        }
	      }()), this.elements = { container: null, fullscreen: null, captions: null, buttons: {}, display: {}, progress: {}, inputs: {}, settings: { popup: null, menu: null, panels: {}, buttons: {} } }, this.captions = { active: null, currentTrack: -1, meta: new WeakMap() }, this.fullscreen = { active: !1 }, this.options = { speed: [], quality: [] }, this.debug = new Vh(this.config.debug), this.debug.log("Config", this.config), this.debug.log("Support", qu), !Zc(this.media) && su(this.media)) {
	        if (this.media.plyr) this.debug.warn("Target already setup");else if (this.config.enabled) {
	          if (qu.check().api) {
	            var r = this.media.cloneNode(!0);r.autoplay = !1, this.elements.original = r;var a = this.media.tagName.toLowerCase(),
	                o = null,
	                s = null;switch (a) {case "div":
	                if (o = this.media.querySelector("iframe"), su(o)) {
	                  if (s = Nh(o.getAttribute("src")), this.provider = function (e) {
	                    return (/^(https?:\/\/)?(www\.)?(youtube\.com|youtube-nocookie\.com|youtu\.?be)\/.+$/.test(e) ? Fh.youtube : /^https?:\/\/player.vimeo.com\/video\/\d{0,9}(?=\b|\/)/.test(e) ? Fh.vimeo : null
	                    );
	                  }(s.toString()), this.elements.container = this.media, this.media = o, this.elements.container.className = "", s.search.length) {
	                    var l = ["1", "true"];l.includes(s.searchParams.get("autoplay")) && (this.config.autoplay = !0), l.includes(s.searchParams.get("loop")) && (this.config.loop.active = !0), this.isYouTube ? (this.config.playsinline = l.includes(s.searchParams.get("playsinline")), this.config.youtube.hl = s.searchParams.get("hl")) : this.config.playsinline = !0;
	                  }
	                } else this.provider = this.media.getAttribute(this.config.attributes.embed.provider), this.media.removeAttribute(this.config.attributes.embed.provider);if (du(this.provider) || !Object.keys(Fh).includes(this.provider)) return void this.debug.error("Setup failed: Invalid provider");this.type = Hh;break;case "video":case "audio":
	                this.type = a, this.provider = Fh.html5, this.media.hasAttribute("crossorigin") && (this.config.crossorigin = !0), this.media.hasAttribute("autoplay") && (this.config.autoplay = !0), (this.media.hasAttribute("playsinline") || this.media.hasAttribute("webkit-playsinline")) && (this.config.playsinline = !0), this.media.hasAttribute("muted") && (this.config.muted = !0), this.media.hasAttribute("loop") && (this.config.loop.active = !0);break;default:
	                return void this.debug.error("Setup failed: unsupported type");}this.supported = qu.check(this.type, this.provider, this.config.playsinline), this.supported.api ? (this.eventListeners = [], this.listeners = new Yh(this), this.storage = new Sh(this), this.media.plyr = this, su(this.elements.container) || (this.elements.container = Au("div", { tabindex: 0 }), Su(this.media, this.elements.container)), $h.migrateStyles.call(this), $h.addStyleHook.call(this), sf.setup.call(this), this.config.debug && Vu.call(this, this.elements.container, this.config.events.join(" "), function (e) {
	              i.debug.log("event: ".concat(e.type));
	            }), this.fullscreen = new zh(this), (this.isHTML5 || this.isEmbed && !this.supported.ui) && $h.build.call(this), this.listeners.container(), this.listeners.global(), this.config.ads.enabled && (this.ads = new lf(this)), this.isHTML5 && this.config.autoplay && setTimeout(function () {
	              return Gu(i.play());
	            }, 10), this.lastSeekTime = 0, this.config.previewThumbnails.enabled && (this.previewThumbnails = new bf(this))) : this.debug.error("Setup failed: no support");
	          } else this.debug.error("Setup failed: no support");
	        } else this.debug.error("Setup failed: disabled by config");
	      } else this.debug.error("Setup failed: no suitable element passed");
	    }return wo(e, [{ key: "play", value: function value() {
	        var e = this;return ru(this.media.play) ? (this.ads && this.ads.enabled && this.ads.managerPromise.then(function () {
	          return e.ads.play();
	        }).catch(function () {
	          return Gu(e.media.play());
	        }), this.media.play()) : null;
	      } }, { key: "pause", value: function value() {
	        return this.playing && ru(this.media.pause) ? this.media.pause() : null;
	      } }, { key: "togglePlay", value: function value(e) {
	        return (iu(e) ? e : !this.playing) ? this.play() : this.pause();
	      } }, { key: "stop", value: function value() {
	        this.isHTML5 ? (this.pause(), this.restart()) : ru(this.media.stop) && this.media.stop();
	      } }, { key: "restart", value: function value() {
	        this.currentTime = 0;
	      } }, { key: "rewind", value: function value(e) {
	        this.currentTime -= tu(e) ? e : this.config.seekTime;
	      } }, { key: "forward", value: function value(e) {
	        this.currentTime += tu(e) ? e : this.config.seekTime;
	      } }, { key: "increaseVolume", value: function value(e) {
	        var t = this.media.muted ? 0 : this.volume;this.volume = t + (tu(e) ? e : 0);
	      } }, { key: "decreaseVolume", value: function value(e) {
	        this.increaseVolume(-e);
	      } }, { key: "toggleCaptions", value: function value(e) {
	        Mh.toggle.call(this, e, !1);
	      } }, { key: "airplay", value: function value() {
	        qu.airplay && this.media.webkitShowPlaybackTargetPicker();
	      } }, { key: "toggleControls", value: function value(e) {
	        if (this.supported.ui && !this.isAudio) {
	          var t = Nu(this.elements.container, this.config.classNames.hideControls),
	              n = void 0 === e ? void 0 : !e,
	              i = ju(this.elements.container, this.config.classNames.hideControls, n);if (i && au(this.config.controls) && this.config.controls.includes("settings") && !du(this.config.settings) && jh.toggleMenu.call(this, !1), i !== t) {
	            var r = i ? "controlshidden" : "controlsshown";Ku.call(this, this.media, r);
	          }return !i;
	        }return !1;
	      } }, { key: "on", value: function value(e, t) {
	        Vu.call(this, this.elements.container, e, t);
	      } }, { key: "once", value: function value(e, t) {
	        Wu.call(this, this.elements.container, e, t);
	      } }, { key: "off", value: function value(e, t) {
	        zu(this.elements.container, e, t);
	      } }, { key: "destroy", value: function value(e) {
	        var t = this,
	            n = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];if (this.ready) {
	          var i = function i() {
	            document.body.style.overflow = "", t.embed = null, n ? (Object.keys(t.elements).length && (xu(t.elements.buttons.play), xu(t.elements.captions), xu(t.elements.controls), xu(t.elements.wrapper), t.elements.buttons.play = null, t.elements.captions = null, t.elements.controls = null, t.elements.wrapper = null), ru(e) && e()) : ($u.call(t), Ou(t.elements.original, t.elements.container), Ku.call(t, t.elements.original, "destroyed", !0), ru(e) && e.call(t.elements.original), t.ready = !1, setTimeout(function () {
	              t.elements = null, t.media = null;
	            }, 200));
	          };this.stop(), clearTimeout(this.timers.loading), clearTimeout(this.timers.controls), clearTimeout(this.timers.resized), this.isHTML5 ? ($h.toggleNativeControls.call(this, !0), i()) : this.isYouTube ? (clearInterval(this.timers.buffering), clearInterval(this.timers.playing), null !== this.embed && ru(this.embed.destroy) && this.embed.destroy(), i()) : this.isVimeo && (null !== this.embed && this.embed.unload().then(i), setTimeout(i, 200));
	        }
	      } }, { key: "supports", value: function value(e) {
	        return qu.mime.call(this, e);
	      } }, { key: "isHTML5", get: function get() {
	        return this.provider === Fh.html5;
	      } }, { key: "isEmbed", get: function get() {
	        return this.isYouTube || this.isVimeo;
	      } }, { key: "isYouTube", get: function get() {
	        return this.provider === Fh.youtube;
	      } }, { key: "isVimeo", get: function get() {
	        return this.provider === Fh.vimeo;
	      } }, { key: "isVideo", get: function get() {
	        return this.type === Hh;
	      } }, { key: "isAudio", get: function get() {
	        return this.type === qh;
	      } }, { key: "playing", get: function get() {
	        return Boolean(this.ready && !this.paused && !this.ended);
	      } }, { key: "paused", get: function get() {
	        return Boolean(this.media.paused);
	      } }, { key: "stopped", get: function get() {
	        return Boolean(this.paused && 0 === this.currentTime);
	      } }, { key: "ended", get: function get() {
	        return Boolean(this.media.ended);
	      } }, { key: "currentTime", set: function set(e) {
	        if (this.duration) {
	          var t = tu(e) && e > 0;this.media.currentTime = t ? Math.min(e, this.duration) : 0, this.debug.log("Seeking to ".concat(this.currentTime, " seconds"));
	        }
	      }, get: function get() {
	        return Number(this.media.currentTime);
	      } }, { key: "buffered", get: function get() {
	        var e = this.media.buffered;return tu(e) ? e : e && e.length && this.duration > 0 ? e.end(0) / this.duration : 0;
	      } }, { key: "seeking", get: function get() {
	        return Boolean(this.media.seeking);
	      } }, { key: "duration", get: function get() {
	        var e = parseFloat(this.config.duration),
	            t = (this.media || {}).duration,
	            n = tu(t) && t !== 1 / 0 ? t : 0;return e || n;
	      } }, { key: "volume", set: function set(e) {
	        var t = e;nu(t) && (t = Number(t)), tu(t) || (t = this.storage.get("volume")), tu(t) || (t = this.config.volume), t > 1 && (t = 1), t < 0 && (t = 0), this.config.volume = t, this.media.volume = t, !du(e) && this.muted && t > 0 && (this.muted = !1);
	      }, get: function get() {
	        return Number(this.media.volume);
	      } }, { key: "muted", set: function set(e) {
	        var t = e;iu(t) || (t = this.storage.get("muted")), iu(t) || (t = this.config.muted), this.config.muted = t, this.media.muted = t;
	      }, get: function get() {
	        return Boolean(this.media.muted);
	      } }, { key: "hasAudio", get: function get() {
	        return !this.isHTML5 || !!this.isAudio || Boolean(this.media.mozHasAudio) || Boolean(this.media.webkitAudioDecodedByteCount) || Boolean(this.media.audioTracks && this.media.audioTracks.length);
	      } }, { key: "speed", set: function set(e) {
	        var t = this,
	            n = null;tu(e) && (n = e), tu(n) || (n = this.storage.get("speed")), tu(n) || (n = this.config.speed.selected);var i = this.minimumSpeed,
	            r = this.maximumSpeed;n = function () {
	          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
	              t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0,
	              n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 255;return Math.min(Math.max(e, t), n);
	        }(n, i, r), this.config.speed.selected = n, setTimeout(function () {
	          t.media.playbackRate = n;
	        }, 0);
	      }, get: function get() {
	        return Number(this.media.playbackRate);
	      } }, { key: "minimumSpeed", get: function get() {
	        return this.isYouTube ? Math.min.apply(Math, Po(this.options.speed)) : this.isVimeo ? .5 : .0625;
	      } }, { key: "maximumSpeed", get: function get() {
	        return this.isYouTube ? Math.max.apply(Math, Po(this.options.speed)) : this.isVimeo ? 2 : 16;
	      } }, { key: "quality", set: function set(e) {
	        var t = this.config.quality,
	            n = this.options.quality;if (n.length) {
	          var i = [!du(e) && Number(e), this.storage.get("quality"), t.selected, t.default].find(tu),
	              r = !0;if (!n.includes(i)) {
	            var a = function (e, t) {
	              return au(e) && e.length ? e.reduce(function (e, n) {
	                return Math.abs(n - t) < Math.abs(e - t) ? n : e;
	              }) : null;
	            }(n, i);this.debug.warn("Unsupported quality option: ".concat(i, ", using ").concat(a, " instead")), i = a, r = !1;
	          }t.selected = i, this.media.quality = i, r && this.storage.set({ quality: i });
	        }
	      }, get: function get() {
	        return this.media.quality;
	      } }, { key: "loop", set: function set(e) {
	        var t = iu(e) ? e : this.config.loop.active;this.config.loop.active = t, this.media.loop = t;
	      }, get: function get() {
	        return Boolean(this.media.loop);
	      } }, { key: "source", set: function set(e) {
	        wf.change.call(this, e);
	      }, get: function get() {
	        return this.media.currentSrc;
	      } }, { key: "download", get: function get() {
	        var e = this.config.urls.download;return fu(e) ? e : this.source;
	      }, set: function set(e) {
	        fu(e) && (this.config.urls.download = e, jh.setDownloadUrl.call(this));
	      } }, { key: "poster", set: function set(e) {
	        this.isVideo ? $h.setPoster.call(this, e, !1).catch(function () {}) : this.debug.warn("Poster can only be set for video");
	      }, get: function get() {
	        return this.isVideo ? this.media.getAttribute("poster") || this.media.getAttribute("data-poster") : null;
	      } }, { key: "ratio", get: function get() {
	        if (!this.isVideo) return null;var e = Qu(Ju.call(this));return au(e) ? e.join(":") : e;
	      }, set: function set(e) {
	        this.isVideo ? nu(e) && Xu(e) ? (this.config.ratio = e, Zu.call(this)) : this.debug.error("Invalid aspect ratio specified (".concat(e, ")")) : this.debug.warn("Aspect ratio can only be set for video");
	      } }, { key: "autoplay", set: function set(e) {
	        var t = iu(e) ? e : this.config.autoplay;this.config.autoplay = t;
	      }, get: function get() {
	        return Boolean(this.config.autoplay);
	      } }, { key: "currentTrack", set: function set(e) {
	        Mh.set.call(this, e, !1);
	      }, get: function get() {
	        var e = this.captions,
	            t = e.toggled,
	            n = e.currentTrack;return t ? n : -1;
	      } }, { key: "language", set: function set(e) {
	        Mh.setLanguage.call(this, e, !1);
	      }, get: function get() {
	        return (Mh.getCurrentTrack.call(this) || {}).language;
	      } }, { key: "pip", set: function set(e) {
	        if (qu.pip) {
	          var t = iu(e) ? e : !this.pip;ru(this.media.webkitSetPresentationMode) && this.media.webkitSetPresentationMode(t ? Uh : Dh), ru(this.media.requestPictureInPicture) && (!this.pip && t ? this.media.requestPictureInPicture() : this.pip && !t && document.exitPictureInPicture());
	        }
	      }, get: function get() {
	        return qu.pip ? du(this.media.webkitPresentationMode) ? this.media === document.pictureInPictureElement : this.media.webkitPresentationMode === Uh : null;
	      } }], [{ key: "supported", value: function value(e, t, n) {
	        return qu.check(e, t, n);
	      } }, { key: "loadSprite", value: function value(e, t) {
	        return Ah(e, t);
	      } }, { key: "setup", value: function value(t) {
	        var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
	            i = null;return nu(t) ? i = Array.from(document.querySelectorAll(t)) : ou(t) ? i = Array.from(t) : au(t) && (i = t.filter(su)), du(i) ? null : i.map(function (t) {
	          return new e(t, n);
	        });
	      } }]), e;
	  }();return Tf.defaults = (kf = _h, JSON.parse(JSON.stringify(kf))), Tf;
	});
	//# sourceMappingURL=plyr.polyfilled.min.js.map
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ })
/******/ ]);